
How to launch the game: 
Run server main (ServerMain.java inside package server.network), then launch the client main (Main.java inside package client). The game will then ask on console whether you wish to play with GUI or CLI (after the second player connects, a timer of 20 seconds will start and then you will be able to play). 



Features:
• Complete rules for 2+ players
• Socket connection
• RMI connection
• Command Line Interface
• Graphic User Interface
• Configurable board (either by loading file or by randomly generating a board through parameters given by the player admin)
• Artificial Intelligence (one computer player or more can be added while configuring the board)

developed by Andreoli Maddalena, Battistello Andrea, de Santis Simone