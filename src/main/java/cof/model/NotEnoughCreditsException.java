package cof.model;

/**
 * Exception thrown by player when he can't afford to pay a specified price
 * @author andrea
 *
 */
public class NotEnoughCreditsException extends RuntimeException {

	private static final long serialVersionUID = 4929227046681856683L;
	
}
