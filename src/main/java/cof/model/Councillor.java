package cof.model;

/**
 * Representation of a councillor
 * @author andrea
 *
 */
public class Councillor {
	public static final String BASE_TAG = "Councillor";
	
	private boolean available;
	private PoliticColor color;
	
	private static int idNumber = 0;
	private String tag;
	
	@SuppressWarnings("unused")
	private Councillor() {
		// Set constructor for gson serializer
	}
	
	/**
	 * Creates a new Councillor with specified color
	 * @param color
	 */
	public Councillor(PoliticColor color) {
		idNumber++;
		tag = BASE_TAG + idNumber;
		this.color = color;
		available = true;
	}
	
	public PoliticColor getColor() {
		return color;
	}
	
	public String getTag() {
		return tag;
	}
	
	protected void setAvailability(boolean available) {
		this.available = available;
	}
	
	public boolean isAvailable(){
		return available;
	}
	
	@Override
	public String toString() {
		return tag + "(" + color + ")";
	}
	
}
