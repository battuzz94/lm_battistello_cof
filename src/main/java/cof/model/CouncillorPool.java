package cof.model;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import common.Algorithms;

/**
 * Set of available councillors
 * @author andrea
 *
 */
public class CouncillorPool {
	private LinkedList<Councillor> availableCouncillors;
	
	/**
	 * Creates a new empty pool
	 */
	public CouncillorPool() {
		availableCouncillors = new LinkedList<>();
	}
	
	/**
	 * Add a councillor to the available councillor list
	 * @param c the councillor to add
	 */
	public void addCouncillor(Councillor c) {
		availableCouncillors.add(c);
	}
	
	/**
	 * Pick a councillor and makes him unavailable to use as long as the unboundCouncillor method is called
	 * @param c the councillor to be bound
	 * @throws NoSuchElementException if the councillor is not available
	 */
	public void bindCouncillor(Councillor c) {
		boolean ret = availableCouncillors.remove(c);
		if (!ret)
			throw new NoSuchElementException();
	}
	
	
	/**
	 * Set the councillor available. This method is nearly the same as add()
	 * @param c councillor to put in free space
	 * @throws NoSuchElementException if the specified councillor is not already bound
	 * @see #addCouncillor(Councillor c)
	 */
	public void unbindCouncillor(Councillor c) {
			availableCouncillors.add(c);
	}
	
	public Councillor getRandomCouncillor() {
		return Algorithms.chooseFromList(availableCouncillors);
	}
	
	public List<Councillor> getAvailableCouncillors(){
		return availableCouncillors;
	}
	
	/**
	 * Return the councillor with given tag if available. Otherwise an exception is thrown
	 * @param tag the councillor tag
	 * @return the Councillor found, if any
	 * @throws NoSuchElementException if no Councillor with given tag is available
	 */
	public Councillor getAvailableCouncillorByTag(String tag) {
		for (Councillor c : availableCouncillors) {
			if (tag.equals(c.getTag()))
				return c;
		}
		throw new NoSuchElementException();
	}
	
	@Override
	public String toString() {
		return "availableCouncillors: " + availableCouncillors;
	}
	
}
