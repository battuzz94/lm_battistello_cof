package cof.model;

import java.util.ArrayList;
import java.util.List;

import cof.commands.CommandNotValidException;

/**
 * Representation of a council
 * @author andrea
 *
 */
public class Council {
	public static final String BASE_TAG = "Council";
	public static final int DEFAULT_COUNCIL_SIZE = 4;
	private static int idNumber = 0;
	
	private String tag;
	private int size = DEFAULT_COUNCIL_SIZE;
	private ArrayList<Councillor> councillors; 
	private transient CouncillorPool pool;
	
	@SuppressWarnings(value = { "unused" })
	private Council() {
		// Set constructor for gson serializer
	}
	
	/**
	 * Creates a new Council with specified pool
	 * @param pool
	 */
	public Council(CouncillorPool pool) {
		this(pool, DEFAULT_COUNCIL_SIZE);
	}
	
	/**
	 * Creates a new Council of specified size
	 * @param pool
	 * @param size
	 */
	public Council(CouncillorPool pool, int size) {
		this.pool = pool;
		this.size = size;
		idNumber++;
		tag = BASE_TAG + idNumber;	
		councillors = new ArrayList<>();
	}
	
	/**
	 * Adds a new councillor to this council, shifting all the other to the left. If that would
	 * exceed the council size, the last councillor will be pushed in the pool.
	 * @param c
	 */
	public void pushCouncillor(Councillor c){
		if (councillors.size() < size) {
			pool.bindCouncillor(c);
			councillors.add(c);
		}
		else {
			Councillor counc = councillors.get(0);
			councillors.remove(0);
			pool.unbindCouncillor(counc); 	// Removes the first councillor from queue and unbound it
			councillors.add(c);			// Add new councillor
			pool.bindCouncillor(c);	// Mark as bound
		}
	}
	
	public List<Councillor> getCouncillors() {
		return councillors;
	}
	
	protected void setCouncillorPool(CouncillorPool p) {
		this.pool = p;
	}
	
	public String getTag() {
		return tag;
	}
	
	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(tag + " [ ");
		for (Councillor c : councillors)
			bld.append(c);
		bld.append(" ]");
		return bld.toString();
	}
	
	/**
	 * Matches the cards and the councillors colors and returns the price needed to satisfy the council
	 * @param cards
	 * @return
	 */
	public Price getSatisfiabilityPrice(Card[] cards) {
		int nMatches;
		int nJolly = 0;
		boolean[] cardsUsed = new boolean[cards.length];
		
		nMatches = getMatchCost(cards, cardsUsed);
		
		for (Card c : cards)
			if (c.isJolly()) {
				nJolly++;
				nMatches++;
			}
		
		int price;
		switch(nMatches) {
		case 1:
			price = 10;
			break;
		case 2:
			price = 7;
			break;
		case 3:
			price = 4;
			break;
		case 4:
			price = 0;
			break;
		default:
			throw new CommandNotValidException();
		}
		
		return new Price(price + nJolly, 0);
	}

	private int getMatchCost(Card[] cards, boolean[] cardsUsed) {
		int nMatches = 0;
		for (Councillor c : councillors) {
			int cardIndex = 0;
			while (cardIndex < cards.length) {
				if (c.getColor().equals(cards[cardIndex].getColor()) && !cardsUsed[cardIndex]) {
					cardsUsed[cardIndex] = true;
					nMatches++;
					break;
				}
				cardIndex++;
			}
		}
		return nMatches;
	}
	
	/**
	 * Checks if the cards that the user tried to bribe with are correct
	 * @param cards
	 * @return
	 */
	public boolean cardsCorrespond(Card[] cards) {
		boolean[] cardUsed = new boolean[cards.length];
		
		for (Councillor counc : councillors) {
			for (int i = 0; i < cards.length; i++) {
				if (!cardUsed[i] && cards[i].getColor() == counc.getColor()) {
					cardUsed[i] = true;
					break;
				}
			}
		}
		
		for (int i = 0; i < cards.length; i++)
			if (!cardUsed[i] && !cards[i].isJolly())
				return false;
		return cards.length <= councillors.size();
	}
}
