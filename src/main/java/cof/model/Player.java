package cof.model;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Collect all the informations and stats of a Player during the game
 * @author Andrea
 *
 */
public class Player {
	private static final Logger LOGGER = Logger.getLogger(Player.class.getName());
	private int assistants;
	private int coins;
	private int victoryPoints;
	private int nobility;
	private boolean alive;
	private List<Permit> permits;
	private List<City> buildedEmporia;
	private List<Card> cards;
	private String username="NO_NAME";
	private int id;
	
	
	/**
	 * Creates a new player with specified coins and assistants
	 * @param coins
	 * @param assistants
	 */
	public Player(int coins, int assistants){
		this.coins=coins;
		this.assistants=assistants;
		this.victoryPoints=0;
		this.nobility=0;
		this.alive=true;
		this.permits=new ArrayList<>();
		this.buildedEmporia=new ArrayList<>();
		this.cards=new ArrayList<>();
	}
	
	/**
	 * Obtains the obtainable o
	 * @param o
	 */
	public void obtain(Obtainable o){
		o.giveTo(this);
	}
	
	
	public void setUsername(String name){
		this.username=name;
	}
	
	/**
	 * Add coins to the player
	 * @param coins
	 * @throws IllegalArgumentException if the coins are < 0
	 */
	public void addCoins(int coins) {
		if(coins>=0)
			this.coins += coins;
		else
			throw new IllegalArgumentException();
			
	}
	
	/**
	 * Add assistants to the player
	 * @param assistants
	 * @throws IllegalArgumentException if assistants < 0
	 */
	public void addAssistants(int assistants) {
		if(assistants>=0)
			this.assistants+=assistants;
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Add victory points to the player
	 * @param victoryPoints
	 * @throws IllegalArgumentException if victoryPoints < 0
	 */
	public void addPoints(int victoryPoints) {
		if(victoryPoints>=0)
			this.victoryPoints+=victoryPoints;
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Advances the player in nobility track
	 * @param nobilityPoints
	 * @throws IllegalArgumentException if nobilityPoints < 0
	 */
	public void addNobility(int nobilityPoints) {
		if(nobilityPoints>=0)
			this.nobility+=nobilityPoints;
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Gives cards to the player
	 * @param c
	 */
	public void addCards(Card c){
		this.cards.add(c);
	}
	
	/**
	 * Give a permit to player
	 * @param p
	 */
	public void addPermit(Permit p){
		this.permits.add(p);
	}
	
	/**
	 * Take coins from player
	 * @param coins
	 * @throws NotEnoughCreditsException if player could not afford the required number of coins
	 */
	public void takeCoins(int coins) {
		if(this.coins >= coins)
			this.coins-=coins;
		else
			throw new NotEnoughCreditsException();
	} 
	
	/**
	 * Take assistants from player
	 * @param assistants
	 * @throws NotEnoughCreditsException if player could not afford the required number of assistants
	 */
	public void takeAssistants(int assistants) {
		if(this.assistants >= assistants)
			this.assistants-=assistants;
		else
			throw new NotEnoughCreditsException();
	}
	
	/**
	 * Take victory points from player
	 * @param victoryPoints
	 * @throws NotEnoughCreditsException if player could not afford the required number of victory points
	 */
	public void takePoints(int victoryPoints) {
		if(this.victoryPoints >= victoryPoints)
			this.victoryPoints-=victoryPoints;
		else
			throw new NotEnoughCreditsException();
	}
	
	/**
	 * Take nobility from player
	 * @param nobilityPoints
	 * @throws NotEnoughCreditsException if player could not afford nobility
	 */
	public void takeNobility(int nobilityPoints) {
		if(this.nobility >= nobilityPoints)
			this.nobility-=nobilityPoints;
		else
			throw new NotEnoughCreditsException();
	}
	
	/**
	 * Take cards from player
	 * @param card
	 * @throws NotEnoughCreditsException if player hasn't the required number of cards
	 */
	public void takeCard(Card card) {
		try {
			this.cards.remove(card);
		} catch(NoSuchElementException e){
			LOGGER.log(Level.FINE, "Could not take card", e);
			throw new NotEnoughCreditsException();
		}
	}
	
	/**
	 * Take permit from player
	 * @param p
	 * @throws NotEnoughCreditsException if no such permit is present
	 */
	public void takePermit(Permit p) {
		try{
			this.permits.remove(p);
		} catch(NoSuchElementException e){
			LOGGER.log(Level.FINE, "Could not take permit", e);
			throw new NotEnoughCreditsException();
		}	
	}

	
	public boolean isAlive(){
		return alive;
	}
	
	/**
	 * Withdraw the coins and assistants specified by p from player
	 * @param p
	 */
	public void pay(Price p){
		p.takeFrom(this);
	}
	
	/**
	 * Builds emporium in city
	 * @param city
	 */
	public void buildEmporium(City city) {
		buildedEmporia.add(city);
		city.addEmporium();
	}
	
	public int getAssistants() {
		return assistants;
	}

	public int getCoins() {
		return coins;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public int getNobility() {
		return nobility;
	}

	public String getUsername() {
		return username;
	}

	public List<Permit> getPermits() {
		return permits;
	}
	
	/**
	 * If the value is lower than current nobility, then the nobility will be set to 
	 * that threshold
	 * @param threshold
	 */
	public void setMaxNobility(int threshold) {
		this.nobility = Math.min(nobility, threshold);
	}

	public List<City> getBuildedEmporia() {
		return buildedEmporia;
	}



	public List<Card> getCards() {
		return cards;
	}
	
	/**
	 * Get the player card with specified tag
	 * @param tag
	 * @return
	 */
	public Card getCardByTag(String tag) {
		for (Card c : cards)
			if (tag.equals(c.getTag())) 
				return c;
		throw new NoSuchElementException();
	}


	/**
	 * Checks if the player can afford to pay the price
	 * @param price
	 * @return
	 */
	public boolean canAfford(Price price) {
		return this.assistants >= price.getAssistants() && this.coins >= price.getCoins();
	}


	/**
	 * Check if player has already built in city
	 * @param city
	 * @return
	 */
	public boolean hasBuiltIn(City city) {
		return buildedEmporia.stream()
				.map(c -> c.getTag())
				.filter(s -> s.equals(city.getTag()))
				.findFirst()
				.isPresent();
	}
	
	@Override
	public String toString() {
		String ret = "";
		ret += "coins : " + coins + "\n";
		ret += "assistants : " + assistants + "\n";
		ret += "victoryPoints: " + victoryPoints + "\n";
		ret += "nobility: " + nobility + "\n";
		ret += "permits: " + permits + "\n";
		ret += "buildedEmporia: " + buildedEmporia + "\n";
		ret += "cards: " + cards + "\n";
		return ret;
	}
	
	public PlayerStat getPlayerStat() {
		return new PlayerStat(this);
	}

	/**
	 * Returns the permit of the player with given tag
	 * @param permitChosenTag
	 * @return
	 * @throws NoSuchElementException if the player doesn't have that permit
	 */
	public Permit getPermitByTag(String permitChosenTag) {
		return permits.stream().filter(p -> p.getTag().equals(permitChosenTag)).findFirst().get();
	}

	public void setID(int id) {
		this.id=id;
	}
	
	public int getID(){
		return id;
	}
	
	/**
	 * Immutable view of this player
	 * @author andrea
	 *
	 */
	public class PlayerStat{
		private int nobilityTrack;
		private int victoryPoints;
		private List<City> buildedEmporia;
		private String username;
		
		
		private PlayerStat(Player p) {
			this.nobilityTrack=p.getNobility();
			this.victoryPoints=p.getVictoryPoints();
			this.buildedEmporia=p.getBuildedEmporia();
			this.username=p.username;
		}
		
		public int getNobilityTrack() {
			return nobilityTrack;
		}

		public int getVictoryPoints() {
			return victoryPoints;
		}

		public List<String> getBuildedEmporia() {
			return buildedEmporia.stream().map(c -> c.getTag()).collect(Collectors.toList());
		}
		
		public String getUsername() {
			return username;
		}
		public String toString(){
			return new String(username+" Victory Points:"+victoryPoints+" Nob Track:"+ nobilityTrack+ " Emporia:"+buildedEmporia.size() );
		}
	}

	
}
