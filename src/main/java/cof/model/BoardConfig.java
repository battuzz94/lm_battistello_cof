package cof.model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Configuration for a board generation
 * @author andrea
 *
 */
public class BoardConfig {
	private static final Logger LOGGER = Logger.getLogger(BoardConfig.class.getName());
	int numberOfRegions;
	int kingCouncillorCount;
	int connectionsSparseness;
	int bonusLevel;
	int permitsPerRegion;
	int citiesPerRegion;
	int councillorsPerRegion;
	int kingBonusCount;
	
	public static BoardConfig getDefaultConfig() {
		BoardConfig conf = new BoardConfig();
		conf.setBonusLevel(2);
		conf.setConnectionsSparseness(2);
		conf.setKingCouncillorCount(4);
		conf.setCitiesPerRegion(5);
		conf.setCouncillorsPerRegion(4);
		conf.setPermitsPerRegion(15);
		conf.setRegions(3);
		conf.setKingBonusCount(5);
		return conf;
	}
	
	public boolean isCorrect() {
		try {
			assert numberOfRegions > 0;
			assert kingCouncillorCount > 0;
			assert connectionsSparseness > 0;
			assert connectionsSparseness < 5;
			assert bonusLevel > 0;
			assert bonusLevel < 5;
			assert permitsPerRegion > 0;
			assert citiesPerRegion > 0;
			assert councillorsPerRegion > 0;
			return true;
		}
		catch (AssertionError e) {
			LOGGER.log(Level.FINEST, "Assertion error occured", e);
			return false;
		}
	}
	
	public int getRegions() {
		return numberOfRegions;
	}
	public void setRegions(int numberOfRegions) {
		this.numberOfRegions = numberOfRegions;
	}
	public int getKingCouncillorCount() {
		return kingCouncillorCount;
	}
	public void setKingCouncillorCount(int kingCouncillorCount) {
		this.kingCouncillorCount = kingCouncillorCount;
	}
	public int getConnectionsSparseness() {
		return connectionsSparseness;
	}
	public void setConnectionsSparseness(int connectionsSparseness) {
		this.connectionsSparseness = connectionsSparseness;
	}
	public int getBonusLevel() {
		return bonusLevel;
	}
	public void setBonusLevel(int bonusLevel) {
		this.bonusLevel = bonusLevel;
	}
	public int getPermitsPerRegion() {
		return permitsPerRegion;
	}
	public void setPermitsPerRegion(int numberOfPermits) {
		this.permitsPerRegion = numberOfPermits;
	}
	public int getCitiesPerRegion() {
		return citiesPerRegion;
	}
	public void setCitiesPerRegion(int numberOfCitiesPerRegion) {
		this.citiesPerRegion = numberOfCitiesPerRegion;
	}
	public int getCouncillorsPerRegion() {
		return councillorsPerRegion;
	}
	public void setCouncillorsPerRegion(int numberOfCouncillorsPerRegion) {
		this.councillorsPerRegion = numberOfCouncillorsPerRegion;
	}
	
	public int getKingBonusCount() {
		return kingBonusCount;
	}
	
	public void setKingBonusCount(int kingBonusCount) {
		this.kingBonusCount = kingBonusCount;
	}
	
}
