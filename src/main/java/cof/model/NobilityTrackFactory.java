package cof.model;

import java.util.Arrays;
import java.util.List;

import cof.bonus.AdditionalActionBonus;
import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.bonus.GainOneCityRewardBonus;
import cof.bonus.GainPermitBonus;
import cof.bonus.GainPermitRewardBonus;
import cof.bonus.GainTwoCityRewardBonus;

/**
 * Generates a nobility track
 * @author Andrea
 *
 */
public class NobilityTrackFactory {
	private static List<Bonus> nobilityTrackBonuses = Arrays.asList(
			null, 
			null,
			new BasicBonus(0, 2, 2, 0, 0),
			null,
			new GainOneCityRewardBonus(),
			null,
			new AdditionalActionBonus(),
			null,
			new BasicBonus(0, 0, 3, 1, 0),
			null,
			new GainPermitBonus(),
			null,
			new BasicBonus(1, 0, 5, 0, 0),
			null,
			new GainPermitRewardBonus(),
			null,
			new GainTwoCityRewardBonus(),
			null,
			new BasicBonus(0, 0, 8, 0, 0),
			new BasicBonus(0, 0, 2, 0, 0),
			new BasicBonus(0, 0, 3, 0, 0));
	
	/**
	 * Builds the nobility track
	 * @return
	 */
	public List<Bonus> build() {
		return nobilityTrackBonuses;
	}
}
