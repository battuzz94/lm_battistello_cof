package cof.model;

import cof.market.Sellable;

/**
 * A game card with a color
 * @author andrea
 *
 */
public class Card implements Sellable {
	private static final String BASE_TAG = "Card";
	private static int idNumber = 0;
	
	private String tag;
	private Price price;
	private PoliticColor color;
	
	protected Card() {
		this.color = PoliticColor.BLACK;
		this.tag = "NOTAG";
	}
	
	/**
	 * Creates a new card with specified color
	 * @param c
	 */
	public Card(PoliticColor c){
		this.color=c;
		idNumber++;
		this.tag = BASE_TAG + idNumber;
	}
	
	/**
	 * Extracts a new card from the deck
	 * @return
	 */
	public static Card drawCard(){
		return new Card(PoliticColor.getRandomPoliticColorWithJolly());
	}


	@Override
	public void takeFrom(Player player){
		player.takeCard(this);
	}
	
	@Override
	public void giveTo(Player player){
		player.addCards(this);
	}

	@Override
	public void setPrice(Price price) {
		this.price = price;
	}

	@Override
	public Price getPrice() {
		return price;
	}
	
	public PoliticColor getColor() {
		return color;
	}
	
	public boolean isJolly() {
		return color.isJolly();
	}
	
	@Override
	public String getTag() {
		return tag;
	}
	
	@Override
	public String toString() {
		return tag + "(" + color + ")";
	}
	
}
