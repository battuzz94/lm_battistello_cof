package cof.model;

import cof.bonus.BasicBonus;

/**
 * Generates a Region, with the relative Council, Permits and cities as specified in the
 * BoardConfig configuration file.
 * @author Andrea
 *
 */
public class RegionFactory {
	private Board board;
	private BoardConfig config;
	
	private int players;
	
	/**
	 * Creates a new Region Factory for the given Board and configuration
	 * @param board
	 * @param config
	 */
	public RegionFactory(Board board, BoardConfig config) {
		this.board = board;
		this.config = config;
	}
	
	/**
	 * Builds a new region
	 * @return
	 */
	public Region build() {
		Council council = new Council(board.getCouncillorPool());
		for (int i = 0; i < config.getCouncillorsPerRegion(); i++)
			council.pushCouncillor(board.getCouncillorPool().getRandomCouncillor());
		
		
		Region region = new Region(council, BasicBonus.setRandom(config.getBonusLevel()));
		
		region.setCouncil(council);
		
		CityFactory factory = new CityFactory(config);
		if (players != 0)
			factory.updateConfigWithPlayers(players);
		
		for (int i = 0; i < config.getCitiesPerRegion(); i++)
			region.addCityToRegion(factory.build());
		
		
		PermitFactory permitFactory = new PermitFactory(region, config);
		if (players != 0)
			permitFactory.setPlayers(players);
		
		Permit[] permits = permitFactory.build();
		
		for (Permit p : permits)
			region.assignPermit(p);
		
		for (int i = 0; i < Region.GAINABLE_PERMITS; i++) {
			region.turnFirstPermit();
		}
		
		return region;
	}

	public void setPlayers(int players) {
		this.players = players;
	}
}
