package cof.model;

/**
 * A factory to create cities
 * @author andrea
 *
 */
public class CityFactory {
	private BoardConfig config;
	
	/**
	 * Creates a new city factory with specified configuration
	 * @param config
	 */
	public CityFactory(BoardConfig config) {
		this.config = config;
	}
	
	/**
	 * Builds a new city
	 * @return
	 */
	public City build() {
		return City.setRandomCity(config.getBonusLevel());
	}
	
	/**
	 * Updates the configuration based on number of players
	 * @param players
	 */
	public void updateConfigWithPlayers(int players) {
		// This factory doesn't scale
	}
}
