package cof.model;

/**
 * This class represents something that can be given to someone
 * @author andrea
 *
 */
@FunctionalInterface
public interface Obtainable {
	/**
	 * Gives the object to player
	 * @param player
	 */
	public void giveTo(Player player);
}
