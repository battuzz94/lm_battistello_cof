package cof.model;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.commands.CommandNotValidException;
import cof.market.Sellable;

/**
 * Representation of a permit
 * @author andrea
 *
 */
public class Permit implements Sellable, JsonSerializer<Permit>, JsonDeserializer<Permit> {
	private static final String PRICE_IDENTIFIER = "price";
	public static final String BASE_TAG = "Permit";
	private static int idNumber = 0;
	
	private String tag;
	private Bonus bonus;
	private ArrayList<City> cities = new ArrayList<>();
	private Price price = new Price(0, 0);
	private boolean used;
	private Player owner;
	
	
	private Permit(){
		// set default constructor for gson serializer
	}
	
	/**
	 * Creates a permit with specified bonus
	 * @param bonus
	 */
	public Permit(Bonus bonus) {
		this(bonus, new Price(0, 0));
	}
	
	/**
	 * Creates a permit with specified bonus and price
	 * @param bonus
	 * @param price
	 */
	public Permit(Bonus bonus, Price price){
		this.bonus=bonus;
		this.price=price;
		this.owner = null;
		this.cities = new ArrayList<>();
		this.used = false;
		
		idNumber++;
		this.tag = BASE_TAG + idNumber;
	}
	
	protected void setRandomBonus() {
		bonus = BasicBonus.setRandom();
	}
	
	public boolean isUsed() {
		return used;
	}
	
	@Override
	public String getTag() {
		return tag;
	}
	
	
	@Override
	public void takeFrom(Player player){
		player.takePermit(this);
		this.owner=player;
	}
	
	@Override
	public void giveTo(Player player){
		player.addPermit(this);
		this.owner = player;
	}
	
	/**
	 * Adds a city to the list of cities in which the player can build
	 * @param c
	 */
	public void addCity(City c){
		cities.add(c);
	}

	@Override
	public void setPrice(Price price) {
		this.price = price;
	}

	@Override
	public Price getPrice() {
		return price;
	}

	public Bonus getBonus() {
		return bonus;
	}
	
	/**
	 * set the permit as used
	 * @throws CommandNotValidException
	 */
	public void setUsed() {
		if (used)
			throw new CommandNotValidException();
		used = true;
	}
	
	public void setBonus(Bonus b) {
		this.bonus = b;
	}
	
	public Player getOwner() {
		return owner;
	}
	
	public List<City> getCities(){
		return cities;
	}
	
	/**
	 * Checks if the permits lets the player to build in a certain city
	 * @param cityToBuild
	 * @return
	 */
	public boolean canBuildIn(City cityToBuild) {
		return cities.stream()
				.filter(c -> c.getTag().equals(cityToBuild.getTag()))
				.findAny()
				.isPresent();
	}
	
	@Override
	public String toString() {
		String ret = tag + ":\n";
		ret += "bonus: " + bonus + "\n";
		ret += "cities: " + cities.stream().map(c -> c.getTag()).reduce((c1, c2) -> c1 + ", " + c2).orElse("") + "\n";
		if(used)
			ret+="used\n";
		return ret;
	}
	
	/**
	 * Formats the permit output to be printed in market
	 * @return
	 */
	public String toStringMarket(){
		String ret= "bonus: " + bonus + "; cities: " + cities.stream().map(c -> c.getTag()).reduce((c1, c2) -> c1 + ", " + c2).orElse("");
		if(used)
			ret+="; used";
		return ret;
	}

	@Override
	public JsonElement serialize(Permit permit, Type type, JsonSerializationContext context) {
		JsonObject ret = new JsonObject();
		ret.addProperty("tag", permit.tag);
		if (permit.isUsed())
				ret.addProperty("used", permit.isUsed());
		ret.add("bonus", context.serialize(permit.bonus, Bonus.class));
		JsonArray citiesJson = new JsonArray();
		for (City c : permit.cities)
			citiesJson.add(c.getTag());
		ret.add("cities", citiesJson);
		ret.addProperty("type", "permit");
		ret.add(PRICE_IDENTIFIER, context.serialize(permit.getPrice()));
		return ret;
	}

	@Override
	public Permit deserialize(JsonElement obj, Type arg1, JsonDeserializationContext context) {
		Permit permit = new Permit();

		JsonObject jsonPermit = obj.getAsJsonObject();
		if (jsonPermit.has("used"))
			permit.used = jsonPermit.get("used").getAsBoolean();
		permit.tag = jsonPermit.get("tag").getAsString();
		permit.bonus = context.deserialize(jsonPermit.get("bonus"), Bonus.class);
		JsonArray citiesJson = jsonPermit.get("cities").getAsJsonArray();
		for (JsonElement e : citiesJson) {
			City c = new City(e.getAsString());
			c.setTag(e.getAsString());
			permit.addCity(c);
		}
		if (jsonPermit.has(PRICE_IDENTIFIER)) {
			Price permitPrice = context.deserialize(jsonPermit.get(PRICE_IDENTIFIER), Price.class);
			permit.setPrice(permitPrice);
		}
			
		
		return permit;
	}
	
	public static JsonSerializer<Permit> getSerializer() {
		return new Permit();
	}

	public static JsonDeserializer<Permit> getDeserializer() {
		return new Permit();
	}
	
}
