package cof.model;

import java.lang.reflect.Type;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cof.bonus.*;
import common.Point;

/**
 * A city is a place, within a region, where the player can build.
 * @author Andrea
 *
 */
public class City implements JsonSerializer<City>, JsonDeserializer<City>{
	private static final String DEFAULT_CITY_NAME = "NO_NAME";
	private static final String COORDINATE_TAG = "coordinate";
	private static final Logger LOGGER = Logger.getLogger(City.class.getName());
	public static final String BASE_TAG = "City";
	
	private String name;
	private String tag;
	private CityColor color;
	private Bonus bonus;
	private ArrayList<City> neighbors=new ArrayList<>();
	private int emporiaCount;

	private Point coordinate;
	
	private static int idNumber=0;
	
	
	// constructors
	private City(){
		this.color = CityColor.BRONZE;
		this.tag = "NOTAG";
		this.name = DEFAULT_CITY_NAME;
		this.coordinate = new Point(0.0,0.0);
		this.emporiaCount=0;
		this.neighbors = new ArrayList<>();
	}
	
	protected City(String name){
		this(null, new BasicBonus(), name);
	}
	
	protected City(Bonus bonus) {
		this(CityColor.getRandomCityColor(), bonus, DEFAULT_CITY_NAME);
	}

	protected City(CityColor color, Bonus bonus) {
		this(color, bonus, DEFAULT_CITY_NAME);
	}
	
	protected City(CityColor color, Bonus bonus, String name){
		this.color=color;
		this.bonus=bonus; 
		
		this.emporiaCount=0;
		this.neighbors = new ArrayList<>();
		idNumber++;
		this.tag=BASE_TAG+idNumber;
		this.name = name;
		this.coordinate = new Point(0.0,0.0);
	}
	
	
	/**
	 * Creates a new city with random color and bonus
	 * @return
	 */
	public static City setRandomCity(){
		return new City(CityColor.getRandomCityColorWithoutPurple(), BasicBonus.setRandom(), DEFAULT_CITY_NAME);
	}
	
	/**
	 * Creates a new city with specified bonus level
	 * @param bonusLevel
	 * @return
	 */
	public static City setRandomCity(int bonusLevel) {
		return new City(CityColor.getRandomCityColorWithoutPurple(), BasicBonus.setRandom(bonusLevel), DEFAULT_CITY_NAME);
	}
	
	public static City setRandomCity(String name){
		return new City(CityColor.getRandomCityColorWithoutPurple(), BasicBonus.setRandom(), name);
	}
	
	
	
	protected void addEmporium() {
		emporiaCount++;
	}
	

	protected void addNeighbor(City neighbor){
		neighbors.add(neighbor);
	}
	
	public String getName() {
		return name;
	}
	
	public Bonus getBonus(){
		return this.bonus;
	}
	
	public List<City> getNeighbors(){
		return this.neighbors;
	}
	
	public int getEmporiaCount(){
		return this.emporiaCount;
	}
	
	public CityColor getColor(){
		return this.color;
	}
	
	public String getTag(){
		return this.tag;
	}
	
	
	/**
	 * Assigns city bonus from uninterrupted paths of emporia to a player: this method will be 
	 * called every turn when a player builds a new emporium as a main action. The method travels 
	 * through the city graph until it finds emporia of said player and gives them their bonus, 
	 * through a BFS algorithm.
	 * @param p the player to whom give the bonuses
	 */
	public void giveBuiltPathBonus(Player p){
		HashSet<City> visitedCities;
		visitedCities=new HashSet<>();
		Queue<City> queue = new LinkedList<>();
		
		queue.add(this);
		
		while (!queue.isEmpty()) {
			City c = queue.poll();
			
			if(p.hasBuiltIn(c) && !visitedCities.contains(c)){
				p.obtain(c.getBonus());
				visitedCities.add(c);
				
				for(City n : c.getNeighbors())
					queue.add(n);
			}
		}
	}
	
	/**
	 * Returns a list of bonus associated with all the connected cities in which the player
	 * has built some emporium
	 * @param p the player who builded the emporium
	 * @return a List of bonuses
	 */
	public List<Bonus> getBonusNearby(Player p) {
		HashSet<City> visitedCities;
		visitedCities=new HashSet<>();
		Queue<City> queue = new LinkedList<>();
		List<Bonus> bonuses = new LinkedList<>();
		
		queue.add(this);
		
		while (!queue.isEmpty()) {
			City c = queue.poll();
			
			if(p.hasBuiltIn(c) && !visitedCities.contains(c)){
				bonuses.add(c.getBonus());
				visitedCities.add(c);
				
				for(City n : c.getNeighbors())
					queue.add(n);
			}
		}
		return bonuses;
	}
	
	
	/**
	 * Calculates the distance between the current city and another city. Uses BFS algorithm.
	 * It will be used mainly (if not exclusively) to calculate the cost of moving the king
	 * from the current location (a city) to the desired one. 
	 * @param destination a city
	 * @return integer, minimum number of arcs between current city and destination.
	 */
	public int getDistanceTo(City destination){
		HashMap<City, Integer> visitedAndCost=new HashMap<>();
		Queue<City> cityQueue=new LinkedList<>();
		
		cityQueue.add(this);
		visitedAndCost.put(this, 0);
		
		while(!cityQueue.isEmpty() && !cityQueue.peek().equals(destination)){
			City c= cityQueue.poll();
			
			int cost=visitedAndCost.get(c);
			for(City n : c.getNeighbors()){
				if (!visitedAndCost.containsKey(n)) {
					cityQueue.add(n);
					visitedAndCost.put(n, cost+1);
				}
			}
		}
		
		return visitedAndCost.get(destination);
		
	}
	
	@Override
	public JsonElement serialize(City city, Type typeOfObj, JsonSerializationContext context) {
		JsonObject ret = new JsonObject();
		try {
			ret.addProperty("name", city.getName());
			ret.addProperty("tag", city.getTag());
			if (city.emporiaCount > 0)
				ret.addProperty("emporiaCount", city.emporiaCount);
			ret.add(COORDINATE_TAG, context.serialize(city.getCoordinate()));
			ret.add("color", context.serialize(city.getColor()));
			ret.add("bonus", context.serialize(city.getBonus(), Bonus.class));
			
			JsonArray neighborsJson = new JsonArray();
			if (city.getNeighbors() != null) 
				for (City n : city.getNeighbors())
					neighborsJson.add(n.getTag());
			ret.add("neighbors", neighborsJson);
		}
		catch (ClassCastException e) {
			LOGGER.log(Level.WARNING, "Could not serialize City", e);
			ret = null;
		}
		return ret;
	}

	@Override
	public City deserialize(JsonElement cityJson, Type arg1, JsonDeserializationContext context) {
		City c = new City();
		JsonObject cityObj = (JsonObject) cityJson;
		c.bonus = context.deserialize(cityObj.get("bonus"), Bonus.class);
		c.color = context.deserialize(cityObj.get("color"), CityColor.class);
		c.name = cityObj.get("name").getAsString();
		c.tag = cityObj.get("tag").getAsString();
		c.neighbors = new ArrayList<>();
		try {
			c.emporiaCount = cityObj.get("emporiaCount").getAsInt();
		}
		catch (Exception e) {
			c.emporiaCount = 0;
			LOGGER.log(Level.FINEST, "No emporia count present", e);
		}
		
		if (cityObj.has(COORDINATE_TAG))
			c.setCoordinate(context.deserialize(cityObj.get(COORDINATE_TAG), Point.class));
		
		
		for (JsonElement el : (JsonArray)cityObj.get("neighbors")) {
			String cityTag = el.getAsString();
			City neighCity = new City();
			neighCity.tag = cityTag;
			c.neighbors.add(neighCity);
		}
		return c;
	}
	
	public static JsonSerializer<City> getSerializer() {
		return new City();
	}
	
	@Override
	public String toString() {
		return tag + ":(" + color + "-" + bonus + ")";
	}

	public static JsonDeserializer<City> getDeserializer() {
		return new City();
	}

	public void setCoordinate(Point value) {
		this.coordinate = value;
		
	}

	public Point getCoordinate() {
		return this.coordinate;
	}

	protected void setTag(String tag) {
		this.tag = tag;
	}

	protected void setColor(CityColor color) {
		this.color = color;
	}
	

}
