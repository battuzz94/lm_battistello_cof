package cof.model;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import cof.bonus.*;
import common.Algorithms;
import common.Point;
import common.Tuple2;
import common.Utility;

/**
 * The board class represent the entire structure of the game.
 * One board has many Regions, and each Region has a set of cities in which the Player can 
 * build. Each Region has a set of gainable permits and a council.
 * There is also a king's council, a set of king's bonuses and a set of available councillors.
 * @author Andrea
 *
 */
public class Board implements JsonSerializer<Board>, JsonDeserializer<Board> {
	private static final Logger LOGGER = Logger.getLogger(Board.class.getName());
	private static final int DISPOSE_ITERATIONS = 1000;
	public static final int WIDTH = 160;
	public static final int HEIGHT = 60;
	public static final int PADDING = 12;
	
	private ArrayList<Region> regions=new ArrayList<>();
	private CouncillorPool pool;
	private City kingPosition;
	private Council kingCouncil;
	private List<ColoredCityBonus> cityBonus;
	private List<Bonus> nobilityTrack;
	private Queue<Bonus> kingBonus;
	
	protected Board(){
		this(null, null);
	}
	
	public Bonus getKingBonus(){
		return (Bonus)kingBonus.element(); 
	}
	
	private Board(City kingPos, Council kingCouncil){
		this.kingPosition=kingPos;
		this.kingCouncil=kingCouncil;
		pool = new CouncillorPool();
		nobilityTrack = new ArrayList<>();
		cityBonus = new ArrayList<>();
		kingBonus = new LinkedList<>();
	}
	
	
	/**
	 * Updates the king council
	 * @param c the new council
	 */
	protected void setKingCouncil(Council c){
		this.kingCouncil=c;
	}
	

	protected void setCouncillorPool(CouncillorPool p) {
		pool = p;
	}
	
	
	/**
	 * Updates the king position
	 * @param kingCityDestination the final position of the king
	 */
	public void moveKing(City kingCityDestination) {
		kingPosition = kingCityDestination;
	}
	
	/**
	 * Adds one region in this board
	 * @param r the region to add
	 */
	protected void putRegionInBoard(Region r){
		regions.add(r);
	}

	/**
	 * Adds a councillor to board pool
	 * @param c
	 */
	public void addCouncillor(Councillor c) {
		pool.addCouncillor(c);
	}


	public City getKingPosition() {
		return kingPosition;
	}
	
	
	/**
	 * Returns the Region with given tag
	 * @param tag the tag of the region to search for
	 * @return the Region with given tag
	 * @throws NoSuchElementException if no such Region is found
	 */
	public Region getRegionByTag(String tag) {
		for(int i=0; i<regions.size(); i++){
			if((regions.get(i)).getTag().equals(tag)){
				return regions.get(i);
			}
		}
		throw new NoSuchElementException();
	}
	
	/**
	 * Return a list of bonus representing the nobility track
	 * @return the nobility track
	 */
	public List<Bonus> getNobilityTrack() {
		return nobilityTrack;
	}

	/**
	 * Returns the city identified by tag
	 * @param tag the tag of the city to search
	 * @return the city found with matching tag
	 * @throws NoSuchElementException if no such city is present or the tag is invalid
	 */
	public City getCityByTag(String tag) {
		for (Region r : regions) {
			if (r.containsCityWithTag(tag))
				return r.getCityByTag(tag);
		}
		throw new NoSuchElementException();
	}
	
	/**
	 * Returns a list of all the cities present in the board
	 * @return the cities contained in this board
	 */
	public List<City> getCities() {
		return regions.stream()
				.flatMap(reg -> reg.getCities().stream())
				.collect(Collectors.toList());
	}
	
	/**
	 * Returns a list of all the regions in the board
	 * @return a list of regions
	 */
	public List<Region> getRegions() {
		return regions;
	}
	
	
	public CouncillorPool getCouncillorPool() {
		return pool;
	}
	
	/**
	 * Search for an available permit with the given tag
	 * @param permitTag the permit tag to search for
	 * @return a permit if present and available
	 * @throws NoSuchElementException if such permit isn't available or the tag is invalid
	 */
	public Permit getPermitByTag(String permitTag) {
		for (Region r : regions) {
			Permit permit = r.getGainablePermitByTag(permitTag);
			if (permit != null)
				return permit;
		}
		throw new NoSuchElementException();
	}


	public Council getKingCouncil() {
		return kingCouncil;
	}
	
	/**
	 * Search for a council with given tag
	 * @param tag the tag of the council to search for
	 * @return a council matching the given tag
	 * @throws NoSuchElementException if no such council is present in this board
	 */
	public Council getCouncilByTag(String tag) {
		if (tag.equals(kingCouncil.getTag()))
			return kingCouncil;
		else {
			Council regionCouncil;
			for (Region r : regions) {
				regionCouncil = r.getRegionCouncil();
				if (tag.equals(regionCouncil.getTag())) {
					return regionCouncil;
				}
			}
		}
		
		throw new NoSuchElementException();
	}

	/**
	 * Search for the councillor with given tag. The councillor must be available or an
	 * exception is thrown
	 * @param tag the councillor tag
	 * @return the Councillor with given tag
	 * @throws NoSuchElementException
	 */
	public Councillor getCouncillorByTag(String tag) {
		return pool.getAvailableCouncillorByTag(tag);
	}
	
	public List<ColoredCityBonus> getCityBonus() {
		return this.cityBonus;
	}


	public static JsonSerializer<Board> getSerializer() {
		return new Board();
	}


	public static JsonDeserializer<Board> getJsonDeserializer() {
		return new Board();
	}


	@Override
	public JsonElement serialize(Board b, Type typeOfObj, JsonSerializationContext context) {
		JsonObject ret = new JsonObject();
		try {
			JsonElement regionJson = context.serialize(b.getRegions());
			ret.add("regions", regionJson);
			ret.add("councillors", context.serialize(b.getCouncillorPool()));
			
			ret.addProperty("kingPosition", b.getKingPosition().getTag());
			ret.add("kingCouncil", context.serialize(b.getKingCouncil()));
			
			JsonArray nobs = new JsonArray();
			b.nobilityTrack.forEach(bon -> nobs.add(context.serialize(bon, Bonus.class)));
			ret.add("nobilityTrack", nobs);
			
			JsonArray cityBonuses = new JsonArray(); 
			b.cityBonus.forEach(bon -> cityBonuses.add(context.serialize(bon, ColoredCityBonus.class)));
			ret.add("cityBonus", cityBonuses);
			
			JsonArray kingBonuses = new JsonArray(); 
			b.kingBonus.forEach(bon -> kingBonuses.add(context.serialize(bon, Bonus.class)));
			ret.add("kingBonus", kingBonuses);
		}
		catch (ClassCastException e) {
			LOGGER.log(Level.SEVERE, "Exception thrown in Board", e);
			ret = null;
		}
		return ret;
	}
	
	@Override
	public Board deserialize(JsonElement json, Type type, JsonDeserializationContext context) {
		JsonObject jsonBoard = json.getAsJsonObject();
		Board b = new Board();
		
		b.pool = (CouncillorPool) context.deserialize(jsonBoard.get("councillors"), CouncillorPool.class);
		
		JsonArray jsonRegions = (JsonArray)jsonBoard.get("regions");
		Region[] regionsJson = context.deserialize(jsonRegions, Region[].class);
		for (Region r : regionsJson) {
			r.getRegionCouncil().setCouncillorPool(b.pool);
			b.regions.add(r);
		}
		
		// Rebuilds City neighbor reference
		for (Region r : b.regions) {
			List<City> cities = r.getCities();
			for (City c : cities) {
				List<City> neighbors = c.getNeighbors();
				for (int i = 0; i < neighbors.size(); i++)
					neighbors.set(i, b.getCityByTag(neighbors.get(i).getTag()));
			}
		}
		

		// Rebuilds Permit city reference
		for (Region r : b.regions) {
			for (Permit permit : r.getPermits()) {
				for (int i = 0; i < permit.getCities().size(); i++) {
					String tag = permit.getCities().get(i).getTag();
					permit.getCities().set(i, b.getCityByTag(tag));
				}
			}
		}
		
		
		b.kingPosition = b.getCityByTag(jsonBoard.get("kingPosition").getAsString());
		b.kingCouncil = context.deserialize(jsonBoard.get("kingCouncil"), Council.class);
		
		b.kingCouncil.setCouncillorPool(b.pool);
		
		JsonArray bonuses = jsonBoard.get("nobilityTrack").getAsJsonArray();
		List<Bonus> nobBonus = new ArrayList<>();
		bonuses.forEach(el -> nobBonus.add(context.deserialize(el, Bonus.class)));
		b.nobilityTrack = nobBonus;
		
		ColoredCityBonus[] cityBonuses = context.deserialize(jsonBoard.get("cityBonus"), ColoredCityBonus[].class);
		b.cityBonus = Arrays.asList(cityBonuses);
		
		
		
		Bonus[] kingBonuses = context.deserialize(jsonBoard.get("kingBonus"), Bonus[].class); 
		
		b.kingBonus = new LinkedList<>(Arrays.asList(kingBonuses));
		return b;
	}
	
	
	/**
	 * Loads a board from file
	 * @param filename the path of file
	 * @return the Board deserialized
	 * @throws IOException
	 */
	public static Board load(String filename) throws IOException {
		Board b = null;
		
		try {
			FileReader fileReader=new FileReader(filename);
			Scanner in = new Scanner(fileReader);
			StringBuilder bld = new StringBuilder();
			while (in.hasNext())
				bld.append(in.nextLine());
			String serializedObj = bld.toString();
			Gson gson = Utility.getJsonSerializer();
			b = gson.fromJson(serializedObj, Board.class);
			in.close();
			fileReader.close();
			
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Error on deserializing board", e);
			b = null;
		}
		return b;
	}


	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("Regions:\n");
		for (Region r : regions)
			bld.append(r.getName() + ": \n" + r);
		
		bld.append("\nNobility Track: " + nobilityTrack);
		bld.append("\nKing Position: " + kingPosition.getTag());
		bld.append("\nKing Council: " + kingCouncil);
		return bld.toString();
	}
	
	
	/**
	 * Calculate all the cities coordinates with some values, calculated in runtime based on 
	 * the number of regions and cities per region.
	 * @return returns a matching between each city and its coordinate.
	 */
	private SortedMap<City,Point> getInitialCoordinates() {
		SortedMap<City, Point> cityCoordinates = new TreeMap<>((c1, c2) -> c1.getTag().compareTo(c2.getTag()));
		
		final int regionWidth = WIDTH / getRegions().size();
		
		for (int r = 0; r < getRegions().size(); r++) {
			Region reg = getRegions().get(r);
			final int radiusX = regionWidth/2 - PADDING;
			final int radiusY = HEIGHT/2 - PADDING;
			final double initialTheta = 0;
			final double offsetTheta = 2*Math.PI / reg.getCities().size();
			
			List<City> cities = reg.getCities();
			for (int i = 0; i < cities.size(); i++) {
				double theta = initialTheta + r*Math.PI/2 + i*offsetTheta;
				
				double centerX = r*regionWidth + regionWidth/2 + radiusX * Math.cos(theta);
				double centerY = HEIGHT/2 + radiusY * Math.sin(theta);
				
				cityCoordinates.put(cities.get(i), new Point(centerX, centerY));	
			}
		}
		return cityCoordinates;
	}
	
	/**
	 * Apply some algorithms and swap cities to better visualize the board on screen
	 */
	public void dispose() {
		SortedMap<City, Point> map = getInitialCoordinates();
		
		pack(map);
		
		for (Entry<City,Point> entry : map.entrySet()) {
			String cityTag = entry.getKey().getTag();
			getCityByTag(cityTag).setCoordinate(entry.getValue());
		}
		
	}
	
	/**
	 * Run a loop where it searches for a pair of city to swap, and evaluate if it is convenient or not.
	 * @param map
	 */
	private void pack(SortedMap<City, Point> map) {		
		int bestMark = getIntersectionCost(map);
		for (int i = 0; i < DISPOSE_ITERATIONS; i++) {	
			Region region = Algorithms.chooseFromList(getRegions());
			City c1 = Algorithms.chooseFromList(region.getCities());
			City c2 = Algorithms.chooseFromList(region.getCities());
			while (c2 == c1)
				c2 = Algorithms.chooseFromList(region.getCities());
			
			// swaps the cities
			Point tmp1 = map.get(c1);
			Point tmp2 = map.get(c2);
			map.put(c1, tmp2);
			map.put(c2, tmp1);
			
			int newMark = getIntersectionCost(map);
			if (newMark > bestMark) {
				map.put(c1, tmp1);
				map.put(c2, tmp2);
			}
			else
				bestMark = newMark;
		}
	}
	
	/**
	 * Evaluates the cost of all the intersections and distances between cities
	 * @param map the mapped coordinates of each city
	 * @return the cost of this board
	 */
	private int getIntersectionCost(final SortedMap<City,Point> map) {
		int count = 0;
		
		Queue<City> cityQueue = new LinkedList<>();
		Set<City> visitedCities = new TreeSet<>((c1, c2) -> c1.getTag().compareTo(c2.getTag()));
		List<Tuple2<Point,Point>> cityConnections = new LinkedList<>();
		
		cityQueue.add(map.firstKey());
		
		
		while (!cityQueue.isEmpty()) {
			City u = cityQueue.poll();
			Point uCoord = map.get(u);
			visitedCities.add(u);
			for (City v : u.getNeighbors()) {
				Point vCoord = map.get(v);
				if (!visitedCities.contains(v)) {
					cityQueue.add(v);
					for (Tuple2<Point, Point> c1 : cityConnections)
						if (Algorithms.intersects(c1.getFirst(), c1.getSecond(), uCoord, vCoord)) 
							count++;
					cityConnections.add(new Tuple2<Point,Point>(uCoord, vCoord));
				}
			}
		}
		
		// Sum up the distances between all the cities
		
		double cost = cityConnections.stream().reduce(0.0, (ret,conn) -> {
				double x1 = conn.getFirst().x();
				double y1 = conn.getFirst().y();
				double x2 = conn.getSecond().x();
				double y2 = conn.getSecond().y();
				
				return ret + Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
			}
		, (r1, r2) -> r1 + r2);

		return count*10 + (int)cost;
	}


	public void setNobilityBonuses(List<Bonus> nobilityBonus) {
		this.nobilityTrack = nobilityBonus;
	}
	
	/**
	 * Adds a new king bonus to the stack
	 * @param b
	 */
	public void addKingBonus(StaticBonus b) {
		kingBonus.add(b);
	}
	
	/**
	 * Adds a new CityColorBonus
	 * @param c
	 * @param basicBonus
	 */
	public void addCityColorBonus(CityColor c, StaticBonus basicBonus) {
		cityBonus.add(new ColoredCityBonus(c, basicBonus));
	}

	/**
	 * Retrieves a king bonus
	 * @return the king bonus, if present
	 * @throws NoSuchElementException if there are no king bonus left
	 */
	public Bonus pollKingBonus() {
		Optional<Bonus> ret = Optional.ofNullable(kingBonus.poll());
		return ret.get();
	}
	
	/**
	 * Checks if there are other king bonus available
	 * @return
	 */
	public boolean hasKingBonus() {
		return !kingBonus.isEmpty();
	}
}
