package cof.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import com.google.gson.Gson;

import cof.bonus.BasicBonus;
import common.Algorithms;
import common.Tuple2;
import common.Tuple3;
import common.Utility;

/**
 * This class generates a brand new Board with the setup specified in the BoardConfig
 * given as parameter.
 * @author Andrea
 *
 */
public class BoardFactory {
	private BoardConfig config;
	private int players;
	
	/**
	 * Creates a new board factory with default configuration
	 */
	public BoardFactory() {
		this(BoardConfig.getDefaultConfig());
	}
	
	/**
	 * Creates a board factory with specified configuration
	 * @param config
	 */
	public BoardFactory(BoardConfig config) {
		if (!config.isCorrect())
			throw new IllegalStateException();
		this.config = config;
	}
	
	/**
	 * Scales the config properties to maintain the game playable for the given number of players.
	 * @param players number of players in the game
	 */
	public void updateConfigWithPlayers(int players) {
		this.players = players;
		config.setRegions(3 + players/5);
		config.setKingBonusCount(5 + players/5);
	}
	
	/**
	 * Builds a new board with the specified parameters
	 * @return
	 */
	public Board build() {
		Board board = new Board();
		
		CouncillorFactory councillorFactory = new CouncillorFactory(config);
		board.setCouncillorPool(councillorFactory.build());
		
		for (int i = 0; i < config.getRegions(); i++) {
			RegionFactory fact = new RegionFactory(board, config);
			if (players != 0)
				fact.setPlayers(players);
			board.putRegionInBoard(fact.build());
		}

		setConnections(board);
		
		Council kingCouncil = new Council(board.getCouncillorPool());
		for (int i = 0; i < config.getKingCouncillorCount(); i++)
			kingCouncil.pushCouncillor(board.getCouncillorPool().getRandomCouncillor());
		
		NobilityTrackFactory nobilityFactory = new NobilityTrackFactory();
		board.setNobilityBonuses(nobilityFactory.build());
		
		board.setKingCouncil(kingCouncil);
		setKingCityPosition(board);
		
		for (int i = 0; i < config.getKingBonusCount(); i++)
			board.addKingBonus(new BasicBonus(0,0,Math.max(3, 20-3*i),0,0));

		for (CityColor c : CityColor.values())
			if (c != CityColor.PURPLE)
				board.addCityColorBonus(c, new BasicBonus(0,0,7,0,0));
		
		board.dispose();
		
		return board;
	}
	
	private void setKingCityPosition(Board board) {
		Region kingRegion = Algorithms.chooseFromList(board.getRegions());
		City kingCity = Algorithms.chooseFromList(kingRegion.getCities());
		kingCity.setColor(CityColor.PURPLE);
		board.moveKing(kingCity);
	}
	
	/**
	 * Loads a board from a serialized json string
	 * @param jsonBoard
	 * @return
	 */
	public Board loadFromString(String jsonBoard) {
		Gson gson = Utility.getJsonSerializer();
		return gson.fromJson(jsonBoard, Board.class);
	}
	
	
	
	
	/**
	 * For each city c1 of the array, this method sets a mean of 3 neighbors chosen randomly 
	 * amongst the other cities in the array.
	 * @param city array of cities previously initialized by the method generate().
	 */
	protected void setRandomCityConnection(Board board){
		List<Tuple2<City, City>> couples = new ArrayList<>();
		
		for (int i = 0; i < board.getRegions().size() - 1; i++)
			couples.addAll(Algorithms.getCouples(board.getRegions().get(i).getCities(), board.getRegions().get(i+1).getCities()));
		for (Region reg : board.getRegions())
			couples.addAll(Algorithms.getCouples(reg.getCities()));
		
		Collections.shuffle(couples);
		
		boolean connected = graphConnected(board);
		int i = 0;
		while (!connected && i < couples.size()) {
			City c1 = couples.get(i).getFirst();
			City c2 = couples.get(i).getSecond();
			c1.addNeighbor(c2);
			c2.addNeighbor(c1);
			i++;
			connected = graphConnected(board);
		}

	}
	
	/**
	 * Checks whether the graph of cities is connected or not
	 * @param board the board with all the cities
	 * @return true if the cities are all connected together
	 */
	private boolean graphConnected(Board board) {
		Set<City> visited = new TreeSet<>((c1, c2) -> c1.getTag().compareTo(c2.getTag()));
		Queue<City> cityQueue = new LinkedList<>();
		int numberOfCities = (int)board.getRegions().stream().flatMap(reg -> reg.getCities().stream()).count();
		if (numberOfCities == 0)
			return false;
		cityQueue.add(board.getRegions().stream().flatMap(r -> r.getCities().stream()).findAny().get());
		while (!cityQueue.isEmpty()) {
			City u = cityQueue.poll();
			visited.add(u);
			for (City v : u.getNeighbors())
				if (!visited.contains(v))
					cityQueue.add(v);
		}
		
		return numberOfCities == visited.size();
	}
	
	/**
	 * Generate random connections with some algorithms. It is guaranteed that the resulting graph is connected.
	 * @param board
	 */
	public void setConnections(Board board) {
		List<City> cities = board.getCities();
		
		List<Tuple2<City, City>> connections = new ArrayList<>();
		
		for (int i = 0; i < board.getRegions().size() - 1; i++)
			connections.addAll(Algorithms.getCouples(board.getRegions().get(i).getCities(), board.getRegions().get(i+1).getCities()));
		for (Region reg : board.getRegions())
			connections.addAll(Algorithms.getCouples(reg.getCities()));
				
		Random random = new Random();
		
		List<Tuple3<City, City,Integer>> cost = new ArrayList<>();
		for (Tuple2<City, City> connection : connections)
			cost.add(new Tuple3<>(connection.getFirst(), connection.getSecond(), random.nextInt(5000)));
		
		cost.sort((c1, c2) -> c1.getThird().compareTo(c2.getThird())); 
		
		int[] unionFind = Algorithms.makeSet(cities);
		
		for (Tuple3<City, City, Integer> t : cost) {
			City cityA = t.getFirst();
			City cityB = t.getSecond();
			int indexA = cities.indexOf(cityA);
			int indexB = cities.indexOf(cityB);
			if (!Algorithms.sameSet(unionFind, indexA, indexB)) {
				cityA.addNeighbor(cityB);
				cityB.addNeighbor(cityA);
				Algorithms.union(unionFind, indexA, indexB);
			}
		}
		
		for (int i = cost.size()-1; i >= cost.size() - config.getConnectionsSparseness(); i--) {
			City cityA = cost.get(i).getFirst();
			City cityB = cost.get(i).getSecond();
			cityA.addNeighbor(cityB);
			cityB.addNeighbor(cityA);
		}
		
		
	}
}
