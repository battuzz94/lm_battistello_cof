package cof.model;

/**
 * Factory to build councillors
 * @author andrea
 *
 */
public class CouncillorFactory {
	private static final int NUMBER_OF_COLORS=6;
	
	private int councillorsCount;
	
	/**
	 * Creates a new factory with specified configuration
	 * @param config
	 */
	public CouncillorFactory(BoardConfig config) {
		this.councillorsCount = config.getKingCouncillorCount() + config.getRegions() * config.getCouncillorsPerRegion() + 8;
	}
	
	/**
	 * Creates the councillor
	 * @return
	 */
	public CouncillorPool build() {
		CouncillorPool pool=new CouncillorPool();
		int councillorForColor=councillorsCount/NUMBER_OF_COLORS;
		int odd=councillorsCount-(councillorForColor*NUMBER_OF_COLORS);
		
		for(int j=0; j<councillorForColor; j++) {
			pool.addCouncillor(new Councillor(PoliticColor.PURPLE));
			pool.addCouncillor(new Councillor(PoliticColor.BLACK));
			pool.addCouncillor(new Councillor(PoliticColor.WHITE));
			pool.addCouncillor(new Councillor(PoliticColor.ORANGE));
			pool.addCouncillor(new Councillor(PoliticColor.PINK));
			pool.addCouncillor(new Councillor(PoliticColor.BLUE));
		}
		for(int j=0; j<odd; j++)
			pool.addCouncillor(new Councillor(PoliticColor.getRandomPoliticColor()));
		
		return pool;
	}
}
