package cof.model;

import java.util.Collections;
import java.util.List;

import cof.bonus.BasicBonus;
import common.Algorithms;
import common.Tuple2;
import common.Tuple3;

/**
 * Generates a list of permits with the options specified in BoardConfig file configuration
 * @author Andrea
 *
 */
public class PermitFactory {
	Region region;
	
	private int players;
	private BoardConfig config;
	
	/**
	 * Creates a new factory for region with specified configuration
	 * @param region
	 * @param config
	 */
	public PermitFactory(Region region, BoardConfig config) {
		this.region = region;
		this.config = config;
	}
	
	/**
	 * Builds a set of permits
	 * @return
	 */
	public Permit[] build() {
		List<City> cities = region.getCities();
		int totalPermits = config.getPermitsPerRegion();
		
		if (players > 4)
			totalPermits += 3*(players - 4);
		
		
		Permit[] permits = new Permit[totalPermits];
		for (int i = 0; i < totalPermits; i++) {
			permits[i] = new Permit(BasicBonus.setRandom(config.getBonusLevel()));
		}
		
		// Builds the permits with 1 city
		
		int permitIndex = 0;
		
		for (; permitIndex < cities.size() && permitIndex < totalPermits; permitIndex++) {
			permits[permitIndex].addCity(cities.get(permitIndex));
		}
		
		
		List<Tuple2<City, City>> couples = Algorithms.getCouples(region.getCities());
		Collections.shuffle(couples);
		int coupleIndex = 0;
		for (; permitIndex < totalPermits/3*2; permitIndex++) {
			if (coupleIndex == couples.size()) {
				Collections.shuffle(couples);
				coupleIndex = 0;
			}
			Tuple2<City, City> p = couples.get(coupleIndex);
			coupleIndex++;
			
			permits[permitIndex].addCity(p.getFirst());
			permits[permitIndex].addCity(p.getSecond());
		}
		
		List<Tuple3<City, City, City>> triplets = Algorithms.getTriplets(region.getCities());
		Collections.shuffle(triplets);
		int tripletIndex = 0;
		for (; permitIndex < totalPermits; permitIndex++) {
			if (tripletIndex == triplets.size()) {
				Collections.shuffle(triplets);
				tripletIndex = 0;
			}
			Tuple3<City, City, City> p = triplets.get(tripletIndex);
			tripletIndex++;
			
			permits[permitIndex].addCity(p.getFirst());
			permits[permitIndex].addCity(p.getSecond());
			permits[permitIndex].addCity(p.getThird());
		}
		
		return permits;
		
		
	}

	public void setPlayers(int players) {
		this.players = players;
	}
}
