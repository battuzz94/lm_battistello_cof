package cof.model;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.*;

/**
 * A board region
 * @author andrea
 *
 */
public class Region {
	private static final Logger LOGGER = Logger.getLogger(Region.class.getName());
	private static final String DEFAULT_REGION_NAME = "NO_NAME";
	public static final int GAINABLE_PERMITS = 2;
	public static final String BASE_TAG = "Region";
	private String tag;
	private static int idNumber=0;
	private String name;
	
	
	/*
	 * The only permits that a player can take (and see) are the two in gainablePermits
	 * the LinkedList (queue) 'permits' is a stack of turned cards and cannot be accessed
	 * but with the method turnFirstPermit.
	 */
	private List<City> cities=new ArrayList<>();
	private LinkedList<Permit> permits= new LinkedList<>(); 
	private LinkedList<Permit> gainablePermits=new LinkedList<>();
	private Council council;
	private Bonus regionBonus;
	
	private boolean bonusAvailable;
	
	@SuppressWarnings("unused")
	private Region(){
		regionBonus = new BasicBonus();
		name = DEFAULT_REGION_NAME;
	}
	
	/**
	 * Creates a new Region with the specified council
	 * @param c
	 */
	public Region(Council c){
		this(c, new BasicBonus(), DEFAULT_REGION_NAME);
	}
	
	/**
	 * Creates a new region with council and bonus
	 * @param c
	 * @param regionBonus
	 */
	public Region(Council c, Bonus regionBonus){
		this(c, regionBonus, DEFAULT_REGION_NAME);
	}
	
	/**
	 * Creates a new Region with council, bonus and a name
	 * @param c
	 * @param regionBonus
	 * @param name
	 */
	public Region(Council c, Bonus regionBonus, String name){
		this.council=c;
		this.regionBonus=regionBonus;
		bonusAvailable=true;
		this.name=name;
		
		idNumber++;
		this.tag=BASE_TAG+idNumber;
	}
	
	
	protected void assignPermit(Permit p){
		permits.add(p);
	}
	
	protected void addCityToRegion(City c){
		cities.add(c);
	}
	
	
	public List<Permit> getGainablePermits(){
		return gainablePermits;
	}
	
	public Council getRegionCouncil(){
		return council;
	}
	
	public List<City> getCities(){
		return cities;
	}
	
	public Bonus getBonus(){
		return regionBonus;
	}
	
	
	public String getTag(){
		return tag;
	}
	
	public String getName(){
		return name;
	}
	
	
	
	protected void setName(String name){
		this.name=name;
	}
	
	protected void setCouncil(Council c){
		this.council=c;
	}
	
	protected void setBonus(Bonus b){
		this.regionBonus=b;
	}
	
	protected void setRandomBonus(){
		this.regionBonus=BasicBonus.setRandom();
	}
	
	/**
	 * Check if the region bonus is still available or not
	 * @return
	 */
	public boolean bonusIsAvailable(){
		return bonusAvailable;
	}
	
	/**
	 * Shuffle the gainable permits
	 */
	public void shufflePermits(){
		permits.addAll(gainablePermits);
		gainablePermits = new LinkedList<>();
		Collections.shuffle(permits);
		for (int i = 0; i < GAINABLE_PERMITS; i++)
			turnFirstPermit();
	}
	
	
	/**
	 * This method takes the first element of the list permits (ideally a queue) and adds it
	 * in the first free position in the array gainablePermits. If there is more than one free
	 * position in gainablePermits (i.e., the array is empty), this method only adds a permit
	 * in the first one and must be called twice.
	 * @throws NoSuchElementException if such element isn't present
	 */
	protected void turnFirstPermit() {
		
		if (gainablePermits.size() < GAINABLE_PERMITS) {
			gainablePermits.add(permits.pop());
		}
		else 
			throw new NoSuchElementException();
	}
	
	
	/**
	 * This method gives a player one of either two permits in gainablePermits.
	 * @param index the index in the array
	 * @param p the player who takes the permit
	 * @throws IllegalArgumentException if the given index is not in the bounds of the array,
	 * the method throws an exception.
	 */
	protected void takePermit(int index, Player p) {
		if (index>= 0 && index < gainablePermits.size()) {
			Permit permit = gainablePermits.remove(index);
			p.addPermit(permit);
		} 
		else 
			throw new IllegalArgumentException();
		 
	}
	
	/**
	 * Search in gainable permits if there is a permit with the given tag.
	 * If the permit has been found, it is removed from the list and a new permit will be
	 * automatically popped from the permit stack.
	 * Otherwise, if no permit matched the given tag, an exception is thrown. 
	 * @param permitTag the tag of the permit chosen
	 * @return a Permit object with the matched tag
	 * @throws NoSuchElementException if no permits matches the given tag
	 */
	public Permit takePermitByTag(String permitTag) {
		boolean found = false;
		Permit ret = null;
		for (int i = 0; i < gainablePermits.size() && !found; i++) {
			if (gainablePermits.get(i).getTag().equals(permitTag)) {
				found = true;
				ret = gainablePermits.get(i);
				try {
					gainablePermits.set(i, permits.pop()); 
				}
				catch (NoSuchElementException e) {
					LOGGER.log(Level.FINE, "No permit found with tag " + permitTag, e);
					gainablePermits.remove(i);
				}
			}
		}
		if (found)
			return ret;
		else
			throw new NoSuchElementException();
	}
	
	
	/**
	 * Search for a city with given tag
	 * @param tag the tag of the city to search
	 * @return the City with the matching tag
	 * @throws NoSuchElementException if such City is not present or tag is invalid
	 */
	protected City getCityByTag(String tag) {
		for(int i=0; i<cities.size(); i++){
			if((cities.get(i)).getTag().equals(tag)){
				return cities.get(i);
			}
		}
		throw new NoSuchElementException();
	}
	
	@Override
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(tag + "\n");
		for (City c : cities)
			bld.append(c + "\n");
		return bld.toString();
	}

	/**
	 * Returns the permit available in this region with given tag, or null if no such
	 * permit is present
	 * @param permitTag tag of the permit to search for
	 * @return the Permit if any. null if no gainable permits mathes the given tag
	 */
	public Permit getGainablePermitByTag(String permitTag) {
		for (Permit p : gainablePermits)
			if (permitTag.equals(p.getTag()))
				return p;
		return null;
	}

	/**
	 * Returns true if the region contains the city with that tag
	 * @param tag
	 * @return
	 */
	public boolean containsCityWithTag(String tag) {
		try {
			getCityByTag(tag);
			return true;
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINER, "this board does not contain " + tag, e);
			return false;
		}
	}

	/**
	 * Set the region bonus as used
	 */
	public void setBonusUsed() {
		this.bonusAvailable = false;
	}


	public List<Permit> getPermits() {
		List<Permit> perm = new ArrayList<>(this.permits);
		perm.addAll(gainablePermits);
		return perm;
	}

}
