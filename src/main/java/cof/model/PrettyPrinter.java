package cof.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import cof.bonus.Bonus;

/**
 * This class will print a nice board on a matrix of characters
 * @author andrea
 *
 */
public class PrettyPrinter {
	
	private static final String EMPORIUM_ASCII = "#";
	private static final String EMPORIUM_OWNED_ASCII="X";
	
	private static final String[] COUNCILLOR_ASCII = {
			" O ",
			"/|\\",
			"/ \\"
	};
	
	private static final String[] PERMIT_ASCII = {
			" ___________ ",
			"|           |",
			"|           |",
			"|           |",
			"|           |",
			" ___________ ",
			"             "
	};
	
	
	private static final String[] CITY_ASCII = {
			 " _   |~  _ ",
			 "[_]--'--[_]",
			 "|'|\"\"`\"\"|'|",
			 "| | /^\\ | |",
			 "|_|_|I|_|_|"};
	private static final int CITY_WIDTH = CITY_ASCII[0].length();
	private static final int CITY_HEIGHT = CITY_ASCII.length;
	
	private static final String[] CROWN = {
			  " _.+._ ",
			  "(\\/^\\/)",
			  " \\@@@/ "
	};
	
	private Board board;
	private Player player=null;
	
	/**
	 * Creates a new prettyPrinter for the specified board
	 * @param board
	 */
	public PrettyPrinter(Board board) {
		this.board = board;
	}
	
	/**
	 * Creates a new PrettyPrinter sized for the player
	 * @param board
	 * @param player
	 */
	public PrettyPrinter(Board board, Player player){
		this.board=board;
		this.player=player;
	}
	
	
	private String[] createEmptyMatrix(int width, int height) {
		String[] matrix = new String[height];
		String emptyLine = StringUtils.repeat(" ", width);
		for (int i = 0; i < height; i++)
			matrix[i] = emptyLine;
		return matrix;
	}
	
	/**
	 * Draws the connection between all cities in matrix
	 * @param matrix
	 */
	private void printCityConnections(String[] matrix) {
		for (City u : board.getCities()) {
			for (City v : u.getNeighbors()) {
				// connect u->v
				if (u.getTag().compareTo(v.getTag()) > 0) { // To make the connection asymmetric
					double x1 = u.getCoordinate().x();
					double y1 = u.getCoordinate().y();
					double x2 = v.getCoordinate().x();
					double y2 = v.getCoordinate().y();
					
					drawLine(matrix, (int)x1, (int)y1, (int)x2, (int)y2);
					
				}
			}
		}
	}
	
	/**
	 * Print all the cities in their position defined by their coordinate
	 * @param matrix
	 */
	private void printCities(String[] matrix) {
		for (Region r : board.getRegions()) {
			for (City city : r.getCities()) {
				int centerX = (int)city.getCoordinate().x();
				int centerY = (int)city.getCoordinate().y();
				
				for (int j = 0; j < CITY_HEIGHT; j++) {
					matrix[j+centerY-CITY_HEIGHT/2] = StringUtils.overlay(matrix[j+centerY-CITY_HEIGHT/2], CITY_ASCII[j], centerX-CITY_WIDTH/2, centerX + (CITY_WIDTH-CITY_WIDTH/2));
				}
				
				
				String cityName="";
				String emporia="";
				
				if(player!=null && player.hasBuiltIn(city))
					emporia+=EMPORIUM_OWNED_ASCII+ StringUtils.repeat(EMPORIUM_ASCII, city.getEmporiaCount()-1);
				else
					emporia+=StringUtils.repeat(EMPORIUM_ASCII, city.getEmporiaCount());
				
				cityName += city.getTag() + " " + emporia;
				
				String bonusName = StringUtils.center(city.getBonus().toString(), CITY_WIDTH);
				matrix[centerY+(CITY_HEIGHT - CITY_HEIGHT/2)] = StringUtils.overlay(matrix[centerY+(CITY_HEIGHT - CITY_HEIGHT/2)], cityName, centerX-CITY_WIDTH/2+1, centerX-CITY_WIDTH/2 + cityName.length() + 1);
				matrix[centerY-(CITY_HEIGHT - CITY_HEIGHT/2)] = StringUtils.overlay(matrix[centerY-(CITY_HEIGHT - CITY_HEIGHT/2)], bonusName, centerX-CITY_WIDTH/2+1, centerX-CITY_WIDTH/2 + bonusName.length() + 1);
				
			}
		}
	}
	
	/**
	 * Prints a crown in king's current city
	 * @param matrix
	 */
	private void printKingPosition(String[] matrix) {
		City kingCity = board.getKingPosition();
		int centerX = (int)kingCity.getCoordinate().x();
		int centerY = (int)kingCity.getCoordinate().y();
		
		for (int i = 0; i < CROWN.length; i++) {
			matrix[centerY - CITY_HEIGHT/2 + i - 4] = StringUtils.overlay(matrix[centerY - CITY_HEIGHT/2 + i - 4], CROWN[i], centerX - CITY_WIDTH/2 + 2, centerX - CITY_WIDTH/2 + CROWN[i].length() + 2);
		}
	}
	
	/**
	 * Overlays the lines to divide the regions
	 * @param matrix
	 */
	private void printRegionDivisors(String[] matrix) {
		int regionWidth = Board.WIDTH / board.getRegions().size();
		for (int i = 1; i < board.getRegions().size(); i++) {
			drawLine(matrix, regionWidth * i, 10, regionWidth * i, Board.HEIGHT - 1);
		}
		
	}
	
	/**
	 * Returns a matrix that contains the permits and councils of all regions
	 * @return a matrix that contains the permits and councils of all regions
	 */
	public String[] printRegionPermitsAndCouncils() {
		int regionWidth = Board.WIDTH / board.getRegions().size();
		String[] ret = createEmptyMatrix(Board.WIDTH, PERMIT_ASCII.length + COUNCILLOR_ASCII.length + 5);
		
		for (int i = 0; i < board.getRegions().size(); i++) {
			Region r = board.getRegions().get(i);
			
			String[] permits = printRegionPermits(r);
			String[] council = printCouncil(r.getRegionCouncil());
			
			String regionDesc = r.getTag() + " --> " + r.getRegionCouncil().getTag() + " | bonus: " + (r.bonusIsAvailable() ? r.getBonus() : "--");
			
			int row = 0;
			
			ret[row] = StringUtils.overlay(
					ret[row], 
					regionDesc, 
					10 + i*regionWidth, 
					10 + i*regionWidth + regionDesc.length());
			row++;
			
			for (int j = 0; j < permits.length; j++) {
				ret[row] = StringUtils.overlay(
						ret[row], 
						permits[j], 
						10 + i*regionWidth, 
						10 + i*regionWidth + permits[j].length());
				row++;
			}
			for (int j = 0; j < council.length; j++) {
				ret[row] = StringUtils.overlay(
						ret[row], 
						council[j], 
						1 + i*regionWidth, 
						1 + i*regionWidth + council[j].length());
				row++;
			}
		}
		return ret;
	}
	
	/**
	 * Returns a string representing the visual representation of the board
	 * @return a string to be printed
	 */
	public String printBoard() {
		String[] matrix = createEmptyMatrix(Board.WIDTH, Board.HEIGHT);
		
		printCityConnections(matrix);
		
		printCities(matrix);
		
		printKingPosition(matrix);
		
		printRegionDivisors(matrix);
		
		String[] permitsAndCouncils = printRegionPermitsAndCouncils();
		
		String[] nobilityTrack = printNobilityTrack();
		
		return StringUtils.join(matrix, '\n') 
				+ "\n" 
				+ StringUtils.join(permitsAndCouncils, "\n")
				+ "\n\n\n"
				+ StringUtils.join(nobilityTrack, '\n')
				+ "\n\n\n"
				+ StringUtils.join(printCouncil(board.getKingCouncil()), "\n");
	}
	
	
	/**
	 * Overlays a line in given matrix connecting (x1,y1) to (x2,y2)
	 * @param matrix the string array on which overlay the symbols
	 * @param x1 x coordinate of first point
	 * @param y1 y coordinate of first point
	 * @param x2 x coordinate of second point
	 * @param y2 y coordinate of second point
	 */
	private static void drawLine(String[] matrix, int x1, int y1, int x2, int y2) {
		if (x1 > x2)
			drawLine(matrix, x2, y2, x1, y1);
		
		double deltax = (double)x2 - x1;
		double deltay = (double)y2 - y1;
		double error = -1.0;
		double deltaerr = Math.abs(deltay / deltax);
		
		int sign = deltay > 0 ? 1 : -1;
		
		
		// Draw vertical line
		if (x1 == x2) {
			for (int y = y1; y < y2; y++)
				matrix[y] = StringUtils.overlay(matrix[y], "|", x1, x1+1);
			for (int y = y1-1; y >= y2; y--)
				matrix[y] = StringUtils.overlay(matrix[y], "|", x1, x1+1);
		}
		else
		{
			int prevy = y1;
			
			int y = y1;
			for (int x = x1; x <= x2; x+=1) {
				
				// Draw vertical line if deltay exceeds 1
				for (int tmp = prevy; tmp < y-1; tmp++) 
					matrix[tmp+1] = StringUtils.overlay(matrix[tmp+1], "|", x, x+1);
				for (int tmp = prevy-1; tmp >= y; tmp--) 
					matrix[tmp+1] = StringUtils.overlay(matrix[tmp+1], "|", x, x+1);
				
				// Draw slashes or underscore if delta y is less than or equal to 1
				if (prevy > y)
					matrix[y+1] = StringUtils.overlay(matrix[y+1], "/", x, x+1);
				else if (prevy < y)
					matrix[y] = StringUtils.overlay(matrix[y], "\\", x, x+1);
				else
					matrix[y] = StringUtils.overlay(matrix[y], "_", x, x+1);
				
				// Calculate next delta y
				prevy = y;
				error = error + deltaerr;
				while (error >= 0.0) {
					y = y+sign;
					error = error - 1;
				}
			}
		}
	}
	
	/**
	 * Return a set of string representing the permits of given region
	 * @param region
	 * @return a list of strings
	 */
	private String[] printRegionPermits(Region region) {
		List<Permit> permits = region.getGainablePermits();
		
		String[] ret = new String[PERMIT_ASCII.length + 2];
		for (int i = 0; i < ret.length; i++)
			ret[i] = StringUtils.repeat(" ", 200);
		
		for (int i = 0; i < permits.size(); i++) {
			String[] permit = printPermit(permits.get(i));
			for (int j = 0; j < permit.length; j++) 
				ret[j] = StringUtils.overlay(ret[j], permit[j], i*permit[j].length(), (i+1)*permit[j].length());
		}
		return ret;
	}
	
	/**
	 * Prints a single permit
	 * @param permit
	 * @return
	 */
	public String[] printPermit(Permit permit) {
		String[] permitArray = PERMIT_ASCII.clone();
		
		int row = 1;
		for (City c : permit.getCities()) {
			permitArray[row] = StringUtils.overlay(permitArray[row], c.getTag(), 1, Math.min(c.getTag().length()+1, permitArray[row].length()-1));
			row++;
		}
		
		permitArray[row] = StringUtils.overlay(permitArray[row], permit.getBonus().toString(), 1, Math.min(permit.getBonus().toString().length()+1, permitArray[row].length()-1));
			
		permitArray[permitArray.length-1] = StringUtils.overlay(permitArray[permitArray.length-1], permit.getTag(), 1, 1 + permit.getTag().length());
		
		return permitArray;
	}
	
	/**
	 * Prints all the nobility track and returns a list of string to be printed
	 * @return
	 */
	public String[] printNobilityTrack() {
		String[] nobilityTrack = createEmptyMatrix(Board.WIDTH+60, 2);
		
		int pos = 1;
		
		for (int i = 0; i < board.getNobilityTrack().size(); i++) {
			Bonus b = board.getNobilityTrack().get(i);
			if (b != null) {
				nobilityTrack[0] = StringUtils.overlay(nobilityTrack[0], b + " | ", pos, pos+b.toString().length()+3);
				nobilityTrack[1] = StringUtils.overlay(nobilityTrack[1], StringUtils.center(Integer.toString(i), b.toString().length()) + " | ", pos, pos+b.toString().length()+3);
				pos+=b.toString().length()+3;
			}
			else {
				nobilityTrack[0] = StringUtils.overlay(nobilityTrack[0], "   | ", pos, pos+5);
				nobilityTrack[1] = StringUtils.overlay(nobilityTrack[1], StringUtils.center(Integer.toString(i), 2) + " | ", pos, pos+5);
				pos += 5;
			}
		}
		return nobilityTrack;
	}
	
	/**
	 * Prints a council in a list of String and returns it
	 * @param council
	 * @return
	 */
	public String[] printCouncil(Council council) {
		String[] ret = createEmptyMatrix(Board.WIDTH, COUNCILLOR_ASCII.length + 2);
		final int councillorWidth = 10;
		final int councillorHeight = COUNCILLOR_ASCII.length;
		int col = 1;
		
		String councilTag;
		if (council == board.getKingCouncil()) {
			councilTag = "King's council (" + council.getTag() + "):";
			ret[2] = StringUtils.overlay(ret[2], councilTag, 1, 1 + councilTag.length());
			col += councilTag.length() + 3;
		}
		
		for (Councillor counc : council.getCouncillors()) {
			
			for (int j = 0; j < councillorHeight; j++) {
				ret[j] = StringUtils.overlay(ret[j], StringUtils.center(COUNCILLOR_ASCII[j], councillorWidth), col, col+councillorWidth);
			}
			
			
			
			ret[councillorHeight] = StringUtils.overlay(ret[councillorHeight], StringUtils.center(counc.getColor().toString(), councillorWidth), col, col+councillorWidth);
			ret[councillorHeight+1] = StringUtils.overlay(ret[councillorHeight+1], StringUtils.center(counc.getTag().substring(10), councillorWidth), col, col+councillorWidth);
			col += councillorWidth;
		}
		
		return ret;
	}
	
	
	
	
	/**
	 * Prints all the available councillors in the councillor pool in a list of strings and returns it
	 * @return
	 */
	public String[] printAvailableCouncillors(){
		String[] ret = createEmptyMatrix(Board.WIDTH, COUNCILLOR_ASCII.length+2);
		final int councillorWidth = 10;
		final int councillorHeight = COUNCILLOR_ASCII.length;
		int col=1;
		
		for (Councillor counc : board.getCouncillorPool().getAvailableCouncillors()) {
			
			for (int j = 0; j < councillorHeight; j++) {
				ret[j] = StringUtils.overlay(ret[j], StringUtils.center(COUNCILLOR_ASCII[j], councillorWidth), col, col+councillorWidth);
			}
			
			
			
			ret[councillorHeight] = StringUtils.overlay(ret[councillorHeight], StringUtils.center(counc.getColor().toString(), councillorWidth), col, col+councillorWidth);
			ret[councillorHeight+1] = StringUtils.overlay(ret[councillorHeight+1], StringUtils.center(counc.getTag().substring(10), councillorWidth), col, col+councillorWidth);
			col += councillorWidth;
		}
		
		return ret;
		
	}
}