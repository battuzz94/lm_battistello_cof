package cof.model;

import java.util.*;

/**
 * The color on cards and councillors
 * @author andrea
 *
 */
public enum PoliticColor {
	PURPLE("Purple"), 
	BLACK("Black"), 
	WHITE("White"), 
	BLUE("Blue"), 
	PINK("Pink"), 
	ORANGE("Orange"), 
	MULTI("Multi");
	
	private String tag;
	private static final PoliticColor[] VALUES= values();
	private static final int SIZE=VALUES.length;
	private static final Random RANDOM=new Random();
	
	private PoliticColor(String tag){
		this.tag=tag;
	}
	
	public static PoliticColor getRandomPoliticColor(){
		PoliticColor ret = VALUES[RANDOM.nextInt(SIZE)];
		while (ret.isJolly())
			ret = VALUES[RANDOM.nextInt(SIZE)];
		return ret;
	}
	
	public static PoliticColor getRandomPoliticColorWithJolly() {
		return VALUES[RANDOM.nextInt(SIZE)];
	}
	
	@Override
	public String toString(){
		return tag.toUpperCase();
	}
	
	public boolean isJolly() {
		return this.equals(MULTI);
				
	}
}
