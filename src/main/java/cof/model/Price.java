package cof.model;

/**
 * Represent something that the player can pay or receive
 * @author andrea
 *
 */
public class Price implements Obtainable {
	private int coins;
	private int assistants;
	
	/**
	 * Constructor
	 * @param	coins 	Amount of coins to pay
	 * @param 	assistants	Amount of assistants to pay
	 */
	public Price(int coins, int assistants){
		this.coins = coins;
		this.assistants = assistants;
	}
	
	/**
	 * Take the correct amount of coins and assistants from the player
	 * @param p
	 * @throws NotEnoughCreditsException if the players hasn't enough coins or assistants to pay
	 */
	public void takeFrom(Player p) {
		if (p.getAssistants() >= assistants && p.getCoins() >= coins) {
			p.takeAssistants(assistants);
			p.takeCoins(coins);
		}
		else
			throw new NotEnoughCreditsException();
	}
	
	@Override
	public void giveTo(Player p){
		p.addAssistants(assistants);
		p.addCoins(coins);
	}


	public int getCoins() {
		return coins;
	}
	
	public int getAssistants() {
		return assistants;
	}

	/**
	 * Add another price to this
	 * @param price
	 */
	public void addPrice(Price price) {
		this.assistants += price.assistants;
		this.coins += price.coins;
	}
	
	@Override
	public String toString(){
		String price="";
		if(this.coins>0){
			price=coins+"$ ";
		}
		if(this.assistants>0)
			price+=assistants+"A ";
		if ("".equals(price))
			price = "--";
		
		return price;
	}
}
