package cof.model;

import java.util.Random;

/**
 * This enum specifies all the city colors
 * @author andrea
 *
 */
public enum CityColor {
	GOLD("Gold"), 
	SILVER("Silver"), 
	BRONZE("Bronze"), 
	IRON("Iron"),
	PURPLE("Purple");
	
	private String tag;
	private static final CityColor[] VALUES= values();
	private static final int SIZE=VALUES.length;
	private static final Random RANDOM=new Random();
	
	private CityColor(String tag){
		this.tag=tag;
	}
	
	
	public static CityColor getRandomCityColor(){
		return VALUES[RANDOM.nextInt(SIZE)];
	}
	
	@Override
	public String toString() {
		return tag.toUpperCase();
	}


	public static CityColor getRandomCityColorWithoutPurple() {
		CityColor ret = getRandomCityColor();
		while (ret == PURPLE)
			ret = getRandomCityColor();
		
		return ret;
	}
}
