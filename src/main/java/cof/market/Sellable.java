package cof.market;

import cof.model.*;

/**
 * Interface for all objects that can be sold or bought during market,
 * i.e. cards, permits and assistants. It itself implements Obtainable,
 * since those objects could be obtained also during the game.
 * @author emmeaa
 *
 */
public interface Sellable extends Obtainable {
	
	@Override
	public void giveTo(Player player);
	
	/**
	 * This method takes a sellable from player.
	 * @param player
	 */
	public void takeFrom(Player player);
	
	/**
	 * This method sets the price of a Sellable object.
	 * @param price
	 */
	public void setPrice(Price price);
	
	/**
	 * This method returns the price of a Sellable object.
	 * @return
	 */
	public Price getPrice();
	
	/**
	 * This method returns the string tag of a Sellable object.
	 * @return
	 */
	public String getTag();
}
