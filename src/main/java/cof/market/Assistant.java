package cof.market;

import cof.model.*;

/**
 * This class represents an assistant as a sellable object. It will only be used during market 
 * as assistants are an integer variable inside the Player class. This class also allows you to 
 * transform one view of the "assistant" resource to the other (by the methods takeFrom and giveTo).
 * @author emmeaa
 *
 */
public class Assistant implements Sellable {
	private Price price;
	private static final String BASE_TAG = "Assistant";
	private static int idNumber = 0;
	String tag;
	
	
	/**
	 * Creates an assistant object by assigning it an incremental tag.
	 * 
	 */
	public Assistant(){
		idNumber++;
		this.tag = BASE_TAG + idNumber;
	}
	
	
	
	/**
	 * Allows a player to give away (typically during a PreMarketSession) one of their assistants by
	 * calling the method takeAssistants(int) belonging to the class Player.
	 * @param player the player who the method takes the assistant from.
	 */
	@Override
	public void takeFrom(Player player){
		player.takeAssistants(1);
	}
	
	
	/**
	 * Allows a player to obtain (typically during a MarketSession) an assistant by
	 * calling the method addAssistants(int) belonging to the class Player.
	 * @param player the player who the method gives the assistant to.
	 */
	@Override
	public void giveTo(Player player){
		player.addAssistants(1);
	}
	
	
	/**
	 * Sets the price for the object on which is called.
	 * @param price
	 */
	@Override
	public void setPrice(Price price){
		this.price = price;
	}
	
	/**
	 * Returns the price of the object on which is called.
	 */
	@Override
	public Price getPrice(){
		return price;
	}
	
	
	/**
	 * Returns the object's tag.
	 */
	@Override
	public String getTag(){
		return tag;
	}
	
	
	
}
