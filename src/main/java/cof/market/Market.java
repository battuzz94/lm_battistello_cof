package cof.market;

import java.util.*;
import java.util.stream.Collectors;

import cof.model.Card;
import cof.model.Permit;
import cof.model.Player;
import common.Tuple2;
import server.controller.PlayerPool;

/**
 * The market is the tuple composed by a list of objects that a player can obtain and 
 * their owner (identified by their ID); it's created anew every pre-market session, 
 * and a player can both put an object (card, permit or assistant) there and buy it, 
 * taking it from the list. 
 * @author emmeaa
 *
 */
public class Market {
	private List<Tuple2<Sellable, Integer>> ownership;
	List<Player> players;
	
	
	/**
	 * Should only be called by a PreMarketSession instance. 
	 * @param pool the pool of players
	 */
	public Market(PlayerPool pool) {
		this(pool.getPlayers());
	}
	
	
	/**
	 * Should only be called for testing in the absence of a PlayerPool.
	 * @param players
	 */
	public Market(List<Player> players) {
		ownership = new ArrayList<>();
		this.players = players;
	}
	
	
	
	/**
	 * This method adds an item and its player to the market.
	 * @param item the item to add
	 * @param playerId the ID of the item's current owner
	 */
	public void addItem(Sellable item, int playerId){
		ownership.add(new Tuple2<>(item, playerId));
	}
	
	
	
	/**
	 * This method checks whether the market contains the item, then returns its owner
	 * by matching a Player to their ID.
	 * @param item
	 * @return The owner of the item
	 */
	public Player getSeller(Sellable item){
		for(Tuple2<Sellable, Integer> elem : ownership){
			if(elem.getFirst().getTag().equals(item.getTag())){
				return players.stream().filter(p -> p.getID() == elem.getSecond()).findFirst().get();
			}
		}
		throw new NoSuchElementException();
	}
	
	
	
	/**
	 * This method checks whether an item is contained in the market, and if so
	 * removes it.
	 * @param item
	 */
	public void removeItem(Sellable item){
		int i;
		for(i=0; i<ownership.size(); i++)
			if(ownership.get(i).getFirst().getTag().equals(item.getTag()))
				break;
		if (i < ownership.size())
			ownership.remove(i);
		else
			throw new NoSuchElementException();
	}
	
	
	
	public List<Sellable> getItemList(){
		return ownership.stream().map(t -> t.getFirst()).collect(Collectors.toList());
	}
	
	
	/**
	 * Checks whether the market contains the given item.
	 * @param item
	 * @return returns true if the market contains the item, false otherwise.
	 */
	public boolean contains(Sellable item){
		for(Tuple2<Sellable, Integer> elem : ownership){
			Sellable s = elem.getFirst();
			if(s.getTag().equals(item.getTag()))
				return true;
		}
		return false;
	}
	
	
	
	/**
	 * This method returns an item by matching it to the given tag.
	 * @param tag
	 * @return if the market contains the item, then it returns the item itself, else 
	 * the method throws an exception
	 */
	public Sellable getByTag(String tag){
		for(Tuple2<Sellable, Integer> elem : ownership){
			Sellable s=elem.getFirst();
			if(s.getTag().equals(tag))
				return s;
		}
		throw new NoSuchElementException();
	}
	
	/**
	 * This method must be called only at the end of the market session;
	 * it returns all items still contained in the market to their owner.
	 */
	public void giveBack(){
		for(Tuple2<Sellable, Integer> elem : ownership){
			Sellable item= elem.getFirst();
			Player owner=getSeller(item);
			
			owner.obtain(item);
		}
	}
	
	
	/**
	 * Gets the real reference of the serialized Sellable passed over the network. 
	 * If the player has no such object to sell, the method throws a NoSuchElementException.
	 * @param reference the reference of the sellable object deserialized
	 * @param player the real player
	 * @throws NoSuchElementException if no such reference is present
	 * @return A reference of the sellable relative to the real player
	 */
	public static Sellable getRealReference(Sellable reference, Player player) {
		if (reference instanceof Assistant && player.getAssistants()>0)
			return reference;
		else if (reference instanceof Permit)
			return player.getPermits().stream()
					.filter(p -> p.getTag().equals(((Permit)reference).getTag()))
					.findAny()
					.get();
		else if (reference instanceof Card) 
			return player.getCards().stream()
					.filter(c -> c.getTag().equals(((Card)reference).getTag()))
					.findAny()
					.get();
		throw new NoSuchElementException();
	}
}
