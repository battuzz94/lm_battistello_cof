package cof.market;

import com.google.gson.TypeAdapterFactory;

import cof.model.Card;
import cof.model.Permit;
import common.RuntimeTypeAdapterFactory;


/**
 * This method aids in making the market serializable by Gson.
 * @author emmeaa
 *
 */
public class MarketTypeAdapter {
	public static TypeAdapterFactory getMarketAdapter() {
		RuntimeTypeAdapterFactory<Sellable> factory = RuntimeTypeAdapterFactory.of(Sellable.class, "type");
		
		factory.registerSubtype(Assistant.class, "assistant");
		factory.registerSubtype(Card.class, "card");
		factory.registerSubtype(Permit.class, "permit");
		
		return factory;
	}
}
