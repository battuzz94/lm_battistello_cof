package cof.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.bonus.GainTwoCityRewardBonus;
import cof.model.Board;
import cof.model.City;
import cof.model.Player;

/**
 * Nobility Action: lets the user choose two cities in which he built and take their bonuses.
 * This is related to GainTwoCityRewardBonus.
 * @author Andrea
 *
 */
public class GainTwoCityRewardCommand implements NobilityTrackAction {
	private static final Logger LOGGER = Logger.getLogger(GainTwoCityRewardCommand.class.getName());
	private String cityTag1;
	private String cityTag2;
	
	/**
	 * Constructs a command with specified object
	 * @param cityChosen1 first city chosen
	 * @param cityChosen2 second city chosen
	 */
	public GainTwoCityRewardCommand(City cityChosen1, City cityChosen2) {
		this.cityTag1 = cityChosen1.getTag();
		this.cityTag2 = cityChosen2.getTag();
	}
	
	/**
	 * Constructs a command with specified string tags
	 * @param cityTag1 tag of first city chosen
	 * @param cityTag2 tag of second city chosen
	 */
	public GainTwoCityRewardCommand(String cityTag1, String cityTag2) {
		this.cityTag1 = cityTag1;
		this.cityTag2 = cityTag2;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if (!isValid(player, board, state))
			throw new CommandNotValidException();
		try {
			City city1 = board.getCityByTag(cityTag1);
			Bonus bonus1 = city1.getBonus();
			
			City city2 = board.getCityByTag(cityTag2);
			Bonus bonus2 = city2.getBonus();
			
			state.pushBonus(bonus1);
			state.pushBonus(bonus2);
			state.pollDynamicBonus(GainTwoCityRewardBonus.class.getName());
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			throw new CommandNotValidException();
		}
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			City city1 = board.getCityByTag(cityTag1);
			Bonus bonus1 = city1.getBonus();
			
			City city2 = board.getCityByTag(cityTag2);
			Bonus bonus2 = city2.getBonus();
			
			// the player must have built an emporium in that city and there is a bonus to be retrieved in the state
			if (!player.hasBuiltIn(city1) || !player.hasBuiltIn(city2) || cityTag1.equals(cityTag2) || !state.canPollDynamicBonus(GainTwoCityRewardBonus.class.getName()))
				return false;
			
			// The bonus can't grant any nobility bonus
			return bonusNotNobility(bonus1) && bonusNotNobility(bonus2);
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);	
			return false;
		}
	}
	
	private boolean bonusNotNobility(Bonus b) {
		return !(b instanceof BasicBonus && ((BasicBonus)b).isNobility()); 
		
	}
	
	@Override
	public String toString() {
		return "Gain the reward of " + cityTag1 + " and " + cityTag2;
	}
}
