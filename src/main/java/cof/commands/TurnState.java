package cof.commands;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

import cof.bonus.Bonus;
import cof.bonus.DynamicBonus;
import cof.bonus.StaticBonus;

/**
 * Representation of the current turn of a generic Player. This class stores all the 
 * bonuses that the player must obtain and checks whether a main or quick action can be 
 * done.
 * This stores also the current market status for the player.
 * @author Andrea
 *
 */
public class TurnState {
	public static final int INITIAL_MAIN_ACTIONS = 1;
	public static final int INITIAL_QUICK_ACTIONS = 1;
	
	private int mainActions;
	private int quickActions;
	
	private Queue<StaticBonus> staticBonuses;
	private Queue<DynamicBonus> dynamicBonuses;
	
	private boolean hasFinishedPreMarket=false;
	
	/**
	 * Default constructor: sets the main and quick actions to default value
	 */
	public TurnState() {
		mainActions = INITIAL_MAIN_ACTIONS;
		quickActions = INITIAL_QUICK_ACTIONS;
		
		staticBonuses = new LinkedList<>();
		dynamicBonuses = new LinkedList<>();
	}
	
	/**
	 * Checks if player has finished preMarket
	 * @return
	 */
	public boolean hasFinishedPreMarket() {
		return hasFinishedPreMarket;
	}

	public void setHasFinishedPreMarket(boolean hasFinishedPreMarket) {
		this.hasFinishedPreMarket = hasFinishedPreMarket;
	}
	
	/**
	 * Resets the flag of pre-market
	 */
	public void resetHasFinishedPreMarket(){
		hasFinishedPreMarket=false;
	}
	
	
	public Queue<DynamicBonus> getDynamicBonuses(){
		return dynamicBonuses;
	}
	
	protected void pushStaticBonus(StaticBonus bonus) {
		staticBonuses.add(bonus);
	}
	
	protected void pushDynamicBonus(DynamicBonus bonus) {
		dynamicBonuses.add(bonus);
	}
	
	protected StaticBonus pollStaticBonus() {
		return Optional.ofNullable(staticBonuses.poll()).get();
	}
	
	protected DynamicBonus pollDynamicBonus() {
		return Optional.ofNullable(dynamicBonuses.poll()).get();
	}
	
	protected void pushBonus(Bonus bonus) {
		if (bonus instanceof StaticBonus)
			pushStaticBonus((StaticBonus)bonus);
		else if (bonus instanceof DynamicBonus)
			pushDynamicBonus((DynamicBonus) bonus);
	}
	
	protected void pushBonuses(List<Bonus> bonuses) {
		bonuses.forEach(this::pushBonus);
	}
	
	/**
	 * Checks if the turn is finished
	 * @return
	 */
	public boolean turnFinished() {
		return mainActions == 0;
	}
	
	/**
	 * Checks if the player can do main actions
	 * @return
	 */
	public boolean canDoMainAction() {
		return mainActions >= 1;
	}
	
	/**
	 * Checks if the player can do quick actions
	 * @return
	 */
	public boolean canDoQuickAction() {
		return quickActions >= 1;
	}
	
	/**
	 * Decrease the count of available main actions
	 * @throws CommandNotValidException if no main action can be done
	 */
	protected void doMainAction() {
		if (mainActions >=  1)
			mainActions--;
		else
			throw new CommandNotValidException();
	}
	
	/**
	 * Decrease the count of available quick actions
	 * @throws CommandNotValidException if no quick action can be done
	 */
	protected void doQuickAction() {
		if (quickActions >= 1)
			quickActions--;
		else
			throw new CommandNotValidException();
	}
	
	protected void addMainAction() {
		mainActions++;
	}
	
	protected void addQuickAction() {
		quickActions++;
	}
	
	/**
	 * Checks if the player can poll static bonus
	 * @return
	 */
	public boolean canPollStaticBonus() {
		return !staticBonuses.isEmpty();
	}
	
	/**
	 * Check if player can poll dynamic bonus (in general)
	 * @return
	 */
	public boolean canPollDynamicBonus() {
		return !dynamicBonuses.isEmpty();
	}
	
	/**
	 * Polls a specific dynamic bonus
	 * @param bonusClassName the class name of dynamic bonus
	 * @return
	 */
	@SuppressWarnings("squid:S1872")
	public void pollDynamicBonus(String bonusClassName) {
		if (dynamicBonuses.peek().getClass().getName().equals(bonusClassName)) {
			dynamicBonuses.poll();
		}
		else 
			throw new CommandNotValidException();
	}
	
	/**
	 * Check if player can poll a specific dynamic bonus
	 * @param name the class name of dynamic bonus
	 * @return
	 */
	public boolean canPollDynamicBonus(String name) {
		return dynamicBonuses.stream()
				.map(b -> b.getClass().getName())
				.filter(s -> s.equals(name))
				.findFirst()
				.isPresent();
	}
	
	/**
	 * Resents the count of main and quick actions to their defaults.
	 */
	public void reset() {
		this.mainActions = INITIAL_MAIN_ACTIONS;
		this.quickActions = INITIAL_QUICK_ACTIONS;
	}
	
	@Override
	public String toString() {
		return "Main actions: " + mainActions + " Quick actions: " + quickActions + "\n" +
				"Static bonuses: " + staticBonuses + "\n" +
				"Dynamic bonuses: " + dynamicBonuses;
	}
}
