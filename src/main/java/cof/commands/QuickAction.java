package cof.commands;

import cof.model.Board;
import cof.model.Player;

/**
 * Quick action abstraction.
 * @author Andrea
 *
 */
public interface QuickAction extends Command{
	@Override
	public void execute(Player player, Board board, TurnState state);
}
