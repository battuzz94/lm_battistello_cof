package cof.commands;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import cof.model.Board;
import cof.model.Card;
import cof.model.City;
import cof.model.Council;
import cof.model.Player;
import cof.model.Price;

/**
 * Main action: bribe king's council with cards, move the king in a specified city and
 * build in that city.
 * @author Andrea
 *
 */
public class BribeKingCommand implements MainAction {
	private static final Logger LOGGER = Logger.getLogger(BribeKingCommand.class.getName());
	public static final int COST_PER_STEP = 2;
	private String kingDestinationTag;
	private List<String> cardTags;
	
	/**
	 * Creates a new BribeKingCommand with specified objects
	 * @param cards
	 * @param kingDestination
	 */
	public BribeKingCommand(List<Card> cards, City kingDestination) {
		cardTags = cards.stream().map(c -> c.getTag()).collect(Collectors.toList());
		this.kingDestinationTag = kingDestination.getTag();
	}
	
	/**
	 * Creates a new BribeKingCommand with given string tags
	 * @param cardTags
	 * @param kingDestinationTag
	 */
	public BribeKingCommand(List<String> cardTags, String kingDestinationTag){
		this.kingDestinationTag=kingDestinationTag;
		this.cardTags=cardTags;
	}
	
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		
		try{
			Council kingCouncil = board.getKingCouncil();
			List<Card> cards = getCardsOfPlayer(player);
			City currentKingPosition = board.getKingPosition();
			City kingDestination = board.getCityByTag(kingDestinationTag);
			
			
			Price bribeCost= kingCouncil.getSatisfiabilityPrice(cards.toArray(new Card[cards.size()]));
			int dist=currentKingPosition.getDistanceTo(kingDestination);
			int kingMoveCost=dist*COST_PER_STEP;
			
			int buildEmporiaCost = kingDestination.getEmporiaCount();
			
			player.pay(bribeCost);
			player.pay(new Price(kingMoveCost, 0));
			player.pay(new Price(0, buildEmporiaCost));
			
			for (Card card : cards)
				player.takeCard(card);
			
			board.moveKing(kingDestination);
			player.buildEmporium(board.getKingPosition());
			state.pushBonuses(kingDestination.getBonusNearby(player));
			
			
			if (board.hasKingBonus() && (BuildEmporiumCommand.checkBuildRegion(player, board, state) || BuildEmporiumCommand.checkColoredCityBonus(player, board, state))) {
				state.pushBonus(board.pollKingBonus());
			}
			
			
			state.doMainAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
		
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			Council kingCouncil = board.getKingCouncil();
			Card[] cards = getCardsOfPlayer(player).toArray(new Card[getCardsOfPlayer(player).size()]);
			City currentKingPosition = board.getKingPosition();
			City kingDestination = board.getCityByTag(kingDestinationTag);
			
			
			Price bribeCost= kingCouncil.getSatisfiabilityPrice(cards);
			int dist=currentKingPosition.getDistanceTo(kingDestination);
			int kingMoveCost=dist*COST_PER_STEP;
			
			int buildEmporiaCost = kingDestination.getEmporiaCount();
			
			Price totalPrice = new Price(kingMoveCost, buildEmporiaCost);
			totalPrice.addPrice(bribeCost);
			
			boolean cardsCorrespondToCouncil=kingCouncil.cardsCorrespond(cards);
			
			return player.canAfford(totalPrice) && state.canDoMainAction() && cardsCorrespondToCouncil && !player.hasBuiltIn(kingDestination);
		}
		catch(Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
	}
	
	private List<Card> getCardsOfPlayer(Player player) {
		return cardTags.stream()
				.map(s -> player.getCardByTag(s))
				.collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return "Bribe king's council with " + cardTags + " and build in " + kingDestinationTag;
	}

}
