package cof.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import cof.model.Board;
import cof.model.Council;
import cof.model.Councillor;
import cof.model.Player;
import cof.model.Region;

/**
 * Generates a list of possible QuickAction for a given state.
 * All the Commands returned by the iterator will be filtered according to the flags
 * set by the generator.
 * @author Andrea
 *
 */
public class QuickActionGenerator implements Iterable<Command> {
	Player player;
	Board board;
	TurnState state;
	private boolean shuffleFilter = false;
	private boolean electFilter = false;
	private boolean validFilter = true;
	private boolean addPrimaryMoveFilter;
	
	/**
	 * Creates a new QuickActionGenerator for the given state
	 * @param player
	 * @param board
	 * @param state
	 */
	public QuickActionGenerator(Player player, Board board, TurnState state) {
		this.player = player;
		this.board = board;
		this.state = state;
	}
	
	/**
	 * If true, the iterator will ignore all the ShufflePermits Commands
	 * This flag is false by default.
	 * @param shuffleFilter
	 */
	public void setShufflePermitFilter(boolean shuffleFilter) {
		this.shuffleFilter  = shuffleFilter;
	}
	
	/**
	 * If true, the iterator will ignore all the ElectWithAssistant Commands.
	 * This flag is false by default.
	 * @param electFilter
	 */
	public void setElectWithAssistantFilter(boolean electFilter) {
		this.electFilter   = electFilter;
	}
	
	/**
	 * If true, the iterator will ignore all the non-valid Commands according to the current
	 * state.
	 * This flag is true by default.
	 * @param validFilter
	 */
	public void setValidCommandFilter(boolean validFilter) {
		this.validFilter  = validFilter;
	}
	
	/**
	 * If true, the iterator will ignore the AddPrimaryMove Command.
	 * This flag is set to false by default.
	 * @param addMoveFilter
	 */
	public void setAddPrimaryMoveFilter(boolean addMoveFilter) {
		this.addPrimaryMoveFilter = addMoveFilter;
	}
	
	/**
	 * Returns an iterator of all commands matching the filters set previously.
	 */
	@Override
	public Iterator<Command> iterator() {
		if (validFilter) 
			return new ValidCommandFilterIterator(new QuickActionIterator(shuffleFilter, electFilter), player, board, state);
		else
			return new QuickActionIterator(shuffleFilter, electFilter);
	}
	
	
	
	class QuickActionIterator implements Iterator<Command> {

		List<Iterator<Command>> iterators;
		Iterator<Iterator<Command>> mainIterator;
		Iterator<Command> currentIterator;
		
		private QuickActionIterator(boolean shuffleFilter, boolean electFilter) {
			List<Command> alwaysValidCommands = new ArrayList<>();
			if (!addPrimaryMoveFilter)
				alwaysValidCommands.add(new AddPrimaryMoveCommand());
			
			alwaysValidCommands.add(new EngageAssistantCommand());
			
			iterators = new ArrayList<>();
			iterators.add(alwaysValidCommands.iterator());
			
			if (!electFilter)
				iterators.add(new ElectWithAssistantIterator());
			
			if (!shuffleFilter)
				iterators.add(new ShufflePermitsIterator());
			
			// Removes iterators with no possible actions
			iterators = iterators.stream().filter(it -> it.hasNext()).collect(Collectors.toList());
			
			mainIterator = iterators.iterator();
			currentIterator = mainIterator.next();
		}
		
		@Override
		public boolean hasNext() {
			return currentIterator.hasNext() || mainIterator.hasNext();
		}
		
		/**
		 * Returns the next main action generated
		 * @throws NoSuchElementException if no such element is found
		 */
		@Override
		public Command next() {
			if (currentIterator.hasNext())
				return currentIterator.next();
			else {
				currentIterator = mainIterator.next();
				while (!currentIterator.hasNext())
					currentIterator = mainIterator.next();
				return currentIterator.next();
			}
		}
		
	}
	
	class ElectWithAssistantIterator implements Iterator<Command> {
		int councillorId;
		int poolCount;
		int councilId;
		List<Councillor> availableCouncillors;
		List<Council> availableCouncils;
		
		private ElectWithAssistantIterator () {
			councillorId = -1;
			councilId = 0;
			availableCouncils = board.getRegions().stream().map(reg -> reg.getRegionCouncil()).collect(Collectors.toList());
			availableCouncils.add(board.getKingCouncil());
			availableCouncillors = board.getCouncillorPool().getAvailableCouncillors();
		}
		
		@Override
		public boolean hasNext() {
			return ( councilId < availableCouncils.size() - 1 ) || 
					( councilId <availableCouncils.size() 
							&& councillorId < availableCouncillors.size() - 1); 
		}

		@Override
		public Command next() {
			if (!hasNext())
				throw new NoSuchElementException();
			
			councillorId++;
			if (councillorId == availableCouncillors.size()) {
				councillorId = 0;
				councilId++;
			}
			return new ElectWithAssistantCommand(availableCouncils.get(councilId), availableCouncillors.get(councillorId));
		}
	}
	
	class ShufflePermitsIterator implements Iterator<Command> {
		int regionId = -1;
		List<Region> regions = board.getRegions();
		
		@Override
		public boolean hasNext() {
			return regionId < regions.size() - 1;
		}

		@Override
		public Command next() {
			regionId++;
			if (regionId < regions.size()) {
				return new ShufflePermitsCommand(regions.get(regionId));
			}
			throw new NoSuchElementException();
		}
		
	}

	

}
