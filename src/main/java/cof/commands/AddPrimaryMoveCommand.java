package cof.commands;

import cof.model.Board;
import cof.model.Player;
import cof.model.Price;

/**
 * Quick action: adds a primary move to the current TurnState
 * @author Andrea
 *
 */
public class AddPrimaryMoveCommand implements QuickAction{
	public static final int ASSISTANT_COST = 3;
	private static final Price PRIMARY_MOVE_COST = new Price(0, ASSISTANT_COST);
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if (!isValid(player, board, state))
			throw new CommandNotValidException();
		try{
			player.pay(PRIMARY_MOVE_COST);
			state.doQuickAction();
			state.addMainAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		return player.canAfford(PRIMARY_MOVE_COST) && state.canDoQuickAction();
	}
	
	@Override
	public String toString() {
		return "Add primary move";
	}
}
