package cof.commands;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.model.*;

/**
 * Main action: choose a Councillor to be added in a specified Council.
 * @author Andrea
 *
 */
public class ElectCouncillorCommand implements MainAction {
	private static final Logger LOGGER = Logger.getLogger(ElectCouncillorCommand.class.getName());
	private String councillorToBeElectedTag;
	private String councilTag;
	public static final int COINS = 4;
	public static final Bonus REWARD = new BasicBonus(0, COINS, 0, 0, 0);  // 4 Coins
	
	
	/**
	 * Set Council and Councilor to be elected.
	 * @param council this is going to be the council of region selected.
	 * @param councillorToBeElected this is the councilor selected by the player.
	 */
	public ElectCouncillorCommand(Council council, Councillor councillorToBeElected) {
		this.councillorToBeElectedTag = councillorToBeElected.getTag();
		this.councilTag = council.getTag();
	}
	
	/**
	 * Builds a new ElectCouncillorCommand with given string tags
	 * @param councilTag
	 * @param councillorTag
	 */
	public ElectCouncillorCommand(String councilTag, String councillorTag) {
		this.councillorToBeElectedTag = councillorTag;
		this.councilTag = councilTag;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		try{
			Councillor realCouncillor = board.getCouncillorByTag(councillorToBeElectedTag);
			
			Council realCouncil = board.getCouncilByTag(councilTag);
			realCouncil.pushCouncillor(realCouncillor);
			state.pushBonus(REWARD);
			
			state.doMainAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			board.getCouncillorByTag(councillorToBeElectedTag);
			
			board.getCouncilByTag(councilTag);
			
			board.getCouncillorPool().getAvailableCouncillorByTag(councillorToBeElectedTag);
			
			return state.canDoMainAction();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
		
	}
	
	@Override
	public String toString() {
		return "Elect Councillor " + councillorToBeElectedTag + " in " + councilTag;
	}

}
