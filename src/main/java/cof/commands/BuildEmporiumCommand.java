package cof.commands;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.ColoredCityBonus;
import cof.model.Board;
import cof.model.City;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Price;
import cof.model.Region;

/**
 * Main action: choose a valid permit and build an emporium in one of the cities listed 
 * in that permit.
 * @author Andrea
 *
 */
public class BuildEmporiumCommand implements MainAction {
	private static final Logger LOGGER = Logger.getLogger(BuildEmporiumCommand.class.getName());
	private String permitChosenTag;
	private String cityChosenTag;
	
	/**
	 * Creates a new BuildEmporiumCommand with specified objects
	 * @param permitChosen
	 * @param cityToBuild
	 */
	public BuildEmporiumCommand(Permit permitChosen, City cityToBuild) {
		this.permitChosenTag = permitChosen.getTag();
		this.cityChosenTag = cityToBuild.getTag();
	}
	
	/**
	 * Creates a new BuildEmporiumCommand with specified String tags
	 * @param permitTag
	 * @param cityTag
	 */
	public BuildEmporiumCommand(String permitTag, String cityTag) {
		this.permitChosenTag = permitTag;
		this.cityChosenTag = cityTag;
	}
	
	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		boolean valid = false;
		try {
			Permit permitChosen = player.getPermitByTag(permitChosenTag);
			City cityChosen = board.getCityByTag(cityChosenTag);
			if(!permitChosen.isUsed() && permitChosen.canBuildIn(cityChosen)) {
				Price buildPrice = new Price(0, cityChosen.getEmporiaCount());
				if (player.canAfford(buildPrice) && !player.hasBuiltIn(cityChosen))
					valid = true;
			}
			
			return valid && state.canDoMainAction();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
			
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		
		if (!isValid(player, board, state))
			throw new CommandNotValidException();
		
		try {
			Permit permitChosen = player.getPermitByTag(permitChosenTag);
			City cityChosen = board.getCityByTag(cityChosenTag);
			
			Price buildPrice = new Price(0, cityChosen.getEmporiaCount());
			player.pay(buildPrice);
			player.buildEmporium(cityChosen);
			permitChosen.setUsed();
			state.pushBonuses(cityChosen.getBonusNearby(player));
			
			
			if (board.hasKingBonus() && (checkBuildRegion(player, board, state) || checkColoredCityBonus(player, board, state))) {
				state.pushBonus(board.pollKingBonus());
			}
			
			
			
			state.doMainAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
	}
	
	
	
	protected static boolean checkBuildRegion(Player player, Board board, TurnState state) {
		boolean bonusTaken = false;
		for (Region reg : board.getRegions()) {
			int regionBuilded = (int) reg.getCities().stream().filter(c -> player.hasBuiltIn(c)).count();
			if (reg.bonusIsAvailable() && regionBuilded == reg.getCities().size()) {
				state.pushBonus(reg.getBonus());
				reg.setBonusUsed();
				bonusTaken = true;
			}
		}
		return bonusTaken;
	}
	
	protected static boolean checkColoredCityBonus(Player player, Board board, TurnState state) {
		List<City> cities = board.getCities();
		for (ColoredCityBonus bonus : board.getCityBonus()) {
			if (!bonus.isAssigned()) {
				boolean takeBonus = cities.stream()
							.filter(x -> x.getColor() == bonus.getColor())
							.allMatch(c -> player.hasBuiltIn(c));
				if (takeBonus) {
					state.pushBonus(bonus.getBonus());
					bonus.bonusNowAssigned();
					return true;
				}
			}
			
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Build emporium in " + cityChosenTag + " with permit: " + permitChosenTag;
	}
}
