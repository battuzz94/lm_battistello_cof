package cof.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.model.*;

/**
 * Main action: bribe a region council with some cards and take one permit
 * @author Andrea
 *
 */
public class BribeCouncilCommand implements MainAction {
	private static final Logger LOGGER = Logger.getLogger(BribeCouncilCommand.class.getName());
	private List<String> cardTags;
	private String councilTag;
	private String permitTag;
	
	/**
	 * Creates a new BribeCOuncilCommand with given objects
	 * @param cards
	 * @param council
	 * @param permit
	 */
	public BribeCouncilCommand(List<Card> cards, Council council, Permit permit) {
		this.cardTags = new ArrayList<>();
		for (int i = 0; i < cards.size(); i++)
			this.cardTags.add(cards.get(i).getTag());
		
		this.councilTag = council.getTag();
		if (permit != null)
			this.permitTag = permit.getTag();
		else
			permitTag = "NO_TAG";
	}
	
	/**
	 * Creates a bew BribeCouncilCommand based on the given string tags
	 * @param cardTags
	 * @param councilTag
	 * @param permitTag
	 */
	public BribeCouncilCommand(List<String> cardTags, String councilTag, String permitTag) {
		this.cardTags = new ArrayList<>(cardTags);
		this.councilTag = councilTag;
		this.permitTag = permitTag;
	}

	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		
		try{
			Council council = board.getCouncilByTag(councilTag);
			Card[] cards = new Card[cardTags.size()];
			Permit permit = board.getPermitByTag(permitTag);
			for (int i = 0; i < cards.length; i++) {
				cards[i] = player.getCardByTag(cardTags.get(i));
			}
			
			Price price= council.getSatisfiabilityPrice(cards);
			player.pay(price);
			player.obtain(permit);
			
			for (Region r : board.getRegions())
				if (r.getGainablePermitByTag(permitTag) != null)
					r.takePermitByTag(permitTag);
			
			state.pushBonus(permit.getBonus());
			
			for (Card c : cards)
				player.takeCard(c);
			state.doMainAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
		
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			Council council = board.getCouncilByTag(councilTag);
			Card[] cards = new Card[cardTags.size()];
			Permit permit = board.getPermitByTag(permitTag);
			
			boolean permitAndCouncilSameRegion = permitCouncilSameRegion(board, council, permit);
			
			for (int i = 0; i < cards.length; i++)
				cards[i] = player.getCardByTag(cardTags.get(i));
			
			boolean cardsCorrespondToCouncil=council.cardsCorrespond(cards);
			
			Price price= council.getSatisfiabilityPrice(cards);
			
			return permitAndCouncilSameRegion && player.canAfford(price) && state.canDoMainAction() && cardsCorrespondToCouncil;
		}
		catch(Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
	}

	private boolean permitCouncilSameRegion(Board board, Council council, Permit permit) {
		boolean permitAndCouncilSameRegion = false;
		for (Region r : board.getRegions()) {
			if (r.getGainablePermits().contains(permit) && r.getRegionCouncil().getTag().equals(council.getTag()))
				permitAndCouncilSameRegion = true;
		}
		return permitAndCouncilSameRegion;
	}
	
	@Override
	public String toString() {
		return "Bribe council " + councilTag + " with cards: " + cardTags + " and take: " + permitTag;
	}

}
