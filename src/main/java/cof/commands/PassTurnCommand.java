package cof.commands;

import cof.model.Board;
import cof.model.Player;

/**
 * This command checks if pass turn is valid for the given state.
 * @author Andrea
 *
 */
public class PassTurnCommand implements Command {

	@Override
	public void execute(Player player, Board board, TurnState state) {
		throw new CommandNotValidException();
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		boolean valid=false;
		if(state.turnFinished())
			valid=true;
		return valid;
	}
	
	@Override
	public String toString() {
		return "Pass turn";
	}
}
