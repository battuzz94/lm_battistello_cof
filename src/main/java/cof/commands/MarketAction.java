package cof.commands;

import cof.market.Market;
import cof.model.*;
/**
 * Market commands abstraction.
 * @author Andrea
 *
 */
public interface MarketAction extends Command {
	  @Override
	  public void execute(Player player, Board board, TurnState state);
	  
	  /**
	   * Adds a reference to the market used during the command execution
	   * @param market
	   */
	  public void setMarket(Market market);
}
