package cof.commands;

import java.util.List;

import cof.bonus.Bonus;
import cof.bonus.StaticBonus;
import cof.model.Board;
import cof.model.Player;

/**
 * Generic Command: when this command is executed, one StaticBonus will be popped out
 * from the TurnState stack and be obtained by the player.
 * @author Andrea
 *
 */
public class PollBonusCommand implements Command {

	private boolean drawCard = true;

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		if (state.canPollStaticBonus())
			return true;
		return false;
	}

	@Override
	public void execute(Player player, Board board, TurnState state) {
		if (isValid(player, board, state)) {
			StaticBonus b = state.pollStaticBonus();
			
			if (b.hasAdditionalMainAction())
				state.addMainAction();
			
			boolean giveCards = b.getDrawCards();
			b.setCards(drawCard);
			
			int prevNobility = player.getNobility();
			player.obtain(b);
			int postNobility = player.getNobility();
			
			player.setMaxNobility(board.getNobilityTrack().size()-1);
			
			b.setCards(giveCards);
			
			List<Bonus> nobilityTrack = board.getNobilityTrack();
			
			for (int n = prevNobility+1; n <= postNobility && n < board.getNobilityTrack().size(); n++) {
				Bonus bonus = nobilityTrack.get(n);
				if (bonus != null) {
					state.pushBonus(bonus);
				}
			}
			
		}
		else
			throw new CommandNotValidException();
	}

	public void setDrawCard(boolean drawCard) {
		this.drawCard  = drawCard;
	}
	
	@Override
	public String toString() {
		return "Poll bonus";
	}
	
}
