package cof.commands;

import cof.market.Market;
import cof.market.Sellable;
import cof.model.Board;
import cof.model.Player;

/**
 * Market action: lets the user buy one of the items available in the market
 * @author Andrea
 *
 */
public class BuyItemCommand implements MarketAction{
	Sellable itemToBuy;
	Market market;
	
	/**
	 * Constructs a new BuyItemCommand
	 * @param item
	 */
	public BuyItemCommand(Sellable item){
		itemToBuy=item;
	}
	
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		
		try{
			Player seller=market.getSeller(itemToBuy);
			seller.obtain(itemToBuy.getPrice());
			player.pay(itemToBuy.getPrice());
			player.obtain(itemToBuy);
			market.removeItem(itemToBuy);
			
		} catch(Exception e){
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
		
	}
	
	@Override
	public void setMarket(Market market){
		this.market=market;
	}
	
	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		if(market==null)
			return false;
		
		return player.canAfford(itemToBuy.getPrice()) && market.contains(itemToBuy);
	}
	
	@Override
	public String toString() {
		return "Buy " + itemToBuy.getTag() + " for: " + itemToBuy.getPrice();
	}
}
