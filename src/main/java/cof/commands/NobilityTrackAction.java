package cof.commands;

/**
 * Abstraction of all commands needed to retrieve a DynamicBonus of NobilityTrack.
 * @author Andrea
 *
 */
public interface NobilityTrackAction extends Command {

}
