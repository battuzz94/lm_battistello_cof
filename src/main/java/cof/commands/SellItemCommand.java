package cof.commands;

import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.market.Market;
import cof.market.Sellable;
import cof.model.Board;
import cof.model.Player;

/**
 * Used in the pre-market stage, this command allows a player to choose what to sell and at what price
 * @author emmeaa
 *
 */
public class SellItemCommand implements MarketAction{
	private static final Logger LOGGER = Logger.getLogger(SellItemCommand.class.getName());
	Sellable toBeSold;
	Market market;
	
	
	/**
	 * Create a new sell item command to be sent to the server
	 * @param toBeSold is a sellable, hence either an assistant, a politic card or a permit tile. Already has the price attached
	 * @param m is a new market in each new market turn
	 */
	public SellItemCommand(Sellable toBeSold) {
		this.toBeSold=toBeSold;
		this.market=null;
	}
	
	@Override
	public void setMarket(Market m){
		this.market=m;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		try{
			Sellable realReference = Market.getRealReference(toBeSold, player);
			realReference.setPrice(toBeSold.getPrice());
			market.addItem(realReference, player.getID());
			realReference.takeFrom(player);
			
		} catch(Exception e){
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
	}

	
	

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		if (market == null)
			return false;
		
		try {
			Market.getRealReference(toBeSold, player);
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINEST, "The reference of " + toBeSold + "was not found", e);
			return false;
		}
		
		return true;
	}

}
