package cof.commands;

/**
 * Exception thrown before or during the execution of a command if it is invalid
 * @author andrea
 *
 */
public class CommandNotValidException extends RuntimeException {
	
	private static final long serialVersionUID = 8929396307038187596L;
	
	static final String DESCRIPTION = "This command is not valid";
	final String reason;
	
	/**
	 * Default constructor
	 */
	public CommandNotValidException() {
		this("");
	}
	
	/**
	 * Constructs a CommandNotValidException with specified reason
	 * @param reason
	 */
	public CommandNotValidException(String reason) {
		this.reason = reason;
	}
}
