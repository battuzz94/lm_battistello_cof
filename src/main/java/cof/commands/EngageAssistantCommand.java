package cof.commands;

import cof.market.Assistant;
import cof.model.Board;
import cof.model.Player;
import cof.model.Price;

/**
 * Quick action: pay some coins and gain one assitant.
 * @author Andrea
 *
 */
public class EngageAssistantCommand implements QuickAction{
	public static final int ASSISTANT_COST = 3;

	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		
		try{
			player.pay(new Price(ASSISTANT_COST, 0));
			player.obtain(new Assistant());
			state.doQuickAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		return player.canAfford(new Price(ASSISTANT_COST, 0))&& state.canDoQuickAction();
	}
	
	@Override
	public String toString() {
		return "Engage assistant";
	}
}
