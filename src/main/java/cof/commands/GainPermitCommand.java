package cof.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.GainPermitBonus;
import cof.model.Board;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Region;

/**
 * Nobility Action: lets the user choose one of the gainable permits and take it.
 * This Command corresponds to GainPermitBonus
 * @author Andrea
 *
 */
public class GainPermitCommand implements NobilityTrackAction {
	private static final Logger LOGGER = Logger.getLogger(GainPermitCommand.class.getName());
	private String permitTag;
	
	/**
	 * Constructs a command with specified object
	 * @param permitChosen
	 */
	public GainPermitCommand(Permit permitChosen) {
		this.permitTag = permitChosen.getTag();
	}
	
	/**
	 * Constructs a command with specified string tag
	 * @param permitTag
	 */
	public GainPermitCommand(String permitTag) {
		this.permitTag = permitTag;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if (!isValid(player, board, state))
			throw new CommandNotValidException();
		try {
			Permit permit = board.getPermitByTag(permitTag);
			player.obtain(permit);
			state.pushBonus(permit.getBonus());
			
			for (Region r : board.getRegions())
				if (r.getGainablePermitByTag(permitTag) != null) {
					r.takePermitByTag(permitTag);
				}
			
			state.pollDynamicBonus(GainPermitBonus.class.getName());
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			throw new CommandNotValidException();
		}
	}
	
	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			board.getPermitByTag(permitTag);
			
			return state.canPollDynamicBonus(GainPermitBonus.class.getName());
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "Gain " + permitTag;
	}
}
