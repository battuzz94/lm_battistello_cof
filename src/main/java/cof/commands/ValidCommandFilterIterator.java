package cof.commands;

import java.util.Iterator;
import java.util.NoSuchElementException;

import cof.model.Board;
import cof.model.Player;

/**
 * Filters all the Commands of a given iterator and returns only the valid ones.
 * @author Andrea
 *
 */
public class ValidCommandFilterIterator implements Iterator<Command> {
	Iterator<Command> generator;
	Command currentCommand;
	Command nextCommand;
	boolean advanced = false;
	
	Player player;
	Board board;
	TurnState state;
	
	/**
	 * Creates a new Valid Filter for given command generator and state
	 * @param generator
	 * @param player
	 * @param board
	 * @param state
	 */
	public ValidCommandFilterIterator(Iterator<Command> generator, Player player, Board board, TurnState state) {
		this.generator = generator;
		this.player = player;
		this.board = board;
		this.state = state;
	}
	
	@Override
	public boolean hasNext() {
		if (!advanced) {
			if (!generator.hasNext())
				return false;
			nextCommand = generator.next();
			while (generator.hasNext() && !nextCommand.isValid(player, board, state)) {
				nextCommand = generator.next();
			}
			advanced = true;
		}
		return nextCommand.isValid(player, board, state);
	}

	@Override
	public Command next() {
		if (hasNext()) {
			advanced = false;
			currentCommand = nextCommand;
			return currentCommand;
		}
		throw new NoSuchElementException();
	}

}
