package cof.commands;
import cof.model.*;

/**
 * Abstraction of a generic command.
 * @author Andrea
 *
 */
public interface Command {
	/**
	 * Executes the command on given player, board, and state
	 * @param player
	 * @param board
	 * @param state
	 * @throws CommandNotValidException if command is not valid or something happened during execution
	 */
	public void execute(Player player, Board board, TurnState state) throws CommandNotValidException;
	
	/**
	 * Checks if the command is valid
	 * @param player
	 * @param board
	 * @param state
	 * @return true if the command is valid, false otherwise
	 */
	boolean isValid(Player player, Board board, TurnState state);
}
