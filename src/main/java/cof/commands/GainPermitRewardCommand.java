package cof.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.Bonus;
import cof.bonus.GainPermitRewardBonus;
import cof.model.Board;
import cof.model.Permit;
import cof.model.Player;

/**
 * Nobility Action: this lets the user choose one of their Permits and gain its bonus.
 * This is related with GainPermitRewardBonus
 * @author Andrea
 *
 */
public class GainPermitRewardCommand implements NobilityTrackAction {
	private static final Logger LOGGER = Logger.getLogger(GainPermitRewardCommand.class.getName());
	private String permitTag;
	
	/**
	 * Constructs a command with specified object
	 * @param permitChosen
	 */
	public GainPermitRewardCommand(Permit permitChosen) {
		this.permitTag = permitChosen.getTag();
	}
	
	/**
	 * Constructs a command with specified string tag
	 * @param permitTag
	 */
	public GainPermitRewardCommand(String permitTag) {
		this.permitTag = permitTag;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if (!isValid(player, board, state))
			throw new CommandNotValidException();
		try {
			Permit permit = player.getPermits().stream()
					.filter(p -> p.getTag().equals(permitTag))
					.findFirst()
					.get();
			
			Bonus bonus = permit.getBonus();
			
			state.pushBonus(bonus);
			
			state.pollDynamicBonus(GainPermitRewardBonus.class.getName());
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			throw new CommandNotValidException();
		}
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			boolean permitValid = player.getPermits().stream()
					.filter(p -> p.getTag().equals(permitTag))
					.findFirst()
					.isPresent();
			
			return permitValid && state.canPollDynamicBonus(GainPermitRewardBonus.class.getName());
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "Gain the reward of " + permitTag;
	}
}
