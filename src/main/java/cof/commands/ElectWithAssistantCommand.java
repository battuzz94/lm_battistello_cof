package cof.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import cof.model.Board;
import cof.model.Council;
import cof.model.Councillor;
import cof.model.Player;
import cof.model.Price;

/**
 * Quick action: choose a Councillor and place it in a specified Council.
 * @author Andrea
 *
 */
public class ElectWithAssistantCommand implements QuickAction{
	private static final Logger LOGGER = Logger.getLogger(ElectWithAssistantCommand.class.getName());
	private String councilTag;
	private String councillorTag;
	
	public static final int ASSISTANT_COST = 1;
	private static final Price ELECT_WITH_ASSISTANT_COST = new Price(0, ASSISTANT_COST);
	
	/**
	 * Creates a new ElectWithAssistant command with specified objects
	 * @param councilToBribe
	 * @param councillorToPush
	 */
	public ElectWithAssistantCommand(Council councilToBribe, Councillor councillorToPush) {
		councilTag = councilToBribe.getTag();
		councillorTag = councillorToPush.getTag();
	}
	
	/**
	 * Creates a new ElectWithAssistant command with specified String tags
	 * @param councilTag
	 * @param councillorTag
	 */
	public ElectWithAssistantCommand(String councilTag, String councillorTag) {
		this.councilTag = councilTag;
		this.councillorTag = councillorTag;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if(!isValid(player, board, state))
			throw new CommandNotValidException();
		
		try{
			Council council = board.getCouncilByTag(councilTag);
			Councillor councillor = board.getCouncillorByTag(councillorTag);
			
			player.pay(ELECT_WITH_ASSISTANT_COST);
			council.pushCouncillor(councillor);
			
			state.doQuickAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
		
	}

	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			board.getCouncilByTag(councilTag);
			board.getCouncillorByTag(councillorTag);
			return player.canAfford(ELECT_WITH_ASSISTANT_COST)&&state.canDoQuickAction();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
		
	}
	
	@Override
	public String toString() {
		return "Elect with assistant " + councillorTag + " in " + councilTag;
	}
}
