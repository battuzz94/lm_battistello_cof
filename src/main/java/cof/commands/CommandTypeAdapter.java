package cof.commands;

import com.google.gson.TypeAdapterFactory;

import common.RuntimeTypeAdapterFactory;

/**
 * Utility class that returns type factories to set up Json serializer with 
 * Command interfaces
 * @author Andrea
 *
 */
public class CommandTypeAdapter {
	private CommandTypeAdapter() {}
	
	public static TypeAdapterFactory getMainActionAdapter() {
		RuntimeTypeAdapterFactory<MainAction> mainActionFactory = RuntimeTypeAdapterFactory.of(MainAction.class, "type");
		mainActionFactory.registerSubtype(BribeCouncilCommand.class, "BribeCouncil");
		mainActionFactory.registerSubtype(BribeKingCommand.class, "BribeKingCommand");
		mainActionFactory.registerSubtype(BuildEmporiumCommand.class, "BuildEmporium");
		mainActionFactory.registerSubtype(ElectCouncillorCommand.class, "ElectCouncillor");
		return mainActionFactory;
	}
	
	public static TypeAdapterFactory getQuickActionAdapter() {
		RuntimeTypeAdapterFactory<QuickAction> quickActionFactory = RuntimeTypeAdapterFactory.of(QuickAction.class, "type");
		quickActionFactory.registerSubtype(AddPrimaryMoveCommand.class, "AddPrimaryMove");
		quickActionFactory.registerSubtype(ElectWithAssistantCommand.class, "ElectWithAssistant");
		quickActionFactory.registerSubtype(EngageAssistantCommand.class, "EngageAssistant");
		quickActionFactory.registerSubtype(ShufflePermitsCommand.class, "ShufflePermits");
		return quickActionFactory;
	}
	
	public static TypeAdapterFactory getNobilityActionAdapter() {
		RuntimeTypeAdapterFactory<NobilityTrackAction> nobilityActionFactory = RuntimeTypeAdapterFactory.of(NobilityTrackAction.class, "type");
		nobilityActionFactory.registerSubtype(GainOneCityRewardCommand.class, "GainOneCityReward");
		nobilityActionFactory.registerSubtype(GainTwoCityRewardCommand.class, "GainTwoCityReward");
		nobilityActionFactory.registerSubtype(GainPermitCommand.class, "GainPermit");
		nobilityActionFactory.registerSubtype(GainPermitRewardCommand.class, "GainPermitReward");
		return nobilityActionFactory;
	}
	
	public static TypeAdapterFactory getCommandAdapter() {
		RuntimeTypeAdapterFactory<Command> factory = RuntimeTypeAdapterFactory.of(Command.class, "type");
		// Registers main actions
		factory.registerSubtype(BribeCouncilCommand.class, "BribeCouncil");
		factory.registerSubtype(BribeKingCommand.class, "BribeKingCommand");
		factory.registerSubtype(BuildEmporiumCommand.class, "BuildEmporium");
		factory.registerSubtype(ElectCouncillorCommand.class, "ElectCouncillor");
		
		//Registers quick actions
		factory.registerSubtype(AddPrimaryMoveCommand.class, "AddPrimaryMove");
		factory.registerSubtype(ElectWithAssistantCommand.class, "ElectWithAssistant");
		factory.registerSubtype(EngageAssistantCommand.class, "EngageAssistant");
		factory.registerSubtype(ShufflePermitsCommand.class, "ShufflePermits");
		
		//Registers nobility actions
		factory.registerSubtype(GainOneCityRewardCommand.class, "GainOneCityReward");
		factory.registerSubtype(GainTwoCityRewardCommand.class, "GainTwoCityReward");
		factory.registerSubtype(GainPermitCommand.class, "GainPermit");
		factory.registerSubtype(GainPermitRewardCommand.class, "GainPermitReward");
		
		//Registers market actions
		factory.registerSubtype(BuyItemCommand.class, "BuyItem");
		factory.registerSubtype(SellItemCommand.class, "SellItem");
		
		//Registers other actions
		factory.registerSubtype(PassTurnCommand.class, "PassTurn");
		factory.registerSubtype(PollBonusCommand.class, "PollBonus");
		
		return factory;
	}
}
