package cof.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cof.model.Board;
import cof.model.Card;
import cof.model.City;
import cof.model.Council;
import cof.model.Councillor;
import cof.model.Permit;
import cof.model.Player;
import cof.model.PoliticColor;
import cof.model.Region;

/**
 * Generates all the possible main actions Commands for a given state.
 * All commands may be filtered by specific flags set before the iterator is created.
 * @author Andrea
 *
 */
public class MainActionGenerator implements Iterable<Command> {
	Board board;
	Player player;
	TurnState state;
	private boolean councillorFilter = false;
	private boolean validFilter = true;
	
	/**
	 * Creates a new MainActionGenerator for a given state
	 * @param player
	 * @param board
	 * @param state
	 */
	public MainActionGenerator(Player player, Board board, TurnState state) {
		this.player = player;
		this.board = board;
		this.state = state;
	}
	
	/**
	 * If councillorFilter is true, all the ElectCouncillor Commands will be cut off and 
	 * won't be listed in the iterator.
	 * This flag is false by default, and can be turned on to reduce computation time.
	 * @param councillorFilter
	 */
	public void setElectCouncillorFilter(boolean councillorFilter) {
		this.councillorFilter = councillorFilter;
	}
	
	/**
	 * If validFilter is set to true, all the commands will be filtered and only the 
	 * valid commands will be returned by the iterator.
	 * The valid filter is set to true by default.
	 * @param validFilter enable/disable tie valid filter.
	 */
	public void setValidCommandFilter(boolean validFilter) {
		this.validFilter  = validFilter;
	}
	
	/**
	 * Returns an iterator listing all the commands for this state, filtered with the
	 * specified flags.
	 */
	@Override
	public Iterator<Command> iterator() {
		if (validFilter)
			return new ValidCommandFilterIterator(new MainActionIterator(councillorFilter), player, board, state);
		else
			return new MainActionIterator(councillorFilter);
	}
	
	
	class MainActionIterator implements Iterator<Command> {
		
		List<Iterator<Command>> iterators;
		Iterator<Iterator<Command>> mainIterator;
		Iterator<Command> currentIterator;
		
		private MainActionIterator(boolean councillorFilter) {
			if (councillorFilter) {
				iterators = Arrays.asList(
						new BribeCouncilIterator(),
						new BribeKingIterator(),
						new BuildEmporiumIterator());
			}
			else {
				iterators = Arrays.asList(
						new ElectCouncillorIterator(),
						new BribeCouncilIterator(),
						new BribeKingIterator(),
						new BuildEmporiumIterator());
			}
			
			// Removes iterators with no possible actions
			iterators = iterators.stream().filter(it -> it.hasNext()).collect(Collectors.toList());
			
			mainIterator = iterators.iterator();
			currentIterator = mainIterator.next();
		}
		
		@Override
		public boolean hasNext() {
			return currentIterator.hasNext() || mainIterator.hasNext();
		}
		
		/**
		 * Returns the next main action generated
		 * @throws NoSuchElementException if no such element is found
		 */
		@Override
		public Command next() {
			if (currentIterator.hasNext())
				return currentIterator.next();
			else {
				currentIterator = mainIterator.next();
				while (!currentIterator.hasNext())
					currentIterator = mainIterator.next();
				return currentIterator.next();
			}
		}
	}
	
	private List<Card> findBestMatch(List<Card> cards, Council council) {
		List<Card> bestMatch = new ArrayList<>();
		List<PoliticColor> councilColors = council.getCouncillors().stream().map(c -> c.getColor()).collect(Collectors.toList());
		councilColors.sort((c1, c2) -> c1.compareTo(c2));
		
		List<Card>cardsCopy = Stream.concat(
						cards.stream().filter(c -> !c.isJolly()).sorted((c1, c2) -> c1.getColor().compareTo(c2.getColor())),
						cards.stream().filter(c -> c.isJolly())
					).collect(Collectors.toList());
		
		int i = 0;
		
		for (Card card : cardsCopy) {
			while (i < councilColors.size() && card.getColor().compareTo(councilColors.get(i)) > 0)
				i++;
			if ((bestMatch.size() < councilColors.size() && card.isJolly()) ||  (i < councilColors.size() && card.getColor() == councilColors.get(i))) {
				bestMatch.add(card);
				i++;
			}
		}
		return bestMatch;
	}
	
	
	class ElectCouncillorIterator implements Iterator<Command> {
		int councillorId;
		int poolCount;
		int councilId;
		List<Councillor> availableCouncillors;
		List<Council> availableCouncils;
		
		private ElectCouncillorIterator () {
			councillorId = -1;
			councilId = 0;
			availableCouncils = board.getRegions().stream().map(reg -> reg.getRegionCouncil()).collect(Collectors.toList());
			availableCouncils.add(board.getKingCouncil());
			
			availableCouncillors = board.getCouncillorPool().getAvailableCouncillors();
			
			availableCouncillors = filterDuplicates(availableCouncillors);
			List<Councillor> avCouncillorsOptimized = filterColors(availableCouncillors);
			if (!avCouncillorsOptimized.isEmpty())
				availableCouncillors = avCouncillorsOptimized;
		}

		private List<Councillor> filterColors(List<Councillor> councillors) {
			return councillors.stream()
					.filter(counc -> player.getCards().stream()
											.filter(card -> counc.getColor() == card.getColor())
											.count() > 0)
					.collect(Collectors.toList());
		}

		private List<Councillor> filterDuplicates(List<Councillor> councillors) {
			Map<PoliticColor, Councillor> map = new LinkedHashMap<>();
			for (Councillor counc : councillors) {
			  map.put(counc.getColor(), counc);
			}
			return new ArrayList<>(map.values());
		}
		
		@Override
		public boolean hasNext() {
			if (availableCouncillors.isEmpty())
				return false;
			return ( councilId < availableCouncils.size() - 1 ) || 
					( councilId <availableCouncils.size() 
							&& councillorId < availableCouncillors.size() - 1); 
		}

		@Override
		public Command next() {
			if (!hasNext())
				throw new NoSuchElementException();
			
			councillorId++;
			if (councillorId == availableCouncillors.size()) {
				councillorId = 0;
				councilId++;
			}
			return new ElectCouncillorCommand(availableCouncils.get(councilId), availableCouncillors.get(councillorId));
		}
	}
	
	class BribeCouncilIterator implements Iterator<Command> {
		int regionId;
		List<Region> availableRegions;
		List<Card> availableCards;
		int permitId;
		int cardId;
		
		
		
		private BribeCouncilIterator () {
			permitId = -1;
			cardId = 0;
			regionId = 0;
			availableRegions = board.getRegions();
			availableCards = player.getCards();
			
		}
		
		@Override
		public boolean hasNext() {
			return ( regionId < availableRegions.size() - 1 ) ||
						((regionId == availableRegions.size() - 1) &&
						(permitId < availableRegions.get(regionId).getGainablePermits().size() - 1));
		}

		@Override
		public Command next() {
			if (!hasNext())
				throw new NoSuchElementException();
			
			permitId++;
			if (permitId >= availableRegions.get(regionId).getGainablePermits().size()) {
				permitId = 0;
				regionId++;
			}
			
			Council council = availableRegions.get(regionId).getRegionCouncil();
			List<Card> cards = findBestMatch(availableCards, council);
			if (permitId >= availableRegions.get(regionId).getGainablePermits().size())
				return new BribeCouncilCommand(cards, council, null);
			else {
				Permit permit = availableRegions.get(regionId).getGainablePermits().get(permitId);
			
				return new BribeCouncilCommand(cards, council, permit);
			}
		}
		
		
	}
	
	class BribeKingIterator implements Iterator<Command> {
		List<Card> availableCards;
		List<City> availableCities;
		int cityId;
		int cardId;
		
		
		
		private BribeKingIterator () {
			cityId = -1;
			cardId = 0;
			availableCards = player.getCards();
			availableCities = board.getCities();
		}
		
		@Override
		public boolean hasNext() {
			return cityId < availableCities.size() - 1;
		}

		@Override
		public Command next() {
			if (!hasNext())
				throw new NoSuchElementException();
			
			cityId++;
			City city = availableCities.get(cityId);
		
			List<Card> cards = findBestMatch(availableCards, board.getKingCouncil());
			
			return new BribeKingCommand(cards, city);
		}
		
	}
	
	class BuildEmporiumIterator implements Iterator<Command> {
		List<Permit> availablePermits;
		Permit currentPermit;
		int permitId;
		int cityId;
		
		
		private BuildEmporiumIterator () {
			cityId = -1;
			permitId = 0;
			
			// Get only the permits not used and with at least one city where the player hasn't built yet
			availablePermits = player.getPermits()
					.stream()
					.filter(p -> !p.isUsed())
					.filter(p -> p.getCities().stream().filter(c -> !player.hasBuiltIn(c)).count() > 0)
					.collect(Collectors.toList());
		}
		
		@Override
		public boolean hasNext() {
			return permitId < availablePermits.size() - 1 ||
					(permitId == availablePermits.size() -1 
					&& cityId < availablePermits.get(permitId).getCities().size()-1);
		}

		@Override
		public Command next() {
			if (!hasNext())
				throw new NoSuchElementException();
			
			cityId++;
			if (cityId >= availablePermits.get(permitId).getCities().size()) {
				cityId = 0;
				permitId++;
			}
			
			Permit permit = availablePermits.get(permitId);
			City city = permit.getCities().get(cityId);
				
			
			return new BuildEmporiumCommand(permit, city);
		}
		
	}
}
