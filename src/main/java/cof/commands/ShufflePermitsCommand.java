package cof.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import cof.model.Board;
import cof.model.Player;
import cof.model.Price;
import cof.model.Region;

/**
 * Quick action: shuffles the Permits of a given Region.
 * @author Andrea
 *
 */
public class ShufflePermitsCommand implements QuickAction{
	private static final Logger LOGGER = Logger.getLogger(ShufflePermitsCommand.class.getName());
	public static final int COST_OF_SHUFFLING = 1;
	private String regionTag;
	
	/**
	 * Builds a new command with specified objects
	 * @param chosenRegionTag
	 */
	public ShufflePermitsCommand(String chosenRegionTag){
		this.regionTag=chosenRegionTag;
	}
	
	/**
	 * Builds a new command with specified string tags
	 * @param chosenRegion
	 */
	public ShufflePermitsCommand(Region chosenRegion){
		this.regionTag=chosenRegion.getTag();
	}
	
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		
		try{
			Region r=board.getRegionByTag(regionTag);
			
			r.shufflePermits();
			player.pay(new Price(0, COST_OF_SHUFFLING));
			
			state.doQuickAction();
		}
		catch (Exception e) {
			CommandNotValidException exc = new CommandNotValidException();
			exc.addSuppressed(e);
			throw exc;
		}
	}
	
	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		boolean valid= false;
		Price price=new Price(0, COST_OF_SHUFFLING);
		try {
			board.getRegionByTag(regionTag);
			if(player.canAfford(price))
				valid=true;
			return valid && state.canDoQuickAction();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "Shuffle permits in " + regionTag;
	}
}
