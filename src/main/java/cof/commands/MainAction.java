package cof.commands;

import cof.model.Board;
import cof.model.Player;

/**
 * Abstraction of a main action
 * @author Andrea
 *
 */
public interface MainAction extends Command{
	
	@Override
	public void execute(Player player, Board board, TurnState state);

}
