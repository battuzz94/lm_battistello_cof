package cof.commands;

import java.util.logging.Level;
import java.util.logging.Logger;

import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.bonus.GainOneCityRewardBonus;
import cof.model.Board;
import cof.model.City;
import cof.model.Player;

/**
 * Nobility Action: lets the user choose one city and gain its bonus.
 * This command is related with the GainOneCityRewardBonus.
 * @author Andrea
 *
 */
public class GainOneCityRewardCommand implements NobilityTrackAction {
	private static final Logger LOGGER = Logger.getLogger(GainOneCityRewardCommand.class.getName());
	private String cityTag;
	
	/**
	 * Constructs a command with specified object
	 * @param cityChosen
	 */
	public GainOneCityRewardCommand(City cityChosen) {
		this.cityTag = cityChosen.getTag();
	}
	
	/**
	 * Constructs a command with specified string tag
	 * @param cityTag
	 */
	public GainOneCityRewardCommand(String cityTag) {
		this.cityTag = cityTag;
	}
	
	@Override
	public void execute(Player player, Board board, TurnState state) {
		if (!isValid(player, board, state))
			throw new CommandNotValidException();
		try {
			City city = board.getCityByTag(cityTag);
			Bonus bonus = city.getBonus();
			
			state.pushBonus(bonus);
			state.pollDynamicBonus(GainOneCityRewardBonus.class.getName());
		}
		catch (Exception e) {
			CommandNotValidException thrown = new CommandNotValidException();
			thrown.addSuppressed(e);
			throw thrown;
		}
	}
	
	@Override
	public boolean isValid(Player player, Board board, TurnState state) {
		try {
			City c = board.getCityByTag(cityTag);
			Bonus bonus = c.getBonus();
			
			// the player must have built an emporium in that city and there is a bonus to be retrieved in the state
			if (!player.hasBuiltIn(c) || !state.canPollDynamicBonus(GainOneCityRewardBonus.class.getName()))
				return false;
			
			// The bonus can't grant any nobility bonus
			if (bonus instanceof BasicBonus && ((BasicBonus)bonus).isNobility()) {
				return false;
			}
			
			return true;
		}
		catch (Exception e) {
			LOGGER.log(Level.FINEST, this + " is not valid", e);
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "Gain the reward of " + cityTag;
	}

}
