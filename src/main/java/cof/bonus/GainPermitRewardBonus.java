package cof.bonus;

import cof.model.Player;

/**
 * Gain the reward of one of the player's permits.
 * @author Andrea
 *
 */
public class GainPermitRewardBonus implements DynamicBonus {
	@Override
	public void giveTo(Player player) {
		// This will be implemented in the correlated command
	}
	
	@Override
	public String toString() {
		return "+1 Permit Reward";
	}
}
