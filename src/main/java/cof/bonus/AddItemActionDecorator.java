package cof.bonus;

import cof.model.Player;

/**
 * This bonus combines a BasicBonus and an AdditionalActionBonus
 * @author Andrea
 *
 */
public class AddItemActionDecorator implements StaticBonus {
	private BasicBonus basicBonus = new BasicBonus();
	private AdditionalActionBonus additionalAction;
	
	/**
	 * Creates a new bonus with main action and basic bonus
	 * @param basicBonus
	 * @param additionalAction
	 */
	public AddItemActionDecorator(BasicBonus basicBonus, AdditionalActionBonus additionalAction) {
		this.basicBonus = basicBonus;
		this.additionalAction = additionalAction;
	}

	public void setBasicBonus(BasicBonus basicBonus) {
		this.basicBonus = basicBonus;
	}
	
	/**
	 * Generates a new AddItemActionDecorator with a random bonus
	 * @return
	 */
	public static AddItemActionDecorator setRandom() {
		return new AddItemActionDecorator(BasicBonus.setRandom(),  new AdditionalActionBonus());
	}

	@Override
	public void giveTo(Player player) {
		basicBonus.giveTo(player);
		additionalAction.giveTo(player);
	}
	
	@Override
	public String toString() {
		return this.basicBonus + " " + this.additionalAction;
	}

	@Override
	public boolean hasAdditionalMainAction() {
		return true;
	}

	@Override
	public boolean getDrawCards() {
		return this.basicBonus.getDrawCards();
	}

	@Override
	public void setCards(boolean drawCard) {
		this.basicBonus.setCards(drawCard);
	}
}
