package cof.bonus;

import cof.model.Player;

/**
 * This bonus lets you choose one city (with no advance in nobility track) and
 * get its bonus
 * @author Andrea
 *
 */
public class GainOneCityRewardBonus implements DynamicBonus {
	@Override
	public void giveTo(Player player) {
		// This is a dynamic bonus, so this will be given by the relative command
	}
	
	@Override
	public String toString() {
		return "+1 City Reward";
	}

}
