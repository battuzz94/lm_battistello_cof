package cof.bonus;

import cof.model.Permit;
import cof.model.Player;

/**
 * A bonus that let the user choose and obtain one of the visible permits.
 * @author Andrea
 *
 */
public class GainPermitBonus implements DynamicBonus {
	private Permit permit;
	
	/**
	 * Creates a new GainPermitBonus
	 */
	public GainPermitBonus() {
		this(null);
	}
	
	/**
	 * Creates a bonus with permit to give to player
	 * @param permit
	 */
	public GainPermitBonus(Permit permit) {
		this.permit = permit;
	}

	@Override
	public void giveTo(Player player) {
		player.addPermit(permit);
		permit.getBonus().giveTo(player);
	}
	
	@Override
	public String toString() {
		return "+1 Permit";
	}

}
