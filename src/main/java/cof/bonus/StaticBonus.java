package cof.bonus;

/**
 * A bonus that can be given immediately without any additional input
 * @author andrea
 *
 */
public interface StaticBonus extends Bonus {
	/**
	 * Checks if the bonus grants an additional main action
	 * @return
	 */
	public boolean hasAdditionalMainAction();
	
	/**
	 * Checks if the bonus let the user draw cards or not
	 * @return
	 */
	public boolean getDrawCards();
	
	/**
	 * Set the drawCard flag on bonus. If true, then no cards will be given to player
	 * @param drawCard
	 */
	public void setCards(boolean drawCard);
}
