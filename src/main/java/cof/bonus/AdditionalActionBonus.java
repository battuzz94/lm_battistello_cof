package cof.bonus;

import cof.model.Player;

/**
 * Adds a primary move to the player's TurnState
 * @author Andrea
 *
 */
public class AdditionalActionBonus implements StaticBonus {
	
	@Override
	public void giveTo(Player player) {
		// This won't be called because this bonus can't have access to TurnState
	}

	@Override
	public boolean hasAdditionalMainAction() {
		return true;
	}

	@Override
	public boolean getDrawCards() {
		return false;
	}

	@Override
	public void setCards(boolean drawCard) {
		// This is not used
	}
	
	@Override
	public String toString() {
		return "+1M";
	}
}
