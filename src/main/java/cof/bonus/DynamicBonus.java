package cof.bonus;

/**
 * An interface representing a type of bonus that requires additional input by the user.
 * Some examples of this type: GainOneCityRewardBonus and GainPermitBonus.
 * @author Andrea
 *
 */
public interface DynamicBonus extends Bonus {

}
