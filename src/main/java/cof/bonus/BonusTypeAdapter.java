package cof.bonus;

import com.google.gson.TypeAdapterFactory;

import common.RuntimeTypeAdapterFactory;

/**
 * Utility class that returns type factories used to configure the Json serializer
 * @author Andrea
 *
 */
public class BonusTypeAdapter {
	private BonusTypeAdapter() {}
	
	public static TypeAdapterFactory getAdapter() {
		RuntimeTypeAdapterFactory<Bonus> bonusAdapterFactory = RuntimeTypeAdapterFactory.of(Bonus.class, "type");
		bonusAdapterFactory.registerSubtype(BasicBonus.class, "BasicBonus");
		bonusAdapterFactory.registerSubtype(AddItemActionDecorator.class, "BasicBonusWithAdditionalAction");
		bonusAdapterFactory.registerSubtype(AdditionalActionBonus.class, "AdditionalAction");
		bonusAdapterFactory.registerSubtype(GainOneCityRewardBonus.class, "GainOneCityRewardBonus");
		bonusAdapterFactory.registerSubtype(GainTwoCityRewardBonus.class, "GainTwoCityRewardBonus");
		bonusAdapterFactory.registerSubtype(GainPermitBonus.class, "GainPermitBonus");
		bonusAdapterFactory.registerSubtype(GainPermitRewardBonus.class, "GainPermitRewardBonus");
		bonusAdapterFactory.registerSubtype(ColoredCityBonus.class, "ColoredCityBonus");
		return bonusAdapterFactory;
	}
	
	public static TypeAdapterFactory getStaticBonusAdapter() {
		RuntimeTypeAdapterFactory<StaticBonus> bonusAdapterFactory = RuntimeTypeAdapterFactory.of(StaticBonus.class, "type");
		bonusAdapterFactory.registerSubtype(BasicBonus.class, "BasicBonus");
		bonusAdapterFactory.registerSubtype(AddItemActionDecorator.class, "BasicBonusWithAdditionalAction");
		bonusAdapterFactory.registerSubtype(AdditionalActionBonus.class, "AdditionalAction");
		bonusAdapterFactory.registerSubtype(ColoredCityBonus.class, "ColoredCityBonus");
		return bonusAdapterFactory;
	}

	public static TypeAdapterFactory getDynamicBonusAdapter() {
		RuntimeTypeAdapterFactory<DynamicBonus> bonusAdapterFactory = RuntimeTypeAdapterFactory.of(DynamicBonus.class, "type");
		bonusAdapterFactory.registerSubtype(GainOneCityRewardBonus.class, "GainOneCityRewardBonus");
		bonusAdapterFactory.registerSubtype(GainTwoCityRewardBonus.class, "GainTwoCityRewardBonus");
		bonusAdapterFactory.registerSubtype(GainPermitBonus.class, "GainPermitBonus");
		bonusAdapterFactory.registerSubtype(GainPermitRewardBonus.class, "GainPermitRewardBonus");
	
		return bonusAdapterFactory;
	}
}
