package cof.bonus;

import java.util.Random;

import cof.model.Card;
import cof.model.Player;

/**
 * BasicBonus if instance object with no parameters, makes it empty. It's
 * possible to instance this bonus with specific value.
 * 
 * @author Simone
 *
 */
public class BasicBonus implements StaticBonus {
	static final int NMAX_BONUS = 4;
	private static final Random RANDOM = new Random();
	private int assistants;
	private int coins;
	private int victoryPoints;
	private int cards;
	private int nobilityTrack;
	private transient boolean drawCard = true;
	
	/**
	 * Creates a new empty basic bonus
	 */
	public BasicBonus() {
		this(0, 0, 0, 0, 0);
	}
	
	/**
	 * Creates a basic bonus with specified parameters
	 * @param assistants
	 * @param coins
	 * @param victoryPoints
	 * @param cards
	 * @param nobilityTrack
	 */
	public BasicBonus(int assistants, int coins, int victoryPoints, int cards, int nobilityTrack) {
		this.assistants = assistants;
		this.coins = coins;
		this.victoryPoints = victoryPoints;
		this.cards = cards;
		this.nobilityTrack = nobilityTrack;
	}
	
	/**
	 * Set a random basic bonus with a certain level.
	 * @param bonusLevel
	 * @return
	 */
	public static BasicBonus setRandom(int bonusLevel) {
		BasicBonus ret = new BasicBonus();
		int level = bonusLevel - RANDOM.nextInt(3);
		
		
		while (level-- > 0) {
			switch(RANDOM.nextInt(5)) {
			case 0:
				ret.assistants++;
				break;
			case 1:
				ret.cards++;
				break;
			case 2:
				ret.coins++;
				break;
			case 3:
				ret.nobilityTrack++;
				break;
			case 4:
				ret.victoryPoints++;
				break;
			default:
				break;
			}
		}
		return ret;
	}
	
	/**
	 * This static Method generate a not null Random BasicBonus.
	 * 
	 * @return A new BasicBonus.
	 */
	public static BasicBonus setRandom() {
		return setRandom(4);
	}

	@Override
	public void giveTo(Player player) {
		player.addAssistants(this.assistants);
		player.addCoins(this.coins);
		player.addNobility(this.nobilityTrack);
		player.addPoints(this.victoryPoints);
		if (drawCard)
			for (int i = 0; i < cards; i++)
				player.addCards(Card.drawCard());
	}

	public boolean isNobility() {
		return this.nobilityTrack != 0;
	}

	@Override
	public boolean hasAdditionalMainAction() {
		return false;
	}

	public int getCoins() {
		return coins;
	}

	public int getAssistants() {
		return assistants;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public int getCards() {
		return cards;
	}

	public int getNobilityTrack() {
		return nobilityTrack;
	}

	@Override
	public boolean getDrawCards() {
		return drawCard;
	}

	@Override
	public void setCards(boolean drawCard) {
		this.drawCard = drawCard;
	}
	
	@Override
	public String toString() {
		String ret =  		
					this.coins + "$ "
				+ 	this.assistants + "A " 
				+ 	this.victoryPoints + "P " 
				+ 	this.cards + "C " 
				+ 	this.nobilityTrack + "N";
		return ret.replaceAll("0.", "").replaceAll("\\s{2,}", " ").trim();
	}
}
