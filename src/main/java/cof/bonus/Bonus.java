package cof.bonus;

import cof.model.Obtainable;
import cof.model.Player;

/**
 * Bonuses are something that the user can only obtain
 * @author andrea
 *
 */
@FunctionalInterface
public interface Bonus extends Obtainable {
	@Override
	public void giveTo(Player player);
}
