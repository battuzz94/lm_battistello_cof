package cof.bonus;

import cof.commands.CommandNotValidException;
import cof.model.*;

/**
 * Bonus given when the user builds in all cities with a specified color
 * @author Andrea
 *
 */
public class ColoredCityBonus implements StaticBonus {
	private boolean assigned = false;
	private CityColor color;
	private StaticBonus cityBonus;
	
	/**
	 * Creates a new coloredCityBonus
	 * @param color
	 * @param citybonus
	 */
	public ColoredCityBonus(CityColor color, StaticBonus citybonus) {
		this.color = color;
		this.cityBonus = citybonus;
		this.assigned = false;
	}
	
	/**
	 * Checks if the bonus was assigned or not
	 */
	public void bonusNowAssigned(){
		this.assigned=true;
	}
	
	public boolean isAssigned() {
		return assigned;
	}

	public void setColor(CityColor color) {
		this.color = color;
	}

	public void setBonus(StaticBonus citybonus) {
		this.cityBonus = citybonus;
	}
	
	public CityColor getColor() {
		return color;
	}
	
	public Bonus getBonus() {
		return cityBonus;
	}
	
	@Override
	public String toString() {
		return "Bonus for: " + color + ": " + cityBonus + " used: " + assigned;
	}
	
	@Override
	public void giveTo(Player player) {
		if (!assigned) {
			player.obtain(this.cityBonus);
			assigned = true;
		}
		else
			throw new CommandNotValidException();
		
	}
	@Override
	public boolean hasAdditionalMainAction() {
		return false;
	}
	@Override
	public boolean getDrawCards() {
		return this.cityBonus.getDrawCards();
	}
	@Override
	public void setCards(boolean drawCard) {
		this.cityBonus.setCards(drawCard);
	}
}
