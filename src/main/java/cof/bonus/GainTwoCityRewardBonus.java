package cof.bonus;

import cof.model.Player;

/**
 * Choose two different cities and gain their bonuses.
 * @author Andrea
 *
 */
public class GainTwoCityRewardBonus implements DynamicBonus {

	@Override
	public void giveTo(Player player) {
		// This will be implemented in the relative Command
	}
	
	@Override
	public String toString() {
		return "+2 City Reward";
	}

}
