package ai;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import cof.commands.Command;
import cof.commands.MainActionGenerator;
import cof.commands.PassTurnCommand;
import cof.commands.QuickActionGenerator;
import cof.commands.TurnState;
import cof.model.Board;
import cof.model.Player;
import common.Duplicator;

/**
 * This class represents a snapshot of the game state from the point of view of a single player. 
 * @author Andrea
 *
 */
public class MoveState {
	private static final Duplicator<Board> boardDup = new Duplicator<>(Board.class);
	private static final Duplicator<Player> playerDup = new Duplicator<>(Player.class);
	private static final Duplicator<TurnState> turnDup = new Duplicator<>(TurnState.class);
	
	private static final Command passTurn = new PassTurnCommand();
	private TurnState state;
	private Board board;
	private Player player;
	
	/**
	 * Creates a new snapshot from previously created MoveState
	 * @param other
	 */
	public MoveState(MoveState other) {
		this(other.getBoard(), other.getPlayer(), other.getState());
	}
	
	/**
	 * Creates a new MoveState by copying the parameters board, player and state
	 * @param board
	 * @param player
	 * @param state
	 */
	public MoveState(Board board, Player player, TurnState state) {
		this.board = boardDup.duplicate(board);
		this.player = playerDup.duplicate(player);
		this.state = turnDup.duplicate(state);
	}
	
	/**
	 * Returns a subset of all possible moves optimized for the computing of the best move for player.
	 * @return A list of all best moves to do
	 */
	public List<Move> allMovesOptimized() {
		List<Command> ret = new ArrayList<>();
		
		QuickActionGenerator quick = new QuickActionGenerator(player, board, state);
		quick.setElectWithAssistantFilter(true);
		quick.setShufflePermitFilter(true);
		quick.setValidCommandFilter(true);
		if (state.canDoMainAction())
			quick.setAddPrimaryMoveFilter(true);  // Removes the add Primary Move as first move
		Iterator<Command> quickIterator = quick.iterator();
		
		quickIterator.forEachRemaining(ret::add);
		
		MainActionGenerator main = new MainActionGenerator(player, board, state);
		if (player.getCoins() > 5 || player.getAssistants() >= 3)
			main.setElectCouncillorFilter(true);
		main.setValidCommandFilter(true);
		Iterator<Command> mainIterator = main.iterator();
		
		if (!mainIterator.hasNext()) {
			main.setElectCouncillorFilter(false);
			mainIterator = main.iterator();
		}
		
		mainIterator.forEachRemaining(ret::add);
		return ret.stream().map(command -> new Move(board, player, state, command)).collect(Collectors.toList());
	}
	
	/**
	 * Returns a list of all possible valid commands for the current GameState
	 * @return a list of valid commands
	 */
	public List<Command> allCommands() {
		List<Command> ret = new ArrayList<>();
		
		MainActionGenerator main = new MainActionGenerator(player, board, state);
		main.setValidCommandFilter(true);
		Iterator<Command> mainIterator = main.iterator();
		
		mainIterator.forEachRemaining(ret::add);
		
		QuickActionGenerator quick = new QuickActionGenerator(player, board, state);
		quick.setValidCommandFilter(true);
		Iterator<Command> quickIterator = quick.iterator();
		
		quickIterator.forEachRemaining(ret::add);
		
		return ret;
	}
	
	/**
	 * Returns a list of all possible Moves with the commands taken by allCommands() function
	 * @return a list of Move
	 */
	public List<Move> allMoves() {
		return allCommands().parallelStream().map(command -> new Move(board, player, state, command)).collect(Collectors.toList());
	}
	
	/**
	 * Checks whether it is possible to pass the turn (final state)
	 * @return true if the state is final, false otherwise
	 */
	public boolean endTurn() {
		return passTurn.isValid(player, board, state);
	}
	
	public Board getBoard() { return board; }
	public Player getPlayer() { return player; }
	public TurnState getState() { return state; }
}

