package ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cof.commands.PassTurnCommand;
import cof.commands.TurnState;
import cof.model.Board;
import cof.model.Card;
import cof.model.City;
import cof.model.Player;
import cof.model.PoliticColor;
import cof.model.PrettyPrinter;
import cof.model.Region;
import common.Tuple2;
import common.Utility;

/**
 * This class evaluates best move to do for a given MoveState
 * @author Andrea
 *
 */
public class Evaluator {
	private double coinFactor = 0.083;
	private double assistantFactor = 0.041;
	private double nobilityFactor = 0.418;
	private double pointFactor = 0.03922;
	private double emporiumFactor = 0.1842;
	private double maxCitiesRegionFactor = 0.2337;
	private String tag;
	
	private static final Random random = new Random();
	
	/**
	 * Builds an evaluator with predefined parameters
	 */
	public Evaluator() {
		this(2.0, 2.0, 1.2, 10.0, 3.0, 6.0);
	}
	
	/**
	 * Builds an Evaluator with specified parameters
	 * @param coinFactor
	 * @param assistantFactor
	 * @param nobilityFactor
	 * @param pointFactor
	 * @param emporiumFactor
	 * @param maxCitiesRegionFactor
	 */
	public Evaluator(double coinFactor, double assistantFactor, double nobilityFactor, double pointFactor,
			double emporiumFactor, double maxCitiesRegionFactor) {
		this.coinFactor = coinFactor;
		this.assistantFactor = assistantFactor;
		this.nobilityFactor = nobilityFactor;
		this.pointFactor = pointFactor;
		this.emporiumFactor = emporiumFactor;
		this.maxCitiesRegionFactor = maxCitiesRegionFactor;
	}
	
	/**
	 * Creates a new normalized Evaluator with random parameters
	 * @return
	 */
	public static Evaluator generateRandomEvaluator() {
		double coinFactor = random.nextDouble();
		double assistantFactor = random.nextDouble();
		double nobilityFactor = random.nextDouble();
		double pointFactor = random.nextDouble();
		double emporiumFactor = random.nextDouble();
		double maxCitiesRegionFactor = random.nextDouble();
		double sum = coinFactor + assistantFactor + nobilityFactor + pointFactor + emporiumFactor + maxCitiesRegionFactor;
		return new Evaluator(
				coinFactor / sum,
				assistantFactor / sum,
				nobilityFactor / sum,
				pointFactor / sum,
				emporiumFactor / sum,
				maxCitiesRegionFactor / sum
			);
	}
	
	/**
	 * Runs a benchmark comparison for normal and parallel stream
	 * @param args
	 */
	public static void main(String[] args) {
	
		Board board = Utility.loadTestBoardDisposed();
		PrettyPrinter pp = new PrettyPrinter(board);
		System.out.println(pp.printBoard());
		Player player = new Player(10, 10);
		
		List<Card> cards = Arrays.asList(new Card(PoliticColor.BLACK),
				new Card(PoliticColor.ORANGE),
				new Card(PoliticColor.BLUE),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.PINK)
				);
		
		cards.forEach(c -> player.obtain(c));
		
		System.out.println(player);
		TurnState state = new TurnState();

		System.out.println(getBestMove(new MoveState(board, player, state)));
		
		benchmark(board, player, state);
		
	}
	
	private static void benchmark(Board board, Player player, TurnState state) {
		computeNormal(board, player, state);
		computeParallel(board, player, state);

		for (int i = 0; i < 10; i++)
			computeParallel(board, player, state);
		
		for (int i = 0; i < 10; i++)
			computeNormal(board, player, state);
	}
	
	/**
	 * Returns the best sequence of moves to do according to the default evaluator function
	 * @param state the initial MoveState
	 * @return a list of moves with best final score
	 */
	public static List<Move> getBestMove(MoveState state) {
		return getBestMove(state, new Evaluator());
	}
	
	/**
	 * Returns the best move for the state evaluated by evaluator
	 * @param state
	 * @param evaluator
	 * @return a sequence of move that lead to the maximum result
	 */
	public static List<Move> getBestMove(MoveState state, Evaluator evaluator) {
		Tuple2<List<Move>, Double> bestMove = allMovesNormal(state).map(move -> {
			MoveState s = move.get(move.size()-1).getCurrentState();
			double val = evaluator.evaluate(s.getBoard(), s.getPlayer());
			return new Tuple2<>(move, val);
		}).max((t1, t2) -> Double.compare(t1.getSecond(), t2.getSecond())).
				orElse(new Tuple2<List<Move>, Double>(Arrays.asList(new Move(state, new PassTurnCommand())), 0.0));
		
		return bestMove.getFirst();
	}
	
	/**
	 * Returns the best move for the state using the default evaluator and computed in parallel
	 * @param state
	 * @return a sequence of move that lead to the maximum result
	 */
	public static List<Move> getBestMoveParallel(MoveState state) {
		return getBestMoveParallel(state, new Evaluator());
	}
	
	/**
	 * Returns the best sequence of moves for state with given evaluator. This will be computed in parallel
	 * @param state
	 * @param evaluator
	 * @return a sequence of move that lead to the maximum result
	 */
	public static List<Move> getBestMoveParallel(MoveState state, Evaluator evaluator) {
		Tuple2<List<Move>, Double> bestMove = allMovesParallel(state).map(move -> {
			MoveState s = move.get(move.size()-1).getCurrentState();
			double val = evaluator.evaluate(s.getBoard(), s.getPlayer());
			return new Tuple2<>(move, val);
		}).max((t1, t2) -> Double.compare(t1.getSecond(), t2.getSecond())).
				orElse(new Tuple2<List<Move>, Double>(Arrays.asList(new Move(state, new PassTurnCommand())), 0.0));
		
		return bestMove.getFirst();
	}
	
	
	private static void computeParallel(Board board, Player player, TurnState state) {
		long currentTime;
		long finishedTime;
		// Parallel
		currentTime = System.currentTimeMillis();
		allMovesParallel(new MoveState(board, player, state)).collect(Collectors.toList());
		finishedTime = System.currentTimeMillis();
		System.out.println("Elapsed time parallel: " + (double)(finishedTime - currentTime)/1000.0);
	}


	private static void computeNormal(Board board, Player player, TurnState state) {
		long currentTime;
		long finishedTime;
		// Normal
		currentTime = System.currentTimeMillis();
		allMovesNormal(new MoveState(board, player, state)).collect(Collectors.toList());
		finishedTime = System.currentTimeMillis();
		System.out.println("Elapsed time normal: " + (double)(finishedTime - currentTime)/1000.0);
	}
	
	/**
	 * Returns a score based on different parameters such as #coins, #victoryPoints etc.
	 * @param board the current Board
	 * @param player the considered Player
	 * @return an estimate of the score for the given state. Higher is better
	 */
	public double evaluate(final Board board, final Player player) {
		int maxCityRegion = 0;
		for (Region reg : board.getRegions()) {
			int c = 0;
			for (City city : reg.getCities())
				if (player.hasBuiltIn(city))
					c++;
			maxCityRegion = Math.max(maxCityRegion, c);
		}
		
		return (coinFactor * player.getCoins() +
				assistantFactor * player.getAssistants() +
				nobilityFactor * player.getNobility() + 
				pointFactor * player.getVictoryPoints() + 
				emporiumFactor * player.getBuildedEmporia().size() + 
				maxCitiesRegionFactor * maxCityRegion) /
				(coinFactor + assistantFactor + nobilityFactor + pointFactor + emporiumFactor + maxCitiesRegionFactor);
		
	}
	    
    /**
     * Computes all moves with a parallel stream
     * @param initialState
     * @return a ParallelStream of possible moves for initialState
     */
    public static Stream<List<Move>> allMovesParallel(MoveState initialState) {
    	return moveCombinationParallelStream(initialState, Collections.emptyList());
    }
    
    private static Stream<List<Move>> moveCombinationParallelStream(MoveState initialState, List<Move> current) {
    	List<Move> moves = initialState.allMovesOptimized();
    	Stream<List<Move>> stream =  moves.parallelStream().flatMap(move -> {
    		if (move.valid()) {
    			move.setDrawCard(false);
    			move.execute();
    			List<Move> list = new ArrayList<>(current);
    			list.add(move);
    			return moveCombinationParallelStream(move.getCurrentState(), list);
    		}
    		else
    			return Stream.empty();
    	});
    	
    	if (initialState.endTurn())
    		stream = Stream.concat(Stream.of(current), stream);
    	return stream;
    }
    
    /**
     * Computes all moves with a sequential stream
     * @param initialState
     * @return a Stream of possible moves for initialState
     */
    public static Stream<List<Move>> allMovesNormal(MoveState initialState) {
    	return moveCombinationStream(initialState, Collections.emptyList());
    }
    
    private static Stream<List<Move>> moveCombinationStream(MoveState initialState, List<Move> current) {
    	List<Move> moves = initialState.allMovesOptimized();
    	Stream<List<Move>> stream =  moves.stream().flatMap(move -> {
    		if (move.valid()) {
    			move.setDrawCard(false);
    			move.execute();
    			List<Move> list = new ArrayList<>(current);
    			list.add(move);
    			return moveCombinationStream(move.getCurrentState(), list);
    		}
    		else
    			return Stream.empty();
    	});
    	
    	if (initialState.endTurn())
    		stream = Stream.concat(Stream.of(current), stream);
    	return stream;
    }
    
    
    public double getCoinFactor() {
		return coinFactor;
	}

	public void setCoinFactor(double coinFactor) {
		this.coinFactor = coinFactor;
	}

	public double getAssistantFactor() {
		return assistantFactor;
	}

	public void setAssistantFactor(double assistantFactor) {
		this.assistantFactor = assistantFactor;
	}

	public double getNobilityFactor() {
		return nobilityFactor;
	}

	public void setNobilityFactor(double nobilityFactor) {
		this.nobilityFactor = nobilityFactor;
	}

	public double getPointFactor() {
		return pointFactor;
	}

	public void setPointFactor(double pointFactor) {
		this.pointFactor = pointFactor;
	}

	public double getEmporiumFactor() {
		return emporiumFactor;
	}

	public void setEmporiumFactor(double emporiumFactor) {
		this.emporiumFactor = emporiumFactor;
	}

	public double getMaxCitiesRegionFactor() {
		return maxCitiesRegionFactor;
	}

	public void setMaxCitiesRegionFactor(double maxCitiesRegionFactor) {
		this.maxCitiesRegionFactor = maxCitiesRegionFactor;
	}
	
	protected void setTag(String tag) {
		this.tag = tag;
	}
	
	protected String getTag() {
		return tag;
	}
	
	@Override
	public String toString() {
		return "\ncoinFactor: " + coinFactor + "\n" + 
		"assistantFactor: " + assistantFactor + "\n" +
		"nobilityFactor: " +  nobilityFactor + "\n" +
		"pointFactor: " + pointFactor + "\n" +
		"emporiumFactor: " + emporiumFactor + "\n" +
		"maxCitiesRegionFactor: " + maxCitiesRegionFactor;
	}

}
