package ai;

import cof.commands.Command;
import cof.commands.PassTurnCommand;
import cof.commands.PollBonusCommand;
import cof.commands.TurnState;
import cof.model.Board;
import cof.model.Player;


/**
 * A Move represent all the context in which a command is being executed, the Board, the
 * Player, the TurnState and the Command to be executed
 * @author Andrea
 *
 */
public class Move {
	MoveState initialState;
	
	private Command command;
	private boolean executed = false;

	private boolean drawCard;
	private static final Command passTurn = new PassTurnCommand();
	private static final PollBonusCommand pollBonus = new PollBonusCommand();
	
	/**
	 * Creates a new Move with a copy of the associated MoveState and command
	 * @param state the context in which the command will be executed
	 * @param command the command to be executed
	 */
	public Move(MoveState state, Command command) {
		this(state.getBoard(), state.getPlayer(), state.getState(), command);
	}
	
	/**
	 * Creates a new Move with the specified parameters. All the variables are copied to a
	 * MoveState
	 * @param board the current Board
	 * @param player the current Player
	 * @param state the current State
	 * @param command the current Command to be executed
	 */
	public Move(Board board, Player player, TurnState state, Command command) {
		this.initialState = new MoveState(board, player, state);
		this.command = command;
		this.drawCard = true;
	}
	
	/**
	 * If set to false all the drawCard bonuses won't be executed
	 * @param drawCard
	 */
	public void setDrawCard(boolean drawCard) {
		this.drawCard = drawCard;
	}
	
	/**
	 * Returns true if the method was already executed.
	 * @return
	 */
	public boolean executed() {
		return executed;
	}
	
	/**
	 * Returns true if the command is valid for this state
	 * @return
	 */
	public boolean valid() {
		if (!executed)
			return command.isValid(initialState.getPlayer(), initialState.getBoard(), initialState.getState());
		else
			throw new IllegalStateException();
	}
	
	/**
	 * Executes the relative command only if it is not previously executed
	 */
	public void execute() {
		if (!executed) {
			command.execute(initialState.getPlayer(), initialState.getBoard(), initialState.getState());
			pollBonuses(initialState.getBoard(), initialState.getState(), initialState.getPlayer());
			executed  = true;
		}
	}
	
	
	/**
	 * Check whether the state after the command execution is a terminal state
	 * @return true if a pass turn can be done in current state (the command is terminal)
	 * @throws IllegalStateException if the command was not already executed
	 */
	public boolean endTurn() {
		if (executed)
			return passTurn.isValid(initialState.getPlayer(), initialState.getBoard(), initialState.getState());
		else
			throw new IllegalStateException();
	}
	
	private void pollBonuses(Board boardCopy, TurnState stateCopy, Player playerCopy) {
		pollBonus.setDrawCard(drawCard);
		while (stateCopy.canPollStaticBonus())
			pollBonus.execute(playerCopy, boardCopy, stateCopy);
	}
	
	@Override
	public String toString() {
		return command.toString();
	}
	
	
	
	public Board getBoard() { return initialState.getBoard(); }
	public Player getPlayer() { return initialState.getPlayer(); }
	public TurnState getState() { return initialState.getState(); }
	public MoveState getCurrentState() { return initialState; }
	public Command getCommand() { return command; }
}
