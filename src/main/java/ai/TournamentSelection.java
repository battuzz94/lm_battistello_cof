package ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import cof.bonus.BasicBonus;
import cof.commands.Command;
import cof.commands.PollBonusCommand;
import cof.commands.TurnState;
import cof.model.Board;
import cof.model.Card;
import cof.model.Player;
import common.Algorithms;
import common.Tuple3;
import common.Utility;

/**
 * This class serves as an utility to find best parameters for AI agents
 * @author andrea
 *
 */
public class TournamentSelection {
	private static final int EMPORIA_TO_FINISH = 10;
	private static final BasicBonus MAJOR_NOBILITY_BONUS = new BasicBonus(0,0,5,0,0);
	private static final BasicBonus MINOR_NOBILITY_BONUS = new BasicBonus(0,0,2,0,0);
	private static final boolean DEBUG = false;
	
	private TournamentSelection() {}
	
	/**
	 * Starts a tournament. This script will ask for number of evaluators (total) and number
	 * of evaluators per game. Then all the calculations will be computed in parallel and the 
	 * result printed as output
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		print("How many agents you want to compare? ");
		int agents = in.nextInt();
		print("How many players per game? ");
		int chunkSize =  in.nextInt();
		
		
		List<Evaluator> evaluators = new ArrayList<>();
		for (int i = 0; i < agents; i++)
			evaluators.add(Evaluator.generateRandomEvaluator());
		
		
		for (int i = 0; i < evaluators.size(); i++) {
			evaluators.get(i).setTag( "Evaluator" + i);
			print(evaluators.get(i).getTag() + ": " + prettyPrint(evaluators.get(i)));
		}
		
		int iteration = 0;
		while (evaluators.size() > 1) {
			print("Running iteration: " + iteration);
			evaluators = Algorithms.chunk(evaluators, chunkSize).parallelStream()
					.map(eval -> {
						Evaluator best = selectWinner(eval);
						print(best.getTag() + " won against " + eval.stream().map(e -> e.getTag()).collect(Collectors.toList()));
						return best;
					})
					.collect(Collectors.toList());
			
			Collections.shuffle(evaluators);
			print ("\nEvaluators remaining: " + evaluators.stream().map(e -> e.getTag()).collect(Collectors.toList()) + "\n\n");
			
			iteration++;
		}
		
		print("Best model overall is: " + evaluators.get(0));
		in.close();
	}


	private static void print(String string) {
		System.out.println(string);
		
	}


	private static String prettyPrint(Evaluator evaluator) {
		return String.format("Ass: %.2f  Coin: %.2f  Emp: %.2f  MCR: %.2f  Nob: %.2f  Point: %.2f", evaluator.getAssistantFactor(), evaluator.getCoinFactor(), evaluator.getEmporiumFactor(), evaluator.getMaxCitiesRegionFactor(), evaluator.getNobilityFactor(), evaluator.getPointFactor());
	}


	/**
	 * Runs a full game with given AI evaluators
	 * @param evaluators
	 * @return
	 */
	public static Evaluator selectWinner(List<Evaluator> evaluators) {
		if (evaluators.size() == 1)
			return evaluators.get(0);
		int coins = 10;
		int assistants = 3;
		List<Player> players = new ArrayList<>();
		for (int i = 0; i < evaluators.size(); i++) {
			Player p = new Player(coins++, assistants++);
			for (int j = 0; j < 5; j++)
				p.obtain(Card.drawCard());
			players.add(p);
		}
		
		List<Tuple3<Player, Evaluator, TurnState>> aiPlayers = Algorithms.zip(players, evaluators, Collections.nCopies(evaluators.size(), new TurnState()));
		
		Board board = Utility.loadTestBoard();
		
		int currentPlayerIndex = 0;
		
		boolean gameFinished = false;
		while (!gameFinished) {
			Player currentPlayer = aiPlayers.get(currentPlayerIndex).getFirst();
			currentPlayer.obtain(Card.drawCard());
			Evaluator currentEvaluator = aiPlayers.get(currentPlayerIndex).getSecond();
			TurnState currentTurnState = aiPlayers.get(currentPlayerIndex).getThird();
			currentTurnState.reset();
			
			MoveState currentState = new MoveState(board, currentPlayer, currentTurnState);
			List<Move> moves = Evaluator.getBestMoveParallel(currentState, currentEvaluator);
			
			for (Move m : moves) {
				Command c = m.getCommand();
				if (DEBUG)
					print("Player: " + currentPlayerIndex + " executes " + c);
				if (c.isValid(currentPlayer, board, currentTurnState)) {
					executeCommand(board, currentPlayer, currentTurnState, c);
				}
			}
			
			currentPlayerIndex = (currentPlayerIndex + 1) % aiPlayers.size();
			gameFinished = checkFinished(aiPlayers.get(currentPlayerIndex).getFirst());
		}
		
		finalizeGame(aiPlayers);
		
		aiPlayers.sort((p1, p2) -> Double.compare(p2.getFirst().getNobility(), p1.getFirst().getNobility()) );
		
		
		
		return aiPlayers.get(0).getSecond();
	}


	private static void executeCommand(Board board, Player currentPlayer, TurnState currentTurnState, Command c) {
		c.execute(currentPlayer, board, currentTurnState);
		while (currentTurnState.canPollStaticBonus())
			(new PollBonusCommand()).execute(currentPlayer, board, currentTurnState);
	}
	
	private static boolean checkFinished(Player player) {
		return player.getBuildedEmporia().size() == EMPORIA_TO_FINISH;
	}
	
	
	private static void finalizeGame(List<Tuple3<Player, Evaluator, TurnState>> aiPlayers) {
		List<Player> orderedPlayers = aiPlayers.stream().map(p -> p.getFirst())
				.sorted((p1, p2) -> Integer.compare(p1.getNobility(), p2.getNobility()))
				.collect(Collectors.toList());
		
		int m = orderedPlayers.size() - 1;
		int i = m;
		assert m >= 2;
		
		while(i >= 0 && orderedPlayers.get(m).getNobility() == orderedPlayers.get(i).getNobility()) {
			orderedPlayers.get(i).obtain(MAJOR_NOBILITY_BONUS);
			i--;
		}
		
		if (i == m-1) {
			m = i;
			while(i >= 0 && orderedPlayers.get(m).getNobility() == orderedPlayers.get(i).getNobility()) {
				orderedPlayers.get(i).obtain(MINOR_NOBILITY_BONUS);
				i--;
			}
		}
		
	}
}
