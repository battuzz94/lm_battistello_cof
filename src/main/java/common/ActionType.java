package common;
/**
 * These are the Type of Action Available, 
 * each sent or received Message has got a type.
 * @author Simone
 *
 */
public enum ActionType {
	MAIN_ACTION, 
	QUICK_ACTION, 
	SELL_MARKET_ACTION,
	BUY_MARKET_ACTION, 
	GENERIC_COMMAND,
	NOBILITY_ACTION,
	PASS_TURN,
	TIME_OUT_NOTIFICATION, // sent from server
	INFO_TIME_OUT,
	INFO, // bidirectional
	ADDITIONAL_INPUT_REQUIRED, //sent from server
	BOARD_UPDATE,
	INIT_TURN,
	INIT_MARKET_TURN,
	BAD_MOVE,
	WAIT_CONFIGURATION,
	CONFIGURE_BOARD,
	BOARD_CONFIGURATION,
	START_TIMEOUT,
	GAME_INIT,
	PLAYER_UPDATE, // sent from server
	GOOD_MOVE,
	OTHER_UPDATE,
	MARKET_UPDATE, 
	AI_AGENTS,
	TURN_FINISHED,
	GAME_FINISHED, 
	TIMER; 
}
