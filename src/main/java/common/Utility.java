package common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import cof.bonus.BonusTypeAdapter;
import cof.commands.CommandTypeAdapter;
import cof.market.MarketTypeAdapter;
import cof.model.Board;
import cof.model.City;
import cof.model.Permit;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class Utility {
	private static final Logger LOGGER = Logger.getLogger(Utility.class.getName());
	private static final Random RANDOM = new Random();
	public static final boolean DEBUG = false;
	
	private Utility() {}
	
	/**
	 * Creates returns the Gson serializer properly configurated 
	 * @return a Gson serializer/deserializer properly configurated
	 */
	public static Gson getJsonSerializer() {
		GsonBuilder bld = new GsonBuilder();
		bld.setPrettyPrinting();
		bld.registerTypeAdapter(Board.class, Board.getSerializer());
		bld.registerTypeAdapter(Board.class, Board.getJsonDeserializer());
		bld.registerTypeAdapter(City.class, City.getSerializer());
		bld.registerTypeAdapter(City.class, City.getDeserializer());
		bld.registerTypeAdapter(Permit.class, Permit.getSerializer());
		bld.registerTypeAdapter(Permit.class, Permit.getDeserializer());
		
		bld.registerTypeAdapterFactory(BonusTypeAdapter.getAdapter());
		bld.registerTypeAdapterFactory(BonusTypeAdapter.getStaticBonusAdapter());
		bld.registerTypeAdapterFactory(BonusTypeAdapter.getDynamicBonusAdapter());
		bld.registerTypeAdapterFactory(CommandTypeAdapter.getCommandAdapter());
		bld.registerTypeAdapterFactory(MarketTypeAdapter.getMarketAdapter());
		bld.registerTypeAdapterFactory(BonusTypeAdapter.getStaticBonusAdapter());
		return bld.create();
	}
	
	public static Board loadBoardFromFile(File file) throws FileNotFoundException {
		Scanner in = new Scanner(file);
	
		String jsonMap = "";
		while (in.hasNextLine())
			jsonMap += in.nextLine();
		in.close();
		Gson gson = Utility.getJsonSerializer();
		return gson.fromJson(jsonMap, Board.class);
    }
	
	/**
	 * Loads a test board from the test resources
	 * @return the Board built from configuration file
	 */
	public static Board loadTestBoard() {
		Gson gson = Utility.getJsonSerializer();
		String json = "";
		try {
			FileInputStream file=new FileInputStream("src/test/resources/testMap.json");
			Scanner in = new Scanner(file);
			while (in.hasNext())
				json += in.nextLine();
			in.close();
			file.close();
		}
		catch(Exception e) {
			LOGGER.log(Level.WARNING, "Could not read board from file", e);
		}
		
		Board b = null;
		
		try {
			b = gson.fromJson(json, Board.class);
		}
		catch(JsonSyntaxException e) {
			LOGGER.log(Level.WARNING, "Could not deserialize test board", e);
			b = null;
		}
		return b;
	}
	
	/**
	 * Loads a test board already disposed from the test resources
	 * @return the Board built from configuration file
	 */
	public static Board loadTestBoardDisposed() {
		Gson gson = Utility.getJsonSerializer();
		String json = "";
		try {
			FileInputStream file=new FileInputStream("src/test/resources/testMapWithCoordinates.json");
			Scanner in = new Scanner(file);
			while (in.hasNext())
				json += in.nextLine();
			in.close();
		}
		catch(Exception e) {
			LOGGER.log(Level.WARNING, "Could not read board from file", e);
		}
		
		Board b = null;
		
		try {
			b = gson.fromJson(json, Board.class);
		}
		catch(JsonSyntaxException e) {
			LOGGER.log(Level.WARNING, "Could not deserialize test board", e);
		}
		return b;
	}
	
	/**
	 * Performs a serialization and a deserialization of the object in order to have a 
	 * distinct deep copy of the original element. 
	 * Please note that the original object must be Serializable and some transient fields
	 * will not be copied. 
	 * @param orig
	 * @return
	 */
	public static Object getDeepCopy(Object orig) {
		Object obj = null;
        try {
            // Write the object out to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(orig);
            out.flush();
            out.close();

            // Make an input stream from the byte array and read
            // a copy of the object back in.
            ObjectInputStream in = new ObjectInputStream(
                new ByteArrayInputStream(bos.toByteArray()));
            obj = in.readObject();
            in.close();
        }
        catch(IOException | ClassNotFoundException e) {
           LOGGER.log(Level.WARNING, "Could not copy " + orig, e);
        }
        return obj;
	}
	
	/**
	 * Generates a random number between min (inclusive) and max (exclusive) 
	 * @param min minimum value of the range
	 * @param max maximum value of the range
	 * @return a random integer between min and max
	 * @throws IllegalArgumentException if one of the two numbers are negative
	 */
	public static int generateRandomNumber(int min, int max) {
		if(max>min)
			return (RANDOM.nextInt(max-min))+min;
		else if(max==min)
			return max;
		else
			return (RANDOM.nextInt(min-max))+max;
	}

	/**
	 * Prints the object on standard output
	 * @param o
	 */
	public static void printDebug(Object o) {
		if (DEBUG) 
			if (o instanceof String)
				System.out.println(StringUtils.abbreviate((String)o, 500));
			else
				System.out.println(o);
	}
	
	
	public static Pane encapsulate(Collection<? extends Node> elements) {
		Pane ret = new Pane();
		ret.getChildren().addAll(elements);
		return ret;
	}
	
	public static <T extends Pane> T encapsulate(T root, Collection<? extends Node> elements) {
		root.getChildren().addAll(elements);
		return root;
	}
	
	public static <T extends Pane> T encapsulate(T root, Node...nodes) {
		root.getChildren().addAll(nodes);
		return root;
	}
}



