package common;

public class Tuple3<T1, T2, T3> {
	T1 fst;
	T2 snd;
	T3 trd;
	public Tuple3(T1 fst, T2 snd, T3 trd) {
		this.fst = fst;
		this.snd = snd;
		this.trd = trd;
	}
	
	public T1 getFirst() { return fst; }
	public T2 getSecond() { return snd; }
	public T3 getThird() { return trd; }
	
}