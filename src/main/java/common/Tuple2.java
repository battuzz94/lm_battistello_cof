package common;

public class Tuple2<T1, T2> {
	T1 fst;
	T2 snd;
	public Tuple2(T1 fst, T2 snd) {
		this.fst = fst;
		this.snd = snd; 
	}
	
	public T1 getFirst() { return fst; }
	public T2 getSecond() { return snd; }
	@Override
	public String toString() {
		return "(" + fst + "," + snd + ")";
	}
}