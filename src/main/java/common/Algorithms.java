package common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * This class is a set of algorithmic functions that are useful for the development
 * @author Andrea
 *
 */
public class Algorithms {
	private Algorithms() {}
	
	/**
	 * Choose a random element from a list
	 * @param list the list of elements
	 * @return one element of the list chosen randomly
	 */
	public static <T> T chooseFromList(List<T> list) {
		Random random = new Random();
		int index = random.nextInt(list.size());
		return list.get(index);
	}
	
	/**
	 * Generates all the possibile distinct pair of values from a list of elements
	 * @param list the list of elements to start with
	 * @return a list of ordered distinct pairs of elements
	 */
	public static <T> List<Tuple2<T, T>> getCouples(List<T> list) {
		List<Tuple2<T,T>> ret = new LinkedList<>();
		
		for (int i = 0; i < list.size(); i++) {
			for (int j = i+1; j < list.size(); j++) {
				ret.add(new Tuple2<T, T>(list.get(i), list.get(j)));
			}
		}
		return ret;
	}
	
	/**
	 * Generates all the possibile distinct couples of values from two lists of elements
	 * @param list1 the list of elements to start with
	 * @param list2 the list of elements to end with
	 * @return a list of ordered distinct couples of elements
	 */
	public static <T> List<Tuple2<T, T>> getCouples(List<T> list1, List<T> list2) {
		List<Tuple2<T, T>> ret = new LinkedList<>();
		for (int i = 0; i < list1.size(); i++) {
			for (int j = 0; j < list2.size(); j++) {
				ret.add(new Tuple2<T, T>(list1.get(i), list2.get(j)));
			}
		}
		return ret;
	}
	
	
	/**
	 * Generates all the possibile distinct triplets of values from a list of elements
	 * @param list the list of elements to start with
	 * @return a list of ordered distinct triplets of elements
	 */
	public static <T> List<Tuple3<T,T,T>> getTriplets(List<T> list) {
		List<Tuple3<T, T,T>> ret = new LinkedList<>();
		
		for (int i = 0; i < list.size(); i++) {
			for (int j = i+1; j < list.size(); j++) {
				for (int k = j+1; k < list.size(); k++)
					ret.add(new Tuple3<T, T, T>(list.get(i), list.get(j), list.get(k)));
			}
		}
		return ret;
	}
	
	/**
	 * Creates a disjoint set structure for the given collection
	 * @param list
	 * @return a disjoint set
	 */
	public static <T> int[] makeSet(Collection<T> list) {
		int[] set = new int[list.size()];
		for (int i = 0; i < list.size(); i++)
			set[i] = i;
		return set;
	}
	
	private static int find(int[] set, int a) {
		if (set[a] != a)
			set[a] = find(set, set[a]);
		return set[a];
	}
	
	/**
	 * Joins the element with index a and the element with index b in the same set.
	 * @param set the previously created set
	 * @param a index of first element to join
	 * @param b index of second element to join
	 */
	public static void union(int[] set, int a, int b) {
		int x = find(set, a);
		int y = find(set, b);
		set[x] = y;
	}
	
	/**
	 * Check if the two elements are in the same set or not
	 * @param set the previously created set
	 * @param a index of first element
	 * @param b index of second element
	 * @return true if set[a] is in the same set of set[b]
	 */
	public static boolean sameSet(int[] set, int a, int b) {
		return find(set, a) == find(set, b);
	}
	
	/**
	 * Checks if the line (p1, p2) intersects the line (p3, p4)
	 * @param p1
	 * @param p2
	 * @param p3
	 * @param p4
	 * @return true if the two lines intersects
	 */
	public static boolean intersects(Point p1, Point p2, Point p3, Point p4) {
		if (Double.compare(p2.x(), p3.x()) == 0 && Double.compare(p2.y(), p3.y()) == 0)
			return false;
		return (ccw(p1, p3, p4) != ccw(p2, p3, p4)) && (ccw(p1, p2, p3) != ccw(p1, p2, p4));
	}
	
	private static boolean ccw(Point p1, Point p2, Point p3) {
		if (p1 == null || p2 == null || p3 == null) 
			return false;
		return (p3.y() - p1.y()) * (p2.x() - p1.x()) > (p2.y() - p1.y()) * (p3.x() - p1.x());
	}
	
	/**
	 * Combines all the elements in a list of tuples with all the corresponding elements of the other list
	 * @param c1 the first list
	 * @param c2 the second list
	 * @return a list of tuples
	 * @throws IllegalStateException if the lists haven't the same length
	 */
	public static <T,U> List<Tuple2<T, U>> zip (List<T> c1, List<U> c2) {
		if (c1.size() != c2.size())
			throw new IllegalStateException();
		List<Tuple2<T,U>> ret = new ArrayList<>();
		for (int i = 0; i < c1.size(); i++) {
			ret.add(new Tuple2<>(c1.get(i), c2.get(i)));
		}
		return ret;
	}
	
	/**
	 * Combines all the elements in a list of tuples 
	 * @param c1 the first list
	 * @param c2 the second list
	 * @param c3 the third list
	 * @return a list of triplets with the corresponding elements
	 * @throws IllegalStateException if the lists haven't the same length
	 */
	public static <T,U,S> List<Tuple3<T, U, S>> zip (List<T> c1, List<U> c2, List<S> c3) {
		if (c1.size() != c2.size() || c1.size() != c3.size() || c2.size() != c3.size())
			throw new IllegalStateException();
		List<Tuple3<T,U, S>> ret = new ArrayList<>();
		for (int i = 0; i < c1.size(); i++) {
			ret.add(new Tuple3<>(c1.get(i), c2.get(i), c3.get(i)));
		}
		return ret;
	}
	
	/**
	 * Partitions the given list in chunks of at most chunkSize elements
	 * @param toChunkList the list of elements to partition
	 * @param chunkSize size of each chunk
	 * @return a list of chunks, where all chunks are intended as a list of elements
	 */
	public static <T> List<List<T>> chunk(List<T> toChunkList, int chunkSize) {
		return toChunkList.stream().<List<List<T>>> reduce(new ArrayList<List<T>>(), (id, elem) -> {
			if (id.isEmpty()) {
				List<T> newChunk = new ArrayList<>();
				newChunk.add(elem);
				id.add(newChunk);
			}
			else {
				List<T> l = id.get(id.size() - 1);
				if (l.size() >= chunkSize) {
					List<T> list = new ArrayList<>();
					list.add(elem);
					id.add(list);
				}
				else
					l.add(elem);
			}
			return id;
		}, (a,b) -> {throw new UnsupportedOperationException();});
	}
}
