package common.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote interface for client
 * @author andrea
 *
 */
@FunctionalInterface
public interface ClientStub extends Remote {
	/**
	 * This method allows the server to send a message to a client.
	 * @param message
	 * @throws RemoteException
	 */
	void send(Message message) throws RemoteException;
}
