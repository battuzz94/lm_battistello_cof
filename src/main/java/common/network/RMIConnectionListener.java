package common.network;

import java.rmi.*;

/**
 * Remote interface of server. This is only used to begin connection
 * @author andrea
 *
 */
@FunctionalInterface
public interface RMIConnectionListener extends Remote {
	
	/**
	 * This method connects a client to the server through RMI. 
	 * @param clientStubImpl
	 * @param username
	 * @return
	 * @throws RemoteException
	 */
	ServerStub connect(ClientStub clientStubImpl, String username) throws RemoteException;
}
