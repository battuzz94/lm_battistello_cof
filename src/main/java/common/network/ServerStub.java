package common.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote interface of server
 * @author andrea
 *
 */
@FunctionalInterface
public interface ServerStub extends Remote {
	
	/**
	 * This message allows a client to send messages to the server.
	 * @param message
	 * @throws RemoteException
	 */
	public void send(Message message) throws RemoteException;
}
