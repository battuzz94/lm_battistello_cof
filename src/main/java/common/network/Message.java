package common.network;

import java.io.Serializable;

import com.google.gson.Gson;

import cof.commands.Command;
import common.ActionType;
import common.Utility;

/**
 * Main messaging class
 * @author andrea
 *
 */
public class Message implements Serializable {
	
	private static final long serialVersionUID = 8232105920537331949L;
	public static final transient Gson gson = Utility.getJsonSerializer();
	String content = "";
	ActionType type;
	
	/**
	 * This constructor creates an INFO message with the content specified in the parameter.
	 * The action type is automatically set to INFO.
	 * @param message
	 */
	public Message(String message) {
		this.content = message;
		this.type=ActionType.INFO;
	}
	
	/**
	 * This constructor creates a generic message by serializing the command.
	 * @param type Type of command sent
	 * @param command Command to send through the connection. 
	 */
	public Message(ActionType type, Command command){
		this.type=type;
		this.content = gson.toJson(command, Command.class);
	}
	
	/**
	 * This constructor creates a blank message; it is used where the only important
	 * part of the message is the type.
	 * @param type
	 */
	public Message(ActionType type){
		this.type=type;
	}
	
	/**
	 * This constructor creates a message given an action type and a string. 
	 * @param actionType
	 * @param string
	 */
	public Message(ActionType actionType, String string) {
		this.type=actionType;
		this.content=string;
	}
	
	public ActionType getType(){
		return this.type;
	}
	
	public String getContent(){
		return this.content;
	}
	
	@Override
	public String toString() {
		return type + ": " + content;
	}
}
