package common;

import com.google.gson.Gson;

/**
 * This class is useful to duplicate with Gson the classes. The object will be serialized and deserialized using the default Json parser
 * @author Andrea
 *
 * @param <T> the type of object to duplicate
 */
public class Duplicator<T> {
	private static final Gson gson = Utility.getJsonSerializer();
	final Class<T> type;
	
	/**
	 * Creates a new duplicator
	 * @param typeParameterClass
	 */
    public Duplicator(Class<T> typeParameterClass) {
        this.type = typeParameterClass;
    }
    
    /**
     * Duplicates the object by serializing and deserializing
     * @param anObject
     * @return
     */
    public T duplicate(T anObject) {
        String json = gson.toJson(anObject, type);
        return gson.fromJson(json, type);
    }
}
