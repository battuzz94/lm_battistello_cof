package common;
/**
 * This Class is helpful to define a 2D position.
 * @author Simone
 *
 */
public class Point extends Tuple2<Double,Double> {
	public Point(Double fst, Double snd) {
		super(fst, snd);
	}
	
	public double x() {
		return getFirst();
	}
	
	public double y() {
		return getSecond();
	}
	
	@Override
	public String toString() {
		return "(" + x() + "," + y() + ")";
	}
	
}