package common;

import java.util.Iterator;
/**
 * This is an utility class that instantiates an iterator that will iterate through an iterable
 * multiple times
 * @author andrea
 *
 * @param <T>
 */
public class CircularIterator<T> implements Iterator<T> {
    
    Iterable<T> iterable;
    Iterator<T> iterator;
    private int cycleCount;
    /**
     * Constructor requires Iterable Type.
     * @param iterable
     */
    /**
     * Creates a new circular iterator
     * @param iterable
     */
    public CircularIterator (Iterable<T> iterable) {
        this.iterable = iterable;
        this.iterator = iterable.iterator();
    }
    /**
     * This method return true, if the cycle is ended, 
     * so the next call to next will be the first element 
     * of the iterable object.
     * @return
     */
    
    public boolean isEndCycle() {
        return !iterator.hasNext();
    }
    
    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public T next() {
        if (!iterator.hasNext()){
            iterator = iterable.iterator();
            cycleCount++;
        }
        return iterator.next();
    }
    
    /**
     * This method return the number of cycle completed.
     * this counter is incremented only at the end of cycle.
     * @return
     */
    
    public int getCycleCount(){
        return cycleCount;
    }

}
