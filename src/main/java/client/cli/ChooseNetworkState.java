package client.cli;

import java.rmi.RemoteException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import common.Utility;
import common.network.Message;

/**
 * Called every time a client starts its local application; in this state the player must
 * choose their own username and the connection implementation they prefer, socket or RMI.
 * After this state (if the client has connected to server successfully), the State Pattern 
 * server-side checks wether the client is the first one to enter a room; if so, the client
 * enters the ChooseBoardState, otherwise it waits 'till the administrator of the room 
 * chooses the board (and then enters the GameRunningState).
 * @author emmeaa
 *
 */
public class ChooseNetworkState implements ClientState {
	private static final Logger LOGGER=Logger.getLogger(ChooseNetworkState.class.getName());
	
	private ConsoleCommandPool pool = ConsoleCommandPool.getInstance();
	private ConsoleController controller;
	private ConsoleView view;
	
	private boolean connected = false;
	
	private static final Pattern connectionPatternRMI=Pattern.compile("^\\s*connect\\s+(?<connectionType>rmi),?\\s+(?<name>[a-zA-Z0-9_]+)\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern connectionPatternSocket=Pattern.compile("^\\s*connect\\s+(?<connectionType>socket),?\\s+(?<name>[a-zA-Z0-9_]+)\\s*", Pattern.CASE_INSENSITIVE);
	private ConsoleCommand connectionSocketCommand;
	private ConsoleCommand connectionRMICommand;
	
	protected ChooseNetworkState(ConsoleController controller, ConsoleView view) {
		this.controller = controller;
		this.view = view;
		
		registerNetworkCommands();
	}
	
	@Override
	public void handleUserInput(String input) {
		try {
			ConsoleCommand c = pool.getCommandByPattern(input);
			c.run(input);
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINE, "input not recognized", e);
			view.notifyNotRecognizedCommand();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINE, "Caught unknown exception while handling user input", e);
			view.notifyNotRecognizedCommand();	
		}
	}

	@Override
	public void handleServerInput(Message message) {
		if (connected) {
			switch(message.getType()) {
			case TIMER:
				if ("0".equals(message.getContent()))
					view.info("Init board configuration");
				else
					view.info("Wait: " + message.getContent());
				break;
			case CONFIGURE_BOARD:
				controller.setState(new ChooseBoardState(controller, view));
				break;
			case WAIT_CONFIGURATION:
				view.info("Wait for admin to choose the board");
				break;
			case GAME_INIT:
				controller.setState(new GameRunningState(controller, view));
				controller.registerGeneralStatusCommands();
				break;
			default:
				Utility.printDebug("Received unknown message in ChooseNetworkState: " + message);
			}
		}
	}
	
	
	private void unregisterCommands() {
		pool.removeCommand(connectionRMICommand);
		pool.removeCommand(connectionSocketCommand);
	}
	
	private void registerNetworkCommands() {
		
		connectionRMICommand = ConsoleCommandPool.createCommand(connectionPatternRMI, 
				"connect RMI <username>", 
				"Connect to server through RMI with username <username>", 
				this::connectRMI);
		
		
		connectionSocketCommand = ConsoleCommandPool.createCommand(connectionPatternSocket, 
				"connect socket <username>", 
				"Connect to server through socket with username <username>", 
				this::connectSocket);
		
		pool.registerCommand(connectionRMICommand);
		pool.registerCommand(connectionSocketCommand);
	}
	
	private void changeState() {
		connected = true;
		unregisterCommands();
	}

	protected void connectSocket(String s) {
		Matcher m=connectionPatternSocket.matcher(s);
		if(m.matches()){
			String connectionType=m.group("connectionType");
			String username=m.group("name");
			try {
				controller.beginConnection(connectionType, username);
				changeState();
			} catch (RemoteException e) {
				controller.showNetworkError();
				LOGGER.log(Level.FINE, "could not connect through socket", e);
			}
		}
	}
	protected void connectRMI(String s) {
		Matcher m=connectionPatternRMI.matcher(s);
		if(m.matches()){
			String connectionType=m.group("connectionType");
			String username=m.group("name");
			try {
				controller.beginConnection(connectionType, username);
				changeState();
			} catch (RemoteException e) {
				controller.showNetworkError();
				LOGGER.log(Level.FINE, "could not connect through RMI", e);
			}
		}
	}
}
