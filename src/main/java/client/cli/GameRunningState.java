package client.cli;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cof.commands.AddPrimaryMoveCommand;
import cof.commands.BribeCouncilCommand;
import cof.commands.BribeKingCommand;
import cof.commands.BuildEmporiumCommand;
import cof.commands.ElectCouncillorCommand;
import cof.commands.ElectWithAssistantCommand;
import cof.commands.EngageAssistantCommand;
import cof.commands.GainOneCityRewardCommand;
import cof.commands.GainPermitCommand;
import cof.commands.GainPermitRewardCommand;
import cof.commands.GainTwoCityRewardCommand;
import cof.commands.ShufflePermitsCommand;
import common.ActionType;
import common.network.Message;

/**
 * Client state the client is in when the game itself is being played (not necessarily by the client
 * itself). It lasts as long as one of the clients connected is playing; when all of them have finished
 * their turn, the server notifies all of them with a message with type SELL_ITEM_COMMAND that enables
 * the change of state from GameRunningState to PreMarketState. 
 * @author emmeaa
 *
 */
public class GameRunningState implements ClientState {
	private static final Logger LOGGER = Logger.getLogger(GameRunningState.class.getName());
	
	private static final String PERMIT="permit";
	private static final String COUNCIL="council";
	private static final String COUNCILLOR="councillor";
	private static final String CITY="city";
	private static final String REGION="region";
	
	private ConsoleController controller;
	private ConsoleView view;
	private ConsoleCommandPool pool = ConsoleCommandPool.getInstance();
	
	//	NOBILITY TRACK ACTIONS
	
	private static final Pattern gainOneCityPattern=Pattern.compile("^\\s*(?<city>City\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern gainTwoCityPattern=Pattern.compile("^\\s*(?<city1>City\\d+),?\\s+(?<city2>City\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern gainPermitRewardPattern=Pattern.compile("^\\s*Gain\\s+reward\\s+(?<permit>Permit\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern gainPermitPattern=Pattern.compile("^\\s*Gain\\s+(?<permit>Permit\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	
	ConsoleCommand gainOneCityCommand;
	ConsoleCommand gainTwoCityCommand;
	ConsoleCommand gainPermitRewardCommand;
	ConsoleCommand gainPermitCommand;
	
	// 	QUICK ACTIONS
	
	private static final Pattern electWithAssistantPattern=Pattern.compile("^\\s*elect\\s+with\\s+assistant:?\\s+(?<councillor>Councillor\\d+)"
			+ "\\s+,?(in)?\\s*(?<council>Council\\d+)", Pattern.CASE_INSENSITIVE);
	private static final Pattern shufflePermitsPattern=Pattern.compile("^\\s*shuffle\\s+permits?\\s+(in\\s+region\\s+)?(?<region>Region\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern engageAssistantPattern=Pattern.compile("^\\s*engage\\s+assistant\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern addMovePattern=Pattern.compile("^\\s*add\\s+(primary\\s+)?move\\s*", Pattern.CASE_INSENSITIVE);
	
	ConsoleCommand electWithAssistantCommand;
	ConsoleCommand shufflePermitsCommand;
	ConsoleCommand engageAssistantCommand;
	ConsoleCommand addMoveCommand;
	
	//	MAIN ACTIONS
	
	private static final Pattern electCouncillorPattern=Pattern.compile("^\\s*elect\\s+(councillor\\s+)?(?<councillor>Councillor\\d+)\\s+(in\\s+)?(?<council>Council\\d+)", Pattern.CASE_INSENSITIVE);
	private static final Pattern buildEmporiumPattern=Pattern.compile("^\\s*build\\s+emporium\\s+"
			+ "(with\\s+)?(?<permit>Permit\\d+)\\s+(in\\s+)?(?<city>City\\d+)\\s*"
			, Pattern.CASE_INSENSITIVE);
	private static final Pattern bribeCouncilPattern=Pattern.compile("^\\s*Bribe\\s+(?<council>Council\\d+)\\s+(with\\s+)?((Card\\d+),?\\s+){1,4}((for\\s+)|(and\\s+take\\s+))?"
			+ "(?<permit>Permit\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern cardPattern=Pattern.compile("(Card\\d+)", Pattern.CASE_INSENSITIVE);
	private static final Pattern bribeKingCouncilPattern=Pattern.compile("^\\s*bribe\\s+king(('s)|(s))?\\s+council\\s+(with\\s+)?((Card\\d+),?\\s+){1,4},?"
			+ "\\s*(destination:?\\s+)?(?<city>City\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	
	ConsoleCommand electCouncillorCommand;
	ConsoleCommand buildEmporiumCommand;
	ConsoleCommand bribeCouncilCommand;
	ConsoleCommand bribeKingCommand;
	
	
	protected GameRunningState(ConsoleController controller, ConsoleView view) {
		this.controller = controller;
		this.view = view;
		
		registerMainActions();
		registerQuickActions();
		registerNobilityTrackCommands();
	}
	
	
	
	@Override
	public void handleUserInput(String input) {
		try {
			ConsoleCommand c = pool.getCommandByPattern(input);
			c.run(input);
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINEST, "User inserted wrong command", e);
			view.notifyNotRecognizedCommand();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINE, "Caught unknown exception while handling user input", e);
			view.notifyNotRecognizedCommand();	
		}
	}

	@Override
	public void handleServerInput(Message message) {
		if(message.getType()==ActionType.SELL_MARKET_ACTION){
			unregisterCommands();
			controller.setState(new PreMarketState(controller, view));
			view.info("All players have had their turns; starting on pre-market now.");
		}
		
		
	}
	
	private void unregisterCommands() {
		pool.removeCommand(bribeCouncilCommand);
		pool.removeCommand(bribeKingCommand);
		pool.removeCommand(buildEmporiumCommand);
		pool.removeCommand(electCouncillorCommand);
		
		pool.removeCommand(electWithAssistantCommand);
		pool.removeCommand(engageAssistantCommand);
		pool.removeCommand(addMoveCommand);
		pool.removeCommand(shufflePermitsCommand);
		
		pool.removeCommand(gainPermitRewardCommand);
		pool.removeCommand(gainOneCityCommand);
		pool.removeCommand(gainPermitCommand);
		pool.removeCommand(gainTwoCityCommand);
	}
	
	
	protected void gainCityReward(String s) {
		Matcher m=gainOneCityPattern.matcher(s);
		if(m.matches()){
			String city=m.group(CITY);
			
			controller.doNobilityTrackAction(new GainOneCityRewardCommand(city));
		}
	}
	
	protected void gainTwoCityReward(String s) {
		Matcher m=gainTwoCityPattern.matcher(s);
		if(m.matches()){
			String city1=m.group("city1");
			String city2=m.group("city2");
			
			controller.doNobilityTrackAction(new GainTwoCityRewardCommand(city1, city2));
		}
	}
	
	protected void gainTwoPermitReward(String s) {
		Matcher m=gainPermitRewardPattern.matcher(s);
		if(m.matches()){
			String permit=m.group(PERMIT);
			
			controller.doNobilityTrackAction(new GainPermitRewardCommand(permit));
		}
	}
	
	protected void gainPermit(String s) {
		Matcher m=gainPermitPattern.matcher(s);
		if(m.matches()){
			String permit=m.group(PERMIT);
			controller.doNobilityTrackAction(new GainPermitCommand(permit));
		}
	}
	
	protected void electWithAssistant(String s) {
		Matcher m=electWithAssistantPattern.matcher(s);
		if(m.matches()){
			String councillor=m.group(COUNCILLOR);
			String council=m.group(COUNCIL);
			
			controller.doQuickAction(new ElectWithAssistantCommand(council, councillor));
		}
	}
	
	private void registerNobilityTrackCommands() {
		gainOneCityCommand = ConsoleCommandPool.createCommand(
				gainOneCityPattern, 
				"<City>", 
				"Additional action: when you are asked to choose a city whose bonus you wish to take thanks to the nobility track, write its tag.", 
				this::gainCityReward
			);
		
		gainTwoCityCommand = ConsoleCommandPool.createCommand(gainTwoCityPattern, 
				"<City1>, <City2>", 
				"Additional action: when you are asked to choose two cities whose bonuses you wish to take thanks to the nobility track, write their tags.",
				this::gainTwoCityReward);
		
		gainPermitRewardCommand = ConsoleCommandPool.createCommand(gainPermitRewardPattern, 
				"Gain reward <Permit>", 
				"Additional action: when you are asked to choose a permit you own whose bonus you wish to take again thanks to the nobility track, write its tag.",
				this::gainTwoPermitReward);
		
		
		gainPermitCommand = ConsoleCommandPool.createCommand(gainPermitPattern, "Gain <Permit>", 
				"Additional action: when you are asked to choose a permit among those obtainable on the board"
				+ " whose bonus you wish to take thanks to the nobility track, write its tag.",
				this::gainPermit);
		
		
		pool.registerCommand(gainOneCityCommand);
		pool.registerCommand(gainTwoCityCommand);
		pool.registerCommand(gainPermitCommand);
		pool.registerCommand(gainPermitRewardCommand);
	}

	private void registerQuickActions() {
		
		electWithAssistantCommand = ConsoleCommandPool.createCommand(electWithAssistantPattern, "Elect with assistant: <councillor> <council>", 
				"QUICK ACTION: elect a councillor (named by its tag) of your choice among those available to put into the council "
				+ "(again named by its tag) either of a region or the king's in exchange for one of your assistants. You do not gain"
				+ "any coins performing this action.", 
				this::electWithAssistant);
		
		shufflePermitsCommand = ConsoleCommandPool.createCommand(shufflePermitsPattern, 
				"Shuffle permits in region <Region>", 
				"QUICK ACTION: shuffle the permits in the region tagged <Region>",
				this::shufflePermits);
		
		engageAssistantCommand = ConsoleCommandPool.createCommand(engageAssistantPattern, 
				"Engage assistant", 
				"QUICK ACTION: increases by one the number of assistants at your disposal, for a price of three coins", 
				this::engageAssistant);
		
		addMoveCommand = ConsoleCommandPool.createCommand(addMovePattern, 
				"Add primary move", 
				"QUICK ACTION: lets you do another main action during your turn, at the cost of three assistants", 
				this::addMove);
		
		pool.registerCommand(addMoveCommand);
		pool.registerCommand(electWithAssistantCommand);
		pool.registerCommand(engageAssistantCommand);
		pool.registerCommand(shufflePermitsCommand);
	}

	protected void addMove(String s) {
		Matcher m=addMovePattern.matcher(s);
		if(m.matches()){
			controller.doQuickAction(new AddPrimaryMoveCommand());
		}
	}

	protected void engageAssistant(String s) {
		Matcher m=engageAssistantPattern.matcher(s);
		if(m.matches()){						
			controller.doQuickAction(new EngageAssistantCommand());
		}
	}

	protected void shufflePermits(String s) {
		Matcher m=shufflePermitsPattern.matcher(s);
		if(m.matches()){
			String region=m.group(REGION);
			
			controller.doQuickAction(new ShufflePermitsCommand(region));
		}
	}

	private void registerMainActions() {
		
		electCouncillorCommand = ConsoleCommandPool.createCommand(electCouncillorPattern, 
				"Elect Councillor <councillor> <council>", 
				"MAIN ACTION: elect a councillor (named by its tag) of your choice among those available to put into the council "
				+ "(again named by its tag) either of a region or the king's.", 
				this::electCouncillor);
		
		buildEmporiumCommand = ConsoleCommandPool.createCommand(buildEmporiumPattern, 
				"Build Emporium with <Permit> in <City>", 
				"MAIN ACTION: builds an emporium using a permit you own in a city of your choice (the city must be listed in the permit)", 
				this::buildEmporium);
	
		bribeCouncilCommand = ConsoleCommandPool.createCommand(bribeCouncilPattern, "Bribe <Region's Council> with <Cards> for <Permit>", "MAIN ACTION: bribe a region's council tagged <Council> "
				+ "with one to four matching politic cards tagged <Card> to buy an available permit tagged <Permit>", 
				this::bribeCouncil);
		
		bribeKingCommand = ConsoleCommandPool.createCommand(bribeKingCouncilPattern,"Bribe king's council with <Cards>, destination: <City>", "MAIN ACTION: bribe the king's council"
				+ "with one to four matching politic cards tagged <Card> to move the king to the destionation chosen (tagged <City>) and build and"
				+ "emporium there.", 
				this::bribeKing);
		
		pool.registerCommand(bribeCouncilCommand);
		pool.registerCommand(bribeKingCommand);
		pool.registerCommand(buildEmporiumCommand);
		pool.registerCommand(electCouncillorCommand);
	}

	protected void bribeKing(String s) {
		Matcher mInput=bribeKingCouncilPattern.matcher(s);
		if(mInput.matches()){
			String destination=mInput.group(CITY);
			
			ArrayList<String> cards=new ArrayList<>();
			Matcher mCards=cardPattern.matcher(s);
			
			while(mCards.find())
				cards.add(mCards.group());
			
			controller.doMainAction(new BribeKingCommand(cards, destination));
		}
	}

	protected void bribeCouncil(String s) {
		Matcher mInput=bribeCouncilPattern.matcher(s);
		if(mInput.matches()){
			String permit=mInput.group(PERMIT);
			String council=mInput.group(COUNCIL);
			
			List<String> cards=new ArrayList<>();
			Matcher mCards=cardPattern.matcher(s);
			
			while(mCards.find())
				cards.add(mCards.group());
			
			controller.doMainAction(new BribeCouncilCommand(cards, council, permit));
		}
	}

	protected void buildEmporium(String s) {
		Matcher m=buildEmporiumPattern.matcher(s);
		if(m.matches()){
			String permit=m.group(PERMIT);
			String city=m.group(CITY);
			
			controller.doMainAction(new BuildEmporiumCommand(permit, city));
		}
	}

	protected void electCouncillor(String s) {
		Matcher m=electCouncillorPattern.matcher(s);
		if(m.matches()){
			String councillor=m.group(COUNCILLOR);
			String council=m.group(COUNCIL);
			
			controller.doMainAction(new ElectCouncillorCommand(council, councillor));
		}
	}

}
