package client.cli;

import java.util.regex.Pattern;
/**
 * This is the structure of a ConsoleCommand.
 * @author Simone
 *
 */
public interface ConsoleCommand {
	public String getCommandName();
	public String getCommandDescription();
	public void run(String input);
	public Pattern getCommandPattern();
}
