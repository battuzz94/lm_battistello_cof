package client.cli;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.market.Market;
import cof.market.Sellable;
import cof.model.Board;
import cof.model.Card;
import cof.model.Permit;
import cof.model.Player;
import cof.model.PrettyPrinter;
import cof.model.Region;

/**
 * The console view reads and writes text on console
 * @author andrea
 *
 */
public class ConsoleView {
	private static final Logger log = Logger.getGlobal();
	
	ConsoleController listener;
	InputListenerThread inputThread;
	boolean running = false;
	ConsoleCommandPool pool = ConsoleCommandPool.getInstance();
	
	/**
	 * Creates a new console view
	 */
	public ConsoleView() {
		this.listener = new ConsoleController(this);
	}
	/**
	 * This method sets the Controlle to the given.
	 * @param c
	 */
	public void setListener(ConsoleController c) {
		this.listener = c;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	private void print(Object o) {
		if (o instanceof String[]) {
			String[] toPrint = (String[]) o;
			for (String line : toPrint)
				print(line);
		}
		else 
			System.out.println(o);
	}
	
	
	
	protected void showHelp() {
		for (ConsoleCommand c : pool.getCommands())
			print("– " + c.getCommandName() + " –––––––> " + c.getCommandDescription() + "\n");
	}
	
	
	protected void showCards(Player p) {
		for (Card c : p.getCards())
			print(c);
	}
	
	
	protected void showCoins(Player p){
		print(p.getCoins());
	}
	
	
	protected void showAssistants(Player p){
		print(p.getAssistants());
	}
	
	
	protected void showStatus(Player p){
		print("Coins: "+p.getCoins());
		print("Assistants: "+p.getAssistants());
		print("Nobility Track: "+p.getNobility());
		print("Victory Points: "+p.getVictoryPoints());
		print("Cards: "+p.getCards());
		print("Permits: "+p.getPermits());
	}
	
	protected void showBoard(Board b){
		PrettyPrinter pp = new PrettyPrinter(b);
		print(pp.printBoard());
	}
	
	
	protected void showBoard(Board b, Player p){
		PrettyPrinter pp = new PrettyPrinter(b, p);
		print(pp.printBoard());
	}
	
	
	protected void showCouncil(Region r){
		print(r.getRegionCouncil());
	}
	
	protected void showKingStatus(Board b){
		print("King's current position: "+ b.getKingPosition());
		print("King's council: "+ b.getKingCouncil());
	}
	
	protected void showGainablePermits(Board b){
		List<Region> reg=b.getRegions();
		for(Region r : reg){
			print(r.getGainablePermits());
		}
	}
	
	protected void showGameRules(){
		Scanner in;
		String line;
		
		try{
			FileInputStream file=new FileInputStream("src/main/resources/HOW_TO_PLAY.txt");
			in=new Scanner(file);
			
			
			while(in.hasNext()){
				line=in.nextLine();
				print(line);
			}
			
			in.close();
			file.close();
			
		} catch(IOException e){
			log.log(Level.FINE, "Could not open file", e);
		}
	}
	
	
	
	
	protected void showAvailableCouncillors(Board b){
		PrettyPrinter pp = new PrettyPrinter(b);
		print(pp.printAvailableCouncillors());
	}
	
	
	
	/**
	 * Starts the view and enables the input thread to receive user input
	 */
	public void start() {
		running = true;
		
		startView();
		print("Welcome to Council of Four client");
		print("How to play:");
		showHelp();
	}
	
	
	/**
	 * Stops the view and the listening thread
	 */
	public void stop() {
		print("Bye!");
		running = false;
		inputThread.stopListening();
		inputThread.interrupt();
	}
	
	
	
	protected void notifyNotRecognizedCommand() {
		print("The command you typed is not recognized.");
		print("Type 'help' to list all available commands.");
	}
	
	
	protected void notifyBadCommand() {
		print("The command you typed is not valid for the current state.");
	}
	
	
	private void startView() {
		inputThread = new InputListenerThread();
		inputThread.start();
	}
	
	
	
	protected void networkError(){
		print("Network error");
	}

	
	protected void info(String info) {
		print(info);
	}
	
	
	protected void showMarket(Market m){
		List<Sellable> items= m.getItemList();
		
		print("\nMarket:\n");
		if(items.isEmpty())
			print("The market is currently empty!");
		
		for(Sellable elem : items){
			String elemToString="";
			elemToString+="– " + elem.getTag();
			
			if(elem instanceof Card)
				elemToString+=" (" + ((Card) elem).getColor() + ")";
			if(elem instanceof Permit)
				elemToString+=" (" + ((Permit)elem).toStringMarket() + ")";
			
			elemToString+=", price: " + elem.getPrice();

			print(elemToString);
		}
		
		print("\n");
	}
	
	class InputListenerThread extends Thread {
		boolean running = true;
		Scanner in;
		
		@Override
		public void run() {
			in = new Scanner(System.in);
			String command;
			while (running) {
				try {
					command  = in.nextLine();
					log.log(Level.FINER, "Command received: " + command);
					if (listener != null)
						listener.handleInput(command);
				}
				catch (IllegalStateException | NoSuchElementException e) {
					running = false;
					log.log(Level.FINE, "Closed input stream", e);
				}
			}
		}
		
		/**
		 * Stops the listening input thread
		 */
		public void stopListening() {
			running = false;
			in.close();
		}
	}
}

