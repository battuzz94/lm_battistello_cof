package client.cli;

import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cof.commands.SellItemCommand;
import cof.market.Assistant;
import cof.model.Price;
import common.ActionType;
import common.network.Message;

/**
 * This state precedes the market. When the client is in this state, it can choose whether to sell
 * one of the items the player owns and put it in the market. Note that this is not a turn-based 
 * state. It is called during the GameRunningState after all player have completed their turns and
 * it activates the MarketState when it receives a notification from server in the form of a message
 * with type BUY_ITEM_COMMAND.
 * @author emmeaa
 *
 */
public class PreMarketState implements ClientState {
	private static final Logger LOGGER=Logger.getLogger(PreMarketState.class.getName());
	
	private ConsoleController controller;
	private ConsoleView view;
	private ConsoleCommandPool pool=ConsoleCommandPool.getInstance();
	
	private static final Pattern sellAssistantPattern=Pattern.compile("^\\s*sell\\s+assistant\\s+for\\s+(?<priceForAssistant>\\d+)(\\s+coins?)?\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern sellCardWithCoinsPattern=Pattern.compile("^\\s*sell\\s+(?<cardTag>Card\\d+)\\s+for\\s+(?<priceForCard>\\d+)(\\s+coins?)?\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern sellPermitWithCoinsPattern=Pattern.compile("^\\s*sell\\s+(?<permitTag>Permit\\d+)\\s+for\\s+(?<priceForPermit>\\d+)(\\s+coins?)?\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern sellCardWithAssistantsPattern=Pattern.compile("^\\s*sell\\s+(?<cardTag>Card\\d+)\\s+for\\s+(?<priceForCard>\\d+)\\s+assistants?\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern sellPermitWithAssistantsPattern=Pattern.compile("^\\s*sell\\s+(?<permitTag>Permit\\d+)\\s+for\\s+(?<priceForPermit>\\d+)\\s+assistants?\\s*", Pattern.CASE_INSENSITIVE);	
	
	private ConsoleCommand sellAssistant;
	private ConsoleCommand sellCardWithCoins;
	private ConsoleCommand sellPermitWithCoins;
	private ConsoleCommand sellCardWithAssistants;
	private ConsoleCommand sellPermitWithAssistants;
	
	
	protected PreMarketState(ConsoleController controller, ConsoleView view){
		this.controller=controller;
		this.view=view;
		
		registerPreMarketCommands();
	}
	
	@Override
	public void handleUserInput(String input) {
		try {
			ConsoleCommand c = pool.getCommandByPattern(input);
			c.run(input);
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINE, "Input not recognised", e);
			view.notifyNotRecognizedCommand();
		}
		catch (Exception e) {
			LOGGER.log(Level.FINE, "Caught unknown exception while handling user input", e);
			view.notifyNotRecognizedCommand();	
		}
	}
	

	@Override
	public void handleServerInput(Message message) {
		if(message.getType()==ActionType.BUY_MARKET_ACTION){
			unregisterCommands();
			controller.setState(new MarketState(controller, view));
			view.info("You can now choose wether you wish to buy something in the market.");
		}
	}
	
	
	
	private void registerPreMarketCommands(){
		sellAssistant= ConsoleCommandPool.createCommand(sellAssistantPattern, "Sell assistant for <price> coins",
				"Offer to sell one assistants for a price given by the player in coins.", 
				this::sellAssistant);
		
		sellCardWithCoins= ConsoleCommandPool.createCommand(sellCardWithCoinsPattern, "Sell <CardTag> for <price> coins", 
				"Offer to sell the card tagged <CardTag> for the price <price> in coins.", 
				this::sellCard);
		
		sellCardWithAssistants= ConsoleCommandPool.createCommand(sellCardWithAssistantsPattern, "Sell <CardTag> for <price> assistants", 
				"Offer to sell the card tagged <CardTag> for the price <price> in assistants.", 
				this::sellCardWithAssistants);
		
		sellPermitWithCoins= ConsoleCommandPool.createCommand(sellPermitWithCoinsPattern, "Sell <PermitTag> for <price> coins", 
				"Offer to sell the permit tagged <PermitTag> for the price <price> in coins.", 
				this::sellPermit);
		
		sellPermitWithAssistants= ConsoleCommandPool.createCommand(sellPermitWithAssistantsPattern, "Sell <PermitTag> for <price> assistants", 
				"Offer to sell the permit tagged <PermitTag> for the price <price> in assistants", 
				this::sellPermitWithAssistants);
		
		pool.registerCommand(sellAssistant);
		pool.registerCommand(sellCardWithCoins);
		pool.registerCommand(sellCardWithAssistants);
		pool.registerCommand(sellPermitWithCoins);
		pool.registerCommand(sellPermitWithAssistants);
	}
	
	
	private void sellAssistant(String s){
		Matcher m=sellAssistantPattern.matcher(s);
		if(m.matches()){
			String price=m.group("priceForAssistant");
			int priceForAssistant=Integer.parseInt(price);
			
			Assistant a=new Assistant();
			a.setPrice(new Price(priceForAssistant, 0));
			
			controller.doPreMarketAction(new SellItemCommand(a));
		}
	}
	
	
	private void sellCard(String s){
		Matcher m=sellCardWithCoinsPattern.matcher(s);
		if(m.matches()){
			String cardTag=m.group("cardTag");
			
			String price=m.group("priceForCard");
			int priceForCard=Integer.parseInt(price);
			
			try{
				controller.getPlayer().getCardByTag(cardTag).setPrice(new Price(priceForCard, 0));
				controller.doPreMarketAction(new SellItemCommand(controller.getPlayer().getCardByTag(cardTag)));
			} catch (NoSuchElementException e){
				view.info("You do not own the card you want to sell!");
				LOGGER.log(Level.FINER, "The player does not own the card indicated", e);
			}
		}
	}
	
	
	
	private void sellCardWithAssistants(String s){
		Matcher m=sellCardWithAssistantsPattern.matcher(s);
		if(m.matches()){
			String cardTag=m.group("cardTag");
			
			String price=m.group("priceForCard");
			int priceForCard=Integer.parseInt(price);
			
			try{
				controller.getPlayer().getCardByTag(cardTag).setPrice(new Price(0, priceForCard));
				controller.doPreMarketAction(new SellItemCommand(controller.getPlayer().getCardByTag(cardTag)));
			} catch (NoSuchElementException e){
				view.info("You do not own the card you want to sell!");
				LOGGER.log(Level.FINER, "The player does not own the card indicated", e);			}
		}
	}
	
	
	private void sellPermit(String s){
		Matcher m=sellPermitWithCoinsPattern.matcher(s);
		if(m.matches()){
			String permitTag=m.group("permitTag");
			
			String price=m.group("priceForPermit");
			int priceForPermit=Integer.parseInt(price);
			
			try{
				controller.getPlayer().getPermitByTag(permitTag).setPrice(new Price(priceForPermit, 0));
				controller.doPreMarketAction(new SellItemCommand(controller.getPlayer().getPermitByTag(permitTag)));
			} catch(NoSuchElementException e){
				view.info("You do not own the permit you want to sell!");
				LOGGER.log(Level.FINER, "The player does not own the permit indicated", e);			}
		}
	}
	
	private void sellPermitWithAssistants(String s){
		Matcher m=sellPermitWithAssistantsPattern.matcher(s);
		if(m.matches()){
			String permitTag=m.group("permitTag");
			
			String price=m.group("priceForPermit");
			int priceForPermit=Integer.parseInt(price);
			
			try{
				controller.getPlayer().getPermitByTag(permitTag).setPrice(new Price(0, priceForPermit));
				controller.doPreMarketAction(new SellItemCommand(controller.getPlayer().getPermitByTag(permitTag)));
			} catch(NoSuchElementException e){
				view.info("You do not own the permit you want to sell!");
				LOGGER.log(Level.FINER, "The player does not own the permit indicated", e);
			}
		}
	}
	
	
	
	private void unregisterCommands(){
		pool.removeCommand(sellCardWithCoins);
		pool.removeCommand(sellAssistant);
		pool.removeCommand(sellPermitWithCoins);
		pool.removeCommand(sellCardWithAssistants);
		pool.removeCommand(sellPermitWithAssistants);
	}

}
