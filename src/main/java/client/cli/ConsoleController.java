package client.cli;

import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import client.AbstractClientController;
import cof.bonus.Bonus;
import cof.bonus.GainOneCityRewardBonus;
import cof.bonus.GainPermitBonus;
import cof.bonus.GainPermitRewardBonus;
import cof.bonus.GainTwoCityRewardBonus;
import cof.commands.PassTurnCommand;
import cof.model.Player.PlayerStat;
import common.ActionType;
import common.network.Message;
/**
 * The console controller handles all inputs from console and from server
 * @author andrea
 *
 */
public class ConsoleController extends AbstractClientController {
	private ConsoleCommandPool pool = ConsoleCommandPool.getInstance();
	private static final Logger LOGGER = Logger.getLogger(ConsoleController.class.getName());
	private ConsoleView view;

	private ClientState state;
	
	/**
	 * Create a new controller with associated view
	 * @param view
	 */
	public ConsoleController(ConsoleView view) {
		this.view = view;

		this.state = new ChooseNetworkState(this, view);

		registerHelpCommand();
		registerStopCommand();
	}
	/**
	 * This method controls the input received.
	 * @param input
	 */
	protected synchronized void handleInput(String input) {
		state.handleUserInput(input);
	}

	@Override
	public void showAdditionalInputMessage(Message message) {
		Bonus bonus = gson.fromJson(message.getContent(), Bonus.class);
		if (bonus instanceof GainOneCityRewardBonus)
			view.info("Choose a city in the board whose bonus you wish to get.");
		else if (bonus instanceof GainPermitBonus)
			view.info("Choose a permit among the obtainable ones to take.");
		else if (bonus instanceof GainPermitRewardBonus)
			view.info("Choose a permit that you already own whose bonus you wish to get.");
		else if (bonus instanceof GainTwoCityRewardBonus)
			view.info("Choose two cities in the board whose bonus you wish to get.");
	}

	/**
	 * Implements the abstract method in AbstractClientController. Only called
	 * for the time out messages, and nothing else.
	 * 
	 * @param message
	 *            must have type ActionType.TIME_OUT_NOTIFICATION
	 */
	@Override
	public void showTimeOutMessage(Message message) {
		view.info("Connection time out: disconnected from server.");
		view.stop();
	}

	/**
	 * Shows the content of a message, implements the abstract method in
	 * AbstractClientController for the console.
	 * 
	 * @param message
	 *            the message whose content needs to be shown on console.
	 */
	@Override
	public void showInfoMessage(Message message) {
		view.info(message.getContent());
	}
	
	@Override
	public void showInfoTimeOut(Message message) {
		view.info(message.getContent());
	}

	protected void registerGeneralStatusCommands() {
		pool.registerCommand(Pattern.compile("^\\s*(my//s+)?status\\s*", Pattern.CASE_INSENSITIVE), "status",
				"Show my current status", s -> view.showStatus(me));

		pool.registerCommand(Pattern.compile("^\\s*board\\s*", Pattern.CASE_INSENSITIVE), "board", "Show the board",
				s -> view.showBoard(board, me));

		pool.registerCommand(Pattern.compile("^\\s*available\\s+councillors?\\s*", Pattern.CASE_INSENSITIVE),
				"available councillors", "Show the councillors still available (not seated in a council)",
				s -> view.showAvailableCouncillors(board));

		pool.registerCommand(Pattern.compile("^\\s*king('?s)?\\s+status\\s*", Pattern.CASE_INSENSITIVE),
				"king's status", "Show the current status for the king, i.e. which city he's in and his council",
				s -> view.showKingStatus(board));

		pool.registerCommand(Pattern.compile("^\\s*how\\s+to\\s+play\\s*", Pattern.CASE_INSENSITIVE),
				"How to play", "Show the game rules", s -> view.showGameRules());

		pool.registerCommand(Pattern.compile("^\\s*other\\s+players?'?\\s+stats?\\s*", Pattern.CASE_INSENSITIVE),
				"Other players' stats",
				"Allows you to see public stats from other players (e.g. nobility points, victory points, builded emporia and username)",
				s -> {
					try {
						handler.sendToServer(new Message(ActionType.OTHER_UPDATE));
					} catch (RemoteException e) {
						view.networkError();
						LOGGER.log(Level.FINE, "network error occurred: " + e.toString(), e);
					}
				});

		pool.registerCommand(Pattern.compile("^\\s*pass\\s+turn\\s*", Pattern.CASE_INSENSITIVE), "Pass turn",
				"Finish your turn", s -> 
					doPassTurnCommand(new PassTurnCommand())
				);
	}

	private void registerStopCommand() {
		pool.registerCommand(Pattern.compile("^\\s*(stop)|(quit)\\s*", Pattern.CASE_INSENSITIVE), "stop",
				"Stops the game", s -> {
					disconnect();
					view.stop();
				});
	}

	private void registerHelpCommand() {
		// Help command
		pool.registerCommand(Pattern.compile("^\\s*help\\s*", Pattern.CASE_INSENSITIVE), "help", "show help message",
				s -> view.showHelp());
	}

	@Override
	public void showNetworkError() {
		view.networkError();
	}

	@Override
	public void showBadCommand() {
		view.notifyBadCommand();
	}

	@Override
	public void handleServerInput(Message message) {
		state.handleServerInput(message);
	}

	protected void setState(ClientState newState) {
		this.state = newState;
	}

	@Override
	public void showCommandValid() {
		view.info("The command is valid and it is executed. ");
		view.info("Type 'board' and 'status' to see what has changed");
	}

	@Override
	protected void gameFinished(List<PlayerStat> finalRanking) {
		this.view.stop();
	}

	@Override
	protected void updateOtherPlayersStats(List<PlayerStat> stats) {
		view.info("Current other players stats: ");
		stats.forEach(stat -> view.info(stat.getUsername() + " -> " + stat.getVictoryPoints() + " points and " + stat.getNobilityTrack() + " nobility"));
	}

}
