package client.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import cof.model.Board;
import cof.model.BoardConfig;
import cof.model.BoardFactory;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * Client state called before the beginning of the game, after client has connected; 
 * the player admin (i.e. the first player to connect to an available room) is asked 
 * to either load a board from file or generate it randomly through some parameters. 
 * All clients who connect to server when a room already contains at least a player 
 * don't go through this state.
 * @author emmeaa
 *
 */

public class ChooseBoardState implements ClientState {
	private static final Logger LOGGER=Logger.getLogger(ChooseBoardState.class.getName());
	
	ConsoleController controller;
	ConsoleCommandPool pool = ConsoleCommandPool.getInstance();
	Board board;
	ConsoleView view;
	
	private static final Pattern loadPattern = Pattern.compile("^\\s*load\\s+(?<filename>[a-zA-Z0-9_/\\-\\.]+)", Pattern.CASE_INSENSITIVE);
	private static final Pattern generatePattern = Pattern.compile("^\\s*generate(\\s+\\d+)?(\\s+\\d+)?(\\s+\\d+)?(\\s+\\d+)?\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern sendPattern = Pattern.compile("^\\s*send");
	private static final Pattern setAIAgentsPattern = Pattern.compile("^add\\s+(?<count>\\d+)(\\s+computer)?(\\s+players?)?");
	
	ConsoleCommand loadCommand = ConsoleCommandPool.createCommand(loadPattern, "load <filename>" , 
			"load a map from file", 
			this::load);
	ConsoleCommand generateCommand = ConsoleCommandPool.createCommand(generatePattern, "Generate <regions> <cities> <connectionSparseness> <bonusLevel>", 
			"Generate a board with given config (all parameters after the number of regions are optional)", 
			this::generate);
	ConsoleCommand sendCommand = ConsoleCommandPool.createCommand(sendPattern, "send", 
			"Sends the last configuration loaded or generated", 
			this::send);
	
	ConsoleCommand setAiAgentsCommand = ConsoleCommandPool.createCommand(setAIAgentsPattern, 
			"Add <count> computer players", 
			"Adds the specified number of mobs", 
			this::setAIAgents);

	private boolean alreadySetAIAgents;
	
	
	protected ChooseBoardState(ConsoleController controller, ConsoleView view) {
		this.controller = controller;
		this.view = view;
		
		registerCommands();
		
		view.info("You are the admin. Please configure the board. Here is the list of available commands:");
		view.showHelp();
	}
	
	
	
	private void registerCommands() {
		pool.registerCommand(loadCommand);
		pool.registerCommand(generateCommand);
		pool.registerCommand(sendCommand);
		pool.registerCommand(setAiAgentsCommand);
	}
	
	private void unregisterCommands() {
		pool.removeCommand(loadCommand);
		pool.removeCommand(generateCommand);
		pool.removeCommand(sendCommand);
		pool.removeCommand(setAiAgentsCommand);
	}
	
	protected void setAIAgents(String input) {
		if (alreadySetAIAgents)
			view.notifyBadCommand();
		else {
			Matcher m = setAIAgentsPattern.matcher(input);
			if (m.matches()) {
				int count = Integer.parseInt(m.group("count"));
				controller.setAIAgents(count);
				alreadySetAIAgents = true;
			}
		}
	}
	
	protected void generate(String s) {
		Matcher m = generatePattern.matcher(s);
		if (m.matches()) {
			BoardConfig conf = BoardConfig.getDefaultConfig();
			try {
				if (m.group(1) != null)
					conf.setRegions( Integer.parseInt(StringUtils.strip(m.group(1))));
				if (m.group(2) != null)
					conf.setCitiesPerRegion(Integer.parseInt(StringUtils.strip(m.group(2))));
				if (m.group(3) != null)
					conf.setConnectionsSparseness(Integer.parseInt(StringUtils.strip(m.group(3))));
				if (m.group(4) != null)
					conf.setBonusLevel(Integer.parseInt(StringUtils.strip(m.group(4))));
			}
			finally {
				BoardFactory factory = new BoardFactory(conf);
				board = factory.build();
				view.showBoard(board);
			}
		}
	}
	
	protected void load(String s) {
		Matcher m = loadPattern.matcher(s);
		if (m.matches()) {
			String filename = m.group("filename");
			try {
				File file = new File(filename);
				Board b = Utility.loadBoardFromFile(file);
				view.showBoard(b);
				this.board = b;
			}
			catch (FileNotFoundException e) {
				LOGGER.log(Level.FINE, "File not found", e);
				view.info("File not found..");
			}
		}
	}
	
	protected void send(String s) {
		if (board != null) {
			controller.configuredBoard(board);
			unregisterCommands();
			
			controller.setState(new GameRunningState(controller, view));
			controller.registerGeneralStatusCommands();
		}
		else
			view.notifyBadCommand();
	}
	
	@Override
	public void handleUserInput(String input) {
		try {
			ConsoleCommand command = pool.getCommandByPattern(input);
			command.run(input);
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINE, "Command does not exists", e);
			view.notifyBadCommand();
		}
	}

	@Override
	public void handleServerInput(Message message) {
		if (message.getType() == ActionType.GAME_INIT) {
			controller.setState(new GameRunningState(controller, view));
		}
	}

}
