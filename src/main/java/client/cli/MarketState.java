package client.cli;

import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import cof.commands.BuyItemCommand;
import cof.commands.CommandNotValidException;
import cof.market.Market;
import cof.market.Sellable;
import common.Utility;
import common.network.Message;


/**
 * client state in which the player may choose, during their turn, whether to buy something from the market.
 * This state is called only ever after the PreMarketState and always activates a new GameRunningState when
 * the server notifies the client with a INIT_TURN message.
 * @author emmeaa
 *
 */
public class MarketState implements ClientState {
	private static final Logger LOGGER = Logger.getLogger(GameRunningState.class.getName());
	
	private ConsoleController controller;
	private ConsoleView view;
	private ConsoleCommandPool pool=ConsoleCommandPool.getInstance();
	private Market market;
	
	
	private static final Pattern seeMarketPattern=Pattern.compile("^\\s*(see\\s+)?market\\s*", Pattern.CASE_INSENSITIVE);
	private static final Pattern buyItemPattern=Pattern.compile("^\\s*buy\\s+(?<itemTag>((Card)|(Permit)|(Assistant))\\d+)\\s*", Pattern.CASE_INSENSITIVE);
	
	private ConsoleCommand seeMarket;
	private ConsoleCommand buyItem;

	private Gson gson = Utility.getJsonSerializer();
	
	
	
	protected MarketState(ConsoleController controller, ConsoleView view){
		this.controller=controller;
		this.view=view;

		registerMarketCommands();
	}
	
	
	@Override
	public void handleUserInput(String input) {
		try {
			ConsoleCommand c = pool.getCommandByPattern(input);
			c.run(input);
		}
		catch (NoSuchElementException e) {
			view.notifyNotRecognizedCommand();
			LOGGER.log(Level.FINE, "Input not recognised", e);
		}
		catch (Exception e) {
			LOGGER.log(Level.FINE, "Caught unknown exception while handling user input", e);
			view.notifyNotRecognizedCommand();	
		}
	}

	@Override
	public void handleServerInput(Message message) {
		switch(message.getType()){
		case MARKET_UPDATE: 
			Market newMarket = gson .fromJson(message.getContent(), Market.class);
			this.market=newMarket;
			view.showMarket(market);
			break;
		case INIT_MARKET_TURN:
			view.info(message.getContent());
			break;
		case INIT_TURN:
			unregisterCommands();
			controller.setState(new GameRunningState(controller, view));
			view.info("A new game turn has started!");
			break;
		default:
		}

	}
	
	
	
	private void registerMarketCommands(){
		seeMarket= ConsoleCommandPool.createCommand(seeMarketPattern, "See market", 
				"Lets you see which items are on sale", 
				this::seeMarket);
		
		buyItem= ConsoleCommandPool.createCommand(buyItemPattern, "Buy <ItemTag>", 
				"Buy the item tagged <ItemTag>.", 
				this::buyItem);
		
		pool.registerCommand(seeMarket);
		pool.registerCommand(buyItem);
	}
	
	private void unregisterCommands(){
		pool.removeCommand(seeMarket);
		pool.removeCommand(buyItem);
	}
	
	
	
	private void seeMarket(String s){
		controller.requestMarketStatus();
	}
	
	
	private void buyItem(String s){
		Matcher m=buyItemPattern.matcher(s);
		if(m.matches()){
			String itemTag=m.group("itemTag");
			Sellable item;
			
			try{
				item=market.getByTag(itemTag);
				controller.doMarketAction(new BuyItemCommand(item));
			} catch (CommandNotValidException e){
				LOGGER.log(Level.FINEST, "User inserted wrong command", e);
				view.notifyNotRecognizedCommand();			}
		}
	}
	
	
	
}
