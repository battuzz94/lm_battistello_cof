package client.cli;

import common.network.Message;

/**
 * Interface for each state in the State Pattern implemented client-side.
 * This State Pattern mirrors the one implemented server-side, and the two
 * evolve in parallel.
 * @author emmeaa
 *
 */
public interface ClientState {
	
	/**
	 * This method must be implemented so that it manages all keyboard input 
	 * from the player allowed in a particular state.
	 * @param input a string sent by the player that must be matched to a command
	 */
	void handleUserInput(String input);
	
	
	/**
	 * This method must be implemented so that it manages only the input from server
	 * expected in this particular state. Depending on the kind of message, the client
	 * state must react accordingly.
	 * @param message the input from server
	 */
	void handleServerInput(Message message);
}
