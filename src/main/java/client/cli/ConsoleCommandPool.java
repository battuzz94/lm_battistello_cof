package client.cli;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * @Singleton
 * Represents a set of commands.
 * The application must register all the commands before using them 
 * @author Andrea
 *
 */
public class ConsoleCommandPool {
	private static ConsoleCommandPool instance = null;
	private ArrayList<ConsoleCommand> commands;
	
	
	private ConsoleCommandPool() {
		commands = new ArrayList<>();
	}
	
	
	public static ConsoleCommandPool getInstance() {
		if (instance == null) 
			instance = new ConsoleCommandPool();
		return instance;
	}
	
	protected synchronized void registerCommand(ConsoleCommand c) {
		commands.add(c);
	}
	
	protected synchronized void registerCommand(Pattern p, String name, String description, Consumer<String> function) {
		commands.add(createCommand(p, name, description, function));
	}
	
	
	protected ConsoleCommand getCommandByPattern(String input) {
		for (ConsoleCommand c : commands) {
			Matcher m = c.getCommandPattern().matcher(input);
			if (m.matches()) {
				return c;
			}
		}
		throw new NoSuchElementException();
	}
	
	
	protected synchronized void removeCommand(ConsoleCommand c) {
		if (!commands.remove(c)) {  // Tries to remove the command given the reference
			boolean removed = false;
			// else, tries to check if a command with same name is present and remove it
			for (int i = 0; i < commands.size() && !removed; i++)
				if (c.getCommandName().equals(commands.get(i).getCommandName())) {
					commands.remove(i);
					removed = true;
				}
			if (!removed)
				throw new NoSuchElementException();
		}
	}
	
	
	/**
	 * 
	 * @param commandName
	 * @throws NoSuchElementException when there is no command with specified name
	 * @return
	 */
	public ConsoleCommand getCommandForName(String commandName) {
		Optional<ConsoleCommand> ret = commands.stream()
				.filter(c -> c.getCommandName().equals(commandName))
				.findFirst();
		
		return ret.get();
	}
	
	
	public List<ConsoleCommand> getCommands() {
		return commands;
	}
	
	
	protected static ConsoleCommand createCommand(Pattern p, String name, String description, Consumer<String> function) {
		return new ConsoleCommand() {
				@Override
				public void run(String input) {
					function.accept(input);
				}
				@Override
				public String getCommandName() {
					return name;
				}
				@Override
				public String getCommandDescription() {
					return description;
				}
				@Override
				public Pattern getCommandPattern() {
					return p;
				}
			};
	}
	
}
