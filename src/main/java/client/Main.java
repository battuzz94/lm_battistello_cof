package client;

import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Starts a new Council of Four game
 * @author andrea
 *
 */
public class Main {
	private Main() {}
	
	
	/**
	 * client main
	 * @param args
	 */
	public static void main(String[] args) {
		Logger log = Logger.getGlobal();
		try {
			FileHandler fh = new FileHandler("client.log");
			fh.setFormatter(new SimpleFormatter());
			log.setLevel(Level.ALL);
			log.addHandler(fh);
			log.fine("created log file");
		}
		catch (Exception e) {
			log.log(Level.FINE, "Could not create log file", e);
		}
		
		
		Scanner in = new Scanner(System.in);
		System.out.println("Type 'gui' for Graphic User Interface or 'cli' for Command Line Interface:");
		String type = in.nextLine();
		
		while (!"gui".equals(type) && !"cli".equals(type)) {
			System.out.println("Type 'gui' for Graphic User Interface or 'cli' for Command Line Interface:");
			type = in.nextLine();
		}
		
		UIFactory.buildUserInterface(type);
		in.close();
	}
}
