package client.network;

import java.rmi.RemoteException;

import com.google.gson.Gson;

import client.AbstractClientController;
import cof.commands.Command;
import cof.commands.MainAction;
import cof.commands.MarketAction;
import cof.commands.NobilityTrackAction;
import cof.commands.QuickAction;
import cof.model.Board;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * Abstraction of the Client connection to the server
 * @author andrea
 *
 */
public abstract class ClientConnectionHandler {
	/**
	 * Start the connection with the server
	 */
	public abstract void connect();
	/**
	 * Disconnects from the server
	 */
	public abstract void disconnect();
	/**
	 * Sends a message to the server
	 * @param message the message to be sent
	 * @throws RemoteException if network error occurred
	 */
	public abstract void sendToServer(Message message) throws RemoteException;
	public abstract void setListener(AbstractClientController listener);
	
	/**
	 * Sends a main action to server
	 * @param command
	 * @throws RemoteException
	 */
	public void sendMainAction(MainAction command) throws RemoteException{
		Message m=new Message(ActionType.MAIN_ACTION, command);
		sendToServer(m);
	}
	
	/**
	 * Sends a quick action to server
	 * @param command
	 * @throws RemoteException
	 */
	public void sendQuickAction(QuickAction command) throws RemoteException{
		Message m=new Message(ActionType.QUICK_ACTION, command);
		sendToServer(m);
	}
	
	/**
	 * Sends a pre-market action to server
	 * @param command
	 * @throws RemoteException
	 */
	public void sendPreMarketAction(MarketAction command) throws RemoteException{
		Message m=new Message(ActionType.SELL_MARKET_ACTION, command);
		sendToServer(m);
	}
	
	/**
	 * Sends a market action to server
	 * @param command
	 * @throws RemoteException
	 */
	public void sendMarketAction(MarketAction command) throws RemoteException{
		Message m=new Message(ActionType.BUY_MARKET_ACTION, command);
		sendToServer(m);
	}
	
	/**
	 * Sends a generic command to server
	 * @param command
	 * @throws RemoteException
	 */
	public void sendCommand(Command command) throws RemoteException{
		Message message=new Message(ActionType.GENERIC_COMMAND, command);
		sendToServer(message);
	}
	
	/**
	 * Sends pass turn command to server
	 * @param command
	 * @throws RemoteException
	 */
	public void sendPassTurnCommand(Command command) throws RemoteException{
		Message message=new Message(ActionType.PASS_TURN, command);
		sendToServer(message);
	}
	
	
	/**
	 * Sends a command to retrieve dynamic bonuses of the nobility track
	 * @param command
	 * @throws RemoteException
	 */
	public void sendNobilityTrackCommand(NobilityTrackAction command) throws RemoteException{
		Message message = new Message(ActionType.NOBILITY_ACTION, command);
		sendToServer(message);
	}
	
	/**
	 * Sends the configured board
	 * @param b the configured board
	 * @throws RemoteException
	 */
	public void sendBoardConfiguration(Board b) throws RemoteException {
		Gson gson = Utility.getJsonSerializer();
		String serializedBoard = gson.toJson(b);
		Message message = new Message(ActionType.BOARD_CONFIGURATION, serializedBoard);
		sendToServer(message);		
	}
}
