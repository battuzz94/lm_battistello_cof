package client.network;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.AbstractClientController;
import common.network.ClientStub;
import common.network.Message;
import common.network.ServerStub;

/**
 * RMI Implementation of ClientConnectionHandler
 * @author andrea
 *
 */
public class ClientStubImpl extends ClientConnectionHandler implements ClientStub {
	private static final Logger LOGGER = Logger.getLogger(ClientStubImpl.class.getName());
	private transient ServerStub serverStubImpl;
	private transient AbstractClientController listener;
	
	public ClientStubImpl() throws RemoteException {
		// Default constructor with a throw declaration
	}
	
	public void setServerStub(ServerStub serverStub) {
		serverStubImpl = serverStub;
	}

	@Override
	public void setListener(AbstractClientController listener) {
		this.listener = listener;
	}

	public ServerStub getServerStub() {
		return serverStubImpl;
	}

	@Override
	public void send(Message message) throws RemoteException {
		if (listener != null)
			listener.messageReceived(message);
	}
	
	@Override
	public void sendToServer(Message message) throws RemoteException {
		new Thread(() -> 
			{
				try {
					serverStubImpl.send(message);
				}
				catch(RemoteException e) {
					LOGGER.log(Level.WARNING, "Network error occured", e);
				}
			}
		).start();
	}

	@Override
	public void connect() {
		// This is not necessary
	}
	
	@Override
	public void disconnect(){
		try {
			boolean unexported = UnicastRemoteObject.unexportObject(this, false);
			if (!unexported)
				unexported = UnicastRemoteObject.unexportObject(this, true);
			
			if (unexported)
				LOGGER.log(Level.FINE, "Remote object successfully unexported");
		}
		catch (NoSuchObjectException e) {
			LOGGER.log(Level.FINE, "Object already unexported", e);
		}
	}


}