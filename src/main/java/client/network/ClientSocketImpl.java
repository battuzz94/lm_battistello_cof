package client.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.AbstractClientController;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * Socket Implementation of ClientConnectionHandler
 * @author andrea
 *
 */
public class ClientSocketImpl extends ClientConnectionHandler {
	private static final Logger LOGGER = Logger.getLogger(ClientSocketImpl.class.getName());
	private int socketPort;
	private static final int INPUT_DELAY_MILLIS = 300;
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;
	private Socket socketServer;
	private boolean isReading;
	private AbstractClientController listener;
	private String username;
	
	/**
	 * Creates a new instance of the Socket Connection
	 * @param hostname the ip address of server
	 * @param socketPort the port of the server
	 * @throws RemoteException if network error occured
	 */
	public ClientSocketImpl(String hostname, int socketPort) throws RemoteException {
		this.socketPort = socketPort;
		try {
			socketServer = new Socket(hostname, this.socketPort);
		} catch (UnknownHostException e) {
			throw new RemoteException("Network error due to Unknown host", e);
		} catch (IOException e) {
			throw new RemoteException("Network error occured", e);
		}
	}

	@Override
	public void connect() {
		try {
			isReading = true;
			OutputStream outputStream = socketServer.getOutputStream();
			outputStream.flush();
			objectOutputStream = new ObjectOutputStream(outputStream);
			InputStream inputStream = socketServer.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
			Thread listenThread = new Thread(() -> listenSocket());
			
			sendToServer(new Message(ActionType.INFO, username));
			
			listenThread.start();
		} catch (UnknownHostException e) {
			LOGGER.log(Level.WARNING, "Could not find host", e);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Input output error", e);
		}
	}

	@Override
	public void disconnect() {
		try {
			objectOutputStream.close();
			objectInputStream.close();
			socketServer.close();
		}
		catch (IOException e) {
			LOGGER.log(Level.FINE, "Exception on closing socket and streams", e);
		}
	}

	private void listenSocket() {
		Message messageClient;
		while (isReading) {
			try {
				messageClient = (Message) objectInputStream.readObject();
				
				if (listener != null)
					listener.messageReceived(messageClient);
				Thread.sleep(INPUT_DELAY_MILLIS);

			} catch (IOException | ClassNotFoundException | InterruptedException e) {
				if (listener != null)
					listener.showNetworkError();
				isReading=false;
				LOGGER.log(Level.FINE, "Network error occured", e);
				Utility.printDebug("Socket connection closed");
			}
		}
	}

	@Override
	public void setListener(AbstractClientController listener) {
		this.listener = listener;

	}

	@Override
	public void sendToServer(Message message) throws RemoteException {
		try {
			objectOutputStream.writeObject(message);
		} catch (IOException e) {
			if (listener != null)
				listener.showNetworkError();
			LOGGER.log(Level.INFO, "Could not send message to server", e);
		}
	}

	protected void addUsername(String username) {
		this.username=username;
	}

}
