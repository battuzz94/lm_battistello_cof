package client.network;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.network.ClientStub;
import common.network.RMIConnectionListener;
import common.network.ServerStub;

/**
 * Creates a new ClientConnectionHandler with a factory method
 * @author andrea
 *
 */
public class ClientConnectionFactory {
	private static final Logger LOGGER = Logger.getLogger(ClientConnectionFactory.class.getName());
	private static final String REGISTRYNAME = "listener";
	private static final String HOSTNAME= "127." + "0.0.1";
	private static final int SOCKET_PORT=7777;
	
	private ClientConnectionFactory() {}
	
	/**
	 * Returns a new connection
	 * @param type "RMI" for a RMI connection or "socket" for socket connection
	 * @param username the username associated with the player
	 * @return a new connection of the type specified
	 * @throws RemoteException if network error occured
	 * @throws IllegalStateException if the type is not recognized
	 */
	public static ClientConnectionHandler getConnection(String type, String username) throws RemoteException{
		if ("RMI".equals(type)) 
			return startRMI(username);
		else if ("socket".equals(type))
			return startSocket(username);
		else
			throw new IllegalArgumentException();
		
	}
	
	private static ClientConnectionHandler startRMI(String username) throws RemoteException {
		ServerStub serverStub = null;
		ClientStubImpl clientStubImpl = null;
		try {
			Registry registry = LocateRegistry.getRegistry();
			RMIConnectionListener rmiConnectionListener = (RMIConnectionListener) registry.lookup(REGISTRYNAME);
			clientStubImpl= new ClientStubImpl();
			ClientStub clientRemote = (ClientStub) UnicastRemoteObject.exportObject(clientStubImpl, 0);
			serverStub=rmiConnectionListener.connect(clientRemote, username);
			clientStubImpl.setServerStub(serverStub);
			
		} catch (NotBoundException e) {
			LOGGER.log(Level.INFO, "Registry already bound", e);
			throw new RemoteException("Registry already bound", e);
		}
		
		return clientStubImpl;
	}

	private static ClientConnectionHandler startSocket(String username) throws RemoteException {
		ClientSocketImpl clientSocketImpl= new ClientSocketImpl(HOSTNAME,SOCKET_PORT);
		clientSocketImpl.addUsername(username);
		clientSocketImpl.connect();
		return clientSocketImpl;
	}

}
