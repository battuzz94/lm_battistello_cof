package client;

import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import client.cli.ConsoleView;
import client.gui.GUIView;

/**
 * Builds a user interface whether it is CLI or GUI
 * @author andrea
 *
 */
public class UIFactory {
	private static final Logger LOGGER = Logger.getLogger(UIFactory.class.getName());
	private UIFactory() {}
	
	/**
	 * Builds a console View or a GUI view depending on the argument
	 * @param uiType 'cli' for a console view or 'gui' for a GUI view
	 * @throws NoSuchElementException if the uiType doesn't match the requirements
	 */
	public static void buildUserInterface(String uiType) {
		String uiTypeLowercase = uiType.toLowerCase();
		
		switch(uiTypeLowercase) {
		case "gui":
			GUIView view = new GUIView();
			view.start();
			break;
		case "cli":
			ConsoleView v = new ConsoleView();
			v.start();
			waitForTermination(v);
			break;
		default:
			throw new NoSuchElementException();
		}
	}

	private static void waitForTermination(ConsoleView v) {
		while (v.isRunning())
			try { 
				Thread.sleep(3000);
			} catch (Exception e) {
				LOGGER.log(Level.FINE, "Thread interrupted", e);
				v.stop();
			}
	}
	
}
