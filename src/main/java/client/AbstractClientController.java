package client;

import java.lang.reflect.Type;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import client.network.ClientConnectionFactory;
import client.network.ClientConnectionHandler;
import cof.commands.BuyItemCommand;
import cof.commands.MainAction;
import cof.commands.NobilityTrackAction;
import cof.commands.PassTurnCommand;
import cof.commands.QuickAction;
import cof.commands.SellItemCommand;
import cof.model.Board;
import cof.model.Player;
import cof.model.Player.PlayerStat;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * This abstracts the client and implement some functionality in common for both GUI and CLI
 * @author andrea
 *
 */
public abstract class AbstractClientController {
	private static final Logger LOGGER = Logger.getLogger(AbstractClientController.class.getName());
	private static final String NETWORK_ERROR = "Network error occured";
	protected ClientConnectionHandler handler;
	protected Board board;
	protected Player me;
	boolean connected=false;
	protected static final Gson gson = Utility.getJsonSerializer();
	private boolean running;
	
	
	public AbstractClientController() {
		
	}
	
	
	public Player getPlayer(){
		return this.me;
	}
	
	/**
	 * Requests an update for the market
	 */
	public void requestMarketStatus(){
		try{
			handler.sendToServer(new Message(ActionType.MARKET_UPDATE));
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
		
	}
	
	/**
	 * Disconnects from server and close all the network streams
	 */
	public void disconnect() {
		Utility.printDebug("**********DISCONNECTED**********");
		handler.disconnect();
		connected = false;
	}
	
	/**
	 * Starts a new connection of given type
	 * @param connectionType "RMI" for RMI connection or "socket" for socket connection
	 * @param username username associated with player
	 * @throws RemoteException
	 */
	public void beginConnection(String connectionType, String username) throws RemoteException {
		handler = ClientConnectionFactory.getConnection(connectionType, username);
		handler.setListener(this);
		showInfoMessage(new Message("Connection established! :D"));
	}
	
	/**
	 * Sends the main action to the server to be executed
	 * @param command
	 */
	public void doMainAction(MainAction command) {
		try{
			handler.sendMainAction(command);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}
	
	/**
	 * Informs the client that a network error occurred
	 */
	public abstract void showNetworkError();
	
	/**
	 * Sends the quick action to server to be executed
	 * @param command
	 */
	public void doQuickAction(QuickAction command) {
		try{
			handler.sendQuickAction(command);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}
	
	/**
	 * Sends the nobility track action to the server to be executed
	 * @param command
	 */
	public void doNobilityTrackAction(NobilityTrackAction command) {
		try{
			handler.sendNobilityTrackCommand(command);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}
	
	/**
	 * Sends a pre-market action to server
	 * @param command
	 */
	public void doPreMarketAction(SellItemCommand command){
		try{
			handler.sendPreMarketAction(command);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}
	
	/**
	 * Sends a market action to the server
	 * @param command
	 */
	public void doMarketAction(BuyItemCommand command){
		try{
			handler.sendMarketAction(command);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}
	
	/**
	 * Sends a pass turn command
	 * @param command
	 */
	public void doPassTurnCommand(PassTurnCommand command) {
		try{
			handler.sendPassTurnCommand(command);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}
	
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * Receives a message from server. The message will be preprocessed here and then forwarded to 
	 * the subclass through handleServerInput()
	 * @param message
	 */
	public synchronized void messageReceived(Message message) {
		Utility.printDebug("Received this message from server: " + message.getType());
		switch(message.getType()){
		case INFO:
			showInfoMessage(message); 
			break;
		case INFO_TIME_OUT:
			showInfoTimeOut(message);
			break;
		case TIME_OUT_NOTIFICATION: 
			showTimeOutMessage(message);
			break;
		case ADDITIONAL_INPUT_REQUIRED: 
			showAdditionalInputMessage(message); 
			break; 
		case BOARD_UPDATE: 
			board = gson.fromJson(message.getContent(), Board.class);  
			break;
		case BAD_MOVE: 
			showBadCommand(); 
			break;
		case GOOD_MOVE: 
			showCommandValid(); 
			break;
		case PLAYER_UPDATE: 
			me = gson.fromJson(message.getContent(), Player.class);
			break;
		case INIT_TURN:
			showInfoMessage(new Message(ActionType.INFO, "It's your turn!"));
			break;
		case TURN_FINISHED:
			showInfoMessage(new Message(ActionType.INFO, "You successfully completed your turn!"));
			break;
		case GAME_FINISHED:
			Type playerStatType = new TypeToken<List<PlayerStat>>(){}.getType();
			List<PlayerStat> ranking = gson.fromJson(message.getContent(), playerStatType);
			gameFinished(ranking);
			disconnect();
			break;
		case OTHER_UPDATE:
			Type listType = new TypeToken<List<PlayerStat>>(){}.getType();
			List<PlayerStat> stats= gson.fromJson(message.getContent(), listType);
			updateOtherPlayersStats(stats);
			break;
		default:
		}
		handleServerInput(message);
	}
	
	
	/**
	 * Update the information on the other players
	 */
	protected abstract void updateOtherPlayersStats(List<PlayerStat> stats);


	/**
	 * Free the resources of implementation
	 */
	protected abstract void gameFinished(List<PlayerStat> finalRanking);


	/**
	 * Informs the client that the command previously sent was not valid
	 */
	public abstract void showBadCommand();
	
	/**
	 * Informs the client that the command previously sent was valid
	 */
	public abstract void showCommandValid();

	/**
	 * Shows information message received from server
	 * @param message
	 */
	public abstract void showInfoMessage(Message message);
	
	/**
	 * Informs the client of another player's timeout notification
	 * @param message
	 */
	public abstract void showInfoTimeOut(Message message);
	
	/**
	 * Informs the client of timeout notification
	 * @param message
	 */
	public abstract void showTimeOutMessage(Message message);
	
	/**
	 * Informs the client that an additional action is required
	 * @param message
	 */
	public abstract void showAdditionalInputMessage(Message message);
	
	/**
	 * Callback method for the messages received from server
	 * @param message
	 */
	public abstract void handleServerInput(Message message);
	
	public boolean isConnected(){
		return connected;
	}
	
	/**
	 * Sends the configured board to the server
	 * @param b
	 */
	public void configuredBoard(Board b) {
		this.board = b;
		try{
			handler.sendBoardConfiguration(b);
		} catch(RemoteException e){
			LOGGER.log(Level.WARNING, NETWORK_ERROR, e);
			showNetworkError();
		}
	}

	/**
	 * Set the number of AI agents for this game
	 * @param value
	 */
	public void setAIAgents(Integer value) {
		try {
			handler.sendToServer(new Message(ActionType.AI_AGENTS, Integer.toString(value)));
		} catch (RemoteException e) {
			LOGGER.log(Level.FINE, NETWORK_ERROR, e);
			showNetworkError();
		}
	}

}
