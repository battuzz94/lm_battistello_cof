package client.gui;

import java.util.List;

import client.gui.factories.CardPaneFactory;
import client.gui.factories.CardPaneFactory.CardPane;
import client.gui.factories.PermitPaneFactory;
import client.gui.factories.PermitPaneFactory.PermitPane;
import cof.commands.BuyItemCommand;
import cof.market.Assistant;
import cof.market.Market;
import cof.market.Sellable;
import cof.model.Card;
import cof.model.Permit;
import cof.model.Player;
import common.Utility;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

/**
 * Creates a buy market pane where the player can buy items
 * @author andrea
 *
 */
public class BuyMarketGUIGrid {

	private static final int BUY_BUTTON_COLUMN = 2;
	private static final int PRICE_COLUMN = 1;
	private static final String ASSISTANT_IMAGE = "/images/assistant.png";
	private Image assImage = new Image(getClass().getResourceAsStream(ASSISTANT_IMAGE));
	private static final CardPaneFactory cardFactory = CardPaneFactory.getInstance();
	private static final PermitPaneFactory permitFactory = PermitPaneFactory.getInstance();
	private static final double V_SPACE = 100.0;
	Player player;
	Market market;
	Node addedNode=null;	
	
	GUIController controller;
	@FXML Button donebutton;
	@FXML BorderPane mainbuymarket;
	@FXML GridPane gridbuymarket;
	@FXML AnchorPane buybottom;
	
	/**
	 * Creates a new Buy pane
	 */
	public BuyMarketGUIGrid() {
		Platform.runLater(this::requestMarket);
	}
	
	/**
	 * This method is called when a market update is required
	 */
	@FXML
	public void requestMarket() {
		gridbuymarket.getChildren().clear();
		controller.requestMarketStatus();
		Utility.printDebug("Requested Market");
	}
	
	@FXML
	void buttonDone(){
		controller.passTurn();
		mainbuymarket.getScene().getWindow().hide();		
	}
	
	/**
	 * Updates the market with all Buyable items
	 * @param market
	 */
	public void marketUpdate(Market market) {
		gridbuymarket.getRowConstraints().clear();
		
		gridbuymarket.setAlignment(Pos.BASELINE_CENTER);
		gridbuymarket.setHgap(20.0);
		
		this.market = market;
		
		List<Sellable> items = market.getItemList();
		int currentLine = 0;
		for (; currentLine < items.size(); currentLine++)
			addSellableItem(items.get(currentLine), currentLine);
		
		
		for (int i = 0; i < currentLine; i++) {
			RowConstraints rc = new RowConstraints(V_SPACE + 20.0);
			gridbuymarket.getRowConstraints().add(rc);
		}
		
		gridbuymarket.setGridLinesVisible(false);
	}
	
	
	private void addSellableItem(Sellable sellable, int currentLine) {
		Label currentPrice = new Label(sellable.getPrice().toString());			
		Button currentButton = new Button();
		currentButton.setMinHeight(10);
		currentButton.setMinWidth(50);
		currentButton.setText("BUY");
		
		if (sellable instanceof Permit)
			addPermit(sellable, currentPrice, currentButton, currentLine);
		
		else if (sellable instanceof Card)
			addCard(sellable, currentPrice, currentButton, currentLine);
		
		else if (sellable instanceof Assistant)
			addAssistant((Assistant)sellable, currentPrice, currentButton, currentLine);
		
		
		gridbuymarket.add(currentPrice,PRICE_COLUMN,currentLine);
		gridbuymarket.add(currentButton,BUY_BUTTON_COLUMN,currentLine);
	}
	
	
	private void addAssistant(Assistant sellable, Label currentPrice, Button currentButton, int currentLine) {
		ImageView assView = new ImageView();
		assView.setImage(assImage);
		assView.setPreserveRatio(true);
		assView.setFitHeight(V_SPACE);
		gridbuymarket.add(assView,0,currentLine);
		currentButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				controller.doMarketAction(new BuyItemCommand(sellable));
				if(player.getCoins()>=sellable.getPrice().getCoins()){	
					gridbuymarket.getChildren().remove(assView);					
					gridbuymarket.getChildren().remove(currentPrice);
					gridbuymarket.getChildren().remove(currentButton);
					gridbuymarket.getRowConstraints().get(currentLine).setPrefHeight(0.0);
				}
			}
		});
	}
	private void addCard(Sellable sellable, Label currentPrice, Button currentButton, int currentLine) {
		CardPane buildedCard = cardFactory.buildCard((Card) sellable);
		gridbuymarket.add(buildedCard,0,currentLine);
		currentButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				controller.doMarketAction(new BuyItemCommand(sellable));
				if(player.getCoins()>=sellable.getPrice().getCoins()){	
					gridbuymarket.getChildren().remove(buildedCard);
					gridbuymarket.getChildren().remove(currentPrice);
					gridbuymarket.getChildren().remove(currentButton);		
					gridbuymarket.getRowConstraints().get(currentLine).setPrefHeight(0.0);
				}
			}
		});
	}
	private void addPermit(Sellable sellable, Label currentPrice, Button currentButton, int currentLine) {
		PermitPane buildedPermit = permitFactory.buildPermit((Permit) sellable);
		gridbuymarket.add(buildedPermit,0,currentLine);
		
		currentButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				controller.doMarketAction(new BuyItemCommand(sellable));
				if(player.getCoins()>=sellable.getPrice().getCoins()){
					gridbuymarket.getChildren().remove(buildedPermit);					
					gridbuymarket.getChildren().remove(currentPrice);
					gridbuymarket.getChildren().remove(currentButton);
					gridbuymarket.getRowConstraints().get(currentLine).setPrefHeight(0.0);
				}
			}
		});
	}
	
	public void setController(GUIController controller) {
		this.controller = controller;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
}
