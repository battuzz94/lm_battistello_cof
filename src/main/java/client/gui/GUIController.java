package client.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.controlsfx.control.Notifications;

import client.AbstractClientController;
import client.gui.events.BribeCouncilListener;
import client.gui.events.BribeKingListener;
import client.gui.events.BuildEmporiumListener;
import client.gui.events.ElectCouncillorListener;
import client.gui.events.ElectWithAssistantListener;
import client.gui.events.EventListener;
import client.gui.events.EventUtils;
import client.gui.events.GainOneCityRewardListener;
import client.gui.events.GainPermitListener;
import client.gui.events.GainPermitRewardListener;
import client.gui.events.GainTwoCityRewardListener;
import client.gui.events.IdleListener;
import client.gui.events.ShufflePermitsListener;
import client.gui.factories.BoardPaneFactory;
import client.gui.factories.BoardPaneFactory.BoardPane;
import client.gui.factories.BonusPaneFactory;
import client.gui.factories.CardPaneFactory;
import client.gui.factories.CardPaneFactory.CardPane;
import client.gui.factories.PermitPaneFactory;
import client.gui.factories.PermitPaneFactory.PermitPane;
import cof.bonus.GainOneCityRewardBonus;
import cof.bonus.GainPermitBonus;
import cof.bonus.GainPermitRewardBonus;
import cof.bonus.GainTwoCityRewardBonus;
import cof.commands.AddPrimaryMoveCommand;
import cof.commands.EngageAssistantCommand;
import cof.commands.PassTurnCommand;
import cof.market.Market;
import cof.model.Board;
import cof.model.Card;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Player.PlayerStat;
import common.Utility;
import common.network.Message;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
/**
 * This Class is the controller of the main GUI.
 * @author Simone
 *
 */
public class GUIController extends AbstractClientController implements Initializable {
	private static final String STYLESHEET_LOCATION = "style.css";
	private static final Logger LOGGER = Logger.getLogger(GUIController.class.getName());
	private static final int SPACE_CARD = 20;
	private static final ExecutorService threadPool = Executors.newSingleThreadExecutor();
	
	private PreMarketGUIGrid preMarketGrid;
	private static final CardPaneFactory cardFactory = CardPaneFactory.getInstance();
	private static final BoardPaneFactory boardFactory = BoardPaneFactory.getInstance();
	private static final BonusPaneFactory bonusFactory = BonusPaneFactory.getInstance();
	private BuyMarketGUIGrid buyGrid;
	private BoardPane boardPane;
	private HBox hboxPlayer;
	
	@FXML private TextArea textareainfo;
	@FXML private Label isyturn;
	@FXML private Label turnDescription;
	@FXML private AnchorPane playeranchor;
	@FXML private Pane playerinfo;
	@FXML private GridPane grid;
	@FXML private GridPane gridnob;
	@FXML private AnchorPane map;
	@FXML private AnchorPane playerStat;
	@FXML private AnchorPane otherStat;

	@FXML private Button electCouncillorButton;
	@FXML ScrollPane scrollpane;
	@FXML ScrollPane playerScrollPane;
	
	private List<CardPane> cards = new ArrayList<>();
	private List<PermitPane> permits = new ArrayList<>();
	private EventListener currentListener;
	
	private Stage stage;
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Platform.runLater(() -> {
			map.getStyleClass().add("root");
			
			// Resize player scroll pane when resizing window
			scrollpane.viewportBoundsProperty().addListener((obj, oldVal, newVal) -> playerScrollPane.setPrefViewportWidth(newVal.getWidth()));
			this.stage = (Stage) map.getScene().getWindow();
			
			turnDescription.setVisible(true);
			turnDescription.setAlignment(Pos.CENTER);
			
			isyturn.setAlignment(Pos.CENTER);
		});
	}
	
	
	@FXML
	void requestStat(){
		Player p1 = new Player(10, 10);
		p1.setUsername("A");
		Player p2 = new Player(10, 10);
		p2.setUsername("B");
		p2.addNobility(5);
		p2.addCoins(5);
		p2.addPoints(10);
		this.gameFinished(Arrays.asList(p1.getPlayerStat(), p2.getPlayerStat(), p1.getPlayerStat(), p2.getPlayerStat(), p1.getPlayerStat(), p2.getPlayerStat()));
		this.updateOtherPlayersStats(Arrays.asList(p1.getPlayerStat(), p2.getPlayerStat(), p1.getPlayerStat(), p2.getPlayerStat(), p1.getPlayerStat(), p2.getPlayerStat()));
	}
	
	
	/**
	 * This method is called on Elect Councillor Main Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void electCouncillor() {
		if (boardPane != null) {
			currentListener.reset();
			currentListener = new ElectCouncillorListener(this);
			setListener(currentListener);
		}
	}
	
	/**
	 * This method is called on Bribe Council Main Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void bribeCouncil() {
		if (boardPane != null) {
			currentListener.reset();
			currentListener = new BribeCouncilListener(this);
			setListener(currentListener);
		}
	}
	
	/**
	 * This method is called on Bribe King Main Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void bribeKing() {
		if (boardPane != null) {
			currentListener.reset();
			currentListener = new BribeKingListener(this);
			setListener(currentListener);
		}
	}
	
	/**
	 * This method is called on Build Emporium Main Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void buildEmporium() {
		if (boardPane != null) {
			currentListener.reset();
			currentListener = new BuildEmporiumListener(this);
			setListener(currentListener);
		}
	}
	
	/**
	 * This method is called on Engage Assistant Quick Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void engageAssistant(ActionEvent event) {
		currentListener.reset();
		currentListener = new IdleListener();
		EngageAssistantCommand command = new EngageAssistantCommand(); 
		if (EventUtils.showConfirmationDialog(event, command)) {
			doQuickAction(command);
		}
	}
	
	/**
	 * This method is called on Add Primary Move Quick Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void addPrimaryMove(ActionEvent event) {
		currentListener.reset();
		currentListener = new IdleListener();
		AddPrimaryMoveCommand command = new AddPrimaryMoveCommand(); 
		if (EventUtils.showConfirmationDialog(event, command)) {
			doQuickAction(command);
		}
	}
	
	/**
	 * This method is called on Elect With Assistant Quick Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void electWithAssistant() {
		if (boardPane != null) {
			currentListener.reset();
			currentListener = new ElectWithAssistantListener(this);
			setListener(currentListener);
		}
	}
	
	/**
	 * This method is called on Shuffle Permit Quick Action, on button pressed.
	 * @param event
	 */
	@FXML
	protected void shufflePermits() {
		if (boardPane != null) {
			currentListener.reset();
			currentListener = new ShufflePermitsListener(this);
			setListener(currentListener);
		}
	}

	@FXML
	protected void passTurn() {
		PassTurnCommand passTurn = new PassTurnCommand();
		doPassTurnCommand(passTurn);
	}

	@FXML
	void testLoadBoard() {
		board = Utility.loadTestBoardDisposed();
		me = new Player(10, 10);
		for (int i = 0; i < 15; i++)
			me.obtain(Card.drawCard());
		me.buildEmporium(board.getCities().get(1));
		new Player(10,10).buildEmporium(board.getCities().get(2));
		new Player(10,10).buildEmporium(board.getCities().get(2));
		new Player(10,10).buildEmporium(board.getCities().get(2));
		new Player(10,10).buildEmporium(board.getCities().get(2));
		Permit p = board.getRegionByTag("Region1").takePermitByTag("Permit1");
		me.obtain(p);
		
		Permit p2 = board.getRegionByTag("Region1").takePermitByTag("Permit2");
		p2.setUsed();
		me.obtain(p2);
		
		
		me.addNobility(20);
		showLoadingIndicator();
		
		printBoard();
		printPlayer();
	}

	private void showLoadingIndicator() {
		final ProgressIndicator progress = new ProgressIndicator();
		progress.setMaxSize(50, 50);
		
		progress.setLayoutX(scrollpane.getBoundsInParent().getWidth() / 2.0);
		progress.setLayoutY(scrollpane.getBoundsInParent().getHeight() / 2.0 + scrollpane.getVvalue());

		map.getChildren().add(progress);
		map.setEffect(new Blend(BlendMode.MULTIPLY));
	}
	
	/**
	 * Set the content of turn description
	 */
	public void setTurnDescriptionContent(String description) {
		if (Platform.isFxApplicationThread())
			turnDescription.setText(description);
		else
			Platform.runLater(() -> turnDescription.setText(description));
	}
	
	/**
	 * Sets the visibility of the turn description label as specified
	 * @param visible
	 */
	public void setTurnDescriptionVisible(boolean visible) {
		if (Platform.isFxApplicationThread())
			turnDescription.setVisible(visible);
		else
			Platform.runLater(() -> turnDescription.setVisible(visible));
	}
	
	@Override
	public void showInfoMessage(Message message) {
		Platform.runLater(
				() -> textareainfo.appendText(message.getContent() + "\n")
			);
	}
	
	@Override
	public void showInfoTimeOut(Message message) {
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.ERROR);
			if (stage != null)
				alert.initOwner(stage);
			alert.setTitle("A player disconnected!");
			alert.setContentText(message.getContent());
			alert.showAndWait();
		});
	}
	
	
	@Override
	public void showTimeOutMessage(Message message) {
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.ERROR);
			if (stage != null)
				alert.initOwner(stage);
			alert.setTitle("Timeout!");
			alert.setContentText("Unfortunately you ran out of time. You won't be able to play anymore");
			alert.showAndWait();
		});
	}
	
	@Override
	public void showAdditionalInputMessage(Message message) {
		Platform.runLater(() -> 
			Notifications.create()
			.title("Additional input required")
			.text("An additional action is required in order to go further")
			.hideAfter(Duration.seconds(3))
			.showInformation());
	}

	@Override
	public void showNetworkError() {
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.ERROR);
			if (stage != null)
				alert.initOwner(stage);
			alert.setTitle("Network error!");
			alert.setContentText("Unfortunately there was a network communication error.");
			alert.showAndWait();
		});
	}

	@Override
	public void showBadCommand() {
		Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.WARNING);
			if (stage != null)
				alert.initOwner(stage);
			alert.setTitle("Command not valid!");
			alert.setContentText("The command you tried to do is not valid!");
			alert.show();
		});

	}

	@Override
	public void showCommandValid() {
		Platform.runLater(() -> Notifications.create().title("Command valid").text("The command was valid")
				.hideAfter(Duration.seconds(3)).showInformation());
	}
	
	/**
	 * This method populate board in the main gui.
	 */
	protected void printBoard() {
		threadPool.submit(this::printBoardService);
	}


	private synchronized void printBoardService() {
		boardPane = boardFactory.buildBoard(board, scrollpane.viewportBoundsProperty(), me);
		currentListener = new IdleListener();
		setTurnDescriptionContent("");
		setListener(currentListener);
		
		
		boardPane.setNobilityCallbacks((ev, bonus) -> {
			currentListener.reset();
			if (bonus instanceof GainOneCityRewardBonus)
				setListener(new GainOneCityRewardListener(this));
			else if (bonus instanceof GainTwoCityRewardBonus)
				setListener(new GainTwoCityRewardListener(this));
			else if (bonus instanceof GainPermitBonus)
				setListener(new GainPermitListener(this));
			else if (bonus instanceof GainPermitRewardBonus)
				setListener(new GainPermitRewardListener(this));
		});

		Platform.runLater(() -> {
			map.getChildren().setAll(boardPane);
			map.setEffect(null); // Removes the darken effect
		});
	}
	
	/**
	 * This set listeners of BoardPane, all Cards and Permits.
	 * @param listener
	 */
	private void setListener(EventListener listener) {
		boardPane.setListeners(listener);
		cards.forEach(c -> {
			String cardTag = c.getId();
			c.setOnMouseEntered(e -> listener.onEnterCard(e, cardTag));
			c.setOnMouseExited(e -> listener.onExitCard(e, cardTag));
			c.setOnMouseClicked(e -> listener.onActionCard(e, cardTag));
		});
		permits.forEach(p -> {
			String permitTag = p.getId();
			p.setOnMouseEntered(e -> listener.onEnterPlayerPermit(e, permitTag));
			p.setOnMouseExited(e -> listener.onExitPlayerPermit(e, permitTag));
			p.setOnMouseClicked(e -> listener.onActionPlayerPermit(e, permitTag));
		});
	}

	
	protected void printPlayer() {
		Platform.runLater(this::printPlayerService);
	}

	private synchronized void printPlayerService() {
		permits.clear();
		cards.clear();
		playeranchor.getChildren().clear();
		hboxPlayer = new HBox();
		hboxPlayer.setSpacing(SPACE_CARD);
		
		cardFactory.setCardHeight(100.0);
		for (Card card : me.getCards()) {
			CardPane buildCard = cardFactory.buildCard(card);
			buildCard.setId(card.getTag());
			cards.add(buildCard);
			hboxPlayer.getChildren().add(buildCard);
		}
		PermitPaneFactory permitFactory = PermitPaneFactory.getInstance();
		permitFactory.setHeight(100.0);
		for (Permit permit : me.getPermits()) {
			PermitPane p = permitFactory.buildPermit(permit);
			p.setId(permit.getTag());
			permits.add(p);
			hboxPlayer.getChildren().add(p);
		}

		playeranchor.getChildren().add(hboxPlayer);
		
		bonusFactory.setBonusWidth(30.0);
		Label playerLabel = new Label(me.getUsername() + ": ");
		playerLabel.setGraphic(bonusFactory.getPlayerInfo(me.getVictoryPoints(), me.getNobility(), me.getCoins(), me.getAssistants()));
		playerLabel.setContentDisplay(ContentDisplay.BOTTOM);
		playerStat.getChildren().clear();
		playerStat.getChildren().add(playerLabel);
	}
	
	
	/**
	 * This method manage Message Received from Client.
	 */
	@Override
	public void handleServerInput(Message message) {
		switch (message.getType()) {
		case BOARD_UPDATE:
			printBoard();
			break;
		case PLAYER_UPDATE:
			if (buyGrid != null) {
				buyGrid.setPlayer(me);
			}
			printPlayer();
			break;
		case INIT_TURN:
			Platform.runLater(() -> setTurnLabel("Is Your Turn"));
			break;
		case TURN_FINISHED:
			isyturn.setVisible(false);
			break;
		case CONFIGURE_BOARD:
			initBoardConfiguration();
			break;
		case SELL_MARKET_ACTION:
			Platform.runLater(() -> setTurnLabel("Market Sell Session!"));			
			startPreMarket();
			break;
		case INIT_MARKET_TURN:
			Platform.runLater(() -> isyturn.setText("Market Buy Session!"));
			startBuyMarket();
			break;
		case BUY_MARKET_ACTION:
			Platform.runLater(() -> isyturn.setVisible(true));			
			break;
		case MARKET_UPDATE:
			Market market = gson.fromJson(message.getContent(), Market.class);
			marketUpdate(market);
			break;
		case TIMER:
			Platform.runLater(() -> setTimerLabel(message));
			break;
		default:
		}
	}


	private void setTurnLabel(String text) {
		isyturn.setText(text);
		isyturn.setVisible(true);
	}


	private void setTimerLabel(Message message) {
		if("0".equals(message.getContent()))
			turnDescription.setText("Waiting for Board Configuration");
		else
			turnDescription.setText("Wait:\n"+message.getContent());
	}

	/**
	 * This method send to BuyMarketController the market.
	 * 
	 * @param market
	 */
	private void marketUpdate(Market market) {
		if (buyGrid != null) {
			Platform.runLater(() -> buyGrid.marketUpdate(market));
		}
	}
	
	private void startBuyMarket() {
		Platform.runLater(() -> {
			try {
				FXMLLoader buyMarketLoader = new FXMLLoader(getClass().getResource("BuyMarketGrid.fxml"));
				Parent buyMarketParent = (Parent) buyMarketLoader.load();
				buyMarketParent.getStylesheets().add(getClass().getResource(STYLESHEET_LOCATION).toExternalForm());
				this.buyGrid = (BuyMarketGUIGrid) buyMarketLoader.getController();
				this.buyGrid.setPlayer(me);
				this.buyGrid.setController(this);
				Stage buyStage = new Stage();
				
				buyStage.initModality(Modality.APPLICATION_MODAL);
				if (buyStage != null)
					buyStage.initOwner(stage);
				buyStage.setTitle("Market");
				buyStage.setScene(new Scene(buyMarketParent));
				buyStage.show();
			} catch (Exception e) {
				LOGGER.log(Level.INFO, "Error on loading Buy Market FXML", e);
			}
		});
	}
	
	/**
	 * This method is called, when GUI receive Sell Market Action.
	 */
	@FXML
	private void startPreMarket(){
		Platform.runLater(this::startPreMarketService);
	}

	/**
	 * Starts a pre market. WARNING: Must be called from JavaFX Thread
	 */
	private void startPreMarketService() {
		try {
			FXMLLoader marketLoader= new FXMLLoader(getClass().getResource("PreMarketGrid.fxml"));
			Parent marketParent=(Parent)marketLoader.load();
			marketParent.getStylesheets().add(getClass().getResource(STYLESHEET_LOCATION).toExternalForm());
			this.preMarketGrid=(PreMarketGUIGrid)marketLoader.getController();
			this.preMarketGrid.setPlayer(me);
			this.preMarketGrid.setController(this);
			this.preMarketGrid.draw();
			Stage marketStage= new Stage();
			marketStage.initModality(Modality.APPLICATION_MODAL);
			if (stage != null) 
				marketStage.initOwner(stage);
			marketStage.setTitle("Market");
			marketStage.setScene(new Scene(marketParent));
			marketStage.show();
		} catch (Exception e) {
			LOGGER.log(Level.FINE, "Error on loading Market FXML", e);
		}
	}

	/**
	 * This method shows a new window that gives the possibility to configure the board, 
	 * called when server receive message: CONFIGURE_BOARD
	 */
	private void initBoardConfiguration() {
		Platform.runLater(() -> {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ChooseBoardConfig.fxml"));
				Parent configParent = (Parent) fxmlLoader.load();

				ChooseBoardConfig configController = fxmlLoader.<ChooseBoardConfig> getController();
				configController.setController(this);
				Stage configurationStage = new Stage();
				configurationStage.initModality(Modality.APPLICATION_MODAL);
				if (configurationStage != null)
					configurationStage.initOwner(stage);
				configurationStage.setTitle("Configure board");
				configurationStage.setMinWidth(1080.0);
				configurationStage.setScene(new Scene(configParent));
				configurationStage.getScene().getStylesheets().add(getClass().getResource(STYLESHEET_LOCATION).toExternalForm());
				configurationStage.show();
			} catch (IOException e) {
				LOGGER.log(Level.FINE, "Error on loading BoardConfiguration FXML", e);
			}
		});
	}

	/**
	 * This is called when the GUI needs to change the listeners associated to
	 * each component
	 * 
	 * @param listener
	 *            the new listener that will manage all the GUI inputs
	 */
	public void setState(EventListener listener) {
		if (boardPane != null)
			boardPane.setListeners(listener);
	}

	public Board getBoard() {
		return board;
	}


	@Override
	protected void gameFinished(List<PlayerStat> finalRanking) {
		VBox rankingVBox = finalRanking.stream()
				.map(stat -> {
					Label label = new Label(stat.getUsername() + ": ");
					label.setGraphic(bonusFactory.getPlayerStatInfo(stat.getVictoryPoints(), stat.getNobilityTrack()));
					label.setContentDisplay(ContentDisplay.RIGHT);
					
					return label;
				})
				.reduce(new VBox(), (vbox, e) -> 
					{
						vbox.getChildren().add(e); 
						return vbox;
					}, 
					(a,b) -> a);
		rankingVBox.setFillWidth(true);
		rankingVBox.setPrefHeight(200.0);
		rankingVBox.setPrefWidth(400.0);
		
		BorderPane rankingBorderPane = new BorderPane();
		rankingBorderPane.setCenter(Utility.encapsulate(new AnchorPane(), rankingVBox));
		
		rankingBorderPane.setPadding(new Insets(100.0));
		
		Platform.runLater(() -> {			
			Dialog<?> alert = new Alert(AlertType.INFORMATION);
			alert.getDialogPane().getStyleClass().add("alert-dialog");
			alert.setTitle("Game finished!");
			alert.setHeaderText(null);
			alert.initOwner(stage);
			alert.setContentText("Here is the final ranking: ");
			alert.getDialogPane().setPrefSize(400.0, 400.0);
			alert.getDialogPane().getChildren().add(rankingBorderPane);
			alert.setHeight(400.0);
			alert.setResizable(true);
			alert.showAndWait();
		});
	}


	@Override
	protected void updateOtherPlayersStats(List<PlayerStat> stats) {
		bonusFactory.setBonusWidth(20.0);
		Platform.runLater(() -> 
			otherStat.getChildren()
				.setAll(stats.stream()
					.map(stat -> {
						Label label = new Label(stat.getUsername() + ": ");
						label.setGraphic(bonusFactory.getPlayerStatInfo(stat.getVictoryPoints(), stat.getNobilityTrack()));
						label.setContentDisplay(ContentDisplay.RIGHT);
						
						return label;
					})
					.reduce(new VBox(), (vbox, e) -> 
						{
							vbox.getChildren().add(e); 
							return vbox;
						}, (a,b) -> a)
				)
		);
				
	}
	
}
