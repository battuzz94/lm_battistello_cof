package client.gui;

import java.util.logging.Level;
import java.util.logging.Logger;

import common.Utility;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Asks the user the connection type and start connection to the server
 * @author andrea
 *
 */
public class LoginController {
	private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());
	
	@FXML private TextField username;
	@FXML private Label label;
	@FXML private RadioButton radioRMI;
	@FXML private RadioButton radioSocket;
	
	private String myUsername;
	private String connectionType;
	private boolean enabled=true;
	/**
	 * When loaded this controller sets by default connection RMI, 
	 * and join with enter key pressed.
	 */
	public void initialize(){
		radioSocket.setSelected(false);
		radioRMI.setSelected(true);
		radioRMI.requestFocus();
		connectionType="RMI";
		Utility.printDebug("GUI:Selected RMI");
		
		username.setOnKeyReleased(new EventHandler<KeyEvent>() {

			
			@Override
			public void handle(KeyEvent event) {
				setUsername();
				 if (event.getCode().equals(KeyCode.ENTER)){
					 join(event);
				 }
			}

		});
		
	}
	
	@FXML
	private void setUsername() {
		myUsername = username.getText();
	}


	@FXML
	private void selectedRMI() {
		radioSocket.setSelected(false);
		radioRMI.setSelected(true);
		radioRMI.requestFocus();
		connectionType="RMI";
		Utility.printDebug("GUI:Selected RMI");
	}

	@FXML
	private void selectedSocket() {
		radioRMI.setSelected(false);
		radioSocket.setSelected(true);
		radioSocket.requestFocus();
		connectionType="socket";
		Utility.printDebug("GUI:Selected Socket");
	}
	
	@FXML
	private void join(Event event) {
		if(myUsername.length()>0&&enabled){
			enabled=false;
		try {
			
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/client/gui/template.fxml"));
			Scene mainScene = new Scene(loader.load());
			mainScene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			GUIController controller = loader.getController();
			
			controller.beginConnection(connectionType, myUsername);
			

			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			
			stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					controller.disconnect();
					System.exit(0);					
				}
			});
			
			stage.setScene(mainScene);
			stage.setMinWidth(961);
			stage.setMinHeight(701);
			// stage.setFullScreen(true)
			
			stage.show();

		} catch (Exception e) {
			LOGGER.log(Level.FINE, "Could not connect to server", e);
			enabled = true;
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Network error!");
			alert.setContentText("Unfortunately there was a network communication error.");
			alert.showAndWait();
		}
	}
	}
}
