package client.gui;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Starts a test view of the main game layout
 * @author andrea
 *
 */
public class TestMainView extends Application {
	
	/**
	 * Starts the application
	 * @param args
	 */
	public static void main(String[] args) {
		launch();
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		startMain(stage);
	}

	private void startMain(Stage stage) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/client/gui/template.fxml"));
		Scene mainScene = new Scene(loader.load());
		mainScene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		
		stage.setScene(mainScene);
		stage.setMinWidth(961);
		stage.setMinHeight(701);
		stage.show();
	}

}
