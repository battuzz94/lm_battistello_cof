package client.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
/**
 * This Class starts the first Window with Login.
 * @author Simone
 *
 */
public class GUIView extends Application{
	private static final Logger LOGGER = Logger.getLogger(GUIView.class.getName());
	private Stage primaryStage;
	private BorderPane rootLayout;

	/**
	 * This class show the login, is the first window when launched GUI.
	 */
	public void start() {
		launch();
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("GUI Client CoF");
		startLayout();
	}

	private void startLayout() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/client/gui/ClientGUI.fxml"));
		
		try {
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			LOGGER.log(Level.FINE, "Exception on creating view layout", e);
		}
	}
}
