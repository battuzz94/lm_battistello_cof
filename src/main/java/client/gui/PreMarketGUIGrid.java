package client.gui;


import client.gui.factories.CardPaneFactory;
import client.gui.factories.CardPaneFactory.CardPane;
import client.gui.factories.PermitPaneFactory;
import client.gui.factories.PermitPaneFactory.PermitPane;
import cof.commands.SellItemCommand;
import cof.market.Assistant;
import cof.model.Card;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Price;
import common.Utility;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;

/**
 * Creates a new sell pane
 * @author andrea
 *
 */
public class PreMarketGUIGrid {
	private static final String SELL_LABEL_TEXT = "set to sell: ";
	private static final int SPINNERS_COLUMN = 1;
	private static final int SELL_BUTTON_COLUMN = 2;
	private static final int ITEM_COLUMN = 0;
	private static final String ASSISTANT_IMAGE = "/images/assistant.png";
	private static final double V_SPACE =  100.0;
	private static final CardPaneFactory cardFactory = CardPaneFactory.getInstance();
	private static final PermitPaneFactory permitFactory = PermitPaneFactory.getInstance();
	
	private Image assImage = new Image(getClass().getResourceAsStream(ASSISTANT_IMAGE));
	
	int availAss;
	Player player;
	ImageView assView;
	
	GUIController controller;
	
	@FXML AnchorPane premarketbottom;
	@FXML BorderPane maingridmarket;
	@FXML GridPane gridmarket;
	
	/**
	 * Starts a new premarket view
	 */
	public PreMarketGUIGrid(){
		assView = new ImageView();
		assView.setImage(assImage);
		assView.setFitHeight(V_SPACE);
		assView.setPreserveRatio(true);
	
	}
	
	public void setPlayer(Player me) {
		this.player = me;
	}
	
	/**
	 * Generates the view
	 */
	public void draw(){
		gridmarket.getRowConstraints().clear();
		gridmarket.getColumnConstraints().clear();
		
		for (int i = 0; i < 3; i++) {
			ColumnConstraints col = new ColumnConstraints();
			col.setHgrow(Priority.ALWAYS);
			gridmarket.getColumnConstraints().add(col);
		}
		
		gridmarket.setAlignment(Pos.BASELINE_CENTER);
		gridmarket.setHgap(20.0);
		
		int currentLine = 0;
		for (int i = 0; i < player.getCards().size(); i++)
			addPlayerCard(player.getCards().get(i), currentLine++);
		
		for (int i = 0; i < player.getPermits().size(); i++)
			addPlayerPermit(player.getPermits().get(i), currentLine++);
		
		
		availAss = player.getAssistants();	
		if (availAss > 0) {
			Spinner<Integer> nAssistant = createSpinner(1, availAss);
			
			VBox vbox= attachLabel("Number Of Assistants:", nAssistant);
			
			Spinner<Integer> priceAssistant = createSpinner(0, 100);
			VBox price= attachLabel("Price:", priceAssistant);
			
			
			gridmarket.add(assView,ITEM_COLUMN,currentLine);
			HBox assistantSpinners = Utility.encapsulate(new HBox(), vbox, price); 
			assistantSpinners.setSpacing(20.0);
			gridmarket.add(assistantSpinners, SPINNERS_COLUMN, currentLine);
			
			Button currentButton = new Button();
			VBox vbutton = attachLabel("", currentButton);
			currentButton.setMinHeight(10);
			currentButton.setMinWidth(50);
			currentButton.setText("SELL");
			final int assistantLine = currentLine;
			currentButton.setOnMouseClicked(event -> { 
					for (int i = 0; i < (int) nAssistant.getValue(); i++) {
						Assistant assistant = new Assistant();
						assistant.setPrice(new Price((int) priceAssistant.getValue(), 0));
	
						controller.doPreMarketAction(new SellItemCommand(assistant));
					}
					Utility.printDebug(SELL_LABEL_TEXT + (int) nAssistant.getValue() + "assistant at price: "
							+ (int) priceAssistant.getValue());
					setAvailAss(getAvailAss() - (int) nAssistant.getValue());
					if (getAvailAss() <= 0) {
						gridmarket.getChildren().remove(assView);
						gridmarket.getChildren().remove(vbox);
						gridmarket.getChildren().remove(price);	
						gridmarket.getChildren().remove(vbutton);
						gridmarket.getChildren().remove(assistantSpinners);
						gridmarket.getRowConstraints().get(assistantLine).setPrefHeight(0.0);
					} else
						nAssistant.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, getAvailAss()));
				});
			gridmarket.add(vbutton, SELL_BUTTON_COLUMN, currentLine);
			currentLine++;		
		}
		
		Button gotoMarket = new Button();
		gotoMarket.setMinHeight(10);
		gotoMarket.setMinWidth(50);
		gotoMarket.setText("Go To Market");
		gotoMarket.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				controller.requestMarketStatus();
				controller.passTurn();
				maingridmarket.getScene().getWindow().hide();
			}
		});
		VBox goMarket = attachLabel("", gotoMarket);
		premarketbottom.getChildren().add(goMarket);
		AnchorPane.setRightAnchor(goMarket, 10.0);
		AnchorPane.setBottomAnchor(goMarket, 8.0);
		AnchorPane.setBottomAnchor(goMarket, 10.0);
		
		
		for (int i = 0; i < currentLine; i++) {
			RowConstraints rc = new RowConstraints(V_SPACE + 20.0);
			gridmarket.getRowConstraints().add(rc);
		}
		
		gridmarket.setGridLinesVisible(false);
	}

	private void addPlayerPermit(Permit permit, final int currentLine) {
		permitFactory.setHeight(V_SPACE);
		PermitPane buildedPermit = permitFactory.buildPermit(permit);
		
		gridmarket.add(buildedPermit,ITEM_COLUMN,currentLine);
		
		Spinner<Integer> currentSpinner = createSpinner(0, 100);			
		VBox vbox= attachLabel("Price:", currentSpinner);
		gridmarket.add(vbox,SPINNERS_COLUMN,currentLine);
		Button currentButton = new Button("SELL");
		VBox vbutton=attachLabel("", currentButton);
		currentButton.setMinHeight(10);
		currentButton.setMinWidth(50);
		currentButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				permit.setPrice(new Price((int) currentSpinner.getValue(), 0));
				controller.doPreMarketAction(new SellItemCommand(permit));
				Utility.printDebug(SELL_LABEL_TEXT + permit + "at price: " + permit.getPrice());
				gridmarket.getChildren().remove(buildedPermit);
				gridmarket.getChildren().remove(vbutton);
				gridmarket.getChildren().remove(vbox);	
				gridmarket.getRowConstraints().get(currentLine).setPrefHeight(0.0);
			}
		});
		gridmarket.add(vbutton,SELL_BUTTON_COLUMN,currentLine);
	}

	private void addPlayerCard(Card card, final int currentLine) {
		cardFactory.setCardHeight(V_SPACE);
		CardPane buildedCard = cardFactory.buildCard(card);
		
		gridmarket.add(buildedCard, ITEM_COLUMN, currentLine);
		
		Spinner<Integer> currentSpinner = createSpinner(0, 100);
		VBox vbox= attachLabel("Price",currentSpinner);
		gridmarket.add(vbox, SPINNERS_COLUMN, currentLine);
		Button currentButton = new Button();
		VBox vbutton= attachLabel("", currentButton);
		currentButton.setMinHeight(10);
		currentButton.setMinWidth(50);
		currentButton.setText("SELL");
		currentButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				card.setPrice(new Price((int) currentSpinner.getValue(), 0));
				controller.doPreMarketAction(new SellItemCommand(card));
				Utility.printDebug(SELL_LABEL_TEXT + card + "at price: " + card.getPrice());
				gridmarket.getChildren().remove(buildedCard);
				gridmarket.getChildren().remove(vbutton);
				gridmarket.getChildren().remove(vbox);	
				gridmarket.getRowConstraints().get(currentLine).setPrefHeight(0.0);
			}
		});
		gridmarket.add(vbutton, SELL_BUTTON_COLUMN, currentLine);
	}
	private Spinner<Integer> createSpinner(int min, int max) {
		Spinner<Integer> currentSpinner = new Spinner<>();
		currentSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(min, max));
		currentSpinner.setEditable(false);
		currentSpinner.setPrefWidth(100.0);
		return currentSpinner;
	}
	
	private void setAvailAss(int availAss) {
		this.availAss = availAss;
	}
	
	private VBox attachLabel(String label, Node node){
		VBox vbox= new VBox();
		vbox.setAlignment(Pos.CENTER);
		Label etichetta= new Label(label);
		vbox.getChildren().add(etichetta);
		vbox.getChildren().add(node);
		return vbox;
		
	}
	
	private int getAvailAss() {
		return this.availAss;
	}
	public void setController(GUIController guiController) {
		this.controller=guiController;
		
	}
	
}
