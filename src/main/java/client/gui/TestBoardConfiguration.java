package client.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Tests the behaviour of ConfigureBoard view
 * @author andrea
 *
 */
public class TestBoardConfiguration extends Application{
	
	private static final Logger LOGGER = Logger.getLogger(TestBoardConfiguration.class.getName());

	/**
	 * Starts the board configuration view for tests
	 * @param stage root stage
	 */
	public void start(final Stage stage) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("ChooseBoardConfig.fxml"));
		try {
			Parent parent = loader.load();
			Scene scene = new Scene(parent);
			scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			stage.setMinWidth(1080.0);
			stage.setScene(scene);
			stage.show();
		}
		catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not load board configuration view", e);
		}
		
	}
	
	/**
	 * Starts the application
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}
}
