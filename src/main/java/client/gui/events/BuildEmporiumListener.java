package client.gui.events;

import client.gui.GUIController;
import cof.commands.BuildEmporiumCommand;
import cof.model.Board;
import cof.model.City;
import cof.model.Permit;
import cof.model.Player;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This is the Listener passed by GUIController, when user perform click on  Build Emporium Main Action.
 * @param controller
 */

public class BuildEmporiumListener implements EventListener {
	private static final String INITIAL_STATE = "Select one of your available permits";
	private static final String SELECTED_PERMIT = "Change permit or select the city to build in";
	private GUIController controller;
	private Node selectedPermit;
	private Board board;
	private Player player;
	
	/**
	 * Creates a new listener to build an emporium
	 * @param controller
	 */
	public BuildEmporiumListener(GUIController controller) {
		this.controller = controller;
		this.board = controller.getBoard();
		this.player = controller.getPlayer();
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}
	
	@Override
	public void onEnterPlayerPermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		if ((selectedPermit == null || !selectedPermit.getId().equals(permitTag)) && !player.getPermitByTag(permitTag).isUsed() ) {	
			source.setTranslateY(-15.0);
		}
		source.getScene().setCursor(Cursor.HAND);
	}

	@Override
	public void onExitPlayerPermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		if ((selectedPermit == null || !selectedPermit.getId().equals(permitTag)) && !player.getPermitByTag(permitTag).isUsed()) {	
			source.setTranslateY(0.0);
		}
		source.getScene().setCursor(Cursor.DEFAULT);
	}

	@Override
	public void onActionPlayerPermit(MouseEvent event, String permitTag) {
		if (!player.getPermitByTag(permitTag).isUsed()) {
			if (selectedPermit != null)
				selectedPermit.setTranslateY(0.0);
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			selectedPermit = source;
			
			controller.setTurnDescriptionContent(SELECTED_PERMIT);
		}
	}

	@Override
	public void onEnterCity(MouseEvent event, String cityTag) {
		if (selectedPermit != null && canBuildInCity(cityTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCity(MouseEvent event, String cityTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(0.0);
		source.getScene().setCursor(Cursor.DEFAULT);
	}

	@Override
	public void onActionCity(MouseEvent event, String cityTag) {
		if (selectedPermit != null && canBuildInCity(cityTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
			
			BuildEmporiumCommand command = new BuildEmporiumCommand(selectedPermit.getId(), cityTag);
			
			if (EventUtils.showConfirmationDialog(event, command)) {
				controller.doMainAction(command);
			}
			
			reset();
			
			((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
			
			controller.setState(new IdleListener());
		}
	}

	@Override
	public void reset() {
		if (selectedPermit != null)
			selectedPermit.setTranslateY(0.0);
		
		controller.setTurnDescriptionContent("");
	}
	
	private boolean canBuildInCity(String cityTag) {
		Permit permit = player.getPermitByTag(selectedPermit.getId());
		City city = board.getCityByTag(cityTag);
		return permit.canBuildIn(city);
	}

}
