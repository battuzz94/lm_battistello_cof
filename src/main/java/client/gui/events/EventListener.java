package client.gui.events;

import javafx.scene.input.MouseEvent;

/**
 * Interface that lets you listen to game events
 * @author andrea
 *
 */
public interface EventListener {
	/**
	 * Fired when mouse entered an available councillor
	 * @param event
	 * @param councillorTag
	 */
	public default void onEnterAvailableCouncillor(MouseEvent event, String councillorTag) {}
	
	/**
	 * Fired when the mouse exited an available councillor
	 * @param event
	 * @param councillorTag
	 */
	public default void onExitAvailableCouncillor(MouseEvent event, String councillorTag) {}
	
	/**
	 * Fired when the user clicks on an available councillor
	 * @param event
	 * @param councillorTag
	 */
	public default void onActionAvailableCouncillor(MouseEvent event, String councillorTag) {}
	
	
	/**
	 * Fired when the mouse enters a council
	 * @param event
	 * @param councilTag
	 */
	public default void onEnterCouncil(MouseEvent event, String councilTag) {}
	
	/**
	 * Fired when the mouse exits a council
	 * @param event
	 * @param councilTag
	 */
	public default void onExitCouncil(MouseEvent event, String councilTag) {}
	
	/**
	 * Fired when the user clicks on a council
	 * @param event
	 * @param councilTag
	 */
	public default void onActionCouncil(MouseEvent event, String councilTag) {}
	
	/**
	 * Fired when the mouse enters a gainable permit
	 * @param event
	 * @param permitTag
	 */
	public default void onEnterGainablePermit(MouseEvent event, String permitTag) {}
	
	/**
	 * Fired when the mouse exits from a gainable permit
	 * @param event
	 * @param permitTag
	 */
	public default void onExitGainablePermit(MouseEvent event, String permitTag) {}
	
	/**
	 * Fired when the user clicks on a gainable permit
	 * @param event
	 * @param permitTag
	 */
	public default void onActionGainablePermit(MouseEvent event, String permitTag) {}
	
	
	/**
	 * Fired when the mouse enters a player permit
	 * @param event
	 * @param permitTag
	 */
	public default void onEnterPlayerPermit(MouseEvent event, String permitTag) {}
	
	/**
	 * Fired when the mouse exits from a player permit
	 * @param event
	 * @param permitTag
	 */
	public default void onExitPlayerPermit(MouseEvent event, String permitTag) {}
	
	/**
	 * Fired when the user clicks on a player permit
	 * @param event
	 * @param permitTag
	 */
	public default void onActionPlayerPermit(MouseEvent event, String permitTag) {}
	
	
	/**
	 * Fired when the mouse enters on a player card
	 * @param event
	 * @param cardTag
	 */
	public default void onEnterCard(MouseEvent event, String cardTag){}
	
	/**
	 * Fired when the mouse exits from a player card
	 * @param event
	 * @param cardTag
	 */
	public default void onExitCard(MouseEvent event, String cardTag) {}
	
	/**
	 * Fired when the user clicks on a card
	 * @param event
	 * @param cardTag
	 */
	public default void onActionCard(MouseEvent event, String cardTag) {}
	
	/**
	 * Fired when the mouse enters a city
	 * @param event
	 * @param cityTag
	 */
	public default void onEnterCity(MouseEvent event, String cityTag) {}
	
	/**
	 * Fired when the mouse exits from a city
	 * @param event
	 * @param cityTag
	 */
	public default void onExitCity(MouseEvent event, String cityTag) {}
	
	/**
	 * Fired when the user clicks on a city
	 * @param event
	 * @param cityTag
	 */
	public default void onActionCity(MouseEvent event, String cityTag) {}
	
	
	/**
	 * This will be called when a change state is needed and must clean all the graphical operations
	 * done so far
	 */
	public default void reset() {}
}
