package client.gui.events;

import client.gui.GUIController;
import cof.commands.GainTwoCityRewardCommand;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * Creates a new listener to handle gain one city reward bonus
 * @author andrea
 *
 */
public class GainTwoCityRewardListener implements EventListener {
	private static final String INITIAL_STATE = "Choose the first city to take its bonus";
	private static final String FIRST_CITY_CHOSEN = "Choose the second city to take its bonus";
	private String firstCityTag;
	private GUIController controller;
	private Node firstCityNode;
	
	/**
	 * Creates a new listener
	 * @param controller
	 */
	public GainTwoCityRewardListener(GUIController controller) {
		this.controller = controller;
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}
	
	@Override
	public void onEnterCity(MouseEvent event, String cityTag) {
		if (firstCityTag == null || !cityTag.equals(firstCityTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCity(MouseEvent event, String cityTag) {
		if (firstCityTag == null || !cityTag.equals(firstCityTag)){
			Node source = (Node)event.getSource();
			source.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
		}
		
	}

	@Override
	public void onActionCity(MouseEvent event, String cityTag) {
		if (firstCityTag == null) {
			firstCityTag = cityTag;
			firstCityNode = (Node)event.getSource();
			((Node)event.getSource()).setTranslateY(-15.0);
			
			controller.setTurnDescriptionContent(FIRST_CITY_CHOSEN);
		}
		else {
			GainTwoCityRewardCommand command = new GainTwoCityRewardCommand(firstCityTag, cityTag);
			
			if (EventUtils.showConfirmationDialog(event, command)) {
				controller.doNobilityTrackAction(command);
			}
			
			reset();
			((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
			
			controller.setState(new IdleListener());
		}
	}

	@Override
	public void reset() {
		if (firstCityNode != null)
			firstCityNode.setTranslateY(0.0);
		
		controller.setTurnDescriptionContent("");
	}
}
