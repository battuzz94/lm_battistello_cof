package client.gui.events;


import client.gui.GUIController;
import client.gui.factories.CouncilPaneFactory.CouncilPane;
import cof.commands.ElectWithAssistantCommand;
import cof.commands.QuickAction;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This is the Listener passed by GUIController, when user perform click on   Elect With Assistant Quick Action.
 * @param controller
 */

public class ElectWithAssistantListener implements EventListener {
	private static final double TRANSLATION_Y = -15.0;
	private static final String INITIAL_STATE = "Select one available councillor";
	private static final String COUNCILLOR_CHOSEN = "Change councillor or select one council";
	String councillorTag = null;
	private Node lastSelected = null;
	private GUIController controller;
	
	boolean activateCouncillors;
	
	/**
	 * Creates a new elect with assistant listener
	 * @param controller
	 */
	public ElectWithAssistantListener(GUIController controller) {
		this.controller = controller;
		activateCouncillors = true;
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}
	
	@Override
	public void onEnterAvailableCouncillor(MouseEvent event, String councillorTag) {
		if (this.councillorTag == null || !this.councillorTag.equals(councillorTag)) {
			Node source = (Node)event.getSource();
			source.getScene().setCursor(Cursor.HAND);
			source.setTranslateY(TRANSLATION_Y);
		}
	}

	@Override
	public void onExitAvailableCouncillor(MouseEvent event, String councillorTag) {
		if (this.councillorTag == null || !this.councillorTag.equals(councillorTag)) {
			Node source = (Node)event.getSource();
			source.getScene().setCursor(Cursor.DEFAULT);
			source.setTranslateY(0.0);
		}
	}

	@Override
	public void onActionAvailableCouncillor(MouseEvent event, String councillorTag) {
		if (lastSelected != null)
			lastSelected.setTranslateY(0.0);
		Node source = (Node)event.getSource();
		source.setTranslateY(TRANSLATION_Y);
		this.councillorTag = councillorTag;
		this.lastSelected = source;
		
		controller.setTurnDescriptionContent(COUNCILLOR_CHOSEN);
	}

	@Override
	public void onEnterCouncil(MouseEvent event, String councilTag) {
		if (councillorTag != null) {
			CouncilPane source = (CouncilPane)event.getSource();
			source.getScene().setCursor(Cursor.HAND);
			source.setTranslateY(TRANSLATION_Y);
		}
	}

	@Override
	public void onExitCouncil(MouseEvent event, String councilTag) {
		if (councillorTag != null) {
			CouncilPane source = (CouncilPane)event.getSource();
			source.getScene().setCursor(Cursor.DEFAULT);
			source.setTranslateY(0.0);
		}
	}

	@Override
	public void onActionCouncil(MouseEvent event, String councilTag) {
		QuickAction command = new ElectWithAssistantCommand(councilTag, councillorTag);
		if (EventUtils.showConfirmationDialog(event, command)) {
			controller.doQuickAction(command);
		}
		
		reset();
		
		CouncilPane source = (CouncilPane)event.getSource();
		source.setTranslateY(0.0);
		source.getScene().setCursor(Cursor.DEFAULT);
		controller.setState(new IdleListener());
	}

	@Override
	public void reset() {
		activateCouncillors = false;
		if (lastSelected != null)
			lastSelected.setTranslateY(0.0);
		
		controller.setTurnDescriptionContent("");
	}
	
}
