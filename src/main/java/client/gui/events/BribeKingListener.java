package client.gui.events;

import java.util.ArrayList;
import java.util.List;

import client.gui.GUIController;
import cof.commands.BribeKingCommand;
import cof.model.Board;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This is the Listener passed by GUIController, when user perform click on Bribe King Main Action.
 * @param controller
 */

public class BribeKingListener implements EventListener {
	
	private static final String INITIAL_STATE = "Select one or more cards to bribe king's council";
	private static final String CARD_CHOSEN = "Select more cards or select the king's council";
	private static final String SELECTED_COUNCIL = "Select the city to build in";
	private GUIController controller;
	private boolean cardChosen = false;
	private List<String> cardTags;
	private List<Node> cardPanes;
	private Node councilPane;
	private Board board;
	private Node cityPane;
	
	/**
	 * Creates a new listener
	 * @param controller
	 */
	public BribeKingListener(GUIController controller) {
		this.controller = controller;
		cardTags = new ArrayList<>();
		cardPanes = new ArrayList<>();
		board = controller.getBoard();
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}

	@Override
	public void onEnterCouncil(MouseEvent event, String councilTag) {
		if (!cardChosen && !cardTags.isEmpty() && kingCouncil(councilTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCouncil(MouseEvent event, String councilTag) {
		if (!cardChosen && !cardTags.isEmpty() && kingCouncil(councilTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	public void onActionCouncil(MouseEvent event, String councilTag) {
		if (!cardTags.isEmpty() && kingCouncil(councilTag)) {
			cardChosen = true;
			councilPane = (Node)event.getSource();
			councilPane.setTranslateY(-15.0);
			
			controller.setTurnDescriptionContent(SELECTED_COUNCIL);
		}
	}

	@Override
	public void onEnterCard(MouseEvent event, String cardTag) {
		if (!cardChosen) {
			Node source = (Node) event.getSource();
			if (!cardTags.contains(cardTag)) {
				source.setTranslateY(-15.0);
			}
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCard(MouseEvent event, String cardTag) {
		if (!cardChosen) {
			Node source = (Node) event.getSource();
			if (!cardTags.contains(cardTag)) {
				source.setTranslateY(0.0);
			}
			source.getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	public void onActionCard(MouseEvent event, String cardTag) {
		if (!cardChosen) {
			if (cardTags.contains(cardTag)) {
				cardTags.remove(cardTag);
				cardPanes.stream()
					.filter(pane -> pane.getId().equals(cardTag))
					.forEach(pane -> pane.setTranslateY(0.0));
				cardPanes.removeIf(pane -> pane.getId().equals(cardTag));
			}
			else {
				cardTags.add(cardTag);
				Node source = (Node)event.getSource();
				source.setTranslateY(-15.0);
				cardPanes.add(source);
			}
			
			if (cardTags.isEmpty())
				controller.setTurnDescriptionContent(INITIAL_STATE);
			else
				controller.setTurnDescriptionContent(CARD_CHOSEN);
		}
	}

	@Override
	public void onEnterCity(MouseEvent event, String cityTag) {
		if (cardChosen) {
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCity(MouseEvent event, String cityTag) {
		if (cardChosen) {
			Node source = (Node)event.getSource();
			source.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	public void onActionCity(MouseEvent event, String cityTag) {
		if (cardChosen) {
			BribeKingCommand command = new BribeKingCommand(cardTags, cityTag);
			this.cityPane = (Node)event.getSource();
			
			if (EventUtils.showConfirmationDialog(event, command)) {
				controller.doMainAction(command);
			}
			
			reset();
			((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
			
			controller.setState(new IdleListener());
		}
	}
	
	private boolean kingCouncil(String councilTag) {
		return board.getKingCouncil().getTag().equals(councilTag);
	}

	@Override
	public void reset() {
		for (Node card: cardPanes)
			card.setTranslateY(0.0);
		if (councilPane != null)
			councilPane.setTranslateY(0.0);
		if (cityPane != null)
			cityPane.setTranslateY(0.0);
		
		controller.setTurnDescriptionContent("");
	}
}
