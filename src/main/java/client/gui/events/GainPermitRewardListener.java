package client.gui.events;

import client.gui.GUIController;
import cof.commands.GainPermitRewardCommand;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This listener lets you choose one of your permit and take its bonus
 * @author andrea
 *
 */
public class GainPermitRewardListener implements EventListener {
	GUIController controller;
	
	/**
	 * Creates a new listener
	 * @param controller
	 */
	public GainPermitRewardListener(GUIController controller) {
		this.controller = controller;
		
		controller.setTurnDescriptionContent("Choose one of your permit to take its bonus");
	}
	
	@Override
	public void onEnterPlayerPermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(-15.0);
		source.getScene().setCursor(Cursor.HAND);
	}

	@Override
	public void onExitPlayerPermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(-0.0);
		source.getScene().setCursor(Cursor.DEFAULT);
	}

	@Override
	public void onActionPlayerPermit(MouseEvent event, String permitTag) {
		GainPermitRewardCommand command = new GainPermitRewardCommand(permitTag);
		
		if (EventUtils.showConfirmationDialog(event, command)) {
			controller.doNobilityTrackAction(command);
		}
		
		reset();
		((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
		
		controller.setState(new IdleListener());
	}
	
	@Override
	public void reset() {
		controller.setTurnDescriptionContent("");
	}
}
