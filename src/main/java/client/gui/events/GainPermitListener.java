package client.gui.events;

import client.gui.GUIController;
import cof.commands.GainPermitCommand;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This listener lets you choose one of the gainable permits and take it
 * @author andrea
 *
 */
public class GainPermitListener implements EventListener {

	private static final String INITIAL_STATE = "Choose one gainable permit to take";
	private GUIController controller;
	
	/**
	 * Creates a new listener
	 * @param controller
	 */
	public GainPermitListener(GUIController controller) {
		this.controller = controller;
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}
	
	
	@Override
	public void onEnterGainablePermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(-15.0);
		source.getScene().setCursor(Cursor.HAND);
	}

	@Override
	public void onExitGainablePermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(0.0);
		source.getScene().setCursor(Cursor.DEFAULT);
	}

	@Override
	public void onActionGainablePermit(MouseEvent event, String permitTag) {
		GainPermitCommand command = new GainPermitCommand(permitTag);
		
		if (EventUtils.showConfirmationDialog(event, command)) {
			controller.doNobilityTrackAction(command);
		}
		
		reset();
		((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
		
		controller.setState(new IdleListener());
	}
	
	@Override
	public void reset() {
		controller.setTurnDescriptionContent("");
	}
}
