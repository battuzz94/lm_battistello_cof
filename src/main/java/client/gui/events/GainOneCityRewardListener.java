package client.gui.events;

import client.gui.GUIController;
import cof.commands.GainOneCityRewardCommand;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * Creates a new listener to handle gain one city reward bonus
 * @author andrea
 *
 */
public class GainOneCityRewardListener implements EventListener {
	
	private static final String INITIAL_STATE = "Choose the city to take its bonus";
	private GUIController controller;
	
	/**
	 * Creates a new listener
	 * @param controller
	 */
	public GainOneCityRewardListener(GUIController controller) {
		this.controller = controller;
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}
	
	@Override
	public void onEnterCity(MouseEvent event, String cityTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(-15.0);
		source.getScene().setCursor(Cursor.HAND);
	}

	@Override
	public void onExitCity(MouseEvent event, String cityTag) {
		Node source = (Node)event.getSource();
		source.setTranslateY(0.0);
		source.getScene().setCursor(Cursor.DEFAULT);
	}

	@Override
	public void onActionCity(MouseEvent event, String cityTag) {
		GainOneCityRewardCommand command = new GainOneCityRewardCommand(cityTag);
		
		if (EventUtils.showConfirmationDialog(event, command)) {
			controller.doNobilityTrackAction(command);
		}
		
		reset();
		((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
		
		controller.setState(new IdleListener());
	}

	@Override
	public void reset() {
		controller.setTurnDescriptionContent("");
	}
}
