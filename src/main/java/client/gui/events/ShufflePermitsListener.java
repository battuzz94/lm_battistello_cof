package client.gui.events;

import client.gui.GUIController;
import cof.commands.QuickAction;
import cof.commands.ShufflePermitsCommand;
import cof.model.Board;
import cof.model.Region;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This is the Listener passed by GUIController, when user perform click on Shuffle Permit Quick Action.
 * @param controller
 */
public class ShufflePermitsListener implements EventListener {
	private static final String INITIAL_STATE = "Select the region permits to shuffle";
	GUIController controller;
	Board board;
	private String chosenRegion;
	
	/**
	 * Creates a new Shuffle permit listener
	 * @param controller
	 */
	public ShufflePermitsListener(GUIController controller) {
		this.controller = controller;
		board = controller.getBoard();
		
		controller.setTurnDescriptionContent(INITIAL_STATE);
	}
	
	@Override
	public void onEnterGainablePermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		Node parent = (Node)source.getParent();
		parent.setTranslateY(-15.0);
		source.getScene().setCursor(Cursor.HAND);
	}
	@Override
	public void onExitGainablePermit(MouseEvent event, String permitTag) {
		Node source = (Node)event.getSource();
		Node parent = (Node)source.getParent();
		parent.setTranslateY(0.0);
		source.getScene().setCursor(Cursor.DEFAULT);
	}
	@Override
	public void onActionGainablePermit(MouseEvent event, String permitTag) {
		for (Region r : board.getRegions())
			if (r.getGainablePermits().stream().filter(p -> p.getTag().equals(permitTag)).findAny().isPresent()) {
				chosenRegion = r.getTag();
				break;
			}
		if (chosenRegion != null) {
			QuickAction command = new ShufflePermitsCommand(chosenRegion);
			
			if (EventUtils.showConfirmationDialog(event, command)) {
				controller.doQuickAction(command);
			}
			
			reset();
			Node source = (Node)event.getSource();
			Node parent = (Node)source.getParent();
			parent.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
			controller.setState(new IdleListener());
		}
	}
	
	@Override
	public void reset() {
		controller.setTurnDescriptionContent("");
	}
	
}
