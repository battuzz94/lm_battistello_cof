package client.gui.events;

import java.util.Optional;

import cof.commands.Command;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 * Some functions common to all events
 * @author andrea
 *
 */
public class EventUtils {
	
	private EventUtils() {}
	
	/**
	 * Shows a confirmation dialog for the specified command and waits for answer.
	 * @param event
	 * @param command
	 * @return true if the user clicked OK, false otherwise
	 */
	public static boolean showConfirmationDialog(Event event, Command command) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.getDialogPane().getStyleClass().add("alert-dialog");
		alert.setTitle("Are you sure?");
		alert.setHeaderText(null);
		alert.initOwner(((Node)event.getSource()).getScene().getWindow());
		alert.setContentText(command.toString());

		Optional<ButtonType> result = alert.showAndWait();
		
		return result.isPresent() && result.get() == ButtonType.OK;	
	}
}
