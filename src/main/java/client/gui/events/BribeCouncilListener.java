package client.gui.events;

import java.util.ArrayList;
import java.util.List;

import client.gui.GUIController;
import cof.commands.BribeCouncilCommand;
import cof.model.Board;
import cof.model.Region;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * This is the Listener passed by GUIController, when user perform click on Bribe Council Main Action.
 * @param controller
 */

public class BribeCouncilListener implements EventListener {
	
	private static final String INITIAL_DESC = "Select one or more cards to bribe the council";
	private static final String CARD_CHOSEN_DESC = "Select more cards or the council to bribe";
	private static final String COUNCIL_CHOSEN = "Select one permit of this region";
	private GUIController controller;
	private boolean cardChosen = false;
	private List<String> cardTags;
	private List<Node> cardPanes;
	private String councilTag;
	private Node councilPane;
	private Board board;
	private Node permitPane;
	
	/**
	 * Starts a new listener that will drive the player through a Bribe Council Command
	 * @param controller
	 */
	public BribeCouncilListener(GUIController controller) {
		this.controller = controller;
		cardTags = new ArrayList<>();
		cardPanes = new ArrayList<>();
		board = controller.getBoard();
		
		controller.setTurnDescriptionContent(INITIAL_DESC);
	}

	@Override
	public void onEnterCouncil(MouseEvent event, String councilTag) {
		if (!cardChosen && !cardTags.isEmpty() && !kingCouncil(councilTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCouncil(MouseEvent event, String councilTag) {
		if (!cardChosen && !cardTags.isEmpty() && !kingCouncil(councilTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	public void onActionCouncil(MouseEvent event, String councilTag) {
		if (!cardTags.isEmpty() && !kingCouncil(councilTag)) {
			cardChosen = true;
			this.councilTag = councilTag;
			councilPane = (Node)event.getSource();
			councilPane.setTranslateY(-15.0);
			
			controller.setTurnDescriptionContent(COUNCIL_CHOSEN);
		}
	}

	private boolean kingCouncil(String councilTag) {
		return board.getKingCouncil().getTag().equals(councilTag);
	}

	@Override
	public void onEnterGainablePermit(MouseEvent event, String permitTag) {
		if (cardChosen && permitValid(permitTag)) {
			Node source = (Node)event.getSource();
			source.setTranslateY(-15.0);
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	private boolean permitValid(String permitTag) {
		for (Region region : board.getRegions())
			if (region.getRegionCouncil().getTag().equals(councilTag) &&
					region.getGainablePermits().stream().filter(p -> p.getTag().equals(permitTag)).findAny().isPresent())
				return true;
		return false;
	}

	@Override
	public void onExitGainablePermit(MouseEvent event, String permitTag) {
		if (cardChosen) {
			Node source = (Node)event.getSource();
			source.setTranslateY(0.0);
			source.getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	public void onActionGainablePermit(MouseEvent event, String permitTag) {
		if (cardChosen) {
			BribeCouncilCommand command = new BribeCouncilCommand(cardTags, councilTag, permitTag);
			this.permitPane = (Node)event.getSource();
			
			if (EventUtils.showConfirmationDialog(event, command)) {
				controller.doMainAction(command);
			}
			
			reset();
			((Node)event.getSource()).getScene().setCursor(Cursor.DEFAULT);
			
			controller.setState(new IdleListener());
		}
	}

	@Override
	public void onEnterCard(MouseEvent event, String cardTag) {
		if (!cardChosen) {
			Node source = (Node) event.getSource();
			if (!cardTags.contains(cardTag)) {
				source.setTranslateY(-15.0);
			}
			source.getScene().setCursor(Cursor.HAND);
		}
	}

	@Override
	public void onExitCard(MouseEvent event, String cardTag) {
		if (!cardChosen) {
			Node source = (Node) event.getSource();
			if (!cardTags.contains(cardTag)) {
				source.setTranslateY(0.0);
			}
			source.getScene().setCursor(Cursor.DEFAULT);
		}
	}

	@Override
	public void onActionCard(MouseEvent event, String cardTag) {
		if (!cardChosen) {
			if (cardTags.contains(cardTag)) {
				cardTags.remove(cardTag);
				cardPanes.stream()
					.filter(pane -> pane.getId().equals(cardTag))
					.forEach(pane -> pane.setTranslateY(0.0));
				cardPanes.removeIf(pane -> pane.getId().equals(cardTag));
			}
			else {
				cardTags.add(cardTag);
				Node source = (Node)event.getSource();
				source.setTranslateY(-15.0);
				cardPanes.add(source);
			}
			
			if (cardTags.isEmpty())
				controller.setTurnDescriptionContent(INITIAL_DESC);
			else
				controller.setTurnDescriptionContent(CARD_CHOSEN_DESC);
		}
	}

	
	@Override
	public void reset() {
		for (Node card: cardPanes)
			card.setTranslateY(0.0);
		if (councilPane != null) {
			councilPane.setTranslateY(0.0);
		}
		if (permitPane != null) {
			permitPane.setTranslateY(0.0);
		}
		
		controller.setTurnDescriptionContent("");
	}
}
