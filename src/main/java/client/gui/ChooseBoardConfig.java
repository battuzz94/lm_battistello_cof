package client.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.controlsfx.control.Notifications;

import client.AbstractClientController;
import client.gui.factories.BoardPaneFactory;
import client.gui.factories.BoardPaneFactory.BoardPane;
import cof.model.Board;
import cof.model.BoardConfig;
import cof.model.BoardFactory;
import common.Utility;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
/**
 * This Class control the configuration window.
 * @author Simone
 *
 */
public class ChooseBoardConfig implements Initializable {
	private static final int MAX_AI_AGENTS = 4;
	private static final Logger LOGGER = Logger.getLogger(ChooseBoardConfig.class.getName());
	private static final String MAP_RESOURCE_FOLDER = "src/main/resources/maps";
	private static final BoardPaneFactory boardFactory = BoardPaneFactory.getInstance();
	
    @FXML private ComboBox<String> loadMapComboBox;
    @FXML private CheckBox resizePlayersCheckBox;
    @FXML private ScrollPane boardPane;
    @FXML private Button chooseMapButton;
    @FXML private Label mapNameLabel;
    @FXML private TextField regionsTextField;
    @FXML private Slider connectionSparsenessSlider;
    @FXML private TextField kingCouncillorsTextField;
    @FXML private TextField kingBonusTextField;
    @FXML private TextField councillorsRegionTextField;
    @FXML private TextField permitRegionTextField;
    @FXML private TextField citiesRegionTextField;
    @FXML private Slider bonusLevelSlider;
    @FXML private Button generateButton;
    @FXML private Spinner<Integer> aiAgentsSpinner;
    
    private static final int SELECTED_PREDEFINED_MAP = 0;
    private static final int SELECTED_CUSTOM_MAP = 1;
    
    private int lastSelected = SELECTED_PREDEFINED_MAP;
    private File customFile;
    private BoardPane boardView;
    
    private Board currentBoard = null;


	private AbstractClientController controller;
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
    	currentBoard = Utility.loadTestBoardDisposed();
    	
    	// Fill the combo box content
    	
    	File folder = new File(MAP_RESOURCE_FOLDER);
    	
    	List<File> mapList = Arrays.asList(folder.listFiles(pathname -> 
				pathname.isFile() && pathname.getName().endsWith(".json")
    	));
    
    	ObservableList<String> items = FXCollections.observableArrayList(
    			mapList.stream()
    			.map(f -> f.getName())
    			.collect(Collectors.toList())
    		);
    	
    	loadMapComboBox.setItems(items);
    	loadMapComboBox.setValue("Choose from list");
    	loadMapComboBox.setUserData(mapList);		// Saves the list of File
    	
    	IntegerSpinnerValueFactory aiAgentsNumberFactory = new IntegerSpinnerValueFactory(0, MAX_AI_AGENTS);
    	aiAgentsNumberFactory.setAmountToStepBy(1);
    	aiAgentsNumberFactory.setValue(0);
    	aiAgentsSpinner.setValueFactory(aiAgentsNumberFactory);
    	
    	Platform.runLater(this::showBoard);
    }
    
    void setController(AbstractClientController controller) {
    	this.controller = controller;
    }
    
    @FXML
    void generateBoard(ActionEvent event) {
    	try {
    		BoardConfig config = readConfig();
    		
    		BoardFactory factory = new BoardFactory(config);
    		currentBoard = factory.build();
    		
    		showBoard();
    	}
    	catch (AssertionError | Exception e) {
    		LOGGER.log(Level.WARNING, "Error in generating board", e);
    		Notifications.create()
    			.title("Configuration not valid")
    			.text("The parameters for the board configuration are not valid")
    			.hideAfter(Duration.seconds(3))
    			.showError();
    	}
    }
    /**
     * This method throw an Exception if condition is not verified.
     * @param condition
     */
    private void check(boolean condition) {
    	if (!condition)
    		throw new AssertionError();
    }
    /**
     * This method check if all parameters inserted are valid and set it, 
     * otherwise will be an Exception.
     * @return
     */
    private BoardConfig readConfig() {
    	int regions = Integer.parseInt(regionsTextField.getText());
		check( regions > 0 );
		check( regions <= 10 );
		
		int kingCouncillors = Integer.parseInt(kingCouncillorsTextField.getText()); 
		check( kingCouncillors > 0 );
		check( kingCouncillors <= 10 );
		
		int kingBonus = Integer.parseInt(kingBonusTextField.getText()); 
		check( kingBonus >= 0);
		check( kingBonus < 50);
		
		int connectionSparseness = (int)connectionSparsenessSlider.getValue(); 
		check( connectionSparseness >= 1);
		check( connectionSparseness <= 5);
		
		int councillorsRegion = Integer.parseInt(councillorsRegionTextField.getText()); 
		check( councillorsRegion > 0);
		check( councillorsRegion <= 10);
		
		int permitRegion = Integer.parseInt(permitRegionTextField.getText()); 
		check( permitRegion > 0);
		check( permitRegion <= 100);
		
		int citiesRegion = Integer.parseInt(citiesRegionTextField.getText()); 
		check( citiesRegion > 0);
		check( citiesRegion <= 10);
		
		int bonusLevel = (int) bonusLevelSlider.getValue(); 
		check( bonusLevel >= 1);
		check( bonusLevel <= 5);
		
		BoardConfig config = new BoardConfig();
		config.setRegions(regions);
		config.setKingCouncillorCount(kingCouncillors);
		config.setKingBonusCount(kingBonus);
		config.setConnectionsSparseness(connectionSparseness);
		config.setCouncillorsPerRegion(councillorsRegion);
		config.setPermitsPerRegion(permitRegion);
		config.setCitiesPerRegion(citiesRegion);
		config.setBonusLevel(bonusLevel);
		
		return config;
    }

    @FXML
    void loadMapFromFile(ActionEvent event) {
    	Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
    	
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.setTitle("Open Resource File");
    	customFile = fileChooser.showOpenDialog(stage);
    	
    	mapNameLabel.setText(customFile.getName());
    	
    	lastSelected=SELECTED_CUSTOM_MAP;
    	showBoardPreview();
    }

    @FXML
    void selectCurrentConfig(ActionEvent event) {
    	if (currentBoard != null && controller != null) {
    		Stage primaryStage = (Stage) ((Button)event.getSource()).getScene().getWindow();
    		controller.setAIAgents(aiAgentsSpinner.getValue());
    		controller.configuredBoard(currentBoard);
    		
    		primaryStage.close();
    	}
    }

    @FXML
    void showPreview() {
    	lastSelected = SELECTED_PREDEFINED_MAP;
    	showBoardPreview();
    }
    
    
    @SuppressWarnings("unchecked")
	void showBoardPreview() {
    	if (lastSelected == SELECTED_CUSTOM_MAP) {
    		try {
				currentBoard = Utility.loadBoardFromFile(customFile);
				
				showBoard();
				
			} catch (FileNotFoundException e) {
				LOGGER.log(Level.WARNING, "Could not preview board", e);
			}
    	}
    	else if (lastSelected == SELECTED_PREDEFINED_MAP) {
    		String filename = loadMapComboBox.getValue();
    		
    		try {
    			List<File> files = (List<File>) loadMapComboBox.getUserData();
    			File chosenFile = files.stream().filter(f -> f.getName().equals(filename)).findFirst().get();
    		
				currentBoard = Utility.loadBoardFromFile(chosenFile);
				
				showBoard();
			}
    		catch (ClassCastException | NoSuchElementException | FileNotFoundException e) {
    			LOGGER.log(Level.WARNING, "Could not load resource folder", e);
    			return;
    		}
    	}
    }

	private void showBoard() {
		boardView = boardFactory.buildBoard(currentBoard, boardPane.viewportBoundsProperty());
		boardPane.setContent(boardView);
	}

	
    
}
