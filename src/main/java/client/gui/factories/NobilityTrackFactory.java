package client.gui.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import cof.bonus.Bonus;
import cof.bonus.DynamicBonus;
import cof.model.Player;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * Singleton Class, give the possibility to build new NobilityTrack.
 * @author Simone
 *
 */

public class NobilityTrackFactory {
	private static NobilityTrackFactory instance;
	private static Image manImage;
	
	private NobilityTrackFactory() {
		manImage = new Image(getClass().getResourceAsStream("/images/man.png"));
	}
	
	public static NobilityTrackFactory getInstance() {
		if (instance == null) {
			instance = new NobilityTrackFactory();
		}
		return instance;
	}
	
	/**
	 * Creates a new nobility track pane
	 * @param nobilityBonus
	 * @return
	 */
	public NobilityTrackPane buildNobilityTrack(List<Bonus> nobilityBonus) {
		return new NobilityTrackPane(nobilityBonus, null);
	}
	
	/**
	 * Creates a new nobility track pane with player information
	 * @param nobilityBonus
	 * @param player
	 * @return
	 */
	public NobilityTrackPane buildNobilityTrack(List<Bonus> nobilityBonus, Player player) {
		return new NobilityTrackPane(nobilityBonus, player);
	}
	
	/**
	 * Creates a new nobility track pane
	 * @author andrea
	 *
	 */
	public class NobilityTrackPane extends HBox {
		private List<Node> nobilityPanes;
		private Player player;
		private List<Bonus> nobilityBonus;
		
		private NobilityTrackPane(List<Bonus> nobilityBonus, Player player) {
			super();
			this.nobilityBonus = nobilityBonus;
			this.player = player;
			nobilityPanes = new ArrayList<>();
			setPrefHeight(100.0);
			setAlignment(Pos.BOTTOM_CENTER);
			getStyleClass().add("nobility-track-pane");
			BonusPaneFactory factory = BonusPaneFactory.getInstance();
			factory.setWidth(50.0);
			factory.setBonusWidth(20.0);
			
			int nobilityAdvance = 0;
			for (Bonus b : nobilityBonus) {
				AnchorPane p = factory.getBonusPane(b);
				p.getStyleClass().add("nobility-track-pane");
				
				p.setPrefWidth(60);
				if (player != null && nobilityAdvance == player.getNobility())
					addPlayerIndicator(p);
				nobilityPanes.add(p);
				nobilityAdvance++;
			}
			getChildren().addAll(nobilityPanes);
		}

		private void addPlayerIndicator(AnchorPane p) {
			ImageView manImageView = new ImageView(manImage);
			manImageView.setFitWidth(40.0);
			manImageView.setFitHeight(40.0);
			manImageView.setPreserveRatio(true);
			p.getChildren().add(manImageView);
			AnchorPane.setBottomAnchor(manImageView, 0.0);
		}
		
		
		public void setCallback(BiConsumer<? super Event, Bonus> callback) {
			for (int i = 0; i < nobilityBonus.size(); i++) {
				Bonus currBonus = nobilityBonus.get(i);
				if (currBonus != null 
						&& currBonus instanceof DynamicBonus
						&& (player == null || player.getNobility() >= i)) 
				{	
					nobilityPanes.get(i).addEventHandler(MouseEvent.MOUSE_ENTERED, e -> 
						((Node)e.getSource()).getScene().setCursor(Cursor.HAND)
					);
					nobilityPanes.get(i).addEventHandler(MouseEvent.MOUSE_EXITED, e -> 
						((Node)e.getSource()).getScene().setCursor(Cursor.DEFAULT)
					);
					nobilityPanes.get(i).addEventHandler(MouseEvent.MOUSE_CLICKED, e -> callback.accept(e, currBonus));
				}
			}
		}
	}
}
