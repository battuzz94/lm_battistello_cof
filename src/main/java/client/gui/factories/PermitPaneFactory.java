package client.gui.factories;

import cof.model.City;
import cof.model.Permit;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextAlignment;

/**
 * Singleton Class, give the possibility to build new Permit.
 * @author Simone
 *
 */

public class PermitPaneFactory {
	private Image permitBackgroundImage;
	
	private static PermitPaneFactory factory;
	private static final BonusPaneFactory bonusFactory = BonusPaneFactory.getInstance();
	
	private double permitHeight = 120.0;
	private double textSize = 15.0;
	
	private PermitPaneFactory() {
		permitBackgroundImage = new Image(getClass().getResourceAsStream("/images/permit.png"));
	}
	
	public void setHeight(double height){
		this.permitHeight=height;
	}
	
	public static PermitPaneFactory getInstance() {
		if (factory == null) {
			factory = new PermitPaneFactory();
		}
		return factory;
	}
	
	/**
	 * Builds a permit pane
	 * @param permit
	 * @return
	 */
	public PermitPane buildPermit(Permit permit) {
		return new PermitPane(permit);
	}
	
	/**
	 * Represent a Permit with background, cities in which you can build and relative bonus
	 * @author andrea
	 *
	 */
	public class PermitPane extends AnchorPane {
		private Pane bonusPane;
		private ImageView permitImageView;
		
		private PermitPane(Permit permit) {
			super();
			
			permitImageView = new ImageView(permitBackgroundImage);
			permitImageView.setPreserveRatio(true);
			permitImageView.setFitHeight(permitHeight);
			
			this.setHeight(permitHeight);
			this.setWidth(permitHeight);
			
			this.setPrefHeight(permitHeight);
			this.setPrefWidth(permitHeight);
			
			this.setMaxWidth(0.0);
			this.setMaxHeight(0.0);
			
			
			bonusFactory.setWidth(permitHeight/4.0 * 3.0);
			bonusFactory.setBonusWidth(permitHeight/5.0);
			bonusPane = bonusFactory.getBonusPane(permit.getBonus());
			AnchorPane.setBottomAnchor(bonusPane, 15.0);
			AnchorPane.setLeftAnchor(bonusPane, 15.0);
			AnchorPane.setRightAnchor(bonusPane, 15.0);
			getChildren().add(permitImageView);
			
			double top = permitHeight/8.0;
			for (City c : permit.getCities()) {
				String s = c.getTag();
				Label label = new Label(s);
				label.setStyle("-fx-font-size: " + textSize + "px");
				label.setTextAlignment(TextAlignment.CENTER);
				AnchorPane.setTopAnchor(label, top);
				AnchorPane.setLeftAnchor(label, 15.0);
				AnchorPane.setRightAnchor(label, 15.0);
				top += textSize;
				getChildren().add(label);
				
			}
			
			if (permit.isUsed()) {
				ColorAdjust colorAdjust = new ColorAdjust();
				colorAdjust.setBrightness(-0.5);
				this.setEffect(colorAdjust);
			}
			
			
			AnchorPane.setBottomAnchor(bonusPane, 15.0);
			bonusPane.setPrefWidth(permitHeight/4.0*3.0);
			
			this.setId(permit.getTag());
			getChildren().addAll(bonusPane);
			
			getStyleClass().add("permit-pane");
		}
	}
	
}
