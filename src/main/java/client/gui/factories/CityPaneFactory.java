package client.gui.factories;

import javafx.scene.text.Font;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.model.City;
import cof.model.Player;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Singleton Class, give the possibility to build new City.
 * @author Simone
 *
 */

public class CityPaneFactory {
	private static final Logger LOGGER=Logger.getLogger(CityPaneFactory.class.getName());
	
	private static CityPaneFactory instance;
	private static final String CITYIRON = "/images/cityiron.png";
	private static final String CITYBRONZE = "/images/citybronze.png";
	private static final String CITYSILVER = "/images/citysilver.png";
	private static final String CITYGOLD = "/images/citygold.png";
	private static final String CITYPURPLE = "/images/citypurple.png";
	private static final String ROYALCROWN = "/images/RoyalCrown.png";
	private static final String EMPORIUM = "/images/emporio.png";
	private static final String MY_EMPORIUM = "/images/emporio_personale.png";
	private static Image imageCityIron; 
	private static Image imageCityBronze; 
	private static Image imageCitySilver; 
	private static Image imageCityGold; 
	private static Image imageCityPurple; 
	private static Image imageCrown; 
	private static Image imageEmporium;
	private static Image imageMyEmporium;
	
	private double height = 80.0;
	private static final BonusPaneFactory bonusFactory = BonusPaneFactory.getInstance();
	/**
	 * This is a Singleton Factory, constructs the City Pane.
	 */
	public CityPaneFactory() {
		imageCityIron = new Image(getClass().getResourceAsStream(CITYIRON));
		imageCityBronze = new Image(getClass().getResourceAsStream(CITYBRONZE));
		imageCitySilver = new Image(getClass().getResourceAsStream(CITYSILVER));
		imageCityGold = new Image(getClass().getResourceAsStream(CITYGOLD));
		imageCityPurple = new Image(getClass().getResourceAsStream(CITYPURPLE));
		imageCrown = new Image(getClass().getResourceAsStream(ROYALCROWN));
		imageEmporium= new Image(getClass().getResourceAsStream(EMPORIUM));
		imageMyEmporium= new Image(getClass().getResourceAsStream(MY_EMPORIUM));
	}
	
	public static CityPaneFactory getInstance() {
		if (instance == null)
			instance = new CityPaneFactory();
		return instance;
	}
	/**
	 * This method construct the CityPane by given City, if there is a King and 
	 * Player to attach the builded emporia.	
	 * @param city
	 * @param isKing
	 * @param player
	 * @return
	 */
	public CityPane buildCity(City city, boolean isKing, Player player){
		return new CityPane(city, isKing, player);
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public class CityPane extends AnchorPane {
		private static final double EMPORIADIM = 25.0;
		VBox vbox;
		City city;
		
		private CityPane(City city,boolean isKing,Player player){
			this(city);
			
			if(isKing){
				ImageView viewCrown= new ImageView(imageCrown);	
				viewCrown.setFitHeight(height/2);
				viewCrown.setFitWidth(height/2);
				getChildren().add(viewCrown);
				AnchorPane.setTopAnchor(viewCrown, -height/2);
				AnchorPane.setLeftAnchor(viewCrown, height/4);
				viewCrown.getStyleClass().add("crown");
			}
			if (player != null) {
				HBox hbox= new HBox();
				int nEmporia=city.getEmporiaCount();
				if(player.hasBuiltIn(city)){
					ImageView viewMyEmporium= new ImageView(imageMyEmporium);
					viewMyEmporium.setFitHeight(EMPORIADIM);
					viewMyEmporium.setFitWidth(EMPORIADIM);
					hbox.getChildren().add(viewMyEmporium);
					nEmporia--;
				}		
				for(int i=0;i<nEmporia;i++){
					ImageView viewEmporium = new ImageView(imageEmporium);
					viewEmporium.setFitHeight(EMPORIADIM);
					viewEmporium.setFitWidth(EMPORIADIM);
					hbox.getChildren().add(viewEmporium);
				}		
				vbox.getChildren().add(hbox);
				AnchorPane.setBottomAnchor(hbox,0.0);
			}
		}
		
		private CityPane(City city) {
			bonusFactory.setWidth(height);
			bonusFactory.setBonusWidth(height/4.0);
			Pane bonusPane = bonusFactory.getBonusPane(city.getBonus());
			
			ImageView imageview = null;
			
			
			switch(city.getColor()){
			case BRONZE:
				 imageview = new ImageView(imageCityBronze);
			break;
			case GOLD:
				 imageview = new ImageView(imageCityGold);
				break;
			case IRON:
				 imageview = new ImageView(imageCityIron);
				break;
			case PURPLE:
				 imageview = new ImageView(imageCityPurple);
				break;
			case SILVER:
			default:
				 imageview = new ImageView(imageCitySilver);
				break;
			}
			
			
			imageview.setPreserveRatio(true);
			imageview.setFitHeight(height);
			imageview.setFitWidth(height);
			bonusPane.setPrefWidth(height);

			Label cityName= new Label();
			
			try {
				Font cityFont=Font.loadFont(new FileInputStream(new File("src/main/resources/Caligraf_1435.ttf")), 24);
				cityName.setFont(cityFont);
			} catch(IOException e){
				LOGGER.log(Level.FINE, "Could not open font file", e);
			}
			
			
			cityName.setText(city.getTag());
			cityName.getStyleClass().add("city-label");
			cityName.setTextFill(Color.WHITE);
			
			
			
			
			this.city=city;
			
			AnchorPane.setTopAnchor(bonusPane, 0.0);
			AnchorPane.setLeftAnchor(bonusPane, 0.0);
			
			setPrefWidth(height);
			
			getChildren().addAll(imageview, bonusPane);
			
			vbox= new VBox();
			vbox.setId("vBottom");
			vbox.setAlignment(Pos.CENTER_RIGHT);
			vbox.getChildren().add(cityName);
			city.getEmporiaCount();
			getChildren().add(vbox);
			AnchorPane.setRightAnchor(vbox, 0.0);
			AnchorPane.setBottomAnchor(vbox, 0.0);
			
			
			this.getStyleClass().add("city-pane");
		}

		void setPosition(double x, double y) {
			this.setLayoutX(x);
			this.setLayoutY(y);
		}
	}

	
}

