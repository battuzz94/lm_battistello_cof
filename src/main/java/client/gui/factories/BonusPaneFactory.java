package client.gui.factories;


import cof.bonus.AdditionalActionBonus;
import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.bonus.ColoredCityBonus;
import cof.bonus.GainOneCityRewardBonus;
import cof.bonus.GainPermitBonus;
import cof.bonus.GainPermitRewardBonus;
import cof.bonus.GainTwoCityRewardBonus;
import common.Utility;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
/**
 * This is a Singleton Factory, constructs the Bonus Pane.
 * @author Simone
 *
 */
public class BonusPaneFactory {
	private static final String ADVANCE = "/images/advance.png";
	private static final String ASSISTANT = "/images/assistant.png";
	private static final String CARD = "/images/card.png";
	private static final String CITYBONUS = "/images/citybonus.png";
	private static final String COIN = "/images/coin.png";
	private static final String MAINACTION = "/images/mainaction.png";
	private static final String PERMITBONUS = "/images/permitbonus.png";
	private static final String POINTS = "/images/points.png";
	private static final String TAKEPERMIT = "/images/takepermit.png";
	private static final String TWO_CITYBONUS = "/images/two_city_bonus.png";
	private static final String BACKIRON = "/images/BackIron.png";
	private static final String BACKBRONZE = "/images/BackBronze.png";
	private static final String BACKSILVER = "/images/BackSilver.png";
	private static final String BACKGOLD = "/images/BackGold.png";
	private static final String BACKPURPLE = "/images/BackPurple.png";
	private Image imageCoin;
	private Image imageAssistant;
	private Image imageCard;
	private Image imageOneCityReward;
	private Image imageTwoCityReward;
	private Image imageMainAction;
	private Image imagePermitBonus;
	private Image imagePoints;
	private Image imageTakePermit;
	private Image imageAdvance;
	private Image imageBackIron;
	private Image imageBackSilver;
	private Image imageBackGold;
	private Image imageBackBronze;
	private Image imageBackPurple;
	private double bonusWidth= 20.0;
	private double width = 50.0;
	ImageView viewBackBonus;
	static BonusPaneFactory instance;
	/**
	 * Singleton Class, give the possibility to get new BonusPane.
	 */
	
	private BonusPaneFactory() {
		imageCoin = new Image(getClass().getResourceAsStream(COIN));
		imageAssistant = new Image(getClass().getResourceAsStream(ASSISTANT));
		imagePoints= new Image(getClass().getResourceAsStream(POINTS));
		imageCard= new Image(getClass().getResourceAsStream(CARD));
		imageAdvance= new Image(getClass().getResourceAsStream(ADVANCE));
		
		imageOneCityReward = new Image(getClass().getResourceAsStream(CITYBONUS));
		imageMainAction = new Image(getClass().getResourceAsStream(MAINACTION));
		imagePermitBonus = new Image(getClass().getResourceAsStream(PERMITBONUS));
		imageTakePermit = new Image(getClass().getResourceAsStream(TAKEPERMIT));
		imageTwoCityReward = new Image(getClass().getResourceAsStream(TWO_CITYBONUS));
		imageBackIron= new Image(BACKIRON);
		imageBackSilver= new Image(BACKSILVER);
		imageBackGold= new Image(BACKGOLD);
		imageBackBronze= new Image(BACKBRONZE);
		imageBackPurple= new Image(BACKPURPLE);
	}
	
	public static BonusPaneFactory getInstance() {
		if (instance == null)
			instance = new BonusPaneFactory();
		return instance;
	}
	/**
	 * This method returns the Node of Player with 
	 * Victory Point and Nobility Track images and Label in horizontal position.
	 * @param victoryPoints
	 * @param nobilityTrack
	 * @return
	 */
	public Node getPlayerStatInfo(int victoryPoints, int nobilityTrack) {
		HBox hbox = new HBox();
		hbox.getChildren().add(getLabelFor(imagePoints, victoryPoints));
		hbox.getChildren().add(getLabelFor(imageAdvance, nobilityTrack));
		hbox.setSpacing(20.0);
		horizontalGrow(hbox);
		return hbox;
	}
	/**
	 * This method, returns the Node of Player with
	 * Victory Point, Nobility Track, Coin and Assistant, with label and images
	 * in horizontal position.
	 * @param victoryPoints
	 * @param nobilityTrack
	 * @param coins
	 * @param assistants
	 * @return
	 */
	public Node getPlayerInfo(int victoryPoints, int nobilityTrack, int coins, int assistants) {
		HBox hbox = new HBox();
		hbox.getChildren().add(getLabelFor(imagePoints, victoryPoints));
		hbox.getChildren().add(getLabelFor(imageAdvance, nobilityTrack));
		hbox.getChildren().add(getLabelFor(imageCoin, coins));
		hbox.getChildren().add(getLabelFor(imageAssistant, assistants));
		horizontalGrow(hbox);
		return hbox;
	}
	
	private void horizontalGrow(HBox hbox) {
		for (Node n : hbox.getChildren())
			HBox.setHgrow(n, Priority.ALWAYS);
	}
	
	private ImageView getFixedSizeView(Image image) {
		ImageView view2 = new ImageView(image);
		view2.setPreserveRatio(true);
		view2.setFitWidth(bonusWidth);
		view2.setFitHeight(bonusWidth);
		return view2;
	}
	
	/**
	 * Require a Bonus, return a new FlowPane representing this bonus.
	 * @param bonus
	 * @return
	 */
	public AnchorPane getBonusPane(Bonus bonus) {
		FlowPane pane;
		
		if (bonus instanceof BasicBonus) {
			pane = getBasicBonusLayout((BasicBonus)bonus);
		}
		else if (bonus instanceof GainOneCityRewardBonus) {
			pane = getImageBonus(imageOneCityReward);
		}
		else if (bonus instanceof AdditionalActionBonus) {
			pane = getImageBonus(imageMainAction);
		}
		else if (bonus instanceof GainPermitBonus) {
			pane = getImageBonus(imageTakePermit);
		}
		else if (bonus instanceof GainPermitRewardBonus) {
			pane = getImageBonus(imagePermitBonus);
		}
		else if (bonus instanceof GainTwoCityRewardBonus) {
			pane = getImageBonus(imageTwoCityReward);
		}
		else if(bonus instanceof ColoredCityBonus ){
			pane=getColoredCityBonus((ColoredCityBonus)bonus);			
		}else{
			pane = new FlowPane();
		}
		
		pane.setPrefWrapLength(width);
		pane.getStyleClass().add("bonus-pane");
		return Utility.encapsulate(new AnchorPane(), pane);
	}
	
	
	private FlowPane getImageBonus(Image bonusImage) {
		FlowPane pane = new FlowPane();
		ImageView imageBonus = new ImageView(bonusImage);
		imageBonus.setPreserveRatio(true);
		imageBonus.setFitHeight(width);
		pane.getChildren().add(imageBonus);
		return pane;
	}
	
	/**
	 * This method return the AnchorPane with King Bonus, rotated.
	 * @param bonus
	 * @return
	 */
	public AnchorPane wrapInKingTile(BasicBonus bonus){
		FlowPane pane = new FlowPane();
		AnchorPane aPane = new AnchorPane();
		int victoryPoints = bonus.getVictoryPoints();
		if (victoryPoints > 0) {
			ImageView view = new ImageView(imagePoints);
			view.setFitHeight(20.0);
			view.setFitWidth(20.0);
			Label label = getLabelFor(imagePoints, victoryPoints);
			pane.prefWidth(100.0);
			viewBackBonus = new ImageView(imageBackPurple);
			viewBackBonus.setFitHeight(100.0);
			viewBackBonus.setFitWidth(60.0);
			aPane.getChildren().add(viewBackBonus);
			aPane.getChildren().add(label);

			AnchorPane.setBottomAnchor(viewBackBonus, 0.0);
			AnchorPane.setRightAnchor(viewBackBonus, 0.0);
			AnchorPane.setBottomAnchor(label, 10.0);
			AnchorPane.setRightAnchor(label, 20.0);
		}

		pane.setMaxWidth(70.0);
		pane.getChildren().add(aPane);
		pane.setRotate(-30.0);
		return Utility.encapsulate(new AnchorPane(), pane);
		
	}
	
	

	private FlowPane getColoredCityBonus(ColoredCityBonus bonus) {
		FlowPane pane = new FlowPane();
		AnchorPane aPane = new AnchorPane();
		int victoryPoints = ((BasicBonus) bonus.getBonus()).getVictoryPoints();
		if (victoryPoints > 0) {
			ImageView view = new ImageView(imagePoints);
			view.setFitHeight(20.0);
			view.setFitWidth(20.0);
			Label label = getLabelFor(imagePoints, victoryPoints);
			pane.prefWidth(100.0);
			Image imageBackBonus=null;
			switch (bonus.getColor()) {
			case BRONZE:
				imageBackBonus=imageBackBronze;
				break;
			case GOLD:
				imageBackBonus=imageBackGold;
				break;
			case IRON:
				imageBackBonus=imageBackIron;
				break;
			case SILVER:
				imageBackBonus=imageBackSilver;
				break;
			case PURPLE:
				imageBackBonus=imageBackPurple;
				break;
			default:
				break;
			}
	
			viewBackBonus = new ImageView(imageBackBonus);
			viewBackBonus.setFitHeight(100.0);
			viewBackBonus.setFitWidth(60.0);
			
			aPane.getChildren().add(viewBackBonus);
			aPane.getChildren().add(label);

			AnchorPane.setBottomAnchor(viewBackBonus, 0.0);
			AnchorPane.setRightAnchor(viewBackBonus, 0.0);
			AnchorPane.setBottomAnchor(label, 10.0);
			AnchorPane.setRightAnchor(label, 20.0);
		}
		pane.setMaxWidth(70.0);
		pane.getChildren().add(aPane);
		pane.setRotate(-30.0);
		
		if(bonus.isAssigned()){
		ColorAdjust colorAdjust = new ColorAdjust();
		colorAdjust.setBrightness(-0.5);
		pane.setEffect(colorAdjust);
		}
		return pane;
	}
	
	
	private FlowPane getBasicBonusLayout(BasicBonus bonus) {
		FlowPane pane = new FlowPane();
		
		int coins = bonus.getCoins();
		if (coins > 0)
			pane.getChildren().add(getLabelFor(imageCoin, coins));
		
		int assistants = bonus.getAssistants();
		if (assistants > 0)
			pane.getChildren().add(getLabelFor(imageAssistant, assistants));
		
		int victoryPoints = bonus.getVictoryPoints();
		if (victoryPoints > 0)
			pane.getChildren().add(getLabelFor(imagePoints, victoryPoints));
		
		int cards = bonus.getCards();
		if (cards > 0)
			pane.getChildren().add(getLabelFor(imageCard, cards));
		
		int nobilityTrack = bonus.getNobilityTrack();
		if (nobilityTrack > 0)
			pane.getChildren().add(getLabelFor(imageAdvance, nobilityTrack));
		
		return pane;
	}

	private Label getLabelFor(Image imageGraphic, int count) {
		Label label = new Label(Integer.toString(count), getFixedSizeView(imageGraphic));
		label.setContentDisplay(ContentDisplay.RIGHT);
		
		return label;
	}

	public void setBonusWidth(double width) {
		this.bonusWidth = width;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
}
