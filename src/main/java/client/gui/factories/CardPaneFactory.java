package client.gui.factories;

import cof.model.Card;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * Singleton Class, give the possibility to build new Card.
 * @author Simone
 *
 */

public class CardPaneFactory {
	double cardWidth=150;
	double cardHeight=75;
	private static CardPaneFactory instance;
	
	Image cardBlack;
	Image cardWhite;
	Image cardJolly;
	Image cardCyan;
	Image cardOrange;
	Image cardPurple;
	Image cardPink;
	
	private CardPaneFactory() {
		cardBlack = new Image(getClass().getResourceAsStream("/images/pc_black.png"));
		cardWhite = new Image(getClass().getResourceAsStream("/images/pc_white.png"));
		cardJolly = new Image(getClass().getResourceAsStream("/images/pc_jolly.png"));
		cardCyan = new Image(getClass().getResourceAsStream("/images/pc_cyan.png"));
		cardOrange = new Image(getClass().getResourceAsStream("/images/pc_orange.png"));
		cardPurple = new Image(getClass().getResourceAsStream("/images/pc_purple.png"));
		cardPink = new Image(getClass().getResourceAsStream("/images/pc_pink.png"));
	}
	
	public void setCardWidth(double width) {
		cardWidth = width;
	}
	public void setCardHeight(double height) {
		cardHeight = height;
	}
	
	public static CardPaneFactory getInstance() {
		if (instance == null)
			instance = new CardPaneFactory();
		return instance;
	}
	
	public CardPane buildCard(Card card) {
		return new CardPane(card);
	}
	
	public class CardPane extends Pane {
		private CardPane(Card card) {
			super();
			ImageView view = new ImageView();
			switch (card.getColor()) {
			case BLACK:
				view.setImage(cardBlack);
				break;
			case BLUE:
				view.setImage(cardCyan);
				break;
			case MULTI:
				view.setImage(cardJolly);
				break;
			case ORANGE:
				view.setImage(cardOrange);
				break;
			case PINK:
				view.setImage(cardPink);
				break;
			case PURPLE:
				view.setImage(cardPurple);
				break;
			case WHITE:
				view.setImage(cardWhite);
				break;
			default:
				break;
			}
			view.setPreserveRatio(true);
			view.setFitWidth(cardWidth);
			view.setFitHeight(cardHeight);
			this.setId(card.getTag());
			this.getChildren().add(view);
			
			this.getStyleClass().add("card-pane");
		}
	}
}
