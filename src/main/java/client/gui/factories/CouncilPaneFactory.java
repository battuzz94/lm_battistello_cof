package client.gui.factories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import cof.model.Council;
import cof.model.Councillor;
import common.Utility;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * Singleton Class, give the possibility to build new Council and Councillors.
 * @author Simone
 *
 */

public class CouncilPaneFactory {
	private Image councilBalconyImage;
	private static CouncilPaneFactory factory;
	
	private double councillorHeight = 50;
	private Image whiteCouncillorImage;
	private Image blackCouncillorImage;
	private Image blueCouncillorImage;
	private Image purpleCouncillorImage;
	private Image pinkCouncillorImage;
	private Image orangeCouncillorImage;
	
	private CouncilPaneFactory() {
		whiteCouncillorImage = new Image(getClass().getResourceAsStream("/images/white_councillor.png"));
		blackCouncillorImage = new Image(getClass().getResourceAsStream("/images/black_councillor.png"));
		blueCouncillorImage = new Image(getClass().getResourceAsStream("/images/blue_councillor.png"));
		purpleCouncillorImage = new Image(getClass().getResourceAsStream("/images/purple_councillor.png"));
		pinkCouncillorImage = new Image(getClass().getResourceAsStream("/images/pink_councillor.png"));
		orangeCouncillorImage = new Image(getClass().getResourceAsStream("/images/orange_councillor.png"));
		
		councilBalconyImage = new Image(getClass().getResourceAsStream("/images/councilBalcony.png"));
	}
	
	
	public static CouncilPaneFactory getInstance() {
		if (factory == null) {
			factory = new CouncilPaneFactory();
		}
		return factory;
	}
	
	/**
	 * Builds a council pane with council councillors
	 * @param council
	 * @return
	 */
	public CouncilPane buildCouncil(Council council) {
		return new CouncilPane(council);
	}
	
	/**
	 * Builds a councillor pane with specified list of councillors
	 * @param councillors
	 * @return
	 */
	public CouncillorsPane buildCouncillors(List<Councillor> councillors) {
		return new CouncillorsPane(councillors);
	}
	
	/**
	 * An hbox of councillors
	 * @author andrea
	 *
	 */
	public class CouncillorsPane extends HBox {
		private List<Councillor> councillors;
		private List<Node> councilPanes;
		
		private CouncillorsPane(List<Councillor> councillors) {
			super();
			councilPanes = new ArrayList<>();
			this.councillors = councillors;
			
			for (Councillor c : this.councillors) {
				ImageView councillorImageView = getImageViewByColor(c);
				councillorImageView.setPreserveRatio(true);
				councillorImageView.setFitHeight(councillorHeight);
				
				
				AnchorPane container = Utility.encapsulate(new AnchorPane(), councillorImageView);
				container.setId(c.getTag());
				
				getChildren().add(container);
				councilPanes.add(container);
				
				container.getStyleClass().add("councillor-pane");
			}
			
		}
		
		protected Stream<Node> stream() {
			return councilPanes.stream();
		}
		
		private ImageView getImageViewByColor(Councillor councillor) {
			ImageView ret;
			switch (councillor.getColor()) {
			case BLUE:
				ret = new ImageView(blueCouncillorImage);
				break;
			case ORANGE:
				ret = new ImageView(orangeCouncillorImage);
				break;
			case PINK:
				ret = new ImageView(pinkCouncillorImage);
				break;
			case PURPLE:
				ret = new ImageView(purpleCouncillorImage);
				break;
			case WHITE:
				ret = new ImageView(whiteCouncillorImage);
				break;
			case BLACK:
				ret = new ImageView(blackCouncillorImage);
				break;
			default:
				ret = new ImageView();
			}
			return ret;
		}
		
		protected <T extends Event> void addCouncillorListener(EventType<T> eventType, EventHandler<? super T> eventHandler) {
			for (Node v : councilPanes) {
				v.addEventHandler(eventType, eventHandler);
			}
		}
		
		protected void resetListeners() {
			for (Node v : councilPanes) {
				v.setOnMouseEntered(null);
				v.setOnMouseExited(null);
				v.setOnMouseClicked(null);
			}
		}
	}
	
	/**
	 * A council pane is a councillor pane with a balcony image
	 * @author andrea
	 *
	 */
	public class CouncilPane extends AnchorPane {
		CouncillorsPane box;
		Council council;
		
		private CouncilPane(Council council) {
			super();
			this.council = council;
			ImageView councilImageView = new ImageView(councilBalconyImage);
			councilImageView.setFitHeight(councillorHeight/2);
			box = new CouncillorsPane(council.getCouncillors());
			
			councilImageView.setFitWidth(box.getBoundsInParent().getWidth() * council.getCouncillors().size() + 10);
			this.setWidth(councilImageView.getFitWidth());
			AnchorPane.setLeftAnchor(councilImageView, 0.0);
			AnchorPane.setLeftAnchor(box, 0.0);
			box.setAlignment(Pos.BASELINE_LEFT);
			
			AnchorPane.setBottomAnchor(councilImageView, 0.0);
			getChildren().addAll(box, councilImageView);
			
			getStyleClass().add("council-pane");
		}
		
		
		protected <T extends Event> void addCouncillorListener(EventType<T> eventType, EventHandler<? super T> eventHandler) {
			box.addCouncillorListener(eventType, eventHandler);
		}
		
		protected void resetListeners() {
			box.resetListeners();
		}
	}
	
}
