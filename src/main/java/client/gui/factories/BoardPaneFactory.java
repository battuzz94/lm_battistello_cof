package client.gui.factories;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

import client.gui.events.EventListener;
import client.gui.factories.CityPaneFactory.CityPane;
import client.gui.factories.CouncilPaneFactory.CouncilPane;
import client.gui.factories.CouncilPaneFactory.CouncillorsPane;
import client.gui.factories.NobilityTrackFactory.NobilityTrackPane;
import client.gui.factories.PermitPaneFactory.PermitPane;
import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import cof.model.Board;
import cof.model.City;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Region;
import common.Utility;
import javafx.beans.property.ObjectProperty;
import javafx.event.Event;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
 * This is a Singleton Factory, constructs the Board.
 * @author Simone
 *
 */

public class BoardPaneFactory {
	private static final String BACKGROUND_BOARD= "/images/board_background.jpg";
	private static final BonusPaneFactory bonusFactory = BonusPaneFactory.getInstance();
	private static final NobilityTrackFactory nobFactory = NobilityTrackFactory.getInstance();
	private static final CouncilPaneFactory councilFactory = CouncilPaneFactory.getInstance();
	private static final PermitPaneFactory permitFactory = PermitPaneFactory.getInstance();
	private static final CityPaneFactory cityFactory = CityPaneFactory.getInstance();
		
	private static final double BOARD_RATIO = 4298.0 / 6548.0;
	private static double citySize = 80.0;
	
	private static Image imageBackground;
	private static BoardPaneFactory instance;
	
	private BoardPaneFactory() {
		imageBackground= new Image(getClass().getResourceAsStream(BACKGROUND_BOARD));
	}
	/**
	 * Return the singleton instance of board factory.
	 * @return
	 */
	public static BoardPaneFactory getInstance() {
		if (instance == null)
			instance = new BoardPaneFactory();
		return instance;
	}
	
	/**
	 * Builds a new BoardPane with player informations
	 * @param board
	 * @param bounds
	 * @param player
	 * @return
	 */
	public BoardPane buildBoard(Board board, ObjectProperty<Bounds> bounds, Player player) {
		return new BoardPane(board, bounds,player);
	}
	
	/**
	 * Builds a new board without player information
	 * @param board
	 * @param bounds
	 * @return
	 */
	public BoardPane buildBoard(Board board, ObjectProperty<Bounds> bounds) {
		return new BoardPane(board, bounds,null);		
	}
	
	/**
	 * Contains the structure of a board
	 * @author andrea
	 *
	 */
	public class BoardPane extends AnchorPane {
		private static final double HEIGHTRATIOBACKGROUND = 0.656;
		private Board board;
		private List<CouncilPane> councils;
		private List<CityPane> cities;
		private List<PermitPane> gainablePermits;
		private CouncillorsPane availableCouncillors;
		
		private Player player;
		private NobilityTrackPane nobilityTrack;
		

		private BoardPane(Board board, ObjectProperty<Bounds> bounds,Player player) {
			super();
			councils = new LinkedList<>();
			cities = new LinkedList<>();
			gainablePermits = new LinkedList<>();
			
			this.player=player;
			
			this.board = board;
			draw(bounds);
		}
		
		public void setListeners(EventListener listener) {
			councils.forEach(council -> {
				String councilTag = council.getId();
				council.setOnMouseEntered(e -> listener.onEnterCouncil(e, councilTag));
				council.setOnMouseExited(e -> listener.onExitCouncil(e, councilTag));
				council.setOnMouseClicked(e -> listener.onActionCouncil(e, councilTag));
			});
			gainablePermits.forEach(permit -> {
				String permitTag = permit.getId();
				permit.setOnMouseEntered(e -> listener.onEnterGainablePermit(e, permitTag));
				permit.setOnMouseExited(e -> listener.onExitGainablePermit(e, permitTag));
				permit.setOnMouseClicked(e -> listener.onActionGainablePermit(e, permitTag));
			});
			cities.forEach(city -> {
				String cityTag = city.getId();
				city.setOnMouseEntered(e -> listener.onEnterCity(e, cityTag));
				city.setOnMouseExited(e -> listener.onExitCity(e, cityTag));
				city.setOnMouseClicked(e -> listener.onActionCity(e, cityTag));
			});
			
			availableCouncillors.stream().forEach(councillor -> {
				String councillorTag = councillor.getId();
				councillor.setOnMouseEntered(e -> listener.onEnterAvailableCouncillor(e, councillorTag));
				councillor.setOnMouseExited(e -> listener.onExitAvailableCouncillor(e, councillorTag));
				councillor.setOnMouseClicked(e -> listener.onActionAvailableCouncillor(e, councillorTag));
			});
		}
		
		public void setNobilityCallbacks(BiConsumer<? super Event, Bonus> callback) {
			if (nobilityTrack != null)
				nobilityTrack.setCallback(callback);
		}
		
		
		protected void draw(ObjectProperty<Bounds> bounds) {	
			VBox mainContainer = new VBox();
			
			AnchorPane boardContent = new AnchorPane();
			
			boardContent.getChildren().addAll(loadImageBackground(bounds));
			
						
			
			boardContent.getChildren().addAll(getCityConnections(bounds));
			boardContent.getChildren().addAll(getCities(bounds));
			boardContent.getChildren().addAll(getDivisionLine(bounds));
			
			mainContainer.getChildren().add(boardContent);
			
			
			//BONUS PER LE CITY
			AnchorPane anchor = new AnchorPane();
			List<AnchorPane> bonusCity = new LinkedList<>();
			
			AnchorPane wrapInKingTile = bonusFactory.wrapInKingTile((BasicBonus)board.getKingBonus());
			bonusCity.add(wrapInKingTile);
			HBox hbox = new HBox();
			board.getCityBonus().forEach(coloredCityBonus -> {
				AnchorPane bonusPane = bonusFactory.getBonusPane(coloredCityBonus);
				bonusCity.add(bonusPane);
			});
			
			
			Utility.encapsulate(hbox,bonusCity);
			Utility.encapsulate(anchor,hbox);
			AnchorPane.setRightAnchor(hbox, 20.0);
			AnchorPane.setBottomAnchor(hbox, 10.0);
		
			
			
			//CREATE A VBOX LIST WITH COUNCIL AND PERMIT FOR EACH REGION
			List<VBox> vList= new LinkedList<>();
			List<Pane> printRegionCouncil = printRegionCouncil();
			List<Pane> permits = getPermits();
			
			for(int i=0;i<printRegionCouncil.size();i++){
				VBox currentVbox= new VBox();
				currentVbox.setSpacing(10.0);
				getTransparent(currentVbox);
				currentVbox.getChildren().add(printRegionCouncil.get(i));
				currentVbox.getChildren().add(permits.get(i));				
				vList.add(currentVbox);
			}
			HBox encapsulatedPermitCouncil = Utility.encapsulate(new HBox(),vList);
			encapsulatedPermitCouncil.setMinWidth(bounds.getValue().getWidth());
			encapsulatedPermitCouncil.getChildren().forEach(node->{
				HBox.setHgrow(node,  Priority.ALWAYS);
			});
			
			//ADD AVAILABLE COUNCILLORS AND KING COUNCIL
			VBox vBoxAccordion = new VBox();
			vBoxAccordion.setSpacing(10.0);
			VBox contentAccordion = Utility.encapsulate(
					getTransparent(vBoxAccordion),
					encapsulatedPermitCouncil, 
					getAvailableCouncillors(),
					getKingCouncil()
				);
			
			Accordion stuffAccordion = createAccordion("Side board",Utility.encapsulate(anchor,contentAccordion));
			
			AnchorPane.setTopAnchor(contentAccordion, 20.0);
			AnchorPane.setBottomAnchor(contentAccordion, 20.0);
			
			stuffAccordion.setPrefWidth(bounds.get().getWidth());
			stuffAccordion.getStyleClass().add(".accordion");
			
			mainContainer.getChildren().add(stuffAccordion);
			
			this.nobilityTrack = printNobilityTrack(player);
			
			ScrollPane nobilityScrollPane = new ScrollPane();
			nobilityScrollPane.setVbarPolicy(ScrollBarPolicy.NEVER);
			nobilityScrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
			nobilityScrollPane.setPrefViewportHeight(100.0);
			nobilityScrollPane.setContent(nobilityTrack);
			nobilityScrollPane.setPrefViewportWidth(bounds.getValue().getWidth());
			bounds.addListener((obj, oldVal, newVal) -> {
				encapsulatedPermitCouncil.setMinWidth(encapsulatedPermitCouncil.getMinWidth()/oldVal.getWidth()*newVal.getWidth());
				nobilityScrollPane.setPrefViewportWidth(newVal.getWidth());
				stuffAccordion.setPrefWidth(newVal.getWidth());
			});
			
			
			mainContainer.getChildren().add(nobilityScrollPane);
			this.getChildren().add(mainContainer);
			this.getStyleClass().add("board-pane");
		}

		private <T extends Node> Accordion createAccordion(String name, T elem) {
			Accordion accordion = new Accordion();
			elem.setStyle("-fx-background-color: transparent");
			TitledPane tPane = new TitledPane(name, elem);
			accordion.getPanes().add(tPane);
			return accordion;

		}
		
		private CouncilPane getKingCouncil() {
			CouncilPane kingCouncil = councilFactory.buildCouncil(board.getKingCouncil());
			kingCouncil.setId(board.getKingCouncil().getTag());
			councils.add(kingCouncil);
			return kingCouncil;
		}

		private CouncillorsPane getAvailableCouncillors() {
			CouncillorsPane pane = councilFactory.buildCouncillors(board.getCouncillorPool().getAvailableCouncillors());
			availableCouncillors = pane;
			return pane;
		}

		private NobilityTrackPane printNobilityTrack(Player player) {
			NobilityTrackPane buildNobilityTrack = nobFactory.buildNobilityTrack(board.getNobilityTrack(), player);
			AnchorPane.setBottomAnchor(buildNobilityTrack, 0.0);
			return buildNobilityTrack;
		}
		
		private List<Pane> getPermits() {
			List<Pane> ret = new LinkedList<>();
			for (Region region : board.getRegions()) {
				// Region permits
				HBox hbox = new HBox();
				for (Permit permit : region.getGainablePermits()) {
					permitFactory.setHeight(120.0);
					PermitPane buildPermit = permitFactory.buildPermit(permit);
					buildPermit.setId(permit.getTag());
					gainablePermits.add(buildPermit);
					hbox.getChildren().add(buildPermit);
				}
				ret.add(hbox);
			}
			return ret;
		}
		
		private List<Pane> printRegionCouncil() {
			List<Pane> ret = new LinkedList<>();
			for (Region region : board.getRegions()) {
				// Region council
				CouncilPane regionCouncil = councilFactory.buildCouncil(region.getRegionCouncil());

				regionCouncil.setId(region.getRegionCouncil().getTag());
				councils.add(regionCouncil);
				ret.add(Utility.encapsulate(new AnchorPane(), regionCouncil));
			}
			return ret;
		}
		
		private List<CityPane> getCities(ObjectProperty<Bounds> bounds) {
			List<CityPane> ret = new LinkedList<>();
			for (City city : board.getCities()) {
				double x = city.getCoordinate().x() / Board.WIDTH * bounds.getValue().getWidth();
				double y = city.getCoordinate().y() / Board.HEIGHT * bounds.getValue().getWidth() * BOARD_RATIO;
				
				CityPane cityPane;
				if(player!=null){
				cityPane = cityFactory.buildCity(city,board.getKingPosition()==city,player);
				}else{
				cityPane = cityFactory.buildCity(city,board.getKingPosition()==city,null);
				}
				
				cityPane.setId(city.getTag());
				cityPane.setPosition(x, y - citySize);
				ret.add(cityPane);
				cities.add(cityPane);
			}
			bounds.addListener((obj, oldVal, newVal) -> {
				for (CityPane c : cities) {
					double x = c.getLayoutX();
					double y = c.getLayoutY();
					c.setLayoutX(x/oldVal.getWidth() * newVal.getWidth());
					c.setLayoutY(y/oldVal.getWidth() * newVal.getWidth());
				}
			});
			return ret;
		}
		
		private List<Line> getCityConnections(ObjectProperty<Bounds> bounds) {
			double conversionRatioX = bounds.getValue().getWidth() / Board.WIDTH;
			double conversionRatioY = bounds.getValue().getWidth() * BOARD_RATIO / Board.HEIGHT;
			List<Line> lines = new LinkedList<>();
			for (City city : board.getCities()) {
				for (City city2 : city.getNeighbors()) {
					if (city.getTag().compareTo(city2.getTag()) > 0) {
						double x = city.getCoordinate().x() * conversionRatioX;
						double y = city.getCoordinate().y() * conversionRatioY - citySize;
						
						double x2 = city2.getCoordinate().x() * conversionRatioX + citySize / 2;
						double y2 = city2.getCoordinate().y() * conversionRatioY - citySize / 2;
						Line line = new Line(x + (citySize / 2), y + (citySize / 2), x2, y2);
						line.setStroke(Color.BROWN);
						line.setStrokeWidth(3);
						lines.add(line);
					}
				}
			}
			bounds.addListener((obj, oldVal, newVal) -> {
				for (Line l: lines) {
					l.setStartX(l.getStartX() / oldVal.getWidth() * newVal.getWidth());
					l.setEndX(l.getEndX() / oldVal.getWidth() * newVal.getWidth());
					
					l.setStartY(l.getStartY() / oldVal.getWidth() * newVal.getWidth());
					l.setEndY(l.getEndY() / oldVal.getWidth() * newVal.getWidth());
				}
			});
			
			return lines;
		}
		
		private List<Line> getDivisionLine(ObjectProperty<Bounds> bounds){
			double width=bounds.getValue().getWidth();
			List<Line> lines= new LinkedList<>();
			
			double translation=50.0;
			for(int i=1;i<board.getRegions().size();i++){
				double currentX1=(i*(width-translation)/board.getRegions().size())+translation;
				Line currentLine= new Line(currentX1,0.0,currentX1,width*HEIGHTRATIOBACKGROUND);
				currentLine.getStrokeDashArray().addAll(5d);
				currentLine.setStrokeWidth(1.5);
				currentLine.setStroke(Color.web("#3D210C"));
				lines.add(currentLine);
			}
			bounds.addListener((obj, oldVal, newVal) -> {
				for (Line l: lines) {
					l.setStartX(l.getStartX() / oldVal.getWidth() * newVal.getWidth());
					l.setEndX(l.getEndX() / oldVal.getWidth() * newVal.getWidth());
					
					l.setStartY(l.getStartY() / oldVal.getWidth() * newVal.getWidth());
					l.setEndY(l.getEndY() / oldVal.getWidth() * newVal.getWidth());
				}
			});
			return lines;
		}
		
		private ImageView loadImageBackground(ObjectProperty<Bounds> bounds) {
			ImageView viewBackground= new ImageView(imageBackground);
			viewBackground.setPreserveRatio(true);
			AnchorPane.setLeftAnchor(viewBackground, 0.0);
			AnchorPane.setTopAnchor(viewBackground, 0.0);
			AnchorPane.setRightAnchor(viewBackground, 0.0);
			bounds.addListener((obj, oldVal, newVal) -> viewBackground.setFitWidth(newVal.getWidth()));
			viewBackground.setFitWidth(bounds.getValue().getWidth());
			
			return viewBackground;
		}
		
		protected void setBoard(Board board) {
			this.board = board;
		}
		
	}


	private <T extends Pane> T getTransparent(T elem) {
		elem.setStyle("-fx-background-color:transparent");
		return elem;
	}


}