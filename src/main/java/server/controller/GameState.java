package server.controller;

import common.network.Message;
/**
 * This is the structure of a GameState, 
 * that describes the current State of the Game.
 * @author Simone
 *
 */
public interface GameState {
	
	public void handlePlayerMessage(Message message, PlayerHandler senderHandler);
	
}
