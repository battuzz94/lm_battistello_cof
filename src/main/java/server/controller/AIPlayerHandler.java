package server.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import ai.Evaluator;
import ai.Move;
import ai.MoveState;
import cof.commands.Command;
import cof.commands.MainAction;
import cof.commands.PassTurnCommand;
import cof.market.Market;
import cof.model.Board;
import cof.model.Player;
import common.ActionType;
import common.Utility;
import common.network.Message;
import server.network.ServerConnectionHandler;

/**
 * An implementation of an Artificial player.
 * This class acts as a real user, but all the moves are decided by the Evaluator algorithms. 
 * @author andrea
 *
 */
public class AIPlayerHandler extends PlayerHandler implements ServerConnectionHandler{
	private static final Gson gson = Utility.getJsonSerializer();
	private static final Logger LOGGER = Logger.getLogger(AIPlayerHandler.class.getName());
	private StateContext listener;
	private int id;
	
	private Board board;
	private Market market;
	private List<Move> movesLeft;
	private ExecutorService threadPool;
	
	public AIPlayerHandler(Player player) {
		super(player);
		this.connectionHandler = this;		// Redirects remote calls to itself
		
		threadPool = Executors.newSingleThreadExecutor();
	}
	
	
	@Override
	public ServerConnectionHandler getConnectionHandler() {
		return this;
	}

	@Override
	public void sendToClient(Message message) {
		threadPool.submit(() -> handle(message));
	}


	private void handle(Message message) {
		switch (message.getType()) {
		case BOARD_UPDATE:
			board = gson.fromJson(message.getContent(), Board.class);
			break;
		case CONFIGURE_BOARD:
			listener.received(new Message(ActionType.BOARD_CONFIGURATION, gson.toJson(Utility.loadTestBoardDisposed())), id);
			break;
		case INIT_MARKET_TURN:
		case SELL_MARKET_ACTION:
			sendPassTurn();
			
			break;
		case INIT_TURN:
			generateAction();
			sendAction();
			break;
		case GOOD_MOVE:
			if (!movesLeft.isEmpty())
				sendAction();
			else
				sendPassTurn();
			break;
		case MARKET_UPDATE:
			market = gson.fromJson(message.getContent(), Market.class);
			break;
		default:
			break;
		}
	}


	private void sendAction() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			LOGGER.log(Level.FINE, "Thread interrupted", e);
			Thread.currentThread().interrupt();
		}
		
		if (movesLeft.isEmpty())
			throw new NoSuchElementException();
		Command toSend = movesLeft.remove(0).getCommand();
		if (toSend instanceof MainAction)
			listener.received(new Message(ActionType.MAIN_ACTION, gson.toJson(toSend, Command.class)), id);
		else
			listener.received(new Message(ActionType.QUICK_ACTION, gson.toJson(toSend, Command.class)), id);
	}


	private void sendPassTurn() {
		listener.received(new Message(ActionType.PASS_TURN, gson.toJson(new PassTurnCommand(), Command.class)), id);
	}


	private void generateAction() {
		movesLeft = Evaluator.getBestMove(new MoveState(board, player, this.playerState));
	}

	@Override
	public void sendPlayerUpdate() {
		// Do nothing
	}
	
	@Override
	public boolean isOnline() {
		return true;
	}

	@Override
	public boolean isOffline() {
		return false;
	}

	@Override
	public void setOffline() {
		// Can't be set offline
	}

	@Override
	public void setOnline() {
		// Always online
	}

	@Override
	public void setListener(StateContext listener) {
		this.listener = listener;
	}

	@Override
	public void setID(int id) {
		this.id = id;
	}
	
	@Override
	public int getID() {
		return id;
	}


	@Override
	public void disconnect() {
		// Does nothing
	}
}
