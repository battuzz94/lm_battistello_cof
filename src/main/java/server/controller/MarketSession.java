package server.controller;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import cof.commands.BuyItemCommand;
import cof.commands.Command;
import cof.commands.MarketAction;
import cof.commands.PassTurnCommand;
import cof.commands.TurnState;
import cof.market.Market;
import cof.model.Board;
import cof.model.Player;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * This is the buy-phase of the market
 * @author andrea
 *
 */
public class MarketSession implements GameState {
	private static final Gson gson = Utility.getJsonSerializer();
	private static int timeTurnExceeded = 300000;
	private static final Logger LOGGER = Logger.getLogger(MarketSession.class.getName());

	
	private Market market;
	private Board board;
	private StateContext context;
	private PlayerPool playerPool;
	private PlayerHandler currentPlayer;
	private Timer marketTimer;

	
	/**
	 * Creates a new Market Session
	 * @param market
	 * @param context
	 */
	public MarketSession(Market market, StateContext context){
		this.market=market;
		this.context=context;
		this.playerPool=context.getPlayerPool();
		this.board=context.getBoard();
		
		playerPool.resetMarket();
		playerPool.sendAll(new Message(ActionType.BUY_MARKET_ACTION));
		
		nextPlayer();
	}
	
	
	@Override
	public void handlePlayerMessage(Message message, PlayerHandler senderHandler) {
		switch(message.getType()){
		case OTHER_UPDATE:
			playerPool.sendPlayersStatTo(senderHandler);
			break;
		case MARKET_UPDATE:
			senderHandler.sendToClient(new Message(ActionType.MARKET_UPDATE, gson.toJson(market)));
			break;
		case INFO:
			Utility.printDebug(senderHandler + " sent this to server: " + message);
			break;
		default:
			if (senderHandler.getID() == currentPlayer.getID())
				handleCurrentPlayerMessage(message, senderHandler);
			else
				senderHandler.sendToClient(new Message(ActionType.INFO, "It is not your turn."));
		}
		
	}
	
	
	private void handleCurrentPlayerMessage(Message message, PlayerHandler senderHandler){
		Command command;
		
		switch(message.getType()){
		case BUY_MARKET_ACTION:
			command=gson.fromJson(message.getContent(), BuyItemCommand.class);
			executeCommand((MarketAction)command, senderHandler);
			break;
		case PASS_TURN:
			command=gson.fromJson(message.getContent(), PassTurnCommand.class);
			handlePassTurn(command, senderHandler);
			break;
		default:
			senderHandler.sendToClient(new Message(ActionType.INFO, "This cannot be done during market."));
		}
		
	}
	
	
	
	private void executeCommand(MarketAction buyCommand, PlayerHandler senderHandler){
		Player senderPlayer = senderHandler.getPlayer();
		TurnState playerState = senderHandler.getState(); 
		
		buyCommand.setMarket(market);
		
		if(buyCommand.isValid(senderPlayer, board, playerState)){
			try {
				buyCommand.execute(senderPlayer, board, playerState);
				
				Utility.printDebug("Command sent from client was valid.");
				senderHandler.sendToClient(new Message(ActionType.GOOD_MOVE));
				playerPool.sendAll(new Message(ActionType.INFO, "[" + senderHandler.getPlayer().getUsername() + "] : " + buyCommand));
				senderHandler.sendPlayerUpdate();
			} 
			catch (Exception e) {
				senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
				LOGGER.log(Level.FINE, "Client sent a not valid command", e);
			}
			
		} else {
			Utility.printDebug("Command sent from client was NOT valid.");
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
		}
		
	}
	
	
	
	
	private void handlePassTurn(Command command, PlayerHandler senderHandler){
		
		if(command.isValid(senderHandler.getPlayer(), board, senderHandler.getState())){
			marketTimer.cancel();
			
			senderHandler.sendToClient(new Message(ActionType.TURN_FINISHED));
			playerPool.sendAll(new Message(ActionType.INFO, "[" + currentPlayer.getPlayer().getUsername() + "]: PASS TURN."));
			nextPlayer();
			
			Utility.printDebug("Player did a pass turn. Now the current player has ID: " + currentPlayer.getID());
			
		}  else {
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
		}
	}
	
	
	
	private void nextPlayer(){
		if(playerPool.allPlayersDoneMarket()){
			market.giveBack();
			GameSession gs=new GameSession(this.context);
			context.setState(gs);
			Utility.printDebug("Market finished. Returning to GameSession");
		}
		else {
			currentPlayer=playerPool.getRandom();
			marketTimer=new Timer();
			marketTimer.schedule(new PlayerTimeoutTask(), timeTurnExceeded);
			currentPlayer.getConnectionHandler().sendToClient(new Message(ActionType.MARKET_UPDATE, gson.toJson(market)));
			currentPlayer.getConnectionHandler().sendToClient(new Message(ActionType.INIT_MARKET_TURN, 
					"It's your turn: you have " + timeTurnExceeded/60000 + " minutes to complete the market phase!"));
		}
	}
	
	
	private class PlayerTimeoutTask extends TimerTask {
		@Override
		public void run() {
			currentPlayer.getConnectionHandler().setOffline();
			currentPlayer.sendToClient(new Message(ActionType.TIME_OUT_NOTIFICATION));
			Utility.printDebug("Timeout occurred in turn of player with ID " + currentPlayer.getID());
			
			nextPlayer();
		}
	}


	public static void setTimeout(int amount) {
		MarketSession.timeTurnExceeded = amount;
	}
}
