package server.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import cof.model.Board;
import cof.model.Player;
import cof.model.Player.PlayerStat;
import common.ActionType;
import common.CircularIterator;
import common.Utility;
import common.network.Message;

/**
 * A collection of players with some utilities
 * @author andrea
 *
 */
public class PlayerPool {
	List<PlayerHandler> players;
	CircularIterator<PlayerHandler> currentIterator; 
	boolean advanced = false;
	boolean[] doneMarket;
	PlayerHandler currentPlayer;
	PlayerHandler nextPlayer;
	
	
	boolean endInsert = false;
	private boolean cycleFinished;
	
	private static final Gson gson = Utility.getJsonSerializer();
	
	/**
	 * Creates a new empty player pool
	 */
	public PlayerPool() {
		players = new ArrayList<>();
		currentIterator = null;
	}
	
	/**
	 * Returns true if all the iterator is at the end of the players list
	 * @return
	 */
	public boolean allPlayersDoneTurn(){
		return cycleFinished;
	}
	
	/**
	 * Returns true if all players have completed market action
	 * @return
	 */
	public boolean allPlayersDoneMarket(){
		
		for(int i=0; i<doneMarket.length; i++){
			if(!doneMarket[i] && players.get(i).getConnectionHandler().isOnline())
				return false;
		}
		return true;
	}
	
	/**
	 * Advances by one position only if not advanced yet
	 */
	private void advance() {
		if (!advanced) {
			int prevCycleCount = currentIterator.getCycleCount();
			nextPlayer = currentIterator.next();
			while (currentIterator.hasNext() && nextPlayer.getConnectionHandler().isOffline())
				nextPlayer = currentIterator.next();
			
			cycleFinished = currentIterator.getCycleCount() != prevCycleCount;
			advanced = true;
		}
	}
	
	/**
	 * Returns the next player
	 * @return
	 */
	public PlayerHandler next() {
		if (!endInsert) {
			endInsert = true;
			currentIterator = new CircularIterator<>(players);
		}
		
		if (allOffline())
			throw new NoSuchElementException();
		advance();
		advanced = false;
		currentPlayer = nextPlayer;
		return currentPlayer;
	}
	
	/**
	 * Check if all players are offline
	 * @return
	 */
	public boolean allOffline() {
		return !players.stream()
				.filter(p -> p.getConnectionHandler().isOnline())
				.findFirst()
				.isPresent();
	}
	
	public PlayerHandler getCurrent() {
		return currentPlayer;
	}
	
	/**
	 * Add a new player
	 * @param player
	 * @throws IllegalStateException if you try to add after you called the first next()
	 */
	public void addPlayer(PlayerHandler player) {
		if (endInsert)
			throw new IllegalStateException();
		players.add(player);
	}
	
	/**
	 * Sends a BOARD_UPDATE to all players
	 * @param board
	 */
	public void sendBoardToAll(Board board) {
		Message serializedBoard = new Message(ActionType.BOARD_UPDATE, gson.toJson(board, Board.class));
		sendAll(serializedBoard);
	}
	
	/**
	 * Sends the other players stats to the player
	 * @param player
	 */
	public void sendPlayersStatTo(PlayerHandler player) {
		List<Player.PlayerStat> stats = players.stream()
				.filter(p -> p.getID() != player.getID())
				.map(p -> p.getStats())
				.collect(Collectors.toList());
		player.getConnectionHandler()
			.sendToClient(new Message(ActionType.OTHER_UPDATE, gson.toJson(stats)));
	}
	
	/**
	 * Sends player stats to all
	 */
	public void sendPlayerStatToAll() {
		players.forEach(this::sendPlayersStatTo);
	}
	
	/**
	 * Sends a message to all
	 * @param m
	 */
	public void sendAll(Message m) {
		players.forEach(p -> p.getConnectionHandler().sendToClient(m));
	}
	
	/**
	 * Sends a player update to all
	 */
	public synchronized void sendPlayerToAll() {
		players.forEach(
				p -> {
					Message msg = new Message(ActionType.PLAYER_UPDATE, gson.toJson(p.getPlayer(), Player.class));
					p.getConnectionHandler().sendToClient(msg);
				}
			);
		
	}
	
	public synchronized int getCount() {
		return players.size();
	}
	
	public boolean isEmpty() {
		return players.isEmpty();
	}
	
	/**
	 * Returns the connection handler associated with player with ID idSender
	 * @param idSender
	 * @return
	 */
	public PlayerHandler getHandlerByID(Integer idSender) {
		return players.stream()
				.filter(p -> p.getID() == idSender)
				.findFirst()
				.get();
	}

	public List<Player> getPlayers() {
		return players.stream().map(p -> p.getPlayer()).collect(Collectors.toList());
	}

	public List<PlayerHandler> getHandlers() {
		return players;
	}
	
	
	protected void resetMarket(){
		this.doneMarket=new boolean[players.size()];
		for(int i=0; i<players.size(); i++)
			doneMarket[i]=false;
	}
	
	protected int getNumberOfPlayers(){
		return this.players.size();
	}
	
	
	public PlayerHandler getRandom(){
		Random rand=new Random();
		int poolSize=players.size();
		
		int randomPlayerIndex=rand.nextInt(poolSize);
		while(doneMarket[randomPlayerIndex] || players.get(randomPlayerIndex).getConnectionHandler().isOffline()){
			randomPlayerIndex=rand.nextInt(poolSize);
		}
		doneMarket[randomPlayerIndex]=true;
		return players.get(randomPlayerIndex);
	}
	
	/**
	 * Returns the player whose ID is id
	 * @param id
	 * @return
	 */
	public Player getPlayerThroughId(int id) {
		return players.stream().map(p -> p.getPlayer()).filter(p -> p.getID()==id).findFirst().get();
	}

	public String getRanking() {
		return players.stream()
				.sorted((p1, p2) -> Integer.compare(p2.getPlayer().getVictoryPoints(), p1.getPlayer().getVictoryPoints()))
				
				.map(p -> p.getPlayer().getUsername() + ": " + p.getPlayer().getVictoryPoints())
				.reduce((s1, s2) -> s1 + "\n" + s2)
				.get();
		
	}
	
	/**
	 * Returns the ranking in terms of player stats
	 * @return
	 */
	public List<PlayerStat> getRanking2() {
		return players.stream()
				.sorted((p1, p2) -> Integer.compare(p2.getPlayer().getVictoryPoints(), p1.getPlayer().getVictoryPoints()))
				.map(p -> p.getPlayer().getPlayerStat())
				.collect(Collectors.toList());
	}
	
	/**
	 * Disconnects all the players from network
	 */
	public void disconnectAll() {
		players.stream().forEach(p -> p.getConnectionHandler().disconnect());
	}
	
	/**
	 * Sends a message to all players except currentPlayer
	 * @param message
	 */
	public void sendOthers(Message message) {
		players.stream()
			.filter(p -> p.getID() != currentPlayer.getID())
			.forEach(p -> p.getConnectionHandler().sendToClient(message));
	}
}
