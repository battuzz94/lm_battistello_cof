package server.controller;

import com.google.gson.Gson;

import cof.model.Board;
import common.ActionType;
import common.Utility;
import common.network.Message;
/**
 * This is a Game state, in this State only the Admin is able to chose the board config. 
 * The other Players are waiting for configuration.
 * @author Simone
 *
 */
public class ChooseBoardSession implements GameState {
	private static final Gson gson = Utility.getJsonSerializer();
	
	StateContext context;
	/**
	 * Constructor requires the context.
	 * @param context
	 */
	
	public ChooseBoardSession(StateContext context) {
		this.context = context;
		
		PlayerHandler admin = context.getAdmin();
		admin.sendToClient(new Message(ActionType.CONFIGURE_BOARD));
	}
	
	/**
	 * This method is called with the received message.
	 */
	
	@Override
	public void handlePlayerMessage(Message message, PlayerHandler senderHandler) {
		if (senderHandler.getID() == context.getAdmin().getID())
			handleAdminMessage(message, senderHandler);
		else {
			senderHandler.sendToClient(new Message(ActionType.WAIT_CONFIGURATION));
		}
	}
	
	private void handleAdminMessage(Message message, PlayerHandler senderHandler) {
		switch (message.getType()) {
		case AI_AGENTS:
			int aiAgents = Integer.parseInt(message.getContent());
			for (int i = 0; i < aiAgents; i++)
				context.addAIPlayer();
			senderHandler.sendToClient(new Message(ActionType.AI_AGENTS, "OK"));
			break;
		case BOARD_CONFIGURATION:
			String boardJson = message.getContent();
			Board board = gson.fromJson(boardJson, Board.class);
			context.setBoard(board);
			
			context.initGame();
			
			context.getPlayerPool().sendAll(new Message(ActionType.GAME_INIT));
			Utility.printDebug("Game started!");
			
			context.setState(new GameSession(context));
			break;
		default:
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
		}
	}

}
