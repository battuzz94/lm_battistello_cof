package server.controller;

import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import cof.model.Board;
import cof.model.Card;
import cof.model.Player;
import common.ActionType;
import common.Utility;
import common.network.Message;
import server.network.ServerConnectionHandler;
import server.network.SessionHandler;

/**
 * This class is the main listener for messages from client. 
 * @author andrea
 *
 */
public class StateContext {
	public static final int STARTING_NUMBEROF_CARDS=5; //the sixth one is draw at the beginning of first turn
	private static final int TIME_ALIVE_MILLS = 20000;
	private static final Logger LOGGER = Logger.getLogger(StateContext.class.getName());
	static int gameIndex = 0;
	
	private GameState gameState;
	
	private PlayerPool playerPool;
	private PlayerHandler currentPlayer;
	
	private boolean isAlive;
	private int coins=10;
	private int assistants=1;
	private PlayerHandler admin;
	private boolean gameRunning;
	private Timer gameTimer;
	
	private Board board;

	private int aiID = 0;
	
	private TimerTask sessionTimeoutTimerTask = new TimerTask() {
		int time=TIME_ALIVE_MILLS/1000;
		int i=1;
		@Override
		public void run() {
			playerPool.sendAll(new Message(ActionType.TIMER,String.valueOf(time-i)));
			if(i==time){
				initBoardConfiguration();
				this.cancel();
			}
			i++;
		}
	};
	
	
	
	/**
	 * Initializes a new room
	 */
	public StateContext(){
		playerPool = new PlayerPool();
		gameIndex++;
		gameTimer = new Timer();
		isAlive=true;
		Utility.printDebug("New room available.");

	}
	
	
	
	protected PlayerHandler getAdmin(){
		return this.admin;
	}
	
	
	protected PlayerHandler getCurrentPlayer(){
		return this.currentPlayer;
	}
	
	
	protected PlayerPool getPlayerPool(){
		return this.playerPool;
	}
	
	protected Board getBoard(){
		return this.board;
	}
	
	
	public boolean isAlive() {
		return isAlive;
	}
	
	
	protected void initBoardConfiguration() {

		gameRunning = true;
		isAlive=false;
		
		gameState = new ChooseBoardSession(this);
		
		Utility.printDebug("Choose configuration..");
	}
	
	protected void initGame() {
		currentPlayer = playerPool.next();
	}
	
	
	/**
	 * Adds a new player to this context
	 * @param identifier
	 * @param serverConnectionHandler
	 * @param nickname
	 */
	public synchronized void addPlayer(int identifier, ServerConnectionHandler serverConnectionHandler, String nickname) {
		if (isAlive) {
			Utility.printDebug("A new player with ID " + identifier + " and username " + nickname + " connected to this room");
			
			Player p=new Player(coins, assistants);
			p.setUsername(nickname);
			p.setID(serverConnectionHandler.getID());
			for (int i = 0; i < STARTING_NUMBEROF_CARDS; i++)
				p.obtain(Card.drawCard());		
			coins++;
			assistants++;
			serverConnectionHandler.setListener(this);
			
			PlayerHandler playerHandler = new PlayerHandler(p, serverConnectionHandler);
			
			
			
			if (playerPool.isEmpty()) {
				admin = playerHandler;
				currentPlayer = playerHandler;
				Utility.printDebug("Admin set to: " + playerHandler);
			}
			else if (playerPool.getCount() == 1) {
				gameTimer.schedule(sessionTimeoutTimerTask, 0,1000);
				Utility.printDebug("Started timer of " + TIME_ALIVE_MILLS/1000 + " seconds.");
			}
			
			playerPool.addPlayer(playerHandler);

			Utility.printDebug("List of connected players: " + playerPool);
		}
	}
	
	/**
	 * Receive and process a message received from client
	 * @param message
	 * @param idSender
	 */
	public synchronized void received(Message message, Integer idSender) {
		Utility.printDebug("Received this message from client with ID " + idSender + ":\n" + message);
		
		try {
			PlayerHandler senderHandler = playerPool.getHandlerByID(idSender);
			if (gameRunning)
				gameState.handlePlayerMessage(message, senderHandler);
			else
				senderHandler.sendToClient(new Message(ActionType.INFO, "The game has not started yet"));
		}
		catch (NoSuchElementException e) {
			Utility.printDebug("Unknown sender sent a message to server.");
			LOGGER.log(Level.FINE, "Unknown sender sent a message to server.", e);
		}
		
	}
	
	protected void setState(final GameState state){
		gameState=state;
	}
	
	protected void setGameRunning(boolean gameRunning){
		this.gameRunning=gameRunning;
	}

	protected void setBoard(Board board) {
		this.board = board;
	}


	/**
	 * Adds a new Artificial intelligence based player
	 */
	public synchronized void addAIPlayer() {
		int networkID = SessionHandler.getID(); 
		Utility.printDebug("A new AI player with ID " + networkID + " and username " + "Computer" + aiID + " connected to this room");
		Player p=new Player(coins, assistants);
		p.setUsername("Computer" + aiID);
		p.setID(networkID);
		for (int i = 0; i < STARTING_NUMBEROF_CARDS; i++)
			p.obtain(Card.drawCard());			
		coins++;
		assistants++;
		aiID++;
		
		AIPlayerHandler ai = new AIPlayerHandler(p);
		ai.setID(p.getID());
		ai.setListener(this);
		
		playerPool.addPlayer(ai);
	}



	protected void endGame() {
		gameRunning = false;
		playerPool.disconnectAll();
	}
}
