package server.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import cof.bonus.BasicBonus;
import cof.commands.Command;
import cof.commands.PassTurnCommand;
import cof.commands.PollBonusCommand;
import cof.commands.TurnState;
import cof.model.Board;
import cof.model.Card;
import cof.model.Player;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * Game session is the main game handler
 * @author andrea
 *
 */
public class GameSession implements GameState {
	private static final Logger LOGGER = Logger.getGlobal();
	private static int timeTurnExceeded = 10 * 60 * 1000; // 10 minutes
	private static final int EMPORIA_TO_FINISH=10;
	
	private StateContext context;
	
	private static final Gson gson = Utility.getJsonSerializer();
	private static final BasicBonus MAJOR_NOBILITY_BONUS = new BasicBonus(0,0,5,0,0);
	private static final BasicBonus MINOR_NOBILITY_BONUS = new BasicBonus(0,0,2,0,0);
	private static final PollBonusCommand pollBonus = new PollBonusCommand();
	
	private PlayerHandler admin;
	private PlayerPool playerPool;
	private PlayerHandler currentPlayer;
	private Timer gameTimer;
	private Board board;
	
	
	/**
	 * Creates a new Game Session
	 * @param context
	 */
	public GameSession(StateContext context){
		this.admin=context.getAdmin();
		this.playerPool=context.getPlayerPool();
		this.currentPlayer=context.getCurrentPlayer();
		this.board=context.getBoard();
		this.context=context;
		
		initFirstTurn();
	}
	
	
	protected void initFirstTurn(){
		gameTimer = new Timer();
		// start the timer for first player
		gameTimer.schedule(new PlayerTimeoutTask(), timeTurnExceeded);
		
		currentPlayer.resetState();
		currentPlayer.getPlayer().obtain(Card.drawCard());
		currentPlayer.sendToClient(new Message(ActionType.INIT_TURN));
		playerPool.sendOthers(new Message(ActionType.INFO, "Now is " + currentPlayer.getPlayer().getUsername() + "'s turn."));
		playerPool.sendPlayerToAll();
		playerPool.sendBoardToAll(board);
	}
	
	@Override
	public void handlePlayerMessage(Message message, PlayerHandler senderHandler) {
		switch (message.getType()) {
		case OTHER_UPDATE: 
			playerPool.sendPlayersStatTo(senderHandler);
			break;
		case INFO:
			Utility.printDebug(senderHandler + " sent this to server: " + message);
			break;
		default:
			if (senderHandler.getID() == currentPlayer.getID())
				handleCurrentPlayerMessage(message, senderHandler);
			else
				senderHandler.sendToClient(new Message(ActionType.INFO, "It is not your turn."));	
		}
	}
	
	
	private void handleCurrentPlayerMessage(Message message, PlayerHandler senderHandler) {
		Command command;
		
		switch (message.getType()) {
		case MAIN_ACTION:
		case QUICK_ACTION:
		case GENERIC_COMMAND:
		case NOBILITY_ACTION:
			command = gson.fromJson(message.getContent(), Command.class);
			executeCommand(command, senderHandler);
			
			break;
		case PASS_TURN:
			command = new PassTurnCommand();
			handlePassTurn(command, senderHandler);
			
			break;
		default:
			Utility.printDebug("Unknown message sent from: " + senderHandler + ": " + message);
			break;
		}
	}

	private void handlePassTurn(Command command, PlayerHandler senderHandler) {
		
		if (command.isValid(senderHandler.getPlayer(), board, senderHandler.getState())) {
			// cancels the timer of the current player
			if(!senderHandler.getState().getDynamicBonuses().isEmpty()){
				senderHandler.sendToClient(new Message(ActionType.INFO, "[SERVER]: Remember that you still have some nobility track bonus to get (by the additional action commands); you should try to obtain them next turn!"));
			}
			gameTimer.cancel();
			
			senderHandler.sendToClient(new Message(ActionType.TURN_FINISHED));
			playerPool.sendAll(new Message(ActionType.INFO, "[" + currentPlayer.getPlayer().getUsername() + "]: PASS TURN."));
			playerPool.sendPlayerStatToAll();
			nextPlayer();
			
			
		} else {
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
			if(senderHandler.getState().canDoMainAction())
				senderHandler.sendToClient(new Message(ActionType.INFO, "[SERVER] You still have to do a main action!"));
		}
		
	}

	private void nextPlayer() {
		try {
			currentPlayer = playerPool.next();
		}
		catch (NoSuchElementException e) {
			LOGGER.log(Level.FINE, "All players disconnected", e);
			Utility.printDebug("No active player. Ending game");
			endGame();
			return;
		}
		if (gameFinished()) {
			Utility.printDebug("Game finished!");
			endGame();
		}
		else if(playerPool.allPlayersDoneTurn()) {
			context.setState(new PreMarketSession(this.context));
		}
		else {
			currentPlayer.resetState();
			currentPlayer.getPlayer().obtain(Card.drawCard());
			currentPlayer.sendPlayerUpdate();
			currentPlayer.sendToClient(new Message(ActionType.INIT_TURN));
			playerPool.sendOthers(new Message(ActionType.INFO, "Now is " + currentPlayer.getPlayer().getUsername() + "'s turn."));
			Utility.printDebug("Player turn finished. Now it's turn of player with ID: " + currentPlayer.getID());
			
			// start the timer for next player
			gameTimer = new Timer();
			gameTimer.schedule(new PlayerTimeoutTask(), timeTurnExceeded);
		}
	}

	private void endGame() {
		context.setGameRunning(false);
		finalizeGame();
		
		Utility.printDebug("The game is finished! The final ranking is:\n");
		String ranking = playerPool.getRanking();
		
		playerPool.sendAll(new Message("The game is finished! The final ranking is:\n" + ranking));
		
		Utility.printDebug(ranking);
		
		playerPool.sendAll(new Message(ActionType.GAME_FINISHED, gson.toJson(playerPool.getRanking2())));
		
		context.endGame();
	}

	private boolean gameFinished() {
		return currentPlayer.getPlayer().getBuildedEmporia().size() == EMPORIA_TO_FINISH;
	}


	private void executeCommand(Command command, PlayerHandler senderHandler) {
		Player senderPlayer = senderHandler.getPlayer();
		TurnState playerState = senderHandler.getState(); 
		if (command.isValid(senderPlayer, board, playerState)) {
			
			try {
				command.execute(senderPlayer, board, playerState);
				while(playerState.canPollStaticBonus())
					pollBonus.execute(senderPlayer, board, playerState);
				
				Utility.printDebug("Command sent from client was valid.");
				senderHandler.sendToClient(new Message(ActionType.GOOD_MOVE));
				playerPool.sendAll(new Message(ActionType.INFO, "[" + senderHandler.getPlayer().getUsername() + "] : " + command));
				senderHandler.sendPlayerUpdate();
				playerPool.sendBoardToAll(board);
			} 
			catch (Exception e) {
				senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
				LOGGER.log(Level.FINE, "Client did a bad move", e);
			}
			
		} else {
			Utility.printDebug("Command sent from client was NOT valid.");
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
		}
	}
	
	
	/**
	 * Adds the points due to advances in nobility track
	 */
	private void finalizeGame() {
		List<Player> players = playerPool.getPlayers().stream()
				.sorted((p1, p2) -> Integer.compare(p1.getNobility(), p2.getNobility()))
				.collect(Collectors.toList());
		
		int m = players.size() - 1;
		int i = m;
		
		while(i >= 0 && players.get(m).getNobility() == players.get(i).getNobility()) {
			players.get(i).obtain(MAJOR_NOBILITY_BONUS);
			i--;
		}
		
		if (i == m-1) {
			m = i;
			while(i >= 0 && players.get(m).getNobility() == players.get(i).getNobility()) {
				players.get(i).obtain(MINOR_NOBILITY_BONUS);
				i--;
			}
		}
		
	}
	
	
	public static void setTimeout(int amount) {
		GameSession.timeTurnExceeded = amount;
	}
	
	
	private class PlayerTimeoutTask extends TimerTask {
		@Override
		public void run() {
			currentPlayer.getConnectionHandler().setOffline();
			currentPlayer.sendToClient(new Message(ActionType.TIME_OUT_NOTIFICATION));
			playerPool.sendOthers(new Message(ActionType.INFO_TIME_OUT, "The player with ID: " + currentPlayer.getID() + " and username: " + currentPlayer.getPlayer().getUsername() + " has disconnected."));
			Utility.printDebug("Timeout occurred for player with ID: " + currentPlayer.getID());
			nextPlayer();
		}
	}
	
}
