package server.controller;

import com.google.gson.Gson;

import cof.commands.TurnState;
import cof.model.Player;
import common.ActionType;
import common.Utility;
import common.network.Message;
import server.network.ServerConnectionHandler;
/**
 * This Class handle all info about player.
 * @author Simone
 *
 */
public class PlayerHandler {
	private static final Gson gson = Utility.getJsonSerializer();
	ServerConnectionHandler connectionHandler;
	Player player;
	TurnState playerState;
	/**
	 * This constructor let create a player without connection, in case of internal player(e.g.AI Player).
	 * 
	 * @param player
	 */
	protected PlayerHandler(Player player) {
		this(player, null);
	}
	/**
	 * This constructor let create a plater handler.
	 * @param player
	 * @param connectionHandler This is the connection Handler paired to the player.
	 */
	public PlayerHandler(Player player, ServerConnectionHandler connectionHandler) {
		this.connectionHandler = connectionHandler;
		this.player = player;
		this.playerState = new TurnState();
	}
	
	public ServerConnectionHandler getConnectionHandler() {
		return connectionHandler;
	}
	
	public TurnState getState() {
		return playerState;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Player.PlayerStat getStats() {
		return player.getPlayerStat();
	}
	
	public int getID() {
		return connectionHandler.getID();
	}
	/**
	 * This method let send a message to Client.
	 * @param message
	 */
	public void sendToClient(Message message) {
		connectionHandler.sendToClient(message);
	}
	/**
	 * This method send an update to Client.
	 */
	public void sendPlayerUpdate() {
		connectionHandler.sendToClient(new Message(ActionType.PLAYER_UPDATE, gson.toJson(player, Player.class)));
	}

	public void resetState() {
		playerState.reset();
	}
	
	
}
