package server.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;


import cof.commands.Command;
import cof.commands.MarketAction;
import cof.commands.PassTurnCommand;
import cof.commands.SellItemCommand;
import cof.commands.TurnState;
import cof.market.Market;
import cof.model.Board;
import cof.model.Player;
import common.ActionType;
import common.Utility;
import common.network.Message;

/**
 * Initialize the sell-phase of the market
 * @author andrea
 *
 */
public class PreMarketSession implements GameState {
	private static final Logger LOGGER = Logger.getLogger(PreMarketSession.class.getName());
	private static final Gson gson = Utility.getJsonSerializer();
	
	private Board board;
	private StateContext context;
	private PlayerPool playerPool;
	private Market market;
	
	/**
	 * Creates a new PreMarket session
	 * @param context
	 */
	public PreMarketSession(StateContext context){
		this.context=context;
		this.board=context.getBoard();
		this.playerPool=context.getPlayerPool();
		this.market=new Market(playerPool);
		
		playerPool.sendAll(new Message(ActionType.SELL_MARKET_ACTION));
		Utility.printDebug("Started pre-market phase for this turn");
	}
	
	
	@Override
	public void handlePlayerMessage(Message message, PlayerHandler senderHandler) {
		Command command;
		
		switch(message.getType()){
		case SELL_MARKET_ACTION:
			command=gson.fromJson(message.getContent(), SellItemCommand.class);
			executeCommand(command, senderHandler);
			break;
		case PASS_TURN:
			command=gson.fromJson(message.getContent(), PassTurnCommand.class);
			handlePassTurn(command, senderHandler);
			break;
		default:
			senderHandler.sendToClient(new Message(ActionType.INFO, "This cannot be done during market."));
		}
	}
	
	
	private void executeCommand(Command sellCommand, PlayerHandler senderHandler){
		Player senderPlayer = senderHandler.getPlayer();
		TurnState playerState = senderHandler.getState(); 
		
		if (sellCommand instanceof MarketAction)
			((MarketAction)sellCommand).setMarket(market);
		
		if (sellCommand.isValid(senderPlayer, board, playerState)) {
			try {
				sellCommand.execute(senderPlayer, board, playerState);
				
				Utility.printDebug("Command sent from client was valid.");
				senderHandler.sendToClient(new Message(ActionType.GOOD_MOVE));
				senderHandler.sendPlayerUpdate();
			} 
			catch (Exception e) {
				senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
				LOGGER.log(Level.INFO, "Client did a bad move", e);
			}
			
		} else {
			Utility.printDebug("Command sent from client was NOT valid.");
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
		}
	}
	
	
	private void handlePassTurn(Command command, PlayerHandler senderHandler){
		if(command.isValid(senderHandler.getPlayer(), board, senderHandler.getState())){
			senderHandler.getState().setHasFinishedPreMarket(true);			
			
			playerPool.sendAll(new Message(ActionType.INFO, "[" + senderHandler.getPlayer().getUsername() + "]: PASS TURN."));
			senderHandler.sendToClient(new Message(ActionType.TURN_FINISHED));
			
			boolean allFinishedPreMarket = playerPool.getHandlers().stream()
					.filter(sh -> sh.getConnectionHandler().isOnline())
					.allMatch(sh -> sh.getState().hasFinishedPreMarket());
			
			if(allFinishedPreMarket){
				playerPool.getHandlers()
						.stream()
						.forEach(sh -> sh.getState().resetHasFinishedPreMarket());
				
				context.setState(new MarketSession(market, context));
			}
		} else{
			senderHandler.sendToClient(new Message(ActionType.BAD_MOVE));
		}
	}

}
