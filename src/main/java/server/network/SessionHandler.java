package server.network;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.controller.StateContext;
/**
 * SessionHandler Singleton class.
 * @author Simone
 *
 */
public class SessionHandler {
	private static final Logger LOGGER = Logger.getLogger(SessionHandler.class.getName());
	private static int socketPort = 7777;
	private static String registryName = "listener";
	private static final int RMI_PORT = 1099;
	private Thread listeningClient;
	private static int counterID = 0;
	private static SessionHandler sessionHanler;
	private boolean isRunning;
	List<StateContext> gameSessions = new ArrayList<>();
	
	private SessionHandler() {
		isRunning = false;
	}
	
	public synchronized StateContext getAvailableContexts() {
		if (gameSessions.isEmpty()) {
			StateContext newSession = new StateContext();
			gameSessions.add(newSession);
			return newSession;
		} else if (gameSessions.get(gameSessions.size()-1).isAlive())
			return gameSessions.get(gameSessions.size()-1);
		StateContext newSession = new StateContext();
		gameSessions.add(newSession);
		return newSession;
	}
	

	public static synchronized int getID() {
		int newID = counterID;
		counterID++;
		return newID;
	}
	
	/**
	 * Starts listening on socket and publish RMI object
	 */
	public void startServer() {
		if (!isRunning) {
			startSocket();
			startRMI();
			isRunning = true;
		}
	}

	public static synchronized SessionHandler getIstance() {
		if (sessionHanler == null)
			sessionHanler = new SessionHandler();
		return sessionHanler;
	}
	
	/**
	 * Start socket listening
	 */
	public void startSocket() {
		listeningClient = new SocketConnectionListener(socketPort);
		listeningClient.start();
	}
	
	/**
	 * Publish the RMI Listener on registry
	 */
	public void startRMI() {
		try {
			Registry registry = LocateRegistry.createRegistry(RMI_PORT);
			RMIConnectionListenerImpl rmiConnectionListenerImpl= new RMIConnectionListenerImpl();
			registry.rebind(registryName, rmiConnectionListenerImpl);
		} catch (RemoteException e) {
			LOGGER.log(Level.FINE, "Network error occurred", e);
		}
	}

	public boolean isRunning() {
		return isRunning;
	}
}
