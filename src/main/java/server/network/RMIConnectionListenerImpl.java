package server.network;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import common.network.ClientStub;
import common.network.RMIConnectionListener;
import common.network.ServerStub;
import server.controller.StateContext;

/**
 * Server implementation of RMIConnectionListener
 * @author andrea
 *
 */
public class RMIConnectionListenerImpl extends UnicastRemoteObject implements RMIConnectionListener {
	private static final long serialVersionUID = 4360698685322493316L;
	private ServerStubImpl serverStubImpl;

	public RMIConnectionListenerImpl() throws RemoteException {
		// Default constructor with throwable exception
	}

	/**
	 * This method return the ServerStub, used by Client, in RMI Connection, to
	 * send Object. Requires clienStubImpl Object to send message to the Client.
	 */
	@Override
	public ServerStub connect(ClientStub clientStubImpl, String nickname) throws RemoteException {
		StateContext context = SessionHandler.getIstance().getAvailableContexts();
		serverStubImpl = new ServerStubImpl(clientStubImpl);
		serverStubImpl.setOnline();
		int id = SessionHandler.getID();
		serverStubImpl.setID(id);
		context.addPlayer(id, serverStubImpl, nickname);

		return serverStubImpl;
	}

}
