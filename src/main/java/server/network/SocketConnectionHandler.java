package server.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.network.Message;
import server.controller.StateContext;


/**
 * Socket implementation of ServerConnectionHandler
 * @author andrea
 *
 */
public class SocketConnectionHandler implements ServerConnectionHandler {
	private static final Logger LOGGER = Logger.getLogger(SocketConnectionHandler.class.getName());
	private int myID;
	private Socket clientSocket;
	private StateContext listener;
	private ObjectInputStream objectInputStream;
	private boolean isReading;
	private Thread listenThread;
	private ObjectOutputStream objectOutputStream;
	private boolean online;
	private String username = null;

	/**
	 * Creates a Connection Handler for a client connected through socket, and creates a thread 
	 * that listens to the socket for messages.
	 * @param socket
	 */
	public SocketConnectionHandler(Socket socket) {
		this.clientSocket = socket;
		try {
			OutputStream outputStream = clientSocket.getOutputStream();
			outputStream.flush();
			objectOutputStream = new ObjectOutputStream(outputStream);
			InputStream inputStream = clientSocket.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not create socket", e);
		}
		isReading = true;
		
		
		try {
			username = ((Message)objectInputStream.readObject()).getContent();
		} catch (ClassNotFoundException | IOException e) {
			LOGGER.log(Level.FINE, "Could not get username of player", e);
			username = "--";
		}
		
		listenThread = new Thread(this::listenSocket);
		listenThread.start();
	}

	@Override
	public void setListener(StateContext listener) {
		this.listener = listener;
	}
	
	/**
	 * Stops the socket connection
	 */
	public void stop() {
		isReading = false;
		try {
			clientSocket.close();
			objectInputStream.close();
			objectOutputStream.close();
		}
		catch (IOException e) {
			LOGGER.log(Level.FINE, "Could not close streams", e);
		}
	}


	private void listenSocket() {
		Message messageClient;
		while (isReading) {
			try {
				messageClient = (Message) objectInputStream.readObject();
				listener.received(messageClient, myID);
			} catch (IOException e) {
				isReading = false;
				LOGGER.log(Level.FINE, "Network error occured", e);
				this.setOffline();
			} catch (ClassNotFoundException e) {
				LOGGER.log(Level.FINE, "Unknown object received.", e);
			} 
		}
	}

	@Override
	public void sendToClient(Message message) {
		try {
			objectOutputStream.writeObject(message);
			objectOutputStream.flush();
		} catch (IOException e) {
			LOGGER.log(Level.FINE, "Could not send object", e);
			this.setOffline();
			isReading = false;
		}
	}

	@Override
	public int getID() {
		return myID;
	}

	@Override
	public void setID(int id) {
		myID = id;

	}

	@Override
	public boolean isOnline() {
		return online;
	}

	@Override
	public void setOffline() {
		online = false;

	}

	@Override
	public void setOnline() {
		online = true;

	}

	@Override
	public boolean isOffline() {
		return !online;
	}

	public String getUsername() {
		return this.username ;
	}

	@Override
	public void disconnect() {
		try {
			this.objectOutputStream.close();
			this.objectInputStream.close();
			this.clientSocket.close();
		} catch (IOException e) {
			LOGGER.log(Level.INFO, "Could not close socket", e);
		}
	}

}
