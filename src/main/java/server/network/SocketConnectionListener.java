package server.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.controller.StateContext;

/**
 * Listens to incoming socket connections
 * @author andrea
 *
 */
public class SocketConnectionListener extends Thread {
	private static final Logger LOGGER = Logger.getLogger(SocketConnectionListener.class.getName());
	ServerSocket serverSocket = null;
	int serverPort;
	
	/**
	 * Starts the server socket
	 * @param serverPort
	 */
	public SocketConnectionListener(int serverPort) {
		this.serverPort = serverPort;
		try {
			serverSocket = new ServerSocket(serverPort);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not instantiate ServerSocket", e);
			return;
		}
	}
	
	@Override
	public void run() {
		Socket clientConnected;
		while (true) {
			try {
				clientConnected = serverSocket.accept();
				SocketConnectionHandler socketConnectionHandler = new SocketConnectionHandler(clientConnected);
				socketConnectionHandler.setOnline();
				StateContext gameSession = SessionHandler.getIstance().getAvailableContexts();
				int id = SessionHandler.getID();
				socketConnectionHandler.setID(id);
				gameSession.addPlayer(id, socketConnectionHandler, socketConnectionHandler.getUsername());
			} catch (IOException e) {
				LOGGER.log(Level.FINE, "Exception occured while reading", e);
			}

		}
	}
}
