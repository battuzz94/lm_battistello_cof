package server.network;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.controller.GameSession;
import server.controller.MarketSession;

/**
 * Main server application
 * @author andrea
 *
 */
public class ServerMain {
	private static final Logger LOGGER = Logger.getLogger(ServerMain.class.getName());
	private ServerMain() {}
	
	
	/**
	 * Server main
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int timeout;
		do {
			System.out.println("Insert game timeout threshold (in seconds): ");
			timeout = in.nextInt();
		} while (timeout <= 0);
		
		GameSession.setTimeout(timeout*1000);
		MarketSession.setTimeout(timeout*1000);
		
		
		SessionHandler sessionHandler= SessionHandler.getIstance();
		sessionHandler.startServer();
		
		System.out.println("ServerStarted");
		while(sessionHandler.isRunning())
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				LOGGER.log(Level.FINE, "Server interrupted", e);
				Thread.currentThread().interrupt();
			}
		
	}
}
