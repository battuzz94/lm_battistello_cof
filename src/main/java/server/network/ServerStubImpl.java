package server.network;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.network.ClientStub;
import common.network.Message;
import common.network.ServerStub;
import server.controller.StateContext;

/**
 * Server implementation of ServerStub interface
 * @author andrea
 *
 */
public class ServerStubImpl extends UnicastRemoteObject implements ServerStub, ServerConnectionHandler {
	private static final Logger LOGGER = Logger.getLogger(ServerStubImpl.class.getName());
	/**
	 * Generated serialVersionUID
	 */
	private static final long serialVersionUID = -6259192438514425055L;
	private transient ClientStub clientStubImpl;
	private transient StateContext listener;
	private int myID;
	private boolean online;
	
	
	/** 
	 * Creates an RMI stub that connects a client to the server.
	 * @param clientStubImpl
	 * @throws RemoteException
	 */
	public ServerStubImpl(ClientStub clientStubImpl) throws RemoteException {
		this.clientStubImpl = clientStubImpl;
	}

	@Override
	public void setListener(StateContext listener) {
		this.listener = listener;
	}

	@Override
	public void send(Message message) throws RemoteException {
		listener.received(message, myID);
	}

	@Override
	public void sendToClient(Message message) {
		try {
			this.clientStubImpl.send(message);
			
		} catch (RemoteException e) {
			LOGGER.log(Level.FINE, "Network error occured", e);
		}

	}

	@Override
	public int getID() {
		return myID;
	}

	@Override
	public void setID(int id) {
		myID = id;

	}

	@Override
	public boolean isOnline() {
		return online;
	}

	@Override
	public void setOffline() {
		online = false;

	}

	@Override
	public void setOnline() {
		online = true;
	}

	@Override
	public boolean isOffline() {
		return !online;
	}

	@Override
	public void disconnect() {
		try {
			UnicastRemoteObject.unexportObject(this, true);
		} catch (NoSuchObjectException e) {
			LOGGER.log(Level.INFO, "Could not unexport object", e);	
		}
	}

}
