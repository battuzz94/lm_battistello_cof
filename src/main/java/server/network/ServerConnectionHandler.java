package server.network;

import common.network.Message;
import server.controller.StateContext;

/**
 * Abstraction of network connection to communicate with client
 * @author andrea
 *
 */
public interface ServerConnectionHandler {
	/**
	 * Sets the location where all messages received from client will be sent
	 * @param listener
	 */
	public void setListener(StateContext listener);
	
	/**
	 * Retrieves the ID of the client
	 * @return
	 */
	public int getID();
	
	/**
	 * Sets the ID of client
	 * @param id
	 */
	public void setID(int id);
	
	/**
	 * Checks if the client is still online
	 * @return
	 */
	public boolean isOnline();
	
	/**
	 * Checks if the client is offline
	 * @return
	 */
	public boolean isOffline();
	
	/**
	 * Set the client as offline
	 */
	public void setOffline();
	
	/**
	 * Set the client as online
	 */
	public void setOnline();
	
	/**
	 * Send a message to client
	 * @param message message to be sent
	 */
	public void sendToClient(Message message);
	
	/**
	 * Disconnect the server from client
	 */
	public void disconnect();

}
