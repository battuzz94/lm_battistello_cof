package cof.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import cof.bonus.BasicBonus;
import common.Utility;

public class TestSerialization {
	
	Board b;
	Gson gson;
	
	@Before
	public void setUp() throws Exception {
		b = new Board();
		gson = Utility.getJsonSerializer();
		
		
		CouncillorPool cp=new CouncillorPool();
		Councillor[] councillor=new Councillor[10];
		for(int i=0; i<10; i++){
			councillor[i]=new Councillor(PoliticColor.getRandomPoliticColor());
			cp.addCouncillor(councillor[i]);
		}
		Council c=new Council(cp);
		
		for (int i = 0; i < 20; i++) {
			b.addCouncillor(new Councillor(PoliticColor.getRandomPoliticColor()));
		}
		
		CouncillorPool pool = b.getCouncillorPool();
		
		
		Council counc = new Council(pool);
		for (int j = 0; j < 4; j++) 
			counc.pushCouncillor(pool.getRandomCouncillor());
		
		b.setKingCouncil(counc);
		
		Region r=new Region(c, BasicBonus.setRandom(), "mare");
		
		City city1=City.setRandomCity("Stoccarda");
		r.addCityToRegion(city1);
		City city2 = City.setRandomCity("Amsterdam");
		r.addCityToRegion(city2);
		City city3 = City.setRandomCity("Belgrado");
		r.addCityToRegion(city3);
		
		city1.addNeighbor(city2);
		city2.addNeighbor(city1);
		city2.addNeighbor(city3);
		city3.addNeighbor(city2);

		
		b.putRegionInBoard(r);
		b.moveKing(city1);
	}

	@Test
	public void testSerialization() {
		String s=gson.toJson(b);
		
		Board b2 = gson.fromJson(s, Board.class);
		
		
		assertNotNull(b2);  // This is done only if serialization goes well
	}
	
	@Test
	public void testDeserialization() {
		BoardFactory factory = new BoardFactory();
		factory.updateConfigWithPlayers(4);
		Board b = factory.build();
		String s=gson.toJson(b);
		Board deserBoard = gson.fromJson(s, Board.class);
		
		String deserStr = gson.toJson(deserBoard);
		
		assertEquals(s, deserStr);
	}

}
