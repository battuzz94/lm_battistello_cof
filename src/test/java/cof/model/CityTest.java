package cof.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import cof.bonus.BasicBonus;

public class CityTest {
	@Test
	public void testCity() {
		City c = new City("CityName");
		assertEquals(c.getEmporiaCount(), 0);
		assertTrue(c.getTag().startsWith(City.BASE_TAG));
		
		City c1 = new City("CityName");
		assertTrue(c1.getTag().startsWith(City.BASE_TAG));
		assertNotEquals(c.getTag(), c1.getTag());
	}

	@Test
	public void testGiveBuiltPathBonus() {
		City c1 = new City("CityName");
		City c2 = new City("CityName");
		City c3 = new City("CityName");
		c1.addNeighbor(c2);
		c2.addNeighbor(c1);
		c2.addNeighbor(c3);
		c3.addNeighbor(c2);
		
		assertEquals(0, c1.getDistanceTo(c1));
		assertEquals(1, c1.getDistanceTo(c2));
		assertEquals(2, c1.getDistanceTo(c3));
		assertEquals(1, c2.getDistanceTo(c1));
		
		c1.addNeighbor(c3);
		assertEquals(1, c1.getDistanceTo(c3));
		
	}
	
	@Test
	public void testGetBonusNearby() {
		City c1 = new City(new BasicBonus(1, 0, 0, 0, 0));
		City c2 = new City(new BasicBonus(0, 1, 0, 0, 0));
		City c3 = new City(new BasicBonus(0, 0, 1, 2, 4));
		c1.addNeighbor(c2);
		c2.addNeighbor(c1);
		c2.addNeighbor(c3);
		c3.addNeighbor(c2);
		
		// the map is: c1 -- c2 -- c3
		
		Player p = new Player(0, 0);
		
		// Must give nothing because p has not built in c1
		c1.giveBuiltPathBonus(p);
		assertEquals(0, p.getAssistants());
		assertEquals(new ArrayList<Card>(), p.getCards());
		assertEquals(0, p.getVictoryPoints());
		assertEquals(0, p.getCoins());
		assertEquals(0, p.getBuildedEmporia().size());
		assertEquals(0, p.getNobility());
		
		// Must give only bonus in c1
		p.buildEmporium(c1);
		c1.giveBuiltPathBonus(p);

		assertEquals(1, p.getAssistants());
		assertEquals(0, p.getCards().size());
 		assertEquals(0, p.getVictoryPoints());
		assertEquals(0, p.getCoins());
		assertEquals(1, p.getBuildedEmporia().size());
		assertEquals(0, p.getNobility());
		
		// Must give only bonus in c1 because p has not built in c2
		p.buildEmporium(c3);
		c1.giveBuiltPathBonus(p);

		assertEquals(2, p.getAssistants());
		assertEquals(0, p.getCards().size());
		assertEquals(0, p.getVictoryPoints());
		assertEquals(0, p.getCoins());
		assertEquals(2, p.getBuildedEmporia().size());
		assertEquals(0, p.getNobility());
		
		// Must give all bonuses of c1, c2 and c3
		p.buildEmporium(c2);
		c1.giveBuiltPathBonus(p);

		assertEquals(3, p.getAssistants());
		assertEquals(2, p.getCards().size());
		assertEquals(1, p.getVictoryPoints());
		assertEquals(1, p.getCoins());
		assertEquals(3, p.getBuildedEmporia().size());
		assertEquals(4, p.getNobility());
	}

}
