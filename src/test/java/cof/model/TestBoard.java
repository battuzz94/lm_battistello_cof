package cof.model;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.google.gson.Gson;

import common.Utility;

public class TestBoard {

	@Test
	public void testLoad() {
		try {
			Board b = Board.load("src/test/resources/testMap.json");
			assertNotNull(b);
			
		}
		catch (IOException e) {
			fail("Exception with IO");
		}
	}

}
