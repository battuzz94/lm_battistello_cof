package cof.model;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import cof.bonus.BasicBonus;
import common.Utility;

public class TestTagAfterLoad {

	@Test
	public void test() {
		Board board = Utility.loadTestBoard();
		
		Council council = new Council(board.getCouncillorPool());
		Councillor councillor = new Councillor(PoliticColor.BLACK);
		
		City city = new City("CityWithName");
		Card card = new Card(PoliticColor.BLACK);
		Permit permit = new Permit(BasicBonus.setRandom());
		
		int councillorNumber = Integer.parseInt(StringUtils.replace(councillor.getTag(), "Councillor", ""));
		int permitNumber = Integer.parseInt(StringUtils.replace(permit.getTag(), "Permit", ""));
		int councilNumber = Integer.parseInt(StringUtils.replace(council.getTag(), "Council", ""));
		
		assertTrue(councillorNumber >= 1);
		assertTrue(permitNumber >= 1);
		assertTrue(councilNumber >= 1);
	}

}
