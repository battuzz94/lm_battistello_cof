package cof.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import cof.bonus.BasicBonus;
import cof.bonus.Bonus;
import common.Utility;

public class TestPermit {
	Board board;
	Gson gson;
	
	@Before
	public void setup() {
		board = Utility.loadTestBoard();
		gson = Utility.getJsonSerializer();
	}
	
	@Test
	public void testPermit() {
		Bonus b = new BasicBonus(1, 2, 5, 3, 8);
		Permit p = new Permit(b);
		Price price = new Price(1, 4);
		p.setPrice(price);
		City c = new City("CityName");
		p.addCity(c);
		
		
		
		p.setBonus(b);
		
		assertEquals(price, p.getPrice());
		assertEquals(c, p.getCities().get(0));
		assertEquals(b, p.getBonus());
	}
	
	@Test
	public void testPermitSerialization() {
		Permit p = board.getPermitByTag("Permit2");
		
		p.addCity(board.getCityByTag("City2"));
		
		String permitJson = gson.toJson(p);
		Permit serPermit = gson.fromJson(permitJson, Permit.class);
		String reserializedJson = gson.toJson(serPermit);
		
		assertEquals(permitJson, reserializedJson);
		
	}

}
