package cof.model;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;

import org.junit.Test;

import com.google.gson.Gson;

import cof.bonus.BasicBonus;
import common.Utility;

public class BoardTest {
	
	@Test
	public void testLoadJsonBoard() {
		Gson gson = Utility.getJsonSerializer();
		Board board = Utility.loadTestBoard();
		
		testBoardNotNullNonEmpty(board);
		
		String json = gson.toJson(board);
		Board reconstructed = gson.fromJson(json, Board.class);
		
		testBoardNotNullNonEmpty(reconstructed);
		
	}
	
	@Test
	public void testGenerate() {
		//4 giocatori
		BoardFactory factory = new BoardFactory();
		Board b = factory.build();
		
		testBoardNotNullNonEmpty(b);
		
		//10 giocatori
		BoardFactory factory2 = new BoardFactory();
		Board b2 = factory.build();
		
		testBoardNotNullNonEmpty(b2);
		
	}
	
	public static void testBoardNotNullNonEmpty(Board b) {
		testFieldsNotNullNonEmpty(b);
		testFieldsNotNullNonEmpty(b.getCouncillorPool());
		
		for (Region region : b.getRegions()) {
			testFieldsNotNullNonEmpty(region);
			for (City city : region.getCities()) {
				testFieldsNotNullNonEmpty(city);
			}
		}
	}
	
	public static void testFieldsNotNullNonEmpty(Object obj) {
		for (Field f : obj.getClass().getDeclaredFields()) {
			try {
				f.setAccessible(true);
				Object ob = f.get(obj);
				assertNotEquals("The field " + f.getName() + " should not be null", null, ob);
				int ans = (int) ob.getClass().getMethod("size", null).invoke(ob, null);
				assertTrue(f.getName() + ".size() must be > 0", ans > 0);
			} 
			catch (Exception e) {}
		}
	}
	
	
	@Test
	public void testDisposeCity() {
		BoardFactory factory = new BoardFactory();
		Board b = factory.build();
		b.dispose();
		for (Region r : b.getRegions())
			for (City c : r.getCities()) {
				assertNotEquals(0.0, c.getCoordinate().x());
				assertNotEquals(0.0, c.getCoordinate().y());
			}
		
		PrettyPrinter p = new PrettyPrinter(b);
		p.printBoard();
	}
	
	@Test
	public void TestGenerateRandomNumber(){
		
		assertTrue(Utility.generateRandomNumber(1, 5)>=1);
		assertTrue(Utility.generateRandomNumber(1, 5)<=5);
		
		int val=Utility.generateRandomNumber(4, 20);
		assertTrue(val>=4 && val<=20);
		
		assertEquals(Utility.generateRandomNumber(1, 1), 1);
		
		int minIsGreaterThanMax=Utility.generateRandomNumber(3, 1);
		assertTrue(minIsGreaterThanMax>=1 && minIsGreaterThanMax<=3);
	}
	
	
	
}
