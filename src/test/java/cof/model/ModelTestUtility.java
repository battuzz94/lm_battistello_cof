package cof.model;

import java.util.ArrayList;

import com.google.gson.Gson;

import common.Utility;

public class ModelTestUtility {
	
	public static CouncillorPool getCouncillorPool(ArrayList<PoliticColor> councillorColors) {
		CouncillorPool p = new CouncillorPool();
		for (int i = 0; i < councillorColors.size(); i++) {
			p.addCouncillor(new Councillor(councillorColors.get(i)));
		}
		
		return p;
	}
	
	public static Council getCouncil(ArrayList<PoliticColor> colors, CouncillorPool p) {
		Council c = new Council(p, colors.size());
		for (Councillor councillor : p.getAvailableCouncillors()) 
			c.pushCouncillor(councillor);
		return c;
	}
	
	
	
}
