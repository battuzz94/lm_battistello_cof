package cof.commands;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cof.bonus.Bonus;
import cof.model.Board;
import cof.model.Card;
import cof.model.Council;
import cof.model.Councillor;
import cof.model.CouncillorPool;
import cof.model.ModelTestUtility;
import cof.model.NotEnoughCreditsException;
import cof.model.Permit;
import cof.model.Player;
import cof.model.PoliticColor;
import common.Utility;

public class TestBribeCouncilCommand {
	Board b;
	@Before
	public void setUp() throws Exception {	
		b = Utility.loadTestBoard();
	}


	@Test(expected=CommandNotValidException.class)
	public void testNotEnoughCredits() {
		Council c = b.getRegions().get(0).getRegionCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLACK));
		cards.add(new Card(PoliticColor.MULTI));
		cards.add(new Card(PoliticColor.WHITE));
		cards.add(new Card(PoliticColor.ORANGE));
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		Player player = new Player(0, 0);
		for (int i = 0; i < cards.size(); i++)
			player.addCards(cards.get(i));
		
		assertFalse(command.isValid(player, b, state));
		command.execute(player, b, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testNotValidCouncil() {
		Council c = b.getKingCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLACK));
		cards.add(new Card(PoliticColor.MULTI));
		cards.add(new Card(PoliticColor.WHITE));
		cards.add(new Card(PoliticColor.ORANGE));
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		Player player = new Player(100, 100);
		for (int i = 0; i < cards.size(); i++)
			player.addCards(cards.get(i));
		
		assertFalse(command.isValid(player, b, state));
		command.execute(player, b, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testNotValidCards() {
		Council c = b.getRegions().get(0).getRegionCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLACK));
		cards.add(new Card(PoliticColor.MULTI));
		cards.add(new Card(PoliticColor.WHITE));
		cards.add(new Card(PoliticColor.ORANGE));
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		Player player = new Player(100, 100);
		// Note: adds all cards BUT ONE
		for (int i = 0; i < cards.size()-1; i++)
			player.addCards(cards.get(i));
		
		assertFalse(command.isValid(player, b, state));
		command.execute(player, b, state);
	}

	@Test
	public void testCorrect() {
		Council c = b.getRegions().get(0).getRegionCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLACK));
		cards.add(new Card(PoliticColor.MULTI));
		cards.add(new Card(PoliticColor.WHITE));
		cards.add(new Card(PoliticColor.ORANGE));
		
		Player player = new Player(1, 0);
		for (int i = 0; i < cards.size(); i++)
			player.obtain(cards.get(i));
		
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		assertTrue(command.isValid(player, b, state));
		command.execute(player, b, state);
		
		Bonus b = p.getBonus();
		
		Player p2 = new Player(0, 0);
		p2.obtain(b);
		
		
		while (state.canPollStaticBonus()) {
			player.obtain(state.pollStaticBonus());
		}
		
		assertEquals(p2.getAssistants(), player.getAssistants());
		assertEquals(p2.getCards().size(), player.getCards().size());
		assertEquals(p2.getCoins(), player.getCoins());
		assertEquals(p2.getNobility(), player.getNobility());
		
		assertEquals(p, player.getPermits().get(0));
		
	}

	@Test
	public void testFourWrongCards() {
		Council c = b.getRegions().get(0).getRegionCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLUE));
		cards.add(new Card(PoliticColor.MULTI));
		cards.add(new Card(PoliticColor.WHITE));
		cards.add(new Card(PoliticColor.BLUE));
		
		Player player = new Player(1, 0);
		for (int i = 0; i < cards.size(); i++)
			player.obtain(cards.get(i));
		
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		assertFalse(command.isValid(player, b, state));
	}
	
	
	@Test
	public void testThreeWrongCards() {
		Council c = b.getRegions().get(0).getRegionCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLUE));
		cards.add(new Card(PoliticColor.MULTI));
		cards.add(new Card(PoliticColor.WHITE));

		
		Player player = new Player(1, 0);
		for (int i = 0; i < cards.size(); i++)
			player.obtain(cards.get(i));
		
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		assertFalse(command.isValid(player, b, state));
	}
	
	
	@Test
	public void testTwoWrongCards() {
		Council c = b.getRegions().get(0).getRegionCouncil();
		TurnState state=new TurnState();
		
		List<Card> cards = new ArrayList<>();
		cards.add(new Card(PoliticColor.BLUE));
		cards.add(new Card(PoliticColor.WHITE));
		
		Player player = new Player(1, 0);
		for (int i = 0; i < cards.size(); i++)
			player.obtain(cards.get(i));
		
		
		Permit p = b.getRegions().get(0).getGainablePermits().get(0);
		
		BribeCouncilCommand command = new BribeCouncilCommand(cards, c, p);
		
		assertFalse(command.isValid(player, b, state));
	}
}
