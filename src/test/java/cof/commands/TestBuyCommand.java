package cof.commands;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import cof.market.Assistant;
import cof.market.Market;
import cof.market.Sellable;
import cof.model.Board;
import cof.model.Card;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Price;
import common.Utility;

public class TestBuyCommand {
	Board board;
	Player buyer;
	Player seller;
	TurnState state;
	Market market;
	
	@Before
	public void setup() {
		board = Utility.loadTestBoard();
		buyer = new Player(10, 10);
		seller = new Player(10, 10);
		state = new TurnState();
		market= new Market(Arrays.asList(seller, buyer));
	}
	
	@Test
	public void testBuyPermit() {
		Permit p = board.getRegions().get(0).takePermitByTag("Permit1");
		
		seller.obtain(p);
		p.setPrice(new Price(5, 0));
		
		SellItemCommand sellCommand = new SellItemCommand(p);
		sellCommand.setMarket(market);
		sellCommand.execute(seller, board, state);
		
		assertEquals(10, seller.getCoins());
		assertEquals("The player must not have the permit", 0, seller.getPermits().size());
		
		BuyItemCommand buyCommand=new BuyItemCommand(p);
		buyCommand.setMarket(market);
		
		assertTrue(buyCommand.isValid(buyer, board, state));
		
		buyCommand.execute(buyer, board, state);
		
		assertEquals(5, buyer.getCoins());
		assertEquals(15, seller.getCoins());
		assertEquals(p.getTag(), buyer.getPermits().get(0).getTag());
		
	}
	
	
	@Test
	public void testBuyCard() {
		Card c = Card.drawCard();
		
		seller.obtain(c);
		c.setPrice(new Price(5, 0));
		
		SellItemCommand sellCommand = new SellItemCommand(c);
		sellCommand.setMarket(market);
		sellCommand.execute(seller, board, state);
		
		assertEquals(10, seller.getCoins());
		assertEquals("The player must not have the card", 0, seller.getCards().size());
		
		BuyItemCommand buyCommand=new BuyItemCommand(c);
		buyCommand.setMarket(market);
		
		assertTrue(buyCommand.isValid(buyer, board, state));
		
		buyCommand.execute(buyer, board, state);
		
		assertEquals(5, buyer.getCoins());
		assertEquals(15, seller.getCoins());
		assertEquals(c.getTag(), buyer.getCards().get(0).getTag());
		
	}
	
	
	@Test
	public void testBuyAssistant() {
		Assistant a = new Assistant();
	
		a.setPrice(new Price(5, 0));
		
		SellItemCommand sellCommand = new SellItemCommand(a);
		sellCommand.setMarket(market);
		sellCommand.execute(seller, board, state);
		
		assertEquals(10, seller.getCoins());
		assertEquals("The player must not have the assistant", 9, seller.getAssistants());
		
		BuyItemCommand buyCommand=new BuyItemCommand(a);
		buyCommand.setMarket(market);
		
		assertTrue(buyCommand.isValid(buyer, board, state));
		
		buyCommand.execute(buyer, board, state);
		
		assertEquals(5, buyer.getCoins());
		assertEquals(15, seller.getCoins());
		assertEquals(9, seller.getAssistants());
		assertEquals(11, buyer.getAssistants());
		
	}
	
	
	@Test
	public void testNotEnoughCredits(){
		Assistant a = new Assistant();
		
		a.setPrice(new Price(15, 0));
		
		SellItemCommand sellCommand = new SellItemCommand(a);
		sellCommand.setMarket(market);
		sellCommand.execute(seller, board, state);
		
		assertEquals(10, seller.getCoins());
		assertEquals("The player must not have the assistant", 9, seller.getAssistants());
		
		BuyItemCommand buyCommand=new BuyItemCommand(a);
		buyCommand.setMarket(market);
		
		assertFalse(buyCommand.isValid(buyer, board, state));
	}

}
