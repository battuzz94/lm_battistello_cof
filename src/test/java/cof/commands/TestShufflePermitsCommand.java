package cof.commands;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cof.model.Board;
import cof.model.Player;
import cof.model.Region;
import common.Utility;

public class TestShufflePermitsCommand {
	Board board;
	Player player;
	Region region;
	List<String> permitsBefore;
	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoard();
		permitsBefore = new ArrayList<>();
		Board b2 = Utility.loadTestBoard();
		Region r2 = b2.getRegions().get(2);
		while (r2.getGainablePermits().size() > 0) {
			permitsBefore.add(r2.takePermitByTag(r2.getGainablePermits().get(0).getTag()).getTag());
		}
		player = new Player(100, 100);
		region = board.getRegions().get(2);
	}

	@Test
	public void testOk() {
		List<String> permitsAfter = new ArrayList<>();
		
		
		TurnState state = new TurnState();
		Command command = new ShufflePermitsCommand(region);
		
		assertEquals(2, region.getGainablePermits().size());
		
		assertTrue(command.isValid(player, board, state));
		command.execute(player, board, state);
		
		assertEquals(100 - 1, player.getAssistants());
		assertEquals(100, player.getCoins());
		
		assertEquals(2, region.getGainablePermits().size());
		
		while (region.getGainablePermits().size() > 0) {
			permitsAfter.add(region.takePermitByTag(region.getGainablePermits().get(0).getTag()).getTag());
		}
		
		assertTrue(permitsAfter.containsAll(permitsBefore));
		assertTrue(permitsBefore.containsAll(permitsAfter));
		
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForInvalidTags() {
		TurnState state = new TurnState();
		Command command = new ShufflePermitsCommand("Region");
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
		
	}

}
