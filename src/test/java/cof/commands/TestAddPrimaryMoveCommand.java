package cof.commands;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cof.model.Board;
import cof.model.Player;
import common.Utility;

public class TestAddPrimaryMoveCommand {
	
	Board board;
	Player player;
	
	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoard();
		player = new Player(100, 100);
	}

	@Test
	public void testOk() {
		TurnState state = new TurnState();
		Command command = new AddPrimaryMoveCommand();
		
		assertTrue(command.isValid(player, board, state));
		command.execute(player, board, state);
		assertEquals(100 - 3, player.getAssistants());
		assertEquals(100, player.getCoins());
		
		assertEquals(true, state.canDoMainAction());
		state.doMainAction();
		assertEquals(true, state.canDoMainAction());
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForAssistants() {
		player.takeAssistants(100);
		
		TurnState state = new TurnState();
		Command command = new AddPrimaryMoveCommand();
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}
	
}
