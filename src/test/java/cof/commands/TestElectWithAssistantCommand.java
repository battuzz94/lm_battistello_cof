package cof.commands;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cof.model.Board;
import cof.model.Council;
import cof.model.Councillor;
import cof.model.Player;
import cof.model.PoliticColor;
import common.Utility;

public class TestElectWithAssistantCommand {
	Board board;
	Player player;
	Council council;
	private Councillor councillor;
	
	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoard();
		player = new Player(100, 100);
		council = board.getKingCouncil();
		councillor = board.getCouncillorPool().getRandomCouncillor();
	}

	@Test
	public void test() {
		TurnState state = new TurnState();
		Command command = new ElectWithAssistantCommand(council, councillor);
		
		assertTrue(command.isValid(player, board, state));
		command.execute(player, board, state);
		
		assertTrue(council.getCouncillors().contains(councillor));
		assertEquals(100 - 1, player.getAssistants());
		assertEquals(100, player.getCoins());
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForAssistants() {
		player.takeAssistants(100);
		TurnState state = new TurnState();
		Command command = new ElectWithAssistantCommand(council, councillor);
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailInvalidTags() {
		TurnState state = new TurnState();
		
		Command command = new ElectWithAssistantCommand("Council", councillor.getTag());
		assertFalse(command.isValid(player, board, state));
		
		command = new ElectWithAssistantCommand(council.getTag(), "Councillor");
		assertFalse(command.isValid(player, board, state));
		
		command.execute(player, board, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailInvalidCouncillor() {
		TurnState state = new TurnState();
		councillor = new Councillor(PoliticColor.BLACK);
		Command command = new ElectWithAssistantCommand(council, councillor);
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}

}
