package cof.commands;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cof.bonus.BasicBonus;
import cof.model.Board;
import cof.model.City;
import cof.model.Permit;
import cof.model.Player;
import common.Utility;

public class TestNobilityBonus {
	Board board;
	Player player;
	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoard();
	}

	@Test
	public void testFirstNobilityBonus() {
		TurnState state = new TurnState();
		Player player = new Player(0, 0);
		
		BasicBonus bonus = new BasicBonus(0, 0, 0, 0, 2); // Advance 2 steps in nobility track
		
		state.pushBonus(bonus);
		
		Command command = new PollBonusCommand();
		
		assertTrue(state.canPollStaticBonus());
		
		command.execute(player, board, state);
		
		assertEquals(2, player.getNobility());
		assertEquals(0, player.getCoins());
		assertEquals(0, player.getVictoryPoints());
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCards().size());
		
		assertTrue(state.canPollStaticBonus());
		
		// Another execution of PollBonus must poll out the nobility bonus pushed
		
		command.execute(player, board, state);
		
		assertEquals(2, player.getNobility());
		assertEquals(2, player.getCoins());
		assertEquals(2, player.getVictoryPoints());
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCards().size());
		
		// Another execution of poll bonus must raise exception
		
		boolean exceptionRaised = false;
		try {
			command.execute(player, board, state);
		}
		catch (CommandNotValidException e) {
			exceptionRaised = true;
		}
		assertTrue(exceptionRaised);
	}
	
	@Test
	public void testGainCityRewardBonus() {
		TurnState state = new TurnState();
		Player player = new Player(0, 0);
		
		player.addNobility(2); //Starts with nobility 2
		
		BasicBonus bonus = new BasicBonus(0, 0, 0, 0, 2); // Advance 2 steps in nobility track
		
		state.pushBonus(bonus);
		
		Command pollCommand = new PollBonusCommand();
		
		assertTrue(state.canPollStaticBonus());
		
		pollCommand.execute(player, board, state);
		
		assertEquals(4, player.getNobility());
		assertEquals(0, player.getCoins());
		assertEquals(0, player.getVictoryPoints());
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCards().size());
		
		assertFalse(state.canPollStaticBonus());
		
		boolean exceptionRaised = false;
		try {
			pollCommand.execute(player, board, state);
		}
		catch (CommandNotValidException e) {
			exceptionRaised = true;
		}
		assertTrue(exceptionRaised);
		
		City city1 = board.getCityByTag("City1");
		player.buildEmporium(city1);
		
		Command gainNobBonus = new GainOneCityRewardCommand("City1");
		
		gainNobBonus.execute(player, board, state);
		pollCommand.execute(player, board, state);
		
		assertEquals(4, player.getNobility());
		assertEquals(0, player.getCoins());
		assertEquals(1, player.getVictoryPoints());
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCards().size());
		
	}
	
	@Test
	public void testAdditionalActionNobilityBonus() {
		TurnState state = new TurnState();
		Player player = new Player(0,0);
		
		player.addNobility(4);
		
		BasicBonus bonus = new BasicBonus(0, 0, 0, 0, 2); // Advance 2 steps in nobility track
		state.pushBonus(bonus);
		
		Command pollCommand = new PollBonusCommand();
		
		assertTrue(state.canPollStaticBonus());
		
		while (state.canPollStaticBonus())
			pollCommand.execute(player, board, state);
		
		
		assertEquals(6, player.getNobility());
		assertEquals(0, player.getCoins());
		assertEquals(0, player.getVictoryPoints());
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCards().size());
		
		assertFalse(state.canPollStaticBonus());
		boolean exceptionRaised = false;
		try {
			pollCommand.execute(player, board, state);
		}
		catch (CommandNotValidException e) {
			exceptionRaised = true;
		}
		assertTrue(exceptionRaised);
		
		assertTrue(state.canDoMainAction());
		state.doMainAction();
		assertTrue(state.canDoMainAction());
		state.doMainAction();
		assertFalse(state.canDoMainAction());
	}
	
	@Test
	public void testGainPermitBonus() {
		TurnState state = new TurnState();
		Player player = new Player(0, 0);
		
		player.addNobility(8); //Starts with nobility 2
		
		BasicBonus bonus = new BasicBonus(0, 0, 0, 0, 2); // Advance 2 steps in nobility track
		
		state.pushBonus(bonus);
		
		Command pollCommand = new PollBonusCommand();
		
		assertTrue(state.canPollStaticBonus());
		
		pollCommand.execute(player, board, state);
		
		assertEquals(10, player.getNobility());
		assertEquals(0, player.getCoins());
		assertEquals(0, player.getVictoryPoints());
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCards().size());
		
		assertFalse(state.canPollStaticBonus());
		
		boolean exceptionRaised = false;
		try {
			pollCommand.execute(player, board, state);
		}
		catch (CommandNotValidException e) {
			exceptionRaised = true;
		}
		assertTrue(exceptionRaised);
		
		Permit permit = board.getRegions().get(0).getGainablePermits().get(0);
		
		Command gainPermit = new GainPermitCommand(permit);
		
		gainPermit.execute(player, board, state);
		while (state.canPollStaticBonus())
			pollCommand.execute(player, board, state);
		
		assertEquals(10, player.getNobility());
		assertEquals(0, player.getCoins());
		assertEquals(0, player.getVictoryPoints());
		assertEquals(1, player.getAssistants());
		assertEquals(3, player.getCards().size());
		assertTrue(permit.getOwner() == player);
		assertEquals(1, player.getPermits().size());
		
	}

}
