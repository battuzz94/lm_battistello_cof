package cof.commands;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;


import cof.model.Board;
import cof.model.Council;
import cof.model.Councillor;
import cof.model.Player;
import common.Utility;

public class TestElectCouncillorCommand {
	Board board;
	
	@Before
	public void setUp() {
		board = Utility.loadTestBoard();
	}
	
	
	@Test
	public void test() {
		Council c = board.getKingCouncil();
		
		List<String> tags = c.getCouncillors().stream().map(in -> in.toString()).collect(Collectors.toList());
		Councillor counc = board.getCouncillorByTag("Councillor17");
		
		ElectCouncillorCommand command = new ElectCouncillorCommand(c, counc);
		Player p = new Player(0, 0);
		TurnState state = new TurnState();
		
		command.execute(p, board, state);
		
		tags.remove(0);
		tags.add(counc.toString());
		
		while (state.canPollStaticBonus())
			p.obtain(state.pollStaticBonus());
		
		assertEquals(ElectCouncillorCommand.COINS, p.getCoins());
		assertEquals(0, p.getAssistants());
		for (int i = 0; i < tags.size(); i++) {
			assertEquals(tags.get(i), c.getCouncillors().get(i).toString());
		}
		
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailInvalidCouncillor() {
		ElectCouncillorCommand command = new ElectCouncillorCommand(board.getKingCouncil().getTag(), "Councillor13");
		Player player = new Player(0, 0);
		TurnState state = new TurnState();
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);		
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailInvalidCouncil() {
		ElectCouncillorCommand command = new ElectCouncillorCommand("Council6", "Councillor18");
		Player player = new Player(0, 0);
		TurnState state = new TurnState();
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);		
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailMalformedTags() {
		ElectCouncillorCommand command = new ElectCouncillorCommand("concil2", "concilor3");
		Player player = new Player(0, 0);
		TurnState state = new TurnState();
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);		
	}

}
