package cof.commands;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import cof.model.Board;
import cof.model.Card;
import cof.model.City;
import cof.model.NotEnoughCreditsException;
import cof.model.Player;
import cof.model.PoliticColor;
import common.Utility;

public class TestBribeKingCommand {
	Board b;
	
	@Before
	public void setUp() throws Exception {
		b = Utility.loadTestBoard();
	}
	
	/**
	 * This test must fail because the player has no cards
	 */
	@Test(expected=CommandNotValidException.class)
	public void testFailForCards() {
		Player player = new Player(200, 200);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.WHITE, PoliticColor.BLACK);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		City destination = b.getCityByTag("City3");
		
		BribeKingCommand command = new BribeKingCommand(new ArrayList<Card>(cards), destination);
		
		assertFalse(command.isValid(player, b, new TurnState()));
		command.execute(player, b, new TurnState());
	}
	
	/**
	 * This test must fail because the player has not enough credits to bribe council
	 */
	@Test(expected=CommandNotValidException.class)
	public void testFailForCredits() {
		Player player = new Player(0, 200);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.PINK, PoliticColor.BLUE, PoliticColor.PINK, PoliticColor.MULTI);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		// Assign cards to player
		cards.forEach(c -> player.addCards(c));
		
		City destination = b.getKingPosition();
		
		BribeKingCommand command = new BribeKingCommand(new ArrayList<Card>(cards), destination);
		
		assertFalse(command.isValid(player, b, new TurnState()));
		command.execute(player, b, new TurnState());
	}
	
	/**
	 * This test must fail because the player has not enough credits to move king
	 */
	@Test(expected=CommandNotValidException.class)
	public void testFailForMoveKing() {
		Player player = new Player(5, 100);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.PINK, PoliticColor.BLUE, PoliticColor.PINK, PoliticColor.MULTI);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		// Assign cards to player
		cards.forEach(c -> player.addCards(c));
		
		City destination = b.getCityByTag("City1");
		
		BribeKingCommand command = new BribeKingCommand(new ArrayList<Card>(cards), destination);
		
		assertFalse(command.isValid(player, b, new TurnState()));
		command.execute(player, b, new TurnState());
	}
	
	/**
	 * This test must fail because the player has not enough assistants to build an emporium
	 */
	@Test(expected=CommandNotValidException.class)
	public void testFailForAssistants() {
		Player player = new Player(5, 0);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.PINK, PoliticColor.BLUE, PoliticColor.PINK, PoliticColor.MULTI);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		// Assign cards to player
		cards.forEach(c -> player.addCards(c));
		
		City destination = b.getKingPosition();
		Player p2 = new Player(100, 100);
		p2.buildEmporium(destination);
		
		BribeKingCommand command = new BribeKingCommand(new ArrayList<Card>(cards), destination);
		
		assertFalse(command.isValid(player, b, new TurnState()));
		command.execute(player, b, new TurnState());
	}
	
	/**
	 * This test must fail because the tags given are not valid
	 */
	@Test
	public void testFailForTagsInvalid() {
		Player player = new Player(100, 100);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.PINK, PoliticColor.BLUE, PoliticColor.PINK, PoliticColor.MULTI);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		// Assign cards to player
		cards.forEach(c -> player.addCards(c));
		
		List<String> cardTags = cards.stream().map(c -> c.getTag()).collect(Collectors.toList());
		String destination = b.getKingPosition().getTag();
		
		// Valid command
		BribeKingCommand command = new BribeKingCommand(cardTags, destination);
		assertTrue(command.isValid(player, b, new TurnState()));
		
		// Try to give invalid City
		command = new BribeKingCommand(cardTags, "City");
		assertFalse(command.isValid(player, b, new TurnState()));
		
		// Try with one invalid card
		cardTags.set(0, "Card");
		command = new BribeKingCommand(cardTags, "City");
		assertFalse(command.isValid(player, b, new TurnState()));
	}
	
	/**
	 * This test must succeed
	 */
	@Test
	public void testSuccess() {
		Player player = new Player(1 + 4*BribeKingCommand.COST_PER_STEP, 0);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.PINK, PoliticColor.BLUE, PoliticColor.PINK, PoliticColor.MULTI);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		// Assign cards to player
		cards.forEach(c -> player.addCards(c));
		
		City destination = b.getCityByTag("City1");
		
		BribeKingCommand command = new BribeKingCommand(new ArrayList<Card>(cards), destination);
		
		TurnState state = new TurnState();
		
		assertTrue(command.isValid(player, b, state));
		command.execute(player, b, state);
		
		while (state.canPollStaticBonus()) {
			player.obtain(state.pollStaticBonus());
		}
		
		assertEquals(0, player.getAssistants());
		assertEquals(0, player.getCoins());
		assertEquals(1, player.getVictoryPoints());  //From the build bonus
		assertEquals(1, player.getBuildedEmporia().size());
		assertEquals(0, player.getPermits().size());
		assertEquals(0, player.getCards().size());
		assertEquals(1, destination.getEmporiaCount());
	}
	
	
	/**
	 * This test must fail because the player gives one card that doesn't match any councillors
	 */
	@Test(expected=CommandNotValidException.class)
	public void testFailForFourWrongCards() {
		Player player = new Player(1 + 4*BribeKingCommand.COST_PER_STEP, 0);
		List<PoliticColor> colors = Arrays.asList(PoliticColor.PINK, PoliticColor.BLACK, PoliticColor.PINK, PoliticColor.MULTI);
		
		List<Card> cards = colors.stream().map(c -> new Card(c)).collect(Collectors.toList());
		
		// Assign cards to player
		cards.forEach(c -> player.addCards(c));
		
		City destination = b.getCityByTag("City1");
		
		BribeKingCommand command = new BribeKingCommand(new ArrayList<Card>(cards), destination);
		
		TurnState state = new TurnState();
		
		assertFalse(command.isValid(player, b, state));
		command.execute(player, b, state);
	}

}
