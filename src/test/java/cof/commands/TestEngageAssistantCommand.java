package cof.commands;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cof.model.Board;
import cof.model.NotEnoughCreditsException;
import cof.model.Player;
import common.Utility;

public class TestEngageAssistantCommand {
	
	Board b;
	
	@Before
	public void setUp() {
		b = Utility.loadTestBoard();
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFail() {
		EngageAssistantCommand command = new EngageAssistantCommand();
		
		Player player = new Player(0, 0);
		
		command.execute(player, b, new TurnState());
	}
	
	@Test
	public void testSuccess() {
		EngageAssistantCommand command = new EngageAssistantCommand();
		
		Player player = new Player(EngageAssistantCommand.ASSISTANT_COST, 0);
		
		command.execute(player, b, new TurnState());
		
		assertEquals(0, player.getCoins());
		assertEquals(1, player.getAssistants());
	}

}
