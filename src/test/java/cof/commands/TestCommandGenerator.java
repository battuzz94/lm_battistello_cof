package cof.commands;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cof.model.Board;
import cof.model.Card;
import cof.model.Player;
import cof.model.PoliticColor;
import cof.model.Region;
import common.Utility;

public class TestCommandGenerator {
	Board board;
	Player player;
	TurnState state;

	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoard();
		player = new Player(100, 10);
		state = new TurnState();

		List<Card> cards = Arrays.asList(new Card(PoliticColor.MULTI), new Card(PoliticColor.BLACK),
				new Card(PoliticColor.PURPLE), new Card(PoliticColor.PURPLE));
		cards.forEach(c -> player.obtain(c));

		player.obtain(board.getRegions().get(0).getGainablePermits().get(0));
		player.obtain(board.getRegions().get(0).getGainablePermits().get(1));
		player.obtain(board.getRegions().get(1).getGainablePermits().get(0));
		player.obtain(board.getRegions().get(1).getGainablePermits().get(1));
	}

	@Test
	public void testMainActions() {
		Command command;
		List<Command> prevCommands = new ArrayList<>();
		Iterator<Command> mainActions = new MainActionGenerator(player, board, state).iterator();
		int count = 0;
		while (mainActions.hasNext()) {
			count++;
			command = mainActions.next();
			assertTrue("All commands must be different", !prevCommands.contains(command));
			prevCommands.add(command);
			assertTrue("The command " + command + " must be valid", command.isValid(player, board, state));
		}

		assertEquals(35, count);
	}

	@Test
	public void testMainActionsWithLessCoins() {
		Command command;
		player.takeCoins(90);
		List<Command> prevCommands = new ArrayList<>();
		Iterator<Command> mainActions = new MainActionGenerator(player, board, state).iterator();
		int count = 0;
		while (mainActions.hasNext()) {
			count++;
			command = mainActions.next();
			assertTrue("All commands must be different", !prevCommands.contains(command));
			prevCommands.add(command);
			assertTrue("The command " + command + " must be valid", command.isValid(player, board, state));
		}

		assertEquals(20, count);
	}

	@Test
	public void testNoMainActions() {
		state.doMainAction();
		Iterator<Command> mainActions = new MainActionGenerator(player, board, state).iterator();
		int count = 0;
		while (mainActions.hasNext()) {
			count++;
			mainActions.next();
		}

		assertEquals(0, count);
	}

	@Test
	public void testNoQuickActions() {
		state.doQuickAction();
		Iterator<Command> quickActions = new QuickActionGenerator(player, board, state).iterator();
		int count = 0;
		while (quickActions.hasNext()) {
			count++;
			quickActions.next();
		}

		assertEquals(0, count);
	}

	@Test
	public void testQuickActions() {
		Command command;
		int count = 0;
		Iterator<Command> quickActions = new QuickActionGenerator(player, board, state).iterator();
		while (quickActions.hasNext()) {
			count++;
			command = quickActions.next();
			assertTrue("The command " + command + " must be valid", command.isValid(player, board, state));
		}

		assertEquals(37, count);
	}
	
	
	@Test
	public void testWithRegionsNoPermit() {
		Region region = board.getRegionByTag("Region3");
		
		while (region.getGainablePermits().size() > 0)
			region.takePermitByTag(region.getGainablePermits().get(0).getTag());
		
		MainActionGenerator generator = new MainActionGenerator(player, board, state);
		Iterator<Command> iterator = generator.iterator();
		while (iterator.hasNext())
			iterator.next();
	}
	
}
