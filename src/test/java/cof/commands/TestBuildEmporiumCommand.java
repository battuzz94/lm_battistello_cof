package cof.commands;

import static org.junit.Assert.*;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import cof.bonus.BasicBonus;
import cof.bonus.ColoredCityBonus;
import cof.model.Board;
import cof.model.City;
import cof.model.Permit;
import cof.model.Player;
import cof.model.Region;
import common.Utility;

public class TestBuildEmporiumCommand {

	Board board;
	Player player;
	Permit permit;
	City city;
	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoard();
		player = new Player(100, 100);
		
		
		// permit with 1 assistant and 3 cards that can build in B
		permit = board.getPermitByTag("Permit1");
		permit = board.getRegionByTag("Region1").takePermitByTag("Permit1");
		player.obtain(permit);
		city = board.getCityByTag("City2");  //Burgen
		
		Player p2 = new Player(100, 100);
		p2.buildEmporium(city);
		
		
		player.obtain(permit);
	}
	

	@Test
	public void testSuccess() {
		Command command = new BuildEmporiumCommand(permit, city);
		TurnState state = new TurnState();
		
		assertTrue(command.isValid(player, board, state));
		command.execute(player, board, state);
		
		while (state.canPollStaticBonus())
			(new PollBonusCommand()).execute(player, board, state);
		
		assertEquals(1, player.getVictoryPoints());
		assertEquals(100 - 1, player.getAssistants());
		assertEquals(0, player.getNobility());
		assertEquals(100, player.getCoins());
		assertEquals(1, player.getCards().size());
		assertEquals(1, player.getBuildedEmporia().size());
		assertTrue(player.hasBuiltIn(city));
	}
	
	@Test
	public void testSuccessWithChain() {
		
		player.buildEmporium(board.getCityByTag("City1"));
		player.buildEmporium(board.getCityByTag("City3"));
		player.buildEmporium(board.getCityByTag("City4"));
		player.buildEmporium(board.getCityByTag("City10"));
		
		Command command = new BuildEmporiumCommand(permit, city);
		TurnState state = new TurnState();
		
		assertTrue(command.isValid(player, board, state));
		command.execute(player, board, state);
		
		while (state.canPollStaticBonus())
			(new PollBonusCommand()).execute(player, board, state);
		
		assertEquals(5, player.getVictoryPoints());
		assertEquals(100 - 1 + 2, player.getAssistants());
		assertEquals(0, player.getNobility());
		assertEquals(100, player.getCoins());
		assertEquals(1, player.getCards().size());
		assertEquals(4 + 1, player.getBuildedEmporia().size());
		assertTrue(player.hasBuiltIn(city));
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForAssistants() {
		TurnState state = new TurnState();
		
		player.takeAssistants(100);
		
		Command command = new BuildEmporiumCommand(permit, city);
		
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForWrongCity() {
		TurnState state = new TurnState();
		
		Command command = new BuildEmporiumCommand(permit, board.getCityByTag("City1"));
		
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForWrongPermit() {
		TurnState state = new TurnState();
		
		Permit wrongPermit = board.getPermitByTag("Permit16");
		
		Command command = new BuildEmporiumCommand(wrongPermit, city);
		
		
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}
	
	@Test(expected=CommandNotValidException.class)
	public void testFailForInvalidTag() {
		TurnState state = new TurnState();
		
		Command command = new BuildEmporiumCommand("Permit", city.getTag());
		
		assertFalse(command.isValid(player, board, state));
		
		command = new BuildEmporiumCommand(permit.getTag(), "City");
		assertFalse(command.isValid(player, board, state));
		command.execute(player, board, state);
	}
	
	
	@Test
	public void testGainRegionBonus() {
		Region region = board.getRegionByTag("Region1");
		TurnState state = new TurnState();
		int cities = region.getCities().size();
		
		for (int i = 0; i < cities - 1; i++) {
			player.buildEmporium(region.getCities().get(i));
		}
		new Permit(new BasicBonus()); // Get rid of tag
		Permit newPermit = new Permit(new BasicBonus());
		newPermit.addCity(region.getCities().get(cities - 1));
		player.obtain(newPermit);
		
		int vpRegion = ((BasicBonus) region.getBonus()).getVictoryPoints();
		int vpKing = ((BasicBonus) board.getKingBonus()).getVictoryPoints();
		int vpCity = ((BasicBonus) region.getCities().get(cities-1).getBonus()).getVictoryPoints();
		
		Gson gson = Utility.getJsonSerializer();
		player = gson.fromJson(gson.toJson(player), Player.class); 
		
		BuildEmporiumCommand command = new BuildEmporiumCommand(newPermit, region.getCities().get(cities - 1));
		
		command.execute(player, board, state);
		while (state.canPollStaticBonus())
			(new PollBonusCommand()).execute(player, board, state);
		
		assertEquals(vpRegion + vpKing + vpCity + 5, player.getVictoryPoints());
	}
	
	
	@Test
	public void testGainColoredCityBonus() {
		
		ColoredCityBonus bonus = board.getCityBonus().get(0);
		
		List<City> cities = board.getCities().stream().filter(c -> c.getColor() == bonus.getColor()).collect(Collectors.toList());
		
		TurnState state = new TurnState();
		
		int len = cities.size();
		
		for (int i = 0; i < len - 1; i++) {
			player.buildEmporium(cities.get(i));
		}
		
		new Permit(new BasicBonus()); // Get rid of tag
		Permit newPermit = new Permit(new BasicBonus());
		newPermit.addCity(cities.get(len - 1));
		player.obtain(newPermit);
		
		int vpKing = ((BasicBonus) board.getKingBonus()).getVictoryPoints();
		int vpCity = ((BasicBonus) bonus.getBonus()).getVictoryPoints();
		
		
		BuildEmporiumCommand command = new BuildEmporiumCommand(newPermit, cities.get(len - 1));
		
		command.execute(player, board, state);
		while (state.canPollStaticBonus())
			(new PollBonusCommand()).execute(player, board, state);
		
		assertEquals(vpKing + vpCity, player.getVictoryPoints());
	}
	
	@Test
	public void testPlayerCantBuildTwice() {
		Command command = new BuildEmporiumCommand(permit, city);
		TurnState state = new TurnState();
		
		for (int i = 0; i < 10; i++)
			new Permit(new BasicBonus()); // Get rid of tags
		
		Permit newPermit = new Permit(new BasicBonus());
		player.obtain(newPermit);
		
		newPermit.addCity(city);
		
		Command newCommand = new BuildEmporiumCommand(newPermit, city);
		
		assertTrue(newCommand.isValid(player, board, state));
		assertTrue(command.isValid(player, board, state));
		command.execute(player, board, state);
		
		while (state.canPollStaticBonus())
			(new PollBonusCommand()).execute(player, board, state);
		
		
		assertFalse("Cant build twice", newCommand.isValid(player, board, state));
	}
}
