package ai;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import cof.commands.Command;
import cof.commands.PassTurnCommand;
import cof.commands.PollBonusCommand;
import cof.commands.TurnState;
import cof.model.Board;
import cof.model.Card;
import cof.model.Player;
import cof.model.PoliticColor;
import common.Utility;

public class TestEvaluator {

	private Board board;
	private Player player;
	private TurnState state;

	@Before
	public void setUp() throws Exception {
		board = Utility.loadTestBoardDisposed();
		
		player = new Player(10, 10);
		
		List<Card> cards = Arrays.asList(new Card(PoliticColor.BLACK),
				new Card(PoliticColor.ORANGE),
				new Card(PoliticColor.BLUE),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.PINK),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.MULTI),
				new Card(PoliticColor.PINK)
				);
		
		cards.forEach(c -> player.obtain(c));
		
		state = new TurnState();
	}

	@Test
	public void testBestMove() {
		List<Move> bestMoves = Evaluator.getBestMove(new MoveState(board, player, state));
		assertNotNull(bestMoves);
		assertTrue("best Moves.size() > 0", bestMoves.size() > 0);
		for (Move m : bestMoves) {
			Command c = m.getCommand();
			assertTrue("command " + c + " must be valid", c.isValid(player, board, state));
			c.execute(player, board, state);
			while (state.canPollStaticBonus())
				(new PollBonusCommand()).execute(player, board, state);
		}
		assertTrue("I must be able to pass turn", (new PassTurnCommand()).isValid(player, board, state));
	}
	
	@Test
	public void testMoves() {
		List<List<Move>> moves = Evaluator.allMovesParallel(new MoveState(board, player, state)).collect(Collectors.toList());
		for (List<Move> sequence : moves) {
			MoveState moveState = new MoveState(board, player, state);
			for (Move m : sequence) {
				Command c = m.getCommand();
				assertTrue("command " + c + " must be valid", c.isValid(moveState.getPlayer(), moveState.getBoard(), moveState.getState()));
				c.execute(moveState.getPlayer(), moveState.getBoard(), moveState.getState());
				while (moveState.getState().canPollStaticBonus())
					(new PollBonusCommand()).execute(moveState.getPlayer(), moveState.getBoard(), moveState.getState());
			}
		}
	}

}
